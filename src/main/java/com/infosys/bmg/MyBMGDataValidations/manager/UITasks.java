package com.infosys.bmg.MyBMGDataValidations.manager;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.infosys.bmg.MyBMGDataValidations.beans.ExcelData;
import com.infosys.bmg.MyBMGDataValidations.utility.Log;
import com.infosys.bmg.MyBMGDataValidations.utility.Objectwait;
import com.infosys.bmg.MyBMGDataValidations.utility.UIElementLocator;

@Service
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UITasks {
	private final static Logger LOGGER = LoggerFactory.getLogger(UITasks.class);

	public void openandlogin(String Module,WebDriver driver,ExcelData excelData) throws Exception {
		try {
			// System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-popup-blocking");
			options.addArguments("--disable-web-security");
			options.addArguments("disable-infobars");
			options.addArguments("--disable-save-password-bubble");
			options.addArguments("--no-sandbox");
			options.addArguments("�disable-dev-shm-usage");
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.password_manager_enabled", false);
			options.setExperimentalOption("prefs", prefs);
			DesiredCapabilities caps = DesiredCapabilities.chrome();
			caps.setCapability(ChromeOptions.CAPABILITY, options);
			caps.setCapability("chrome.switches", Arrays.asList("--disable-javascript"));
			driver = new ChromeDriver(caps);
			driver.manage().window().maximize();
			driver.get(excelData.getSiteURL());
			Objectwait.elementWait();

			driver.findElement(By.id(UIElementLocator.UserName)).sendKeys(excelData.getUserName());
			Log.info("Username entered in the Username text box");
			driver.findElement(By.id(UIElementLocator.Password)).sendKeys(excelData.getPassword());
			Log.info("password  entered in the password text box");
			driver.findElement(By.name(UIElementLocator.LoginBTN)).click();
			Objectwait.elementWait();
			WebElement Element = driver.findElement(By.className(UIElementLocator.Greeting));
			if (!(Element.isDisplayed())) {
				throw new Exception("User is not logged in");

			}

			if (Module.equalsIgnoreCase("Recording")) {
				driver.findElement(By.xpath(UIElementLocator.ModuleSelection)).click();
				Objectwait.elementWait();
			}
			if (Module.equalsIgnoreCase("Analysis")) {
				WebElement element = driver.findElement(By.xpath(UIElementLocator.AnalysisModuleSelection));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
				Thread.sleep(500);
				driver.findElement(By.xpath(UIElementLocator.AnalysisModuleSelection)).click();
				Objectwait.elementWait();
			}
		} catch (Exception e) {
			LOGGER.error("Unable to open and login the browser");
			throw e;
		}
	}
}
