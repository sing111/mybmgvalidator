package com.infosys.bmg.MyBMGDataValidations.manager;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.infosys.bmg.MyBMGDataValidations.beans.TestDataBean;
import com.infosys.bmg.MyBMGDataValidations.utility.StatusMessage;

@Service
public class TestDataProgressBar {
	
	private static Integer processIdCounter=0;
	private static Map<Integer, TestDataBean> progressBars = new HashMap<>();
	private final Logger LOGGER = LoggerFactory.getLogger(TestDataProgressBar.class);

	public TestDataBean getProgressBar(Integer processId) {

		TestDataBean progBar = progressBars.get(processId);
		if (progBar == null) {
			LOGGER.info("progress bar not found for " + String.valueOf(processId) + " list progressBArs: "
					+ progressBars.toString());
			return null;

		}
		removeprevProgressBars();
		return progBar;
	}

	private void removeprevProgressBars() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		Date currDate = cal.getTime();

		for (TestDataBean progTest : progressBars.values()) {
			if (progTest.getCompletedDate() != null && currDate.after(progTest.getCompletedDate())) {
				LOGGER.info("progress bar removed for " + String.valueOf(progTest) );
				progressBars.remove(progTest.getProcessId());
			}
		}
	}
	public TestDataBean populateProgressBar() {
		synchronized (LOGGER) {
			processIdCounter++;
			Integer newPId=processIdCounter;
			TestDataBean progressBar = new TestDataBean();
			progressBar.setCompletedDate(null);
			progressBar.setCreatedDate(new Date());
			progressBar.setProcessId(newPId);
			progressBar.setStatusMessage(StatusMessage.FILE_UPLOAD_STARTED.getMessage());
			progressBar.setStatusCode(StatusMessage.FILE_UPLOAD_STARTED.getCode());
			progressBars.put(newPId, progressBar);
			return progressBar;
		}

	}

}
