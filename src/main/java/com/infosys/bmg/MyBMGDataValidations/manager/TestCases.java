package com.infosys.bmg.MyBMGDataValidations.manager;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;

import com.infosys.bmg.MyBMGDataValidations.beans.CrossClient;
import com.infosys.bmg.MyBMGDataValidations.beans.ExcelData;
import com.infosys.bmg.MyBMGDataValidations.beans.SingleClient;
import com.infosys.bmg.MyBMGDataValidations.beans.TestCase;
import com.infosys.bmg.MyBMGDataValidations.beans.TestDataBean;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBConnectionForRoyaltyTrend;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueriesForTopTerritories;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueriesForTotalAlbumSingle;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_LatestStatement;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_NewStatementDue;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_PipelineRoyalty_Publishing;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_PipelineRoyalty_Recording;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_StatementBalance;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_TopLicensingProducts;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_TopPerformingTerritories;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_TopSyncProducts;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_TopTracks;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_Top_Mrchndise_Products;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_Top_PP_Products;
import com.infosys.bmg.MyBMGDataValidations.utility.Constants;
import com.infosys.bmg.MyBMGDataValidations.utility.Log;
import com.infosys.bmg.MyBMGDataValidations.utility.ReadExcelData;

public class TestCases {
	WebDriver driver;
	CommonMethods commonMethods;
	ReadExcelData readExcel;
	TestDataBean testData;
	ExcelData excelData;
	TestCase testcase;

	DBConnectionForRoyaltyTrend dbConnectionForRoyaltyTrend;
	DBQueries_LatestStatement dbQueries_LatestStatement;
	UserSelectClient userSelectClient;
	DBQueries_StatementBalance dbQueries_StatementBalance;
	DBQueriesForTopTerritories dbQueriesForTopTerritories;
	DBQueries_Top_PP_Products dBQueries_Top_PP_Products;
	DBQueriesForTotalAlbumSingle dBQueriesForTotalAlbumSingle;
	DBQueries_TopTracks dbQueries_TopTracks;
	DBQueries_PipelineRoyalty_Recording dbQueries_PipelineRoyalty_Recording;
	DBQueries_NewStatementDue dbQueries_NewStatementDue;
	DBQueries_TopLicensingProducts dbQueries_TopLicensingProducts;
	DBQueries_PipelineRoyalty_Publishing dbQueries_PipelineRoyalty_Publishing;
	DBQueries_Top_Mrchndise_Products dbQueries_Top_Mrchndise_Products;
	DBQueries_TopPerformingTerritories dbQueries_TopPerformingTerritories;
	DBQueries_TopSyncProducts dbQueries_TopSyncProducts;

	public TestCases(WebDriver driver, ReadExcelData readExcel, TestDataBean testData, ExcelData excelData,
			TestCase testcase) {
		this.driver = driver;
		this.readExcel = readExcel;
		this.testData = testData;
		this.excelData = excelData;
		this.testcase = testcase;

		this.dbConnectionForRoyaltyTrend = new DBConnectionForRoyaltyTrend();
		this.dbQueries_LatestStatement = new DBQueries_LatestStatement();
		this.userSelectClient = new UserSelectClient(driver);
		this.dbQueries_StatementBalance = new DBQueries_StatementBalance();
		this.dbQueriesForTopTerritories = new DBQueriesForTopTerritories();
		this.dBQueries_Top_PP_Products = new DBQueries_Top_PP_Products();
		this.dBQueriesForTotalAlbumSingle = new DBQueriesForTotalAlbumSingle();
		this.dbQueries_TopTracks = new DBQueries_TopTracks();
		this.dbQueries_PipelineRoyalty_Recording = new DBQueries_PipelineRoyalty_Recording();
		this.dbQueries_NewStatementDue = new DBQueries_NewStatementDue();
		this.dbQueries_TopLicensingProducts = new DBQueries_TopLicensingProducts();
		this.dbQueries_PipelineRoyalty_Publishing = new DBQueries_PipelineRoyalty_Publishing();
		this.dbQueries_Top_Mrchndise_Products = new DBQueries_Top_Mrchndise_Products();
		this.dbQueries_TopPerformingTerritories = new DBQueries_TopPerformingTerritories();
		this.dbQueries_TopSyncProducts = new DBQueries_TopSyncProducts();
		commonMethods = new CommonMethods(dbConnectionForRoyaltyTrend, dbQueries_LatestStatement, userSelectClient,
				dbQueries_StatementBalance, dbQueriesForTopTerritories, dBQueries_Top_PP_Products,
				dBQueriesForTotalAlbumSingle, dbQueries_TopTracks, dbQueries_PipelineRoyalty_Recording,
				dbQueries_NewStatementDue, dbQueries_TopLicensingProducts, dbQueries_PipelineRoyalty_Publishing,
				dbQueries_Top_Mrchndise_Products, dbQueries_TopPerformingTerritories, dbQueries_TopSyncProducts);
	}

	public void TC01_Validate_StatementBalance_Single_Client_Selection() throws Exception {
		try {

			ArrayList<String> crossClientList = new ArrayList<String>();
			List<List<String>> statementDataUI = new ArrayList<List<String>>();
			for (SingleClient singleClient : excelData.getSinglsClient()) {
				if (singleClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					userSelectClient.user_select_client(singleClient.getRoyaltorCode(), singleClient.getCurrency_Desc(),
							null, false, singleClient.getRoyaltorName());
					commonMethods.fetchStatementBalanceFromUIandDb(singleClient.getRoyaltorCode(),
							singleClient.getRoyaltorName(), crossClientList, null, null, null,
							singleClient.getRoyaltorSite());
					// commonMethods.fetchDataFromDB(singleClient.getRoyaltorCode(),singleClient.getRoyaltorSite());

				}
			}

			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.statementComparison);
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in TC01: " + e.getLocalizedMessage());
		}

	}

	public void TC02_Validate_StatementBalance_All_Client_Selection() throws Exception {

		ArrayList<String> crossClientList = new ArrayList<String>();
		ArrayList<String> crossClientNamesList = new ArrayList<String>();
		commonMethods.statementDataUI = new ArrayList<List<String>>();

		for (CrossClient crossClient : excelData.getCrossClient()) {

			if (crossClient.getRoyaltorCode().isEmpty()) {
				break;
			} else {
				if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

					userSelectClient.user_select_client(crossClient.getRoyaltorCode(), crossClient.getCurrency_Desc(),
							null, true, crossClient.getRoyaltorName());
					commonMethods.fetchStatementBalanceFromUIandDb(crossClient.getRoyaltorCode(),
							crossClient.getRoyaltorName(), crossClientList, crossClientNamesList, null, null,
							crossClient.getRoyaltorSite());
					crossClientList = new ArrayList<String>();
					crossClientNamesList = new ArrayList<String>();
				} else {
					crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
					crossClientNamesList.add("'" + crossClient.getRoyaltorName() + "'");
				}

			}
		}

		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.statementComparison);
	}

	public void TC03_Validate_StatementBalance_Different_CrossClient_Payee_Selection() throws Exception {
		ArrayList<String> crossClientList = new ArrayList<String>();
		String crossClientName = null;
		commonMethods.statementDataUI = new ArrayList<List<String>>();
		for (CrossClient crossClient : excelData.getCrossClient()) {

			if (crossClient.getRoyaltorCode().isEmpty()) {
				break;
			} else {
				if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {
					for (String id : crossClientList) {
						String ids[] = id.split(",");
						id = ids[0];
						crossClientName = ids[1];
						userSelectClient.user_select_client(crossClient.getRoyaltorCode(),
								crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorName());
						commonMethods.fetchStatementBalanceFromUIandDb(crossClient.getRoyaltorCode(),
								crossClient.getRoyaltorName(), null, null, crossClientName, id,
								crossClient.getRoyaltorSite());
						crossClientList = new ArrayList<String>();
					}
				} else {
					crossClientList.add(
							"'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorName() + "'");
				}

			}
		}

		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.statementComparison);

	}

	public void TC04_Validate_TopTerr_Single_Client_Selection() {

		try {
			ArrayList<String> crossClientList = new ArrayList<String>();
			List<String> allPeriods = new ArrayList<String>();
			commonMethods.statementDataUI2 = new ArrayList<List<String>>();
			for (SingleClient singleClient : excelData.getSinglsClient()) {
				if (singleClient.getRoyaltorCode() == null) {
					break;
				} else {
					userSelectClient.user_select_client(singleClient.getRoyaltorCode(), singleClient.getCurrency_Desc(),
							null, false, singleClient.getRoyaltorName());
					allPeriods = commonMethods.fetchPeriodFromUI();
					// commonMethods.fetchTopTerrAndPeriodFromUIandDb(singleClient.getRoyaltorCode(),crossClientList,null,singleClient.getRoyaltorSite(),allPeriods);
					for (String period : allPeriods) {
						commonMethods.fetchTopTerrAndPeriodFromUI(singleClient.getRoyaltorCode(),
								singleClient.getRoyaltorName(), crossClientList, null, null, null,
								singleClient.getRoyaltorSite(), period);
					}
				}
			}
			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topTerrComparison);
			// commonMethods.time();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC05_Validate_TopTerr_Single_Client_Selection() throws Exception {
		ArrayList<String> crossClientList = new ArrayList<String>();
		ArrayList<String> crossClientNamesList = new ArrayList<String>();
		List<String> allPeriods = new ArrayList<String>();
		commonMethods.statementDataUI2 = new ArrayList<List<String>>();
		for (CrossClient crossClient : excelData.getCrossClient()) {
			if (crossClient.getRoyaltorCode().isEmpty()) {
				break;
			} else {
				if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {
					userSelectClient.user_select_client(crossClient.getRoyaltorCode(), crossClient.getCurrency_Desc(),
							null, true, crossClient.getRoyaltorName());
					allPeriods = commonMethods.fetchPeriodFromUI();
					for (String period : allPeriods) {
						commonMethods.fetchTopTerrAndPeriodFromUI(crossClient.getRoyaltorCode(),
								crossClient.getRoyaltorName(), crossClientList, crossClientNamesList, null, null,
								crossClient.getRoyaltorSite(), period);
					}
					crossClientList = new ArrayList<String>();
					crossClientNamesList = new ArrayList<String>();
				} else {
					crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
					crossClientNamesList.add("'" + crossClient.getRoyaltorName() + "'");

				}

			}
		}

		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topTerrComparison);
	}

	public void TC06_Validate_TopTerr_Different_CrossClient_Payee_Selection() throws Exception {
		ArrayList<String> crossClientList = new ArrayList<String>();
		List<String> allPeriods = new ArrayList<String>();
		String crossClientName = null;
		commonMethods.statementDataUI2 = new ArrayList<List<String>>();
		for (CrossClient crossClient : excelData.getCrossClient()) {
			if (crossClient.getRoyaltorCode().isEmpty()) {
				break;
			} else {
				if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

					for (String id : crossClientList) {
						String ids[] = id.split(",");
						id = ids[0];
						crossClientName = ids[1];
						userSelectClient.user_select_client(crossClient.getRoyaltorCode(),
								crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorName());
						allPeriods = commonMethods.fetchPeriodFromUI();
						for (String period : allPeriods) {
							commonMethods.fetchTopTerrAndPeriodFromUI(crossClient.getRoyaltorCode(),
									crossClient.getRoyaltorName(), null, null, crossClientName, id,
									crossClient.getRoyaltorSite(), period);
						}
						allPeriods = new ArrayList<String>();
						crossClientList = new ArrayList<String>();
					}
				} else {
					crossClientList.add(
							"'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorName() + "'");
				}

			}
		}

		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topTerrComparison);

	}

	public void TC10_Validate_TotalNumOfAlbumorSingles_Single_Client_Selection() {
		try {
			ArrayList<String> crossClientList = new ArrayList<String>();
			List<String> allPeriods = new ArrayList<String>();
			commonMethods.totalAlbumSingleList = new ArrayList<List<String>>();
			commonMethods.clickAlbumSingleTab();
			for (SingleClient singleClient : excelData.getSinglsClient()) {
				if (singleClient.getRoyaltorCode() == null) {
					break;
				} else {
					userSelectClient.user_select_client(singleClient.getRoyaltorCode(), singleClient.getCurrency_Desc(),
							null, false, singleClient.getRoyaltorName());
					allPeriods = commonMethods.fetchPeriodFromUI();
					// commonMethods.fetchTotalAlbumSingleFromUI(singleClient.getRoyaltorCode(),crossClientList,null,singleClient.getRoyaltorSite(),null,allPeriods);
					for (String period : allPeriods) {
						commonMethods.fetchTotalAlbumSingleFromUI(singleClient.getRoyaltorCode(),
								singleClient.getRoyaltorName(), crossClientList, null, null, null,
								singleClient.getRoyaltorSite(), period, null);
					}
				}
			}
			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.totalNoOfAlbumSingleComparision);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void TC11_Validate_TopNumOfAlbumSingles_All_Client_Selection() {
		try {
			ArrayList<String> crossClientList = new ArrayList<String>();
			ArrayList<String> crossClientNamesList = new ArrayList<String>();
			List<String> allPeriods = new ArrayList<String>();
			commonMethods.totalAlbumSingleList = new ArrayList<List<String>>();

			commonMethods.clickAlbumSingleTab();
			for (CrossClient crossClient : excelData.getCrossClient()) {
				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						userSelectClient.user_select_client(crossClient.getRoyaltorCode(),
								crossClient.getCurrency_Desc(), null, true, crossClient.getRoyaltorName());
						allPeriods = commonMethods.fetchPeriodFromUI();
						for (String period : allPeriods) {
							commonMethods.fetchTotalAlbumSingleFromUI(crossClient.getRoyaltorCode(),
									crossClient.getRoyaltorName(), crossClientList, crossClientNamesList, null, null,
									crossClient.getRoyaltorSite(), period, null);
						}
						allPeriods = new ArrayList<String>();
						crossClientList = new ArrayList<String>();
						crossClientNamesList = new ArrayList<String>();
					} else {
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
						crossClientNamesList.add("'" + crossClient.getRoyaltorName() + "'");

					}

				}
			}

			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.totalNoOfAlbumSingleComparision);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC12_Validate_TopNumOfAlbumSingles_Different_CrossClient_Payee_Selection() throws Exception {
		ArrayList<String> crossClientList = new ArrayList<String>();
		String crossClientName = null;
		List<String> allPeriods = new ArrayList<String>();
		commonMethods.totalAlbumSingleList = new ArrayList<List<String>>();
		commonMethods.clickAlbumSingleTab();
		for (CrossClient crossClient : excelData.getCrossClient()) {
			if (crossClient.getRoyaltorCode().isEmpty()) {
				break;
			} else {
				if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {
					for (String id : crossClientList) {
						String ids[] = id.split(",");
						id = ids[0];
						crossClientName = ids[1];
						userSelectClient.user_select_client(crossClient.getRoyaltorCode(),
								crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorName());
						allPeriods = commonMethods.fetchPeriodFromUI();
						for (String period : allPeriods) {
							commonMethods.fetchTotalAlbumSingleFromUI(crossClient.getRoyaltorCode(),
									crossClient.getRoyaltorName(), null, null, crossClientName, id,
									crossClient.getRoyaltorSite(), period, allPeriods);
						}
						allPeriods = new ArrayList<String>();
						crossClientList = new ArrayList<String>();
					}
				} else {
					crossClientList.add(
							"'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorName() + "'");
				}
			}
		}

		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.totalNoOfAlbumSingleComparision);

	}

	public void TC13_Validate_Royalties_trend_Physical_Single_Client_Selection() {
		try {
			ArrayList<String> crossClientList = new ArrayList<String>();
			commonMethods.royaltyTrendPhyList = new ArrayList<List<String>>();
			List<String> allYears = new ArrayList<String>();
			for (SingleClient singleClient : excelData.getSinglsClient()) {
				if (singleClient.getRoyaltorCode() == null) {
					break;
				} else {
					userSelectClient.user_select_client(singleClient.getRoyaltorCode(), singleClient.getCurrency_Desc(),
							null, false, singleClient.getRoyaltorName());
					allYears = commonMethods.fetchYearsFromUI();
					// commonMethods.fetchRoyaltyTrendPhys(singleClient.getRoyaltorCode(),crossClientList,null,singleClient.getRoyaltorSite(),null,allYears);
					for (String year : allYears) {
						commonMethods.fetchRoyaltyTrendPhys(singleClient.getRoyaltorCode(),
								singleClient.getRoyaltorName(), crossClientList, null, null, null,
								singleClient.getRoyaltorSite(), year, null);
					}
				}
			}
			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendPhysical);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC14_Validate_Royalties_trend_Physical_All_Client_Selection() {
		try {
			ArrayList<String> crossClientList = new ArrayList<String>();
			ArrayList<String> crossClientNamesList = new ArrayList<String>();
			commonMethods.royaltyTrendPhyList = new ArrayList<List<String>>();
			List<String> allYears = new ArrayList<String>();
			for (CrossClient crossClient : excelData.getCrossClient()) {
				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						userSelectClient.user_select_client(crossClient.getRoyaltorCode(),
								crossClient.getCurrency_Desc(), null, true, crossClient.getRoyaltorName());
						allYears = commonMethods.fetchYearsFromUI();
						for (String year : allYears) {
							commonMethods.fetchRoyaltyTrendPhys(crossClient.getRoyaltorCode(),
									crossClient.getRoyaltorName(), crossClientList, crossClientNamesList, null, null,
									crossClient.getRoyaltorSite(), year, null);
						}
						allYears = new ArrayList<String>();
						crossClientList = new ArrayList<String>();
						crossClientNamesList = new ArrayList<String>();
					} else {
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
						crossClientNamesList.add("'" + crossClient.getRoyaltorName() + "'");

					}
				}
			}

			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendPhysical);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC15_Validate_Royalties_trend_Physical_Different_CrossClient_Payee_Selection() throws Exception {
		ArrayList<String> crossClientList = new ArrayList<String>();
		commonMethods.royaltyTrendPhyList = new ArrayList<List<String>>();
		String crossClientName = null;
		List<String> allYears = new ArrayList<String>();
		for (CrossClient crossClient : excelData.getCrossClient()) {
			if (crossClient.getRoyaltorCode().isEmpty()) {
				break;
			} else {
				if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {
					for (String id : crossClientList) {
						String ids[] = id.split(",");
						id = ids[0];
						crossClientName = ids[1];
						userSelectClient.user_select_client(crossClient.getRoyaltorCode(),
								crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorName());
						allYears = commonMethods.fetchYearsFromUI();
						for (String year : allYears) {
							commonMethods.fetchRoyaltyTrendPhys(crossClient.getRoyaltorCode(),
									crossClient.getRoyaltorName(), null, null, crossClientName, id,
									crossClient.getRoyaltorSite(), year, null);
						}
						allYears = new ArrayList<String>();
						crossClientList = new ArrayList<String>();
					}
				} else {
					crossClientList.add(
							"'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorName() + "'");
				}
			}
		}

		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendPhysical);
	}

	public void TC16_Validate_Royalties_trend_Digital_Single_Client_Selection() {
		try {
			ArrayList<String> crossClientList = new ArrayList<String>();
			commonMethods.royaltyTrendDigitalFinalList = new ArrayList<List<String>>();
			List<String> allYears = new ArrayList<String>();
			for (SingleClient singleClient : excelData.getSinglsClient()) {
				if (singleClient.getRoyaltorCode() == null) {
					break;
				} else {
					userSelectClient.user_select_client(singleClient.getRoyaltorCode(), singleClient.getCurrency_Desc(),
							null, false, singleClient.getRoyaltorName());
					allYears = commonMethods.fetchYearsFromUI();
					for (String year : allYears) {

						commonMethods.fetchRoyaltyTrendDigital(singleClient.getRoyaltorCode(),
								singleClient.getRoyaltorName(), crossClientList, null, null, null,
								singleClient.getRoyaltorSite(), year, null);
					}
				}
			}
			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendDigital);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC17_Validate_Royalties_trend_Digital_All_Client_Selection() {
		try {
			ArrayList<String> crossClientList = new ArrayList<String>();
			ArrayList<String> crossClientNamesList = new ArrayList<String>();
			commonMethods.royaltyTrendDigitalFinalList = new ArrayList<List<String>>();
			List<String> allYears = new ArrayList<String>();
			for (CrossClient crossClient : excelData.getCrossClient()) {
				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						userSelectClient.user_select_client(crossClient.getRoyaltorCode(),
								crossClient.getCurrency_Desc(), null, true, crossClient.getRoyaltorName());
						allYears = commonMethods.fetchYearsFromUI();
						for (String year : allYears) {
							commonMethods.fetchRoyaltyTrendDigital(crossClient.getRoyaltorCode(),
									crossClient.getRoyaltorName(), crossClientList, crossClientNamesList, null, null,
									crossClient.getRoyaltorSite(), year, null);
						}
						allYears = new ArrayList<String>();
						crossClientList = new ArrayList<String>();
						crossClientNamesList = new ArrayList<String>();
					} else {
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
						crossClientNamesList.add("'" + crossClient.getRoyaltorName() + "'");
					}
				}
			}

			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendDigital);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC18_Validate_Royalties_trend_Digital_Different_CrossClient_Payee_Selection() throws Exception {
		ArrayList<String> crossClientList = new ArrayList<String>();
		commonMethods.royaltyTrendDigitalFinalList = new ArrayList<List<String>>();
		String crossClientName = null;
		List<String> allYears = new ArrayList<String>();
		for (CrossClient crossClient : excelData.getCrossClient()) {
			if (crossClient.getRoyaltorCode().isEmpty()) {
				break;
			} else {
				if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {
					for (String id : crossClientList) {
						String ids[] = id.split(",");
						id = ids[0];
						crossClientName = ids[1];
						userSelectClient.user_select_client(crossClient.getRoyaltorCode(),
								crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorName());
						allYears = commonMethods.fetchYearsFromUI();
						for (String year : allYears) {
							commonMethods.fetchRoyaltyTrendDigital(crossClient.getRoyaltorCode(),
									crossClient.getRoyaltorName(), null, null, crossClientName, id,
									crossClient.getRoyaltorSite(), year, null);
						}
						allYears = new ArrayList<String>();
						crossClientList = new ArrayList<String>();
					}
				} else {
					crossClientList.add(
							"'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorName() + "'");
				}
			}
		}

		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendDigital);
	}

	public void TC19_Validate_Royalties_trend_TotalRoyalty_Single_Client_Selection() {
		try {
			ArrayList<String> crossClientList = new ArrayList<String>();
			commonMethods.royaltyTrendTotalRoyaltyFinalList = new ArrayList<List<String>>();
			List<String> allYears = new ArrayList<String>();
			for (SingleClient singleClient : excelData.getSinglsClient()) {
				if (singleClient.getRoyaltorCode() == null) {
					break;
				} else {
					userSelectClient.user_select_client(singleClient.getRoyaltorCode(), singleClient.getCurrency_Desc(),
							null, false, singleClient.getRoyaltorName());
					allYears = commonMethods.fetchYearsFromUI();
					commonMethods.fetchRoyaltyTrendTotalRoyalty(singleClient.getRoyaltorCode(),
							singleClient.getRoyaltorName(), crossClientList, null, null, null,
							singleClient.getRoyaltorSite(), null, allYears);
					for (String year : allYears) {
						commonMethods.fetchRoyaltyTrendTotalRoyalty(singleClient.getRoyaltorCode(),
								singleClient.getRoyaltorName(), crossClientList, null, null, null,
								singleClient.getRoyaltorSite(), year, null);
					}
				}
			}
			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendTotalRoyalty);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void TC20_Validate_Royalties_trend_TotalRoyalty_All_Client_Selection() {
		try {
			ArrayList<String> crossClientList = new ArrayList<String>();
			ArrayList<String> crossClientNamesList = new ArrayList<String>();
			commonMethods.royaltyTrendTotalRoyaltyFinalList = new ArrayList<List<String>>();
			List<String> allYears = new ArrayList<String>();
			for (CrossClient crossClient : excelData.getCrossClient()) {
				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						userSelectClient.user_select_client(crossClient.getRoyaltorCode(),
								crossClient.getCurrency_Desc(), null, true, crossClient.getRoyaltorName());
						allYears = commonMethods.fetchYearsFromUI();
						commonMethods.fetchRoyaltyTrendTotalRoyalty(crossClient.getRoyaltorCode(),
								crossClient.getRoyaltorName(), crossClientList, crossClientNamesList, null, null,
								crossClient.getRoyaltorSite(), null, allYears);
						for (String year : allYears) {
							commonMethods.fetchRoyaltyTrendTotalRoyalty(crossClient.getRoyaltorCode(),
									crossClient.getRoyaltorName(), crossClientList, crossClientNamesList, null, null,
									crossClient.getRoyaltorSite(), year, null);
						}
						allYears = new ArrayList<String>();
						crossClientList = new ArrayList<String>();
						crossClientNamesList = new ArrayList<String>();
					} else {
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
						crossClientNamesList.add("'" + crossClient.getRoyaltorName() + "'");
					}
				}
			}

			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendTotalRoyalty);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC21_Validate_Royalties_trend_TotalRoyalty_Different_CrossClient_Payee_Selection() throws Exception {
		ArrayList<String> crossClientList = new ArrayList<String>();
		commonMethods.royaltyTrendTotalRoyaltyFinalList = new ArrayList<List<String>>();
		String crossClientName = null;
		List<String> allYears = new ArrayList<String>();
		for (CrossClient crossClient : excelData.getCrossClient()) {
			if (crossClient.getRoyaltorCode().isEmpty()) {
				break;
			} else {
				if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {
					for (String id : crossClientList) {
						String ids[] = id.split(",");
						id = ids[0];
						crossClientName = ids[1];
						userSelectClient.user_select_client(crossClient.getRoyaltorCode(),
								crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorName());
						allYears = commonMethods.fetchYearsFromUI();
						commonMethods.fetchRoyaltyTrendTotalRoyalty(crossClient.getRoyaltorCode(),
								crossClient.getRoyaltorName(), null, null, crossClientName, id,
								crossClient.getRoyaltorSite(), null, allYears);
						for (String year : allYears) {
							commonMethods.fetchRoyaltyTrendTotalRoyalty(crossClient.getRoyaltorCode(),
									crossClient.getRoyaltorName(), null, null, crossClientName, id,
									crossClient.getRoyaltorSite(), year, null);
						}
						allYears = new ArrayList<String>();
						crossClientList = new ArrayList<String>();
					}
				} else {
					crossClientList.add(
							"'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorName() + "'");
				}
			}
		}
		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendTotalRoyalty);
	}

	public void TC23_Validate_TopTracks_Single_Client_Selection() throws Exception {
		ArrayList<String> crossClientList = new ArrayList<String>();
		commonMethods.statementDataUI = new ArrayList<List<String>>();
		for (SingleClient singleClient : excelData.getSinglsClient()) {
			if (singleClient.getRoyaltorCode() == null) {
				break;
			} else {
				userSelectClient.user_select_client(singleClient.getRoyaltorCode(), singleClient.getCurrency_Desc(),
						null, false, singleClient.getRoyaltorName());
				commonMethods.fetchTopTracksFromUIandDb(singleClient.getRoyaltorCode(), singleClient.getRoyaltorName(),
						crossClientList, null, singleClient.getRoyaltorSite());
				// commonMethods.fetchDataFromDB(singleClient.getRoyaltorCode(),singleClient.getRoyaltorSite());

			}
		}

		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topTracks);
	}

	public void TC24_Validate_TopTracks_All_Client_Selection() throws Exception {

		ArrayList<String> crossClientList = new ArrayList<String>();
		commonMethods.statementDataUI = new ArrayList<List<String>>();
		for (CrossClient crossClient : excelData.getCrossClient()) {

			if (crossClient.getRoyaltorCode().isEmpty()) {
				break;
			} else {
				if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

					userSelectClient.user_select_client(crossClient.getRoyaltorCode(), crossClient.getCurrency_Desc(),
							null, true, crossClient.getRoyaltorName());
					commonMethods.fetchTopTracksFromUIandDb(crossClient.getRoyaltorCode(),
							crossClient.getRoyaltorName(), crossClientList, null, crossClient.getRoyaltorSite());
					crossClientList = new ArrayList<String>();
				} else {
					crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
					crossClientList.add("'" + crossClient.getRoyaltorName() + "'");
				}

			}
		}

		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topTracks);
	}

	public void TC25_Validate_TopTracks_Different_CrossClient_Payee_Selection() throws Exception {
		ArrayList<String> crossClientList = new ArrayList<String>();

		commonMethods.statementDataUI = new ArrayList<List<String>>();
		for (CrossClient crossClient : excelData.getCrossClient()) {

			if (crossClient.getRoyaltorCode().isEmpty()) {
				break;
			} else {
				if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

					for (String id : crossClientList) {

						userSelectClient.user_select_client(crossClient.getRoyaltorCode(),
								crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorName());
						commonMethods.fetchTopTracksFromUIandDb(crossClient.getRoyaltorCode(),
								crossClient.getRoyaltorName(), null, id, crossClient.getRoyaltorSite());
						crossClientList = new ArrayList<String>();
					}
				} else {
					crossClientList.add(
							"'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorName() + "'");
				}

			}
		}

		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topTracks);
	}

	public void TC26_Validate_NewStatementDue_Single_Client_Selection() throws Exception

	{
		ArrayList<String> crossClientList = new ArrayList<String>();
		commonMethods.newStatementDueFinalResult = new ArrayList<List<String>>();
		for (SingleClient singleClient : excelData.getSinglsClient()) {
			if (singleClient.getRoyaltorCode() == null) {
				break;
			} else {
				userSelectClient.user_select_client(singleClient.getRoyaltorCode(), singleClient.getCurrency_Desc(),
						null, false, singleClient.getRoyaltorName());
				commonMethods.fetchNewStatementDueFromUIandDb(singleClient.getRoyaltorCode(),
						singleClient.getRoyaltorName(), crossClientList, null, null, null,
						singleClient.getRoyaltorSite());
				// commonMethods.fetchDataFromDB(singleClient.getRoyaltorCode(),singleClient.getRoyaltorSite());

			}
		}

		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.newStatementDueComparison);
	}

	public void TC27_Validate_NewStatementDue_All_Client_Selection() throws Exception {
		ArrayList<String> crossClientList = new ArrayList<String>();
		ArrayList<String> crossClientNamesList = new ArrayList<String>();
		commonMethods.newStatementDueFinalResult = new ArrayList<List<String>>();
		for (CrossClient crossClient : excelData.getCrossClient()) {

			if (crossClient.getRoyaltorCode().isEmpty()) {
				break;
			} else {
				if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

					userSelectClient.user_select_client(crossClient.getRoyaltorCode(), crossClient.getCurrency_Desc(),
							null, true, crossClient.getRoyaltorName());
					commonMethods.fetchNewStatementDueFromUIandDb(crossClient.getRoyaltorCode(),
							crossClient.getRoyaltorName(), crossClientList, crossClientNamesList, null, null,
							crossClient.getRoyaltorSite());
					crossClientList = new ArrayList<String>();
					crossClientNamesList = new ArrayList<String>();
				} else {
					crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
					crossClientNamesList.add("'" + crossClient.getRoyaltorName() + "'");
				}

			}
		}

		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.newStatementDueComparison);
	}

	public void TC28_Validate_NewStatementDue_Diff_CrossClient_Payee_Selection() throws Exception {
		ArrayList<String> crossClientList = new ArrayList<String>();
		String crossClientName = null;
		commonMethods.newStatementDueFinalResult = new ArrayList<List<String>>();
		for (CrossClient crossClient : excelData.getCrossClient()) {

			if (crossClient.getRoyaltorCode().isEmpty()) {
				break;
			} else {
				if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

					for (String id : crossClientList) {
						String ids[] = id.split(",");
						id = ids[0];
						crossClientName = ids[1];
						userSelectClient.user_select_client(crossClient.getRoyaltorCode(),
								crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorName());
						commonMethods.fetchNewStatementDueFromUIandDb(crossClient.getRoyaltorCode(),
								crossClient.getRoyaltorName(), null, null, crossClientName, id,
								crossClient.getRoyaltorSite());
						crossClientList = new ArrayList<String>();
					}
				} else {
					crossClientList.add(
							"'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorName() + "'");
				}

			}
		}

		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.newStatementDueComparison);
	}

	public void TC29_Validate_Pipeline_Royalty_Recording_Diff_CrossClient_Payee_Selection() throws Exception {

		ArrayList<String> crossClientList = new ArrayList<String>();

		commonMethods.pipelineRoyltyRecordFinalResult = new ArrayList<List<String>>();
		for (CrossClient crossClient : excelData.getCrossClient()) {

			if (crossClient.getRoyaltorCode().isEmpty()) {
				break;
			} else {
				if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

					for (String id : crossClientList) {

						userSelectClient.user_select_client(crossClient.getRoyaltorCode(),
								crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorName());
						commonMethods.fetchPipelineRoyaltyRecordFromUIandDb(crossClient.getRoyaltorCode(), null, id,
								crossClient.getRoyaltorSite());
						crossClientList = new ArrayList<String>();
					}
				} else {
					crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
				}

			}
		}

		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.pipelineRoyltyRecordComparsion);
	}

	public void TC30_Validate_Pipeline_Royalty_Recording_All_Client_Selection() throws Exception {

		ArrayList<String> crossClientList = new ArrayList<String>();
		commonMethods.pipelineRoyltyRecordFinalResult = new ArrayList<List<String>>();
		for (CrossClient crossClient : excelData.getCrossClient()) {

			if (crossClient.getRoyaltorCode().isEmpty()) {
				break;
			} else {
				if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

					userSelectClient.user_select_client(crossClient.getRoyaltorCode(), crossClient.getCurrency_Desc(),
							null, true, crossClient.getRoyaltorName());
					commonMethods.fetchPipelineRoyaltyRecordFromUIandDb(crossClient.getRoyaltorCode(), crossClientList,
							null, crossClient.getRoyaltorSite());
					crossClientList = new ArrayList<String>();
				} else {
					crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
				}

			}
		}

		commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.pipelineRoyltyRecordComparsion);
	}

	 
	public void TC31_Validate_Pipeline_Royalty_Recording_Single_Client_Selection() throws Exception

	{
		ArrayList<String> crossClientList = new ArrayList<String>();
		commonMethods.pipelineRoyltyRecordFinalResult = new ArrayList<List<String>>();
		for (SingleClient singleClient : excelData.getSinglsClient()) {
			if (singleClient.getRoyaltorCode() == null) {
				break;
			} else {
				commonMethods.user_successfully_selects_client(singleClient.getRoyaltorCode(), singleClient.getCurrency_Desc(), null,
						false, singleClient.getRoyaltorName());
				commonMethods.fetchPipelineRoyaltyRecordFromUIandDb(singleClient.getRoyaltorCode(), crossClientList,
						null, singleClient.getRoyaltorSite());
				// commonMethods.fetchDataFromDB(singleClient.getRoyaltorCode(),singleClient.getRoyaltorSite());

			}
		}

		commonMethods
				.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.pipelineRoyltyRecordComparsion );

	}

	public void TC32_Validate_Royalties_trend_Licensing_Single_Client_Selection() {
		try {
				ArrayList<String> crossClientList = new ArrayList<String>();
				commonMethods.royaltyTrendLicensingFinalList = new ArrayList<List<String>>();
				List<String> allYears = new ArrayList<String>();
				for (SingleClient singleClient : excelData.getSinglsClient()) {
					if (singleClient.getRoyaltorCode() == null) {
						break;
					} else {
						commonMethods.user_successfully_selects_client(singleClient.getRoyaltorCode(),
								singleClient.getCurrency_Desc(), null, false, singleClient.getRoyaltorName());
						allYears = commonMethods.fetchYearsFromUI();
						// commonMethods.fetchRoyaltyTrendTotalRoyalty(singleClient.getRoyaltorCode(),crossClientList,null,singleClient.getRoyaltorSite(),null,allYears);
						for (String year : allYears) {
							commonMethods.fetchRoyaltyTrendLicensing(singleClient.getRoyaltorCode(), singleClient.getRoyaltorName(),
									crossClientList, null, null, null, singleClient.getRoyaltorSite(), year, null);
						}
					}
				}
				commonMethods
						.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendLicensing );
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC33_Validate_Royalties_trend_Licensing_All_Client_Selection() {
		try {

				ArrayList<String> crossClientList = new ArrayList<String>();
				ArrayList<String> crossClientNamesList = new ArrayList<String>();
				commonMethods.royaltyTrendLicensingFinalList = new ArrayList<List<String>>();
				List<String> allYears = new ArrayList<String>();
				for (CrossClient crossClient : excelData.getCrossClient()) {
					if (crossClient.getRoyaltorCode().isEmpty()) {
						break;
					} else {
						if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), null, true, crossClient.getRoyaltorSite());
							allYears = commonMethods.fetchYearsFromUI();
							for (String year : allYears) {
								commonMethods.fetchRoyaltyTrendLicensing(crossClient.getRoyaltorCode(),
										crossClient.getRoyaltorSite(), crossClientList, crossClientNamesList, null, null,
										crossClient.getRoyaltorSite(), year, null);
							}
							allYears = new ArrayList<String>();
							crossClientList = new ArrayList<String>();
							crossClientNamesList = new ArrayList<String>();

						} else {
							crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
							crossClientNamesList.add("'" + crossClient.getRoyaltorSite() + "'");
						}
					}
				}

				commonMethods
						.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendLicensing );
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC34_Validate_Royalties_trend_Licensing_Different_CrossClient_Payee_Selection() throws Exception {
			ArrayList<String> crossClientList = new ArrayList<String>();
			commonMethods.royaltyTrendLicensingFinalList = new ArrayList<List<String>>();
			String crossClientName = null;
			List<String> allYears = new ArrayList<String>();
			for (CrossClient crossClient : excelData.getCrossClient()) {
				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {
						for (String id : crossClientList) {
							String ids[] = id.split(",");
							id = ids[0];
							crossClientName = ids[1];
							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorSite());
							allYears = commonMethods.fetchYearsFromUI();
							for (String year : allYears) {
								commonMethods.fetchRoyaltyTrendLicensing(crossClient.getRoyaltorCode(),
										crossClient.getRoyaltorSite(), null, null, crossClientName, id, crossClient.getRoyaltorSite(), year,
										null);
							}
							allYears = new ArrayList<String>();
							crossClientList = new ArrayList<String>();
						}
					} else {
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorSite() + "'");
					}
				}
			}
			commonMethods
					.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendLicensing );
	}

	public void TC35_Validate_Royalties_trend_Sync_Single_Client_Selection() {
		try {
				ArrayList<String> crossClientList = new ArrayList<String>();
				List<String> allYears = new ArrayList<String>();
				commonMethods.royaltyTrendSyncFinalList = new ArrayList<List<String>>();
				for (SingleClient singleClient : excelData.getSinglsClient()) {
					if (singleClient.getRoyaltorCode() == null) {
						break;
					} else {
						commonMethods.user_successfully_selects_client(singleClient.getRoyaltorCode(),
								singleClient.getCurrency_Desc(), null, false, singleClient.getRoyaltorName());
						allYears = commonMethods.fetchYearsFromUI();
						for (String year : allYears) {
							commonMethods.fetchRoyaltyTrendSync(singleClient.getRoyaltorCode(), singleClient.getRoyaltorName(),
									crossClientList, null, null, null, singleClient.getRoyaltorSite(), year, null);
						}
					}
				}
				commonMethods
						.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendSync );
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC36_Validate_Royalties_trend_Sync_All_Client_Selection() {
		try {
				ArrayList<String> crossClientList = new ArrayList<String>();
				ArrayList<String> crossClientNamesList = new ArrayList<String>();
				commonMethods.royaltyTrendSyncFinalList = new ArrayList<List<String>>();
				List<String> allYears = new ArrayList<String>();
				for (CrossClient crossClient : excelData.getCrossClient()) {
					if (crossClient.getRoyaltorCode().isEmpty()) {
						break;
					} else {
						if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), null, true, crossClient.getRoyaltorSite());
							allYears = commonMethods.fetchYearsFromUI();
							for (String year : allYears) {
								commonMethods.fetchRoyaltyTrendSync(crossClient.getRoyaltorCode(), crossClient.getRoyaltorSite(),
										crossClientList, crossClientNamesList, null, null, crossClient.getRoyaltorSite(), year,
										null);
							}
							allYears = new ArrayList<String>();
							crossClientList = new ArrayList<String>();
							crossClientNamesList = new ArrayList<String>();
						} else {
							crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
							crossClientNamesList.add("'" + crossClient.getRoyaltorSite() + "'");
						}
					}
				}
				commonMethods
						.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendSync);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC37_Validate_Royalties_trend_Sync_Different_CrossClient_Payee_Selection() throws Exception {
			ArrayList<String> crossClientList = new ArrayList<String>();
			commonMethods.royaltyTrendSyncFinalList = new ArrayList<List<String>>();
			String crossClientName = null;
			List<String> allYears = new ArrayList<String>();
			for (CrossClient crossClient : excelData.getCrossClient()) {
				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {
						for (String id : crossClientList) {
							String ids[] = id.split(",");
							id = ids[0];
							crossClientName = ids[1];
							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorSite());
							allYears = commonMethods.fetchYearsFromUI();
							for (String year : allYears) {
								commonMethods.fetchRoyaltyTrendSync(crossClient.getRoyaltorCode(), crossClient.getRoyaltorSite(),
										null, null, crossClientName, id, crossClient.getRoyaltorSite(), year, null);
							}
							allYears = new ArrayList<String>();
							crossClientList = new ArrayList<String>();
						}
					} else {
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorSite() + "'");
					}
				}
			}
			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendSync);
	}

	public void TC38_Validate_Royalties_trend_PublicPerformance_Single_Client_Selection() {
		try {
				ArrayList<String> crossClientList = new ArrayList<String>();
				List<String> allYears = new ArrayList<String>();
				commonMethods.royaltyTrendPublicPerformanceFinalList = new ArrayList<List<String>>();
				for (SingleClient singleClient : excelData.getSinglsClient()) {
					if (singleClient.getRoyaltorCode() == null) {
						break;
					} else {
						commonMethods.user_successfully_selects_client(singleClient.getRoyaltorCode(),
								singleClient.getCurrency_Desc(), null, false, singleClient.getRoyaltorName());
						allYears = commonMethods.fetchYearsFromUI();
						for (String year : allYears) {
							commonMethods.fetchRoyaltyTrendPublicPerformance(singleClient.getRoyaltorCode(),
									singleClient.getRoyaltorName(), crossClientList, null, null, null, singleClient.getRoyaltorSite(), year,
									null);
						}
					}
				}
				commonMethods
						.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendPublicPerformance);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC39_Validate_Royalties_trend_PublicPerformance_All_Client_Selection() {
		try {

				ArrayList<String> crossClientList = new ArrayList<String>();
				ArrayList<String> crossClientNamesList = new ArrayList<String>();
				commonMethods.royaltyTrendPublicPerformanceFinalList = new ArrayList<List<String>>();
				List<String> allYears = new ArrayList<String>();
				for (CrossClient crossClient : excelData.getCrossClient()) {
					if (crossClient.getRoyaltorCode().isEmpty()) {
						break;
					} else {
						if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), null, true, crossClient.getRoyaltorSite());
							allYears = commonMethods.fetchYearsFromUI();
							for (String year : allYears) {
								commonMethods.fetchRoyaltyTrendPublicPerformance(crossClient.getRoyaltorCode(),
										crossClient.getRoyaltorSite(), crossClientList, crossClientNamesList, null, null,
										crossClient.getRoyaltorSite(), year, null);
							}
							allYears = new ArrayList<String>();
							crossClientList = new ArrayList<String>();
							crossClientNamesList = new ArrayList<String>();
						} else {
							crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
							crossClientNamesList.add("'" + crossClient.getRoyaltorSite() + "'");
						}
					}
				}

				commonMethods
						.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendPublicPerformance);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC40_Validate_Royalties_trend_PublicPerfomance_Different_CrossClient_Payee_Selection()
			throws Exception {
			ArrayList<String> crossClientList = new ArrayList<String>();
			commonMethods.royaltyTrendPublicPerformanceFinalList = new ArrayList<List<String>>();
			String crossClientName = null;
			List<String> allYears = new ArrayList<String>();
			for (CrossClient crossClient : excelData.getCrossClient()) {
				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {
						for (String id : crossClientList) {
							String ids[] = id.split(",");
							id = ids[0];
							crossClientName = ids[1];
							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorSite());
							allYears = commonMethods.fetchYearsFromUI();
							for (String year : allYears) {
								commonMethods.fetchRoyaltyTrendPublicPerformance(crossClient.getRoyaltorCode(),
										crossClient.getRoyaltorSite(), null, null, crossClientName, id, crossClient.getRoyaltorSite(), year,
										null);
							}
							allYears = new ArrayList<String>();
							crossClientList = new ArrayList<String>();
						}
					} else {
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorSite() + "'");
					}
				}
			}
			commonMethods
					.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendPublicPerformance);
	}

	public void TC41_Validate_Royalties_trend_Merchandise_Single_Client_Selection() {
		try {
				ArrayList<String> crossClientList = new ArrayList<String>();
				List<String> allYears = new ArrayList<String>();
				commonMethods.royaltyTrendMerchandiseFinalList = new ArrayList<List<String>>();
				for (SingleClient singleClient : excelData.getSinglsClient()) {
					if (singleClient.getRoyaltorCode() == null) {
						break;
					} else {
						commonMethods.user_successfully_selects_client(singleClient.getRoyaltorCode(),
								singleClient.getCurrency_Desc(), null, false, singleClient.getRoyaltorName());
						allYears = commonMethods.fetchYearsFromUI();
						for (String year : allYears) {
							commonMethods.fetchRoyaltyTrendMerchandise(singleClient.getRoyaltorCode(),
									singleClient.getRoyaltorName(), crossClientList, null, null, null, singleClient.getRoyaltorSite(), year,
									null);
						}
					}
				}
				commonMethods
						.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendMerchandise);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void TC42_Validate_Royalties_trend_Merchandise_All_Client_Selection() {
		try {
				ArrayList<String> crossClientList = new ArrayList<String>();
				ArrayList<String> crossClientNamesList = new ArrayList<String>();
				commonMethods.royaltyTrendMerchandiseFinalList = new ArrayList<List<String>>();
				List<String> allYears = new ArrayList<String>();
				for (CrossClient crossClient : excelData.getCrossClient()) {
					if (crossClient.getRoyaltorCode().isEmpty()) {
						break;
					} else {
						if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), null, true, crossClient.getRoyaltorSite());
							allYears = commonMethods.fetchYearsFromUI();
							for (String year : allYears) {
								commonMethods.fetchRoyaltyTrendMerchandise(crossClient.getRoyaltorCode(),
										crossClient.getRoyaltorSite(), crossClientList, crossClientNamesList, null, null,
										crossClient.getRoyaltorSite(), year, null);
							}
							allYears = new ArrayList<String>();
							crossClientList = new ArrayList<String>();
							crossClientNamesList = new ArrayList<String>();
						} else {
							crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
							crossClientNamesList.add("'" + crossClient.getRoyaltorSite() + "'");
						}
					}
				}

				commonMethods
						.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendMerchandise);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC43_Validate_Royalties_trend_Merchandise_Different_CrossClient_Payee_Selection() throws Exception {
			ArrayList<String> crossClientList = new ArrayList<String>();
			commonMethods.royaltyTrendMerchandiseFinalList = new ArrayList<List<String>>();
			String crossClientName = null;
			List<String> allYears = new ArrayList<String>();
			for (CrossClient crossClient : excelData.getCrossClient()) {
				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {
						for (String id : crossClientList) {
							String ids[] = id.split(",");
							id = ids[0];
							crossClientName = ids[1];
							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorSite());
							allYears = commonMethods.fetchYearsFromUI();
							for (String year : allYears) {
								commonMethods.fetchRoyaltyTrendMerchandise(crossClient.getRoyaltorCode(),
										crossClient.getRoyaltorSite(), null, null, crossClientName, id, crossClient.getRoyaltorSite(), year,
										null);
							}
							allYears = new ArrayList<String>();
							crossClientList = new ArrayList<String>();
						}
					} else {
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorSite() + "'");
					}
				}
			}
			commonMethods
					.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendMerchandise);
	}

	public void TC44_Validate_Royalties_trend_DonutGraph_Single_Client_Selection() {
		try {
				ArrayList<String> crossClientList = new ArrayList<String>();
				commonMethods.royaltyTrendDonutGraphFinalList = new ArrayList<List<String>>();
				List<String> allYears = new ArrayList<String>();
				for (SingleClient singleClient : excelData.getSinglsClient()) {
					if (singleClient.getRoyaltorCode() == null) {
						break;
					} else {

						commonMethods.user_successfully_selects_client(singleClient.getRoyaltorCode(),
								singleClient.getCurrency_Desc(), null, false, singleClient.getRoyaltorName());
						allYears = commonMethods.fetchYearsFromUI();
						for (String year : allYears) {
							commonMethods.fetchRoyaltyTrendDonutGraph(singleClient.getRoyaltorCode(),
									singleClient.getRoyaltorName(), crossClientList, null, null, null, singleClient.getRoyaltorSite(), year,
									null);
						}
					}
				}
				commonMethods
						.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendDonutGraph);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC45_Validate_Royalties_trend_DonutGraph_All_Client_Selection() {
		try {
				ArrayList<String> crossClientList = new ArrayList<String>();
				ArrayList<String> crossClientNamesList = new ArrayList<String>();
				commonMethods.royaltyTrendDonutGraphFinalList = new ArrayList<List<String>>();
				List<String> allYears = new ArrayList<String>();
				for (CrossClient crossClient : excelData.getCrossClient()) {
					if (crossClient.getRoyaltorCode().isEmpty()) {
						break;
					} else {
						if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), null, true, crossClient.getRoyaltorSite());
							allYears = commonMethods.fetchYearsFromUI();
							for (String year : allYears) {
								commonMethods.fetchRoyaltyTrendDonutGraph(crossClient.getRoyaltorCode(),
										crossClient.getRoyaltorSite(), crossClientList, crossClientNamesList, null, null,
										crossClient.getRoyaltorSite(), year, null);
							}
							allYears = new ArrayList<String>();
							crossClientList = new ArrayList<String>();
							crossClientNamesList = new ArrayList<String>();
						} else {
							crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
							crossClientNamesList.add("'" + crossClient.getRoyaltorSite() + "'");
						}
					}
				}

				commonMethods
						.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendDonutGraph);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void TC46_Validate_Royalties_trend_DonutGraph_Different_CrossClient_Payee_Selection() throws Exception {
			ArrayList<String> crossClientList = new ArrayList<String>();
			String crossClientName = null;
			List<String> allYears = new ArrayList<String>();
			for (CrossClient crossClient : excelData.getCrossClient()) {
				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {
						for (String id : crossClientList) {
							String ids[] = id.split(",");
							id = ids[0];
							crossClientName = ids[1];
							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorSite());
							allYears = commonMethods.fetchYearsFromUI();
							for (String year : allYears) {
								commonMethods.fetchRoyaltyTrendDonutGraph(crossClient.getRoyaltorCode(),
										crossClient.getRoyaltorSite(), null, null, crossClientName, id, crossClient.getRoyaltorSite(), year,
										null);
							}
							allYears = new ArrayList<String>();
							crossClientList = new ArrayList<String>();
						}
					} else {
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorSite() + "'");
					}
				}
			}
			commonMethods
					.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.RoyaltyTrendDonutGraph);
	}

	public void TC47_Validate_Pipeline_Royalty_Publishing_Single_Client_Selection() throws Exception

	{
		try {
				ArrayList<String> crossClientList = new ArrayList<String>();
				commonMethods.pipelineRoyltyPublishFinalResult = new ArrayList<List<String>>();
				for (SingleClient singleClient : excelData.getSinglsClient()) {
					if (singleClient.getRoyaltorCode() == null) {
						break;
					} else {
						commonMethods.user_successfully_selects_client(singleClient.getRoyaltorCode(),
								singleClient.getCurrency_Desc(), null, false, singleClient.getRoyaltorName());
						commonMethods.fetchPipelineRoyaltyPublishingFromUIandDb(singleClient.getRoyaltorCode(),
								singleClient.getRoyaltorName(), crossClientList, null, null, null, singleClient.getRoyaltorSite());
						// commonMethods.fetchDataFromDB(singleClient.getRoyaltorCode(),singleClient.getRoyaltorSite());

					}
				}

				commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),
						Constants.pipelineRoyltyPublishingComparsion);
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in TC47:" + e.getCause());
		}
	}

	public void TC50_Validate_TopLicensingProducts_Single_Client_Selection() throws Exception

	{
		try {

				ArrayList<String> crossClientList = new ArrayList<String>();
				commonMethods.topLicensingProductFinalList = new ArrayList<List<String>>();
				for (SingleClient singleClient : excelData.getSinglsClient()) {
					if (singleClient.getRoyaltorCode() == null) {
						break;
					} else {
						commonMethods.user_successfully_selects_client(singleClient.getRoyaltorCode(),
								singleClient.getCurrency_Desc(), null, false, singleClient.getRoyaltorName());
						commonMethods.fetchTopLicensingProductsFromUIandDb(singleClient.getRoyaltorCode(),
								singleClient.getRoyaltorName(), crossClientList, null, null, null, singleClient.getRoyaltorSite());
						// commonMethods.fetchDataFromDB(,[i][1],singleClient.getRoyaltorSite());

					}
				}

				commonMethods
						.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topLicensingProducts);
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in TC50:" + e.getCause());
		}
	}

	public void TC51_Validate_TopLicensingProducts_All_Client_Selection() throws Exception {
			ArrayList<String> crossClientList = new ArrayList<String>();
			ArrayList<String> crossClientNamesList = new ArrayList<String>();
			commonMethods.topLicensingProductFinalList = new ArrayList<List<String>>();
			for (CrossClient crossClient : excelData.getCrossClient()) {

				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(), crossClient.getCurrency_Desc(),
								null, true, crossClient.getRoyaltorSite());
						commonMethods.fetchTopLicensingProductsFromUIandDb(crossClient.getRoyaltorCode(),
								crossClient.getRoyaltorSite(), crossClientList, crossClientNamesList, null, null,
								crossClient.getRoyaltorSite());
						crossClientList = new ArrayList<String>();
						crossClientNamesList = new ArrayList<String>();
					} else {
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
						crossClientNamesList.add("'" + crossClient.getRoyaltorSite() + "'");
					}

				}
			}

			commonMethods
					.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topLicensingProducts);
		

	}

	public void TC52_Validate_TopLicesingProducts_Diff_CrossClient_Payee_Selection() throws Exception {

			ArrayList<String> crossClientList = new ArrayList<String>();
			commonMethods.topLicensingProductFinalList = new ArrayList<List<String>>();
			String crossClientName = null;
			for (CrossClient crossClient : excelData.getCrossClient()) {

				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						for (String id : crossClientList) {
							String ids[] = id.split(",");
							id = ids[0];
							crossClientName = ids[1];
							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorSite());
							commonMethods.fetchTopLicensingProductsFromUIandDb(crossClient.getRoyaltorCode(),
									crossClient.getRoyaltorSite(), null, null, crossClientName, id, crossClient.getRoyaltorSite());
							crossClientList = new ArrayList<String>();
						}
					} else {
						// crossClientList.add("'"+crossClient.getRoyaltorCode()+"'");
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorSite() + "'");
					}

				}
			}

			commonMethods
					.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topLicensingProducts);

	}

	public void TC53_Validate_TopSyncProducts_Single_Client_Selection() throws Exception {
			ArrayList<String> crossClientList = new ArrayList<String>();
			commonMethods.topSyncProductFinalList = new ArrayList<List<String>>();
			for (SingleClient singleClient : excelData.getSinglsClient()) {
				if (singleClient.getRoyaltorCode() == null) {
					break;
				} else {
					commonMethods.user_successfully_selects_client(singleClient.getRoyaltorCode(), singleClient.getCurrency_Desc(),
							null, false, singleClient.getRoyaltorName());
					commonMethods.fetchTopSyncProductsFromUIandDb(singleClient.getRoyaltorCode(), singleClient.getRoyaltorName(),
							crossClientList, null, null, null, singleClient.getRoyaltorSite());
					// commonMethods.fetchDataFromDB(,[i][1],singleClient.getRoyaltorSite());

				}
			}

			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topSyncProducts);

	}

	public void TC54_Validate_TopSyncProducts_All_Client_Selection() throws Exception {
			ArrayList<String> crossClientList = new ArrayList<String>();
			ArrayList<String> crossClientNamesList = new ArrayList<String>();
			commonMethods.topSyncProductFinalList = new ArrayList<List<String>>();
			for (CrossClient crossClient : excelData.getCrossClient()) {

				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(), crossClient.getCurrency_Desc(),
								null, true, crossClient.getRoyaltorSite());

						commonMethods.fetchTopSyncProductsFromUIandDb(crossClient.getRoyaltorCode(), crossClient.getRoyaltorSite(),
								crossClientList, crossClientNamesList, null, null, crossClient.getRoyaltorSite());
						crossClientList = new ArrayList<String>();
						crossClientNamesList = new ArrayList<String>();
					} else {
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
						crossClientNamesList.add("'" + crossClient.getRoyaltorSite() + "'");
					}

				}
			}

			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topSyncProducts);
	 
	}

	public void TC55_Validate_TopSyncProducts_Diff_CrossClient_Payee_Selection() throws Exception {
		 	ArrayList<String> crossClientList = new ArrayList<String>();
			String crossClientName = null;
			commonMethods.topSyncProductFinalList = new ArrayList<List<String>>();
			for (CrossClient crossClient : excelData.getCrossClient()) {

				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						for (String id : crossClientList) {
							String ids[] = id.split(",");
							id = ids[0];
							crossClientName = ids[1];
							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorSite());
							commonMethods.fetchTopSyncProductsFromUIandDb(crossClient.getRoyaltorCode(),
									crossClient.getRoyaltorSite(), null, null, crossClientName, id, crossClient.getRoyaltorSite());
							crossClientList = new ArrayList<String>();
						}
					} else {
						// crossClientList.add("'"+crossClient.getRoyaltorCode()+"'");
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorSite() + "'");
					}
				}
			}

			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topSyncProducts);
		 
	}

	public void TC56_Validate_TopPublicPerformanceProducts_Single_Client_Selection() throws Exception {
	 		ArrayList<String> crossClientList = new ArrayList<String>();
			commonMethods.topPPProductFinalList = new ArrayList<List<String>>();
			for (SingleClient singleClient : excelData.getSinglsClient()) {
				if (singleClient.getRoyaltorCode() == null) {
					break;
				} else {
					commonMethods.user_successfully_selects_client(singleClient.getRoyaltorCode(), singleClient.getCurrency_Desc(),
							null, false, singleClient.getRoyaltorName());
					commonMethods.fetchTopPPProductsFromUIandDb(singleClient.getRoyaltorCode(), singleClient.getRoyaltorName(),
							crossClientList, null, null, null, singleClient.getRoyaltorSite());
					// commonMethods.fetchDataFromDB(,[i][1],singleClient.getRoyaltorSite());

				}
			}

			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topPPProducts);
		 
	}

	public void TC57_Validate_TopPublicPerformanceProducts_All_Client_Selection() throws Exception {
		 	ArrayList<String> crossClientList = new ArrayList<String>();
			ArrayList<String> crossClientNamesList = new ArrayList<String>();
			commonMethods.topPPProductFinalList = new ArrayList<List<String>>();
			for (CrossClient crossClient : excelData.getCrossClient()) {

				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(), crossClient.getCurrency_Desc(),
								null, true, crossClient.getRoyaltorSite());
						commonMethods.fetchTopPPProductsFromUIandDb(crossClient.getRoyaltorCode(), crossClient.getRoyaltorSite(),
								crossClientList, crossClientNamesList, null, null, crossClient.getRoyaltorSite());
						crossClientList = new ArrayList<String>();
						crossClientNamesList = new ArrayList<String>();
					} else {
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
						crossClientNamesList.add("'" + crossClient.getRoyaltorSite() + "'");
					}
				}
			}

			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topPPProducts);
	 
	}

	public void TC58_Validate_TopPublicPerformanceProducts_Diff_CrossClient_Payee_Selection() throws Exception {
 	ArrayList<String> crossClientList = new ArrayList<String>();
			String crossClientName = null;
			commonMethods.topPPProductFinalList = new ArrayList<List<String>>();
			for (CrossClient crossClient : excelData.getCrossClient()) {

				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						for (String id : crossClientList) {
							String ids[] = id.split(",");
							id = ids[0];
							crossClientName = ids[1];
							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorSite());
							commonMethods.fetchTopPPProductsFromUIandDb(crossClient.getRoyaltorCode(),
									crossClient.getRoyaltorSite(), null, null, crossClientName, id, crossClient.getRoyaltorSite());
							crossClientList = new ArrayList<String>();
						}
					} else {
						// crossClientList.add("'"+crossClient.getRoyaltorCode()+"'");
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorSite() + "'");
					}

				}
			}

			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topPPProducts);
	 

	}

	public void TC59_Validate_TopMerchandiseProduct_Single_Client_Selection() throws Exception {
		 
			ArrayList<String> crossClientList = new ArrayList<String>();
			commonMethods.topMrchndiseProductFinalList = new ArrayList<List<String>>();
			for (SingleClient singleClient : excelData.getSinglsClient()) {
				if (singleClient.getRoyaltorCode() == null) {
					break;
				} else {
					commonMethods.user_successfully_selects_client(singleClient.getRoyaltorCode(), singleClient.getCurrency_Desc(),
							null, false, singleClient.getRoyaltorName());
					commonMethods.fetchTopMrchndiseProductsFromUIandDb(singleClient.getRoyaltorCode(),
							singleClient.getRoyaltorName(), crossClientList, null, null, null, singleClient.getRoyaltorSite());
					// commonMethods.fetchDataFromDB(,[i][1],singleClient.getRoyaltorSite());

				}
			}

			commonMethods
					.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topMrchndiseProducts);
		 
	}

	public void TC60_Validate_TopMerchandiseProduct_All_Client_Selection() throws Exception {
	 		ArrayList<String> crossClientList = new ArrayList<String>();
			ArrayList<String> crossClientNamesList = new ArrayList<String>();
			commonMethods.topMrchndiseProductFinalList = new ArrayList<List<String>>();
			for (CrossClient crossClient : excelData.getCrossClient()) {

				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(), crossClient.getCurrency_Desc(),
								null, true, crossClient.getRoyaltorSite());
						commonMethods.fetchTopMrchndiseProductsFromUIandDb(crossClient.getRoyaltorCode(),
								crossClient.getRoyaltorSite(), crossClientList, crossClientNamesList, null, null,
								crossClient.getRoyaltorSite());
						crossClientList = new ArrayList<String>();
						crossClientNamesList = new ArrayList<String>();
					} else {
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
						crossClientNamesList.add("'" + crossClient.getRoyaltorSite() + "'");
					}

				}
			}

			commonMethods
					.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topMrchndiseProducts);
		 

	}

	public void TC61_Validate_TopMerchandiseProduct_Diff_CrossClient_Payee_Selection() throws Exception {
		 	ArrayList<String> crossClientList = new ArrayList<String>();
			String crossClientName = null;
			commonMethods.topMrchndiseProductFinalList = new ArrayList<List<String>>();
			for (CrossClient crossClient : excelData.getCrossClient()) {

				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						for (String id : crossClientList) {

							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorSite());
							commonMethods.fetchTopMrchndiseProductsFromUIandDb(crossClient.getRoyaltorCode(),
									crossClient.getRoyaltorSite(), null, null, crossClientName, id, crossClient.getRoyaltorSite());
							crossClientList = new ArrayList<String>();
						}
					} else {
						// crossClientList.add("'"+crossClient.getRoyaltorCode()+"'");
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorSite() + "'");
					}

				}
			}

			commonMethods
					.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topMrchndiseProducts);
	 
	}

	public void TC62_Validate_TopTerritories_Single_Client_Selection() throws Exception {
	 	ArrayList<String> crossClientList = new ArrayList<String>();
			commonMethods.topPerfromTerrFinalList = new ArrayList<List<String>>();
			for (SingleClient singleClient : excelData.getSinglsClient()) {
				if (singleClient.getRoyaltorCode() == null) {
					break;
				} else {
					commonMethods.user_successfully_selects_client(singleClient.getRoyaltorCode(), singleClient.getCurrency_Desc(),
							null, false, singleClient.getRoyaltorName());
					commonMethods.fetchTopPerformTerrFromUIandDb(singleClient.getRoyaltorCode(), singleClient.getRoyaltorName(),
							crossClientList, null, null, null, singleClient.getRoyaltorSite());
					// commonMethods.fetchDataFromDB(,[i][1],singleClient.getRoyaltorSite());

				}
			}

			commonMethods
					.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topPerformingTerritories);
	 

	}

	public void TC63_Validate_TopPerformingTerritories_All_Client_Selection() throws Exception {

			ArrayList<String> crossClientList = new ArrayList<String>();
			ArrayList<String> crossClientNamesList = new ArrayList<String>();

			commonMethods.topPerfromTerrFinalList = new ArrayList<List<String>>();
			for (CrossClient crossClient : excelData.getCrossClient()) {

				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(), crossClient.getCurrency_Desc(),
								null, true, crossClient.getRoyaltorSite());
						commonMethods.fetchTopPerformTerrFromUIandDb(crossClient.getRoyaltorCode(), crossClient.getRoyaltorSite(),
								crossClientList, crossClientNamesList, null, null, crossClient.getRoyaltorSite());
						crossClientList = new ArrayList<String>();
						crossClientNamesList = new ArrayList<String>();
					} else {
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
						crossClientNamesList.add("'" + crossClient.getRoyaltorSite() + "'");
					}

				}
			}

			commonMethods
					.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topPerformingTerritories);

	}

	public void TC64_Validate_TopPerformingTerr_Diff_CrossClient_Payee_Selection() throws Exception {
			ArrayList<String> crossClientList = new ArrayList<String>();
			String crossClientName = null;
			commonMethods.topPerfromTerrFinalList = new ArrayList<List<String>>();
			for (CrossClient crossClient : excelData.getCrossClient()) {

				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						for (String id : crossClientList) {
							String ids[] = id.split(",");
							id = ids[0];
							crossClientName = ids[1];
							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorSite());
							commonMethods.fetchTopPerformTerrFromUIandDb(crossClient.getRoyaltorCode(),
									crossClient.getRoyaltorSite(), null, null, crossClientName, id, crossClient.getRoyaltorSite());
							crossClientList = new ArrayList<String>();
						}
					} else {
						// crossClientList.add("'"+crossClient.getRoyaltorCode()+"'");
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorSite() + "'");
					}

				}
			}

			commonMethods
					.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.topPerformingTerritories);

	}

	public void TC65_Validate_LatestStatement_Single_Client_Selection() throws Exception {
			ArrayList<String> crossClientList = new ArrayList<String>();
			commonMethods.latestStatementFinalList = new ArrayList<List<String>>();
			for (SingleClient singleClient : excelData.getSinglsClient()) {
				if (singleClient.getRoyaltorCode() == null) {
					break;
				} else {
					commonMethods.user_successfully_selects_client(singleClient.getRoyaltorCode(), singleClient.getCurrency_Desc(),
							null, false, singleClient.getRoyaltorName());
					commonMethods.fetchLatestStatementFromUIandDb(singleClient.getRoyaltorCode(), singleClient.getRoyaltorName(),
							crossClientList, null, null, null, singleClient.getRoyaltorSite());
					// commonMethods.fetchDataFromDB(,[i][1],singleClient.getRoyaltorSite());

				}
			}

			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.latestStatement);

	}

	public void TC66_Validate_LatestStatement_All_Client_Selection() throws Exception {

			ArrayList<String> crossClientList = new ArrayList<String>();
			ArrayList<String> crossClientNamesList = new ArrayList<String>();

			commonMethods.latestStatementFinalList = new ArrayList<List<String>>();
			for (CrossClient crossClient : excelData.getCrossClient()) {

				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(), crossClient.getCurrency_Desc(),
								null, true, crossClient.getRoyaltorSite());
						commonMethods.fetchLatestStatementFromUIandDb(crossClient.getRoyaltorCode(), crossClient.getRoyaltorSite(),
								crossClientList, crossClientNamesList, null, null, crossClient.getRoyaltorSite());
						crossClientList = new ArrayList<String>();
						crossClientNamesList = new ArrayList<String>();
					} else {
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'");
						crossClientNamesList.add("'" + crossClient.getRoyaltorSite() + "'");
					}

				}
			}

			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.latestStatement);

	}

	public void TC67_Validate_LatestStatement_Diff_CrossClient_Payee_Selection() throws Exception {

			ArrayList<String> crossClientList = new ArrayList<String>();
			String crossClientName = null;
			commonMethods.latestStatementFinalList = new ArrayList<List<String>>();
			for (CrossClient crossClient : excelData.getCrossClient()) {

				if (crossClient.getRoyaltorCode().isEmpty()) {
					break;
				} else {
					if (crossClient.getPayeeOrCrossClient().equalsIgnoreCase("Payee")) {

						for (String id : crossClientList) {
							String ids[] = id.split(",");
							id = ids[0];
							crossClientName = ids[1];
							commonMethods.user_successfully_selects_client(crossClient.getRoyaltorCode(),
									crossClient.getCurrency_Desc(), id, false, crossClient.getRoyaltorSite());
							commonMethods.fetchLatestStatementFromUIandDb(crossClient.getRoyaltorCode(),
									crossClient.getRoyaltorSite(), null, null, crossClientName, id, crossClient.getRoyaltorSite());
							crossClientList = new ArrayList<String>();
						}
					} else {
						// crossClientList.add("'"+crossClient.getRoyaltorCode()+"'");
						crossClientList.add("'" + crossClient.getRoyaltorCode() + "'" + ",'" + crossClient.getRoyaltorSite() + "'");
					}

				}
			}

			commonMethods.showComparison(testData.getResultFileNameWithPath(),testcase.getTestCaseNumber(),Constants.latestStatement );
		

	}

}
