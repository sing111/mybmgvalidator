package com.infosys.bmg.MyBMGDataValidations.manager;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBConnectionForRoyaltyTrend;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueriesForTopTerritories;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueriesForTotalAlbumSingle;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_LatestStatement;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_NewStatementDue;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_PipelineRoyalty_Publishing;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_PipelineRoyalty_Recording;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_StatementBalance;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_TopLicensingProducts;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_TopPerformingTerritories;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_TopSyncProducts;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_TopTracks;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_Top_Mrchndise_Products;
import com.infosys.bmg.MyBMGDataValidations.db.configurations.DBQueries_Top_PP_Products;
import com.infosys.bmg.MyBMGDataValidations.utility.Constants;
import com.infosys.bmg.MyBMGDataValidations.utility.Log;
import com.infosys.bmg.MyBMGDataValidations.utility.ReadExcelData;

public class CommonMethods {

	private final static Logger LOGGER = LoggerFactory.getLogger(CommonMethods.class);
	ReadExcelData readExcel;
	DBConnectionForRoyaltyTrend dbConnectionForRoyaltyTrend;
	DBQueries_LatestStatement dbQueries_LatestStatement;
	UserSelectClient userSelectClient;
	DBQueries_StatementBalance dbQueries_StatementBalance;
	DBQueriesForTopTerritories dbQueriesForTopTerritories;
	DBQueries_Top_PP_Products dBQueries_Top_PP_Products;
	DBQueriesForTotalAlbumSingle dBQueriesForTotalAlbumSingle;
	DBQueries_TopTracks dbQueries_TopTracks;
	DBQueries_PipelineRoyalty_Recording dbQueries_PipelineRoyalty_Recording;
	DBQueries_NewStatementDue dbQueries_NewStatementDue;
	DBQueries_TopLicensingProducts dbQueries_TopLicensingProducts;
	DBQueries_PipelineRoyalty_Publishing dbQueries_PipelineRoyalty_Publishing;
	DBQueries_Top_Mrchndise_Products dbQueries_Top_Mrchndise_Products;
	DBQueries_TopPerformingTerritories dbQueries_TopPerformingTerritories;
	DBQueries_TopSyncProducts dbQueries_TopSyncProducts;

	public CommonMethods(DBConnectionForRoyaltyTrend dbConnectionForRoyaltyTrend,
			DBQueries_LatestStatement dbQueries_LatestStatement, UserSelectClient userSelectClient,
			DBQueries_StatementBalance dbQueries_StatementBalance,
			DBQueriesForTopTerritories dbQueriesForTopTerritories, DBQueries_Top_PP_Products dBQueries_Top_PP_Products,
			DBQueriesForTotalAlbumSingle dBQueriesForTotalAlbumSingle, DBQueries_TopTracks dbQueries_TopTracks,
			DBQueries_PipelineRoyalty_Recording dbQueries_PipelineRoyalty_Recording,
			DBQueries_NewStatementDue dbQueries_NewStatementDue,
			DBQueries_TopLicensingProducts dbQueries_TopLicensingProducts,
			DBQueries_PipelineRoyalty_Publishing dbQueries_PipelineRoyalty_Publishing,
			DBQueries_Top_Mrchndise_Products dbQueries_Top_Mrchndise_Products,
			DBQueries_TopPerformingTerritories dbQueries_TopPerformingTerritories,
			DBQueries_TopSyncProducts dbQueries_TopSyncProducts) {
		this.readExcel=new ReadExcelData();
		this.dbConnectionForRoyaltyTrend = dbConnectionForRoyaltyTrend;
		this.dbQueries_LatestStatement = dbQueries_LatestStatement;
		this.userSelectClient = userSelectClient;
		this.dbQueries_StatementBalance = dbQueries_StatementBalance;
		this.dbQueriesForTopTerritories = dbQueriesForTopTerritories;
		this.dBQueries_Top_PP_Products = dBQueries_Top_PP_Products;
		this.dBQueriesForTotalAlbumSingle = dBQueriesForTotalAlbumSingle;
		this.dbQueries_TopTracks = dbQueries_TopTracks;
		this.dbQueries_PipelineRoyalty_Recording = dbQueries_PipelineRoyalty_Recording;
		this.dbQueries_NewStatementDue = dbQueries_NewStatementDue;
		this.dbQueries_TopLicensingProducts = dbQueries_TopLicensingProducts;
		this.dbQueries_PipelineRoyalty_Publishing = dbQueries_PipelineRoyalty_Publishing;
		this.dbQueries_Top_Mrchndise_Products = dbQueries_Top_Mrchndise_Products;
		this.dbQueries_TopPerformingTerritories = dbQueries_TopPerformingTerritories;
		this.dbQueries_TopSyncProducts = dbQueries_TopSyncProducts;

	}

	public String statementCurrency = null;
	public String statementAmount = null;
	public String statementPeriod = null;
	public double closingBalanceDestinationDb = 0;
	public String closingPeriodDestiantionDb = null;
	public String currencyDestinationDb = null;
	public double closingBalanceSourceDb = 0;
	public String periodTopTrackUI = null;
	public String periodSourceDb = null;
	public List<List<String>> topTerrAmmtUI = new ArrayList<List<String>>();
	public String testCaseNo = null;
	public List<String> country = new ArrayList<>();
	public List<String> ammount = new ArrayList<>();

	public String topTerrPeriodUI = null;
	public String allPeriodsUI = null;
	public List<List<String>> territoriesUI = new ArrayList<List<String>>();
	public String grandTotalTopTerrUI = null;
	public String grandTotalTopTerrDest = null;
	public Map<String, String> territoriesDestDb = new HashMap<>();
	public Map<String, String> terrAmmtDetailsDestDb = new HashMap<>();
	public Map<String, String> terrAmmtDetailsSourceDb = new HashMap<>();
	public String territoryName;
	public List<List<String>> territoriesProductUI;
	public List<String> territoryListUI = new ArrayList<>();
	public List<List<String>> statementDataUI2 = new ArrayList<List<String>>();
	public ArrayList<List<String>> topProductsByTerrListSrcDb;
	public ArrayList<List<String>> topProductsByTerrListDestDb;
	public List<List<String>> topProductsByTerrList = new ArrayList<List<String>>();
	public Integer totalNumOfAlbumSingleUI;
	public Integer totalNumOfAlbumSingleDestDb;
	public Integer totalNumOfAlbumSingleSrcDb;
	public List<List<String>> totalAlbumSingleList = new ArrayList<List<String>>();
	public String royaltyTrendPhysUI;
	public String royaltyTrendDestDb;
	public String royaltyTrendSourceDb;
	public List<List<String>> royaltyTrendPhyList = new ArrayList<List<String>>();
	public String royaltyTrendDigitalUI;
	public String royaltyTrendDigitalDestDb;
	public String royaltyTrendDigitalSourceDb;
	public List<List<String>> royaltyTrendDigitalFinalList = new ArrayList<List<String>>();
	public String royaltyTrendTotalRoyaltyUI;
	public String royaltyTrendTotalRoyaltyDestDb;
	public String royaltyTrendTotalRoyaltySrcDb;
	public List<List<String>> royaltyTrendTotalRoyaltyFinalList = new ArrayList<List<String>>();
	public String royaltyTrendLicensingUI;
	public String royaltyTrendLicensingDestDb;
	public String royaltyTrendLicensingSourceDb;
	public List<List<String>> royaltyTrendLicensingFinalList = new ArrayList<List<String>>();
	public String royaltyTrendSyncUI;
	public String royaltyTrendSyncDestDb;
	public String royaltyTrendSyncSourceDb;
	public List<List<String>> royaltyTrendSyncFinalList = new ArrayList<List<String>>();
	public String royaltyTrendPublicPerformanceUI;
	public String royaltyTrendPublicPerformanceDestDb;
	public String royaltyTrendPublicPerformanceSourceDb;
	public List<List<String>> royaltyTrendPublicPerformanceFinalList = new ArrayList<List<String>>();
	public String royaltyTrendMerchandiseUI;
	public String royaltyTrendMerchandiseDestDb;
	public String royaltyTrendMerchandiseSourceDb;
	public List<List<String>> royaltyTrendMerchandiseFinalList = new ArrayList<List<String>>();
	public List<List<String>> royaltyTrendDonutGraphFinalList = new ArrayList<List<String>>();
	public String totalRoyaltyUI;
	public String royaltyTrendDonutGraphDestDb;
	public String royaltyTrendDonutGraphSourceDb;
	public ArrayList<String> listDonutGraphUI = new ArrayList<>();
	public List<List<String>> statementDataUI = new ArrayList<List<String>>();
	public List<List<String>> topTracksList = new ArrayList<List<String>>();

	public Map<String, String> mapTopTracksRoyaltyByDownload = new HashMap<>();
	public Map<String, String> mapTopTracksRoyaltyByStream = new HashMap<>();
	public Map<String, String> mapTopTracksUnitsByDownload = new HashMap<>();
	public Map<String, String> mapTopTracksUnitsByStream = new HashMap<>();
	public Map<String, String> maptopTrackProductTitleDest = new HashMap<>();
	public Map<String, String> mapTopTracksRoyaltyDownloadSource = new HashMap<>();
	public Map<String, String> mapTopTracksRoyaltyStreamSource = new HashMap<>();
	public Map<String, String> mapTopTracksUnitsStreamSource = new HashMap<>();
	public Map<String, String> maptopTrackProductTitleSource = new HashMap<>();
	public Map<String, String> mapTopTracksUnitDownloadSource = new HashMap<>();
	public List<String> topTracksProductIsrcCode = new ArrayList<String>();
	public List<String> topTracksProductIsrcCodeSource = new ArrayList<String>();

	public List<List<String>> statementDataUI3 = new ArrayList<List<String>>();
	public List<List<String>> newStatementDueFinalResult = new ArrayList<List<String>>();
	public List<List<String>> pipelineRoyltyRecordFinalResult = new ArrayList<List<String>>();
	public List<List<String>> pipelineRoyltyPublishFinalResult = new ArrayList<List<String>>();

	public String newStatementDueUI = null;
	public String newStatementDueDest = null;
	public String newStatementDueSource = null;

	public String pipelineRoyaltyRecordUI = null;
	public String pipelineRoyaltyRecordDest = null;
	public String pipelineRoyaltyRecordSource = null;
	public String periodPipelineRoyaltyRecordUI = null;
	public String pipelineRoyaltyCurrencyUI = null;
	public String pipelineRoyltyPeriodRecordDest = null;
	public String pipelineRoyltyPeriodRecordSource = null;

	public String pipelineRoyaltyPublishingUI = null;
	public String periodPipelineRoyaltyPublishingUI = null;
	public String pipelineRoyaltyCurrencyPublishingUI = null;

	public String pipelineRoyaltyPublishingDest1 = null;
	public String pipelineRoyaltyPublishingSource1 = null;
	public String pipelineRoyltyPeriodPublishingDest1 = null;
	public String pipelineRoyltyPeriodPublishingSource1 = null;
	public String pipelineRoyaltyPublishingSource2 = null;
	public String pipelineRoyltyPeriodPublishingSource2 = null;
	public String pipelineRoyltyPeriodPublishingDest2 = null;
	public String pipelineRoyaltyPublishingDest2 = null;
	public String pipelineRoyltyPeriodPublishingDest3 = null;
	public String pipelineRoyaltyPublishingDest3 = null;

	// topLicensing Products

	public Map<String, String> mapTopLicensingRoyaltyDest = new HashMap<>();
	public Map<String, String> mapTopLicensingConfigDest = new HashMap<>();
	public Map<String, String> mapTopLicensingProductTitleDest = new HashMap<>();
	public Map<String, String> mapTopLicensingPeriodDest = new HashMap<>();
	public List<String> topLicensingProductCodeDest = new ArrayList<String>();
	public Map<String, String> mapTopLicensingRoyaltySource = new HashMap<>();
	public Map<String, String> mapTopLicensingProductTitleSource = new HashMap<>();
	public Map<String, String> mapTopLicensingConfigSource = new HashMap<>();
	public Map<String, String> mapTopLicensingPeriodSource = new HashMap<>();
	public List<String> topLicensingProductCodeSource = new ArrayList<String>();
	public String periodTopLicensingUI = null;
	public List<List<String>> topLicensingProductFinalList = new ArrayList<List<String>>();

	// topSync Products
	public Map<String, String> mapTopSyncRoyaltyDest = new HashMap<>();
	public Map<String, String> mapTopSyncConfigDest = new HashMap<>();
	public Map<String, String> mapTopSyncProductTitleDest = new HashMap<>();
	public Map<String, String> mapTopSyncPeriodDest = new HashMap<>();
	public List<String> topSyncProductCodeDest = new ArrayList<String>();
	public Map<String, String> mapTopSyncRoyaltySource = new HashMap<>();
	public Map<String, String> mapTopSyncProductTitleSource = new HashMap<>();
	public Map<String, String> mapTopSyncConfigSource = new HashMap<>();
	public Map<String, String> mapTopSyncPeriodSource = new HashMap<>();
	public List<String> topSyncProductCodeSource = new ArrayList<String>();
	public String periodTopSyncUI = null;
	public List<List<String>> topSyncProductFinalList = new ArrayList<List<String>>();
	// top Public Performance Products
	public Map<String, String> mapTopPPRoyaltyDest = new HashMap<>();
	public Map<String, String> mapTopPPConfigDest = new HashMap<>();
	public Map<String, String> mapTopPPProductTitleDest = new HashMap<>();
	public Map<String, String> mapTopPPPeriodDest = new HashMap<>();
	public List<String> topPPProductCodeDest = new ArrayList<String>();
	public Map<String, String> mapTopPPRoyaltySource = new HashMap<>();
	public Map<String, String> mapTopPPProductTitleSource = new HashMap<>();
	public Map<String, String> mapTopPPConfigSource = new HashMap<>();
	public Map<String, String> mapTopPPPeriodSource = new HashMap<>();
	public List<String> topPPProductCodeSource = new ArrayList<String>();
	public String periodTopPPUI = null;
	public List<List<String>> topPPProductFinalList = new ArrayList<List<String>>();
//top Merchandise Products
	public Map<String, String> mapTopMrchndiseRoyaltyDest = new HashMap<>();
	public Map<String, String> mapTopMrchndiseConfigDest = new HashMap<>();
	public Map<String, String> mapTopMrchndiseProductTitleDest = new HashMap<>();
	public Map<String, String> mapTopMrchndisePeriodDest = new HashMap<>();
	public List<String> topMrchndiseProductCodeDest = new ArrayList<String>();
	public Map<String, String> mapTopMrchndiseRoyaltySource = new HashMap<>();
	public Map<String, String> mapTopMrchndiseProductTitleSource = new HashMap<>();
	public Map<String, String> mapTopMrchndiseConfigSource = new HashMap<>();
	public Map<String, String> mapTopMrchndisePeriodSource = new HashMap<>();
	public List<String> topMrchndiseProductCodeSource = new ArrayList<String>();
	public String periodTopMrchndiseUI = null;
	public List<List<String>> topMrchndiseProductFinalList = new ArrayList<List<String>>();

//top Perform Terr 
	public String periodTopPerformTerrUI = null;
	public Map<String, String> mapTopPerformTerrRoyaltyDest = new HashMap<>();
	public List<String> mapTopPerformTerrNameDest = new ArrayList<String>();
	public Map<String, String> mapTopPerformTerrPeriodDest = new HashMap<>();
	public Map<String, String> mapTopPerformTerrRoyaltySource = new HashMap<>();
	public List<String> mapTopPerformTerrNameSource = new ArrayList<String>();
	public Map<String, String> mapTopPerformTerrPeriodSource = new HashMap<>();
	public List<List<String>> topPerfromTerrFinalList = new ArrayList<List<String>>();

//Latest Stamenent Dashboard
	public Map<String, String> mapLatestStatementRoyaltyDest = new HashMap<String, String>();
	public Map<String, String> mapLatestStatementRoyaltorNameDest = new HashMap<String, String>();
	public Map<String, String> mapLatestStatementPeriodDest = new HashMap<String, String>();
	public List<String> latestStatmntRoyaltorCodeDest = new ArrayList<String>();
	public List<String> latestStatmntRoyaltorCodeSource = new ArrayList<String>();
	public Map<String, String> mapLatestStatementPeriodSource = new HashMap<String, String>();
	public Map<String, String> mapLatestStatementRoyaltySource = new HashMap<String, String>();
	public Map<String, String> mapLatestStatementRoyaltorNameSource = new HashMap<String, String>();
	public String periodLatestStatementUI = null;
	public List<List<String>> latestStatementFinalList = new ArrayList<List<String>>();
	public ArrayList<String> topLicensingChildList = new ArrayList<>();

	public List<List<String>> topSyncChildList = new ArrayList<>();

	public void user_successfully_selects_client(String client, String currency, String id, boolean selectAllClients,
			String royaltorName) throws Exception {
		LOGGER.info("Client---->" + client + " Cross Client ---->" + id);
		userSelectClient.user_select_client(client, currency, id, selectAllClients, royaltorName);
	}

	public void fetchDataFromDB(String client, ArrayList<String> scrossClientList, String site, String crossClientId)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		try {
			closingBalanceDestinationDb = 0;
			closingPeriodDestiantionDb = null;
			periodSourceDb = null;

			dbQueries_StatementBalance.DB_Query(site, client, scrossClientList, crossClientId, "Statement Balance",
					null);
			closingBalanceDestinationDb = dbQueries_StatementBalance.closingBlanaceDestinationDb;
			closingPeriodDestiantionDb = dbQueries_StatementBalance.periodDestinationDb;
			currencyDestinationDb = dbQueries_StatementBalance.currencyDestinationDb;
			closingBalanceSourceDb = dbQueries_StatementBalance.closingBalanaceSoruceDb;
			periodSourceDb = dbQueries_StatementBalance.periodSourceDb;
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetch Data from DB: " + e);
		}

	}

	public List<String> fetchPeriodFromUI() {
		return userSelectClient.fetchPeriodFromUI();

	}

	// @Then("^read UI data$")
	public void fetchTopTerrAndPeriodFromUI(String client, String clientName, ArrayList<String> scrossClientList,
			ArrayList<String> crossClientNamesList, String crossClientName, String crossClientId, String site,
			String period) throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		allPeriodsUI = null;
		topTerrPeriodUI = null;
		territoriesUI = null;
		grandTotalTopTerrUI = null;
		terrAmmtDetailsDestDb = null;
		terrAmmtDetailsSourceDb = null;

		userSelectClient.fetchTopTerrFromUI(period);
		topTerrPeriodUI = userSelectClient.periodUI;
		territoriesUI = userSelectClient.territoriesUI;
		grandTotalTopTerrUI = userSelectClient.grandTotalForTopTerrUI;
		fetchDataForTerritoriesFromDb(client, scrossClientList, crossClientId, site, period, null);
		topTerrPeriodUI = period;
		for (List<String> t : territoriesUI) {
			ArrayList<String> statementDetailsUI = new ArrayList<String>();
			statementDetailsUI.add(site);
			if (crossClientId != null) {
				statementDetailsUI.add(client + "," + crossClientId.toString().replace("'", ""));
				statementDetailsUI.add(clientName + "," + crossClientName.toString().replace("'", ""));
			} else if (!scrossClientList.isEmpty()) {
				statementDetailsUI.add(client + "," + scrossClientList.toString().replace("['", "").replace("']", ""));
				statementDetailsUI
						.add(clientName + "," + crossClientNamesList.toString().replace("['", "").replace("']", ""));
			} else {
				statementDetailsUI.add(client);
				statementDetailsUI.add(clientName);
			}

			statementDetailsUI.add(topTerrPeriodUI);
			statementDetailsUI.add(t.get(0));
			String arr[] = null;
			arr = t.get(1).split(" ");
			statementDetailsUI.add(arr[0]);
			statementDetailsUI.add(arr[1]);
			terrAmmtDetailsDestDb = dbQueriesForTopTerritories.terrAmmtDetailsDestDb;
			for (Map.Entry<String, String> map : terrAmmtDetailsDestDb.entrySet()) {
				if (map.getKey().matches(t.get(0))) {
					statementDetailsUI.add(map.getValue());
					break;
				}
			}
			terrAmmtDetailsSourceDb = dbQueriesForTopTerritories.terrAmmtDetailsSourceDb;
			for (Map.Entry<String, String> map : terrAmmtDetailsSourceDb.entrySet()) {
				if (map.getKey().matches(t.get(0))) {
					statementDetailsUI.add(map.getValue());
					break;

				}

			}

			String total[] = null;
			total = grandTotalTopTerrUI.split(" ");
			statementDetailsUI.add(total[1]);
			// statementDetailsUI.add(grandTotalTopTerrUI);
			statementDetailsUI.add(dbQueriesForTopTerritories.grandtotalDestDb);
			statementDetailsUI.add(dbQueriesForTopTerritories.grandtotalSourceDb);
			statementDataUI2.add(statementDetailsUI);

		}
		LOGGER.info(statementDataUI2 + ":statementDataUI2");
	}

	// @Then("^fetch Data from Db$")
	public void fetchDataForTerritoriesFromDb(String client, ArrayList<String> scrossClientList, String crossClientId,
			String site, String period, List<String> allPeriods)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		terrAmmtDetailsDestDb = null;
		terrAmmtDetailsSourceDb = null;
		// grandTotalTopTerrUI=null;
		dbQueriesForTopTerritories.DB_Query_Terr(site, client, period, scrossClientList, crossClientId, allPeriods);

	}

	public void fetchTopTerrAndPeriodFromUIandDb(String client, ArrayList<String> scrossClientList,
			String crossClientId, String site, List<String> allPeriods) {
		allPeriodsUI = null;
		topTerrPeriodUI = null;
		territoriesUI = null;
		grandTotalTopTerrUI = null;
		try {
			userSelectClient.fetchTopTerrFromUIAllPeriods();
			territoriesUI = userSelectClient.territoriesUI;
			grandTotalTopTerrUI = userSelectClient.grandTotalForTopTerrUI;
			fetchDataForTerritoriesFromDb(client, scrossClientList, crossClientId, site, null, allPeriods);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void fetchTopTerrProductsAndPeriodFromUI(String client, ArrayList<String> scrossClientList,
			String crossClientId, String site, String period, List<String> allPeriods, boolean allTerr, String terrName,
			List<String> terrList) {
		territoriesProductUI = new ArrayList<List<String>>();
		topProductsByTerrListSrcDb = null;
		topProductsByTerrListDestDb = null;

		if (allPeriods != null) {
			userSelectClient.fetchTopTerrProductsFromUI(null, allPeriods, allTerr, terrName, terrList);

		} else {
			userSelectClient.fetchTopTerrProductsFromUI(period, null, allTerr, terrName, terrList);
		}

		territoryName = userSelectClient.territoryName;
		// territoryListUI=userSelectClient.terrList;
		territoriesProductUI = userSelectClient.territoriesProduct;
		fetchDataForTerritoriesProductsFromDb(client, scrossClientList, crossClientId, site, period, terrName,
				allPeriods, terrList, allTerr);

		for (List<String> terr : territoriesProductUI) {
			ArrayList<String> statementDetailsUI = new ArrayList<String>();
			statementDetailsUI.add(client);
			statementDetailsUI.add(site);
			statementDetailsUI.add(period);
			if (terrName == null) {
				terrName = territoryName;
			}
			statementDetailsUI.add(terrName);
			statementDetailsUI.addAll(terr);
			/*
			 * statementDetailsUI.add(terr.get(1)); statementDetailsUI.add(terr.get(2));
			 * statementDetailsUI.add(terr.get(3));
			 */

			// topProductsByTerrListDestDb=dbQueriesForTopTerritories.topProductsByTerrListDestDb;

			for (List<String> destdb : topProductsByTerrListDestDb) {

				if (terr.get(0).equalsIgnoreCase(destdb.get(0)) && terr.get(1).equalsIgnoreCase(destdb.get(1))) {
					// String percentage = new
					// Double(destdb.get(2)).toString().substring(0,destdb.get(2).indexOf('.'));
					double per = Double.valueOf(destdb.get(2));
					double roundOff = Math.round(per * 100.0) / 100.0;

					// String ammt = new
					// Double(destdb.get(3)).toString().substring(0,destdb.get(3).indexOf('.'));
					// double amt= Double.valueOf(destdb.get(3));
					// double result1 = Math.round(amt*1000.0)/1000.0;

					String terrPerUI = terr.get(2).substring(0, terr.get(2).indexOf("%"));
					// String terrAmtUI=terr.get(3).substring(1,terr.get(3).length()-1);
					double uiper = Double.parseDouble(terrPerUI);
					// double uiAmt=Double.parseDouble(terrAmtUI);
					if (uiper == roundOff) {
						statementDetailsUI.addAll(destdb);
						break;
					} else {
						statementDetailsUI.addAll(destdb);
					}

				}
			}

			// topProductsByTerrListSrcDb=dbQueriesForTopTerritories.topProductsByTerrListSrcDb;
			for (List<String> sourceDb : topProductsByTerrListSrcDb) {
				if ((terr.get(0).equalsIgnoreCase(sourceDb.get(0)))
						&& (terr.get(1).equalsIgnoreCase(sourceDb.get(1)))) {

					double per = Double.valueOf(sourceDb.get(2));
					double roundOff = Math.round(per * 100.0) / 100.0;
					String terrPerUI = terr.get(2).substring(0, terr.get(2).indexOf("%"));
					double uiper = Double.parseDouble(terrPerUI);
					if (uiper == roundOff) {
						statementDetailsUI.addAll(sourceDb);
						break;
					} else {
						statementDetailsUI.addAll(sourceDb);
					}

				}
			}

			topProductsByTerrList.add(statementDetailsUI);
			LOGGER.info(topProductsByTerrList + ":topProductsByTerrList");
		}

	}

	// @Then("^fetch Data from Db$")
	public void fetchDataForTerritoriesProductsFromDb(String client, ArrayList<String> scrossClientList,
			String crossClientId, String site, String period, String territoryName, List<String> allPeriods,
			List<String> terrList, Boolean allTerr) {
		try {
			// dbQueriesForTopTerritories.DB_Query_TopTerrProducts(site, client,
			// period,scrossClientList,
			// crossClientId,territoryName,allPeriods,terrList,allTerr);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public List<String> fetchTerrListByPeriodFromUI(String period) {

		return userSelectClient.fetchTerrListByPeriodFromUI(period);
	}

	public void fetchTotalAlbumSingleFromUI(String client, String clientName, ArrayList<String> scrossClientList,
			ArrayList<String> crossClientNamesList, String crossClientName, String crossClientId, String site,
			String period, List<String> allPeriods) {
		totalNumOfAlbumSingleUI = null;
		totalNumOfAlbumSingleDestDb = null;
		totalNumOfAlbumSingleSrcDb = null;
		boolean allPeriodflag = false;
		if (allPeriods != null) {
			allPeriodflag = true;
		}
		userSelectClient.fetchTotalAlbumSingleFromUI(period, allPeriodflag);
		totalNumOfAlbumSingleUI = userSelectClient.totalNumOfAlbumSingle;

		dBQueriesForTotalAlbumSingle.DB_Query_TopAlbumSingle(client, scrossClientList, crossClientId, site, period,
				allPeriods);
		totalNumOfAlbumSingleDestDb = dBQueriesForTotalAlbumSingle.totalNumOfAlbumSingleDestDb;
		totalNumOfAlbumSingleSrcDb = dBQueriesForTotalAlbumSingle.totalNumOfAlbumSingleSourceDb;

		ArrayList<String> totalAlbumSingleCount = new ArrayList<String>();
		totalAlbumSingleCount.add(site);

		if (crossClientId != null) {
			totalAlbumSingleCount.add(client + "," + crossClientId.toString().replace("'", ""));
			totalAlbumSingleCount.add(clientName + "," + crossClientName.toString().replace("'", ""));
		} else if (!scrossClientList.isEmpty()) {
			totalAlbumSingleCount.add(
					client + "," + scrossClientList.toString().replace("['", "").replace("']", "").replace("'", ""));
			totalAlbumSingleCount.add(clientName + ","
					+ crossClientNamesList.toString().replace("['", "").replace("']", "").replace("'", ""));
		} else {
			totalAlbumSingleCount.add(client);
			totalAlbumSingleCount.add(clientName);
		}

		totalAlbumSingleCount.add(period);
		totalAlbumSingleCount.add(totalNumOfAlbumSingleUI.toString());
		totalAlbumSingleCount.add(totalNumOfAlbumSingleDestDb.toString());
		totalAlbumSingleCount.add(totalNumOfAlbumSingleSrcDb.toString());
		totalAlbumSingleList.add(totalAlbumSingleCount);
	}

	public void clickAlbumSingleTab() {
		userSelectClient.clickAlbumSingleTab();

	}

	public List<String> fetchYearsFromUI() {

		return userSelectClient.fetchYearsFromUI();
	}

	public void fetchRoyaltyTrendPhys(String client, String clientName, ArrayList<String> scrossClientList,
			ArrayList<String> crossClientNamesList, String crossClientName, String crossClientId, String site,
			String year, List<String> allYears) throws SQLException {
		royaltyTrendPhysUI = null;
		royaltyTrendDestDb = null;
		royaltyTrendSourceDb = null;

		if (allYears != null) {
			userSelectClient.fetchRoyaltyTrendPhysAllPeriodsFromUI();
		} else {
			userSelectClient.fetchRoyaltyTrendPhysFromUI(year);
		}

		royaltyTrendPhysUI = userSelectClient.RoyaltyTrendPhysUI;
		dbConnectionForRoyaltyTrend.DB_Query_RoyaltyTrendPhysical(client, scrossClientList, crossClientId, site, year);
		royaltyTrendDestDb = dbConnectionForRoyaltyTrend.royaltyTrendPhyDestDb;
		royaltyTrendSourceDb = dbConnectionForRoyaltyTrend.royaltyTrendPhySourceDb;

		ArrayList<String> royaltyTrendPhysList = new ArrayList<String>();
		royaltyTrendPhysList.add(site);
		if (crossClientId != null) {
			royaltyTrendPhysList.add(client + "," + crossClientId.toString().replace("'", ""));
			royaltyTrendPhysList.add(clientName + "," + crossClientName.toString().replace("'", ""));
		} else if (!scrossClientList.isEmpty()) {
			royaltyTrendPhysList.add(
					client + "," + scrossClientList.toString().replace("['", "").replace("']", "").replace("'", ""));
			royaltyTrendPhysList.add(clientName + ","
					+ crossClientNamesList.toString().replace("['", "").replace("']", "").replace("'", ""));
		} else {
			royaltyTrendPhysList.add(client);
			royaltyTrendPhysList.add(clientName);
		}
		royaltyTrendPhysList.add(year);
		String currency[] = null;
		currency = royaltyTrendPhysUI.split(" ");
		royaltyTrendPhysList.add(currency[0]);
		royaltyTrendPhysList.add(currency[1]);
		// royaltyTrendPhysList.add(royaltyTrendPhysUI);
		royaltyTrendPhysList.add(royaltyTrendDestDb);
		royaltyTrendPhysList.add(royaltyTrendSourceDb);
		royaltyTrendPhyList.add(royaltyTrendPhysList);
	}

	public void fetchRoyaltyTrendDigital(String client, String clientName, ArrayList<String> scrossClientList,
			ArrayList<String> crossClientNamesList, String crossClientName, String crossClientId, String site,
			String year, List<String> allYears) {
		try {
			royaltyTrendDigitalUI = null;
			royaltyTrendDigitalDestDb = null;
			royaltyTrendDigitalSourceDb = null;
			userSelectClient.fetchRoyaltyTrendDigitalFromUI(year);
			royaltyTrendDigitalUI = userSelectClient.royaltyTrendDigitalUI;
			dbConnectionForRoyaltyTrend.DB_Query_RoyaltyTrendDigital(client, scrossClientList, crossClientId, site,
					year);
			royaltyTrendDigitalDestDb = dbConnectionForRoyaltyTrend.royaltyTrendDigitalDestDb;
			royaltyTrendDigitalSourceDb = dbConnectionForRoyaltyTrend.royaltyTrendDigitalSourceDb;

			ArrayList<String> royaltyTrendDigitalList = new ArrayList<String>();
			royaltyTrendDigitalList.add(site);
			if (crossClientId != null) {
				royaltyTrendDigitalList.add(client + "," + crossClientId.toString().replace("'", ""));
				royaltyTrendDigitalList.add(clientName + "," + crossClientName.toString().replace("'", ""));
			} else if (!scrossClientList.isEmpty()) {
				royaltyTrendDigitalList.add(client + ","
						+ scrossClientList.toString().replace("['", "").replace("']", "").replace("'", ""));
				royaltyTrendDigitalList.add(clientName + ","
						+ crossClientNamesList.toString().replace("['", "").replace("']", "").replace("'", ""));
			} else {
				royaltyTrendDigitalList.add(client);
				royaltyTrendDigitalList.add(clientName);
			}
			royaltyTrendDigitalList.add(year);
			String currency[] = null;
			currency = royaltyTrendDigitalUI.split(" ");
			royaltyTrendDigitalList.add(currency[0]);
			royaltyTrendDigitalList.add(currency[1]);
			// royaltyTrendDigitalList.add(royaltyTrendDigitalUI);
			royaltyTrendDigitalList.add(royaltyTrendDigitalDestDb);
			royaltyTrendDigitalList.add(royaltyTrendDigitalSourceDb);
			royaltyTrendDigitalFinalList.add(royaltyTrendDigitalList);
		} catch (Exception ex) {
			ex.printStackTrace();

		}
	}

	public void fetchRoyaltyTrendTotalRoyalty(String client, String clientName, ArrayList<String> scrossClientList,
			ArrayList<String> crossClientNamesList, String crossClientName, String crossClientId, String site,
			String year, List<String> allYears) {
		try {
			royaltyTrendTotalRoyaltyUI = null;
			royaltyTrendTotalRoyaltyDestDb = null;
			royaltyTrendTotalRoyaltySrcDb = null;

			if (allYears != null) {
				userSelectClient.fetchRoyaltyTrendTotalRoyAllPeriodsFromUI();
			} else {
				userSelectClient.fetchRoyaltyTrendTotalRoyByYearFromUI(year);
			}
			royaltyTrendTotalRoyaltyUI = userSelectClient.royaltyTrendTotalRoyUI;
			dbConnectionForRoyaltyTrend.DB_Query_RoyaltyTrendTotalRoyalty(client, scrossClientList, crossClientId, site,
					year, allYears);
			royaltyTrendTotalRoyaltyDestDb = dbConnectionForRoyaltyTrend.royaltyTrendTotalRoyaltyDestDb;
			royaltyTrendTotalRoyaltySrcDb = dbConnectionForRoyaltyTrend.royaltyTrendTotalRoyaltySourceDb;

			ArrayList<String> royaltyTrendRoyaltyTotalList = new ArrayList<String>();
			royaltyTrendRoyaltyTotalList.add(site);
			if (crossClientId != null) {
				royaltyTrendRoyaltyTotalList.add(client + "," + crossClientId.toString().replace("'", ""));
				royaltyTrendRoyaltyTotalList.add(clientName + "," + crossClientName.toString().replace("'", ""));
			} else if (!scrossClientList.isEmpty()) {
				royaltyTrendRoyaltyTotalList.add(client + ","
						+ scrossClientList.toString().replace("['", "").replace("']", "").replace("'", ""));
				royaltyTrendRoyaltyTotalList.add(clientName + ","
						+ crossClientNamesList.toString().replace("['", "").replace("']", "").replace("'", ""));
			} else {
				royaltyTrendRoyaltyTotalList.add(client);
				royaltyTrendRoyaltyTotalList.add(clientName);
			}
			if (allYears != null) {
				royaltyTrendRoyaltyTotalList.add("All Periods");
			} else {
				royaltyTrendRoyaltyTotalList.add(year);
			}

			String currency[] = null;
			currency = royaltyTrendTotalRoyaltyUI.split("\n");
			royaltyTrendRoyaltyTotalList.add(currency[0]);
			royaltyTrendRoyaltyTotalList.add(currency[1]);
			// royaltyTrendRoyaltyTotalList.add(royaltyTrendTotalRoyaltyUI);
			royaltyTrendRoyaltyTotalList.add(royaltyTrendTotalRoyaltyDestDb);
			royaltyTrendRoyaltyTotalList.add(royaltyTrendTotalRoyaltySrcDb);
			royaltyTrendTotalRoyaltyFinalList.add(royaltyTrendRoyaltyTotalList);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// @Given("^fetch Top Tracsks from UI$")
	public void fetchTopTracksFromUIandDb(String client, String clientName, ArrayList<String> scrossClientList,
			String crossClientId, String site)
			throws InterruptedException, ClassNotFoundException, SQLException, IOException {
		try {
			periodTopTrackUI = null;
			userSelectClient.getTopTracksUI();
			periodTopTrackUI = userSelectClient.periodTopTracksUI;
			List<List<String>> topTracksList = new ArrayList<List<String>>();
			topTracksList = userSelectClient.topTracksList;
			fetchDataFromDBForTopTracks(client, scrossClientList, site, crossClientId, periodTopTrackUI);
			for (List<String> t : topTracksList) {
				ArrayList<String> statementDetailsUI = new ArrayList<String>();

				statementDetailsUI.add(site);

				if (crossClientId != null) {
					statementDetailsUI.add(client + "," + crossClientId);
					String crossClient[] = crossClientId.split(",");
					statementDetailsUI.add(client + "," + crossClient[0].toString().replace("'", ""));
					statementDetailsUI.add(clientName + "," + crossClient[1].toString().replace("'", ""));
				} else if (!scrossClientList.isEmpty()) {
					statementDetailsUI
							.add(client + "," + scrossClientList.toString().replace("[", "").replace("]", ""));
					statementDetailsUI
							.add(client + "," + scrossClientList.get(0).toString().replace("['", "").replace("']", ""));
					statementDetailsUI.add(
							clientName + "," + scrossClientList.get(1).toString().replace("['", "").replace("']", ""));
				} else {
					statementDetailsUI.add(client);
					statementDetailsUI.add(clientName);
				}

				String[] arr = null;
				arr = t.get(1).split(":");
				String productUserCodeUI = arr[1];
				statementDetailsUI.add(periodTopTrackUI);
				statementDetailsUI.add(t.get(0));
				statementDetailsUI.add(productUserCodeUI);
				String arr4[] = null;
				arr4 = t.get(2).split(" ");
				statementDetailsUI.add(arr4[0]);
				statementDetailsUI.add(arr4[1]);

				String arr7[] = null;
				arr7 = t.get(3).split(" ");
				statementDetailsUI.add(arr7[0]);
				String arr5[] = null;
				arr5 = t.get(4).split(" ");
				statementDetailsUI.add(arr5[1]);
				String arr6[] = null;
				arr6 = t.get(5).split(" ");
				statementDetailsUI.add(arr6[0]);

				for (Map.Entry<String, String> map : maptopTrackProductTitleDest.entrySet()) {
					if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
						statementDetailsUI.add(map.getValue());
						break;
					}
				}

				for (String isrc : topTracksProductIsrcCode) {
					if (isrc.trim().equalsIgnoreCase(productUserCodeUI.trim())) {
						statementDetailsUI.add(isrc);
						break;
					}
				}

				for (Map.Entry<String, String> map : mapTopTracksRoyaltyByDownload.entrySet()) {
					if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
						statementDetailsUI.add(map.getValue());
						break;
					}
				}

				for (Map.Entry<String, String> map : mapTopTracksUnitsByDownload.entrySet()) {
					if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
						statementDetailsUI.add(map.getValue());
						break;
					}
				}

				for (Map.Entry<String, String> map : mapTopTracksRoyaltyByStream.entrySet()) {
					if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
						statementDetailsUI.add(map.getValue());
						break;
					}
				}

				for (Map.Entry<String, String> map : mapTopTracksUnitsByStream.entrySet()) {
					if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
						statementDetailsUI.add(map.getValue());
						break;
					}
				}

				for (Map.Entry<String, String> map : maptopTrackProductTitleSource.entrySet()) {
					if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
						statementDetailsUI.add(map.getValue());
						break;
					}
				}
				for (String isrc : topTracksProductIsrcCodeSource) {
					if (isrc.trim().equalsIgnoreCase(productUserCodeUI.trim())) {
						statementDetailsUI.add(isrc);
						break;
					}
				}

				for (Map.Entry<String, String> map : mapTopTracksRoyaltyDownloadSource.entrySet()) {
					if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
						statementDetailsUI.add(map.getValue());
						break;
					}
				}

				for (Map.Entry<String, String> map : mapTopTracksUnitDownloadSource.entrySet()) {
					if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
						statementDetailsUI.add(map.getValue());
						break;
					}
				}

				for (Map.Entry<String, String> map : mapTopTracksRoyaltyStreamSource.entrySet()) {
					if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
						statementDetailsUI.add(map.getValue());
						break;
					}
				}

				for (Map.Entry<String, String> map : mapTopTracksUnitsStreamSource.entrySet()) {
					if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
						statementDetailsUI.add(map.getValue());
						break;
					}
				}

				Map<String, String> statusMap = new HashMap<String, String>();
				statusMap = getStatusMapTracks(statementDetailsUI);

				for (Map.Entry<String, String> map3 : statusMap.entrySet()) {
					if (map3.getKey().matches(Constants.statusPass)) {
						statementDetailsUI.add(Constants.statusPass);
						statementDetailsUI.add("");
					} else if (map3.getKey().matches(Constants.statusFail)) {
						statementDetailsUI.add(Constants.statusFail);
						statementDetailsUI.add(map3.getValue());
					}
				}
				statementDataUI3.add(statementDetailsUI);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchTopTracks :" + e);
		}

	}

	private Map<String, String> getStatusMapTracks(ArrayList<String> statementDetailsUI) {
		Map<String, String> statusMap = new HashMap<String, String>();
		if (statementDetailsUI.get(3).trim().equalsIgnoreCase(statementDetailsUI.get(10).trim())) {
			if (statementDetailsUI.get(10).trim().equalsIgnoreCase(statementDetailsUI.get(16).trim())) {
				if (statementDetailsUI.get(4).trim().equalsIgnoreCase(statementDetailsUI.get(11).trim())) {
					if (statementDetailsUI.get(11).trim().equalsIgnoreCase(statementDetailsUI.get(17).trim())) {
						statusMap = getTestCaseStatus(statementDetailsUI.get(6).trim(),
								statementDetailsUI.get(12).trim(), statementDetailsUI.get(18).trim());
						for (Map.Entry<String, String> map : statusMap.entrySet()) {
							if (map.getKey().matches(Constants.statusPass)) {
								statusMap = getTestCaseStatus(statementDetailsUI.get(7).trim(),
										statementDetailsUI.get(13).trim(), statementDetailsUI.get(19).trim());
								for (Map.Entry<String, String> map1 : statusMap.entrySet()) {
									if (map1.getKey().matches(Constants.statusPass)) {
										statusMap = getTestCaseStatus(statementDetailsUI.get(8).trim(),
												statementDetailsUI.get(14).trim(), statementDetailsUI.get(20).trim());
										for (Map.Entry<String, String> map2 : statusMap.entrySet()) {
											if (map2.getKey().matches(Constants.statusPass)) {
												statusMap = getTestCaseStatus(statementDetailsUI.get(9).trim(),
														statementDetailsUI.get(15).trim(),
														statementDetailsUI.get(21).trim());
												for (Map.Entry<String, String> map3 : statusMap.entrySet()) {
													if (map3.getKey().matches(Constants.statusPass)) {
														statusMap.put(Constants.statusPass, "");
													} else if (map.getKey().matches(Constants.statusFail)) {
														if (map.getValue().equalsIgnoreCase(Constants.errMessage1)) {
															statusMap.put(Constants.statusFail,
																	"Track Units by income type Stream UI and Stream Destination donot match");
														} else if (map.getValue().equalsIgnoreCase(
																"source and destiantion value is null but UI value is not null")) {
															statusMap.put(Constants.statusFail,
																	"Track Units by income type Stream Source  and Stream Destination is null but Royalty by Download UI is not null");
														} else if (map.getValue()
																.equalsIgnoreCase(Constants.errMessage3)) {
															statusMap.put(Constants.statusFail,
																	"Track Units by income type Stream Source  and Stream Destination donot match");
														} else if (map.getValue()
																.equalsIgnoreCase("UI Field value is null")) {
															statusMap.put(Constants.statusFail,
																	"Track Units by income type Stream UI is null");
														}
														break;

													}
												}

											} else if (map.getKey().matches(Constants.statusFail)) {
												if (map.getValue().equalsIgnoreCase(Constants.errMessage1)) {
													statusMap.put(Constants.statusFail,
															"Track Royalties by income type Stream UI and Stream Destination donot match");
												} else if (map.getValue().equalsIgnoreCase(
														"source and destiantion value is null but UI value is not null")) {
													statusMap.put(Constants.statusFail,
															"Track Royalties by income type Stream Source  and Stream Destination is null but Royalty by Download UI is not null");
												} else if (map.getValue().equalsIgnoreCase(Constants.errMessage3)) {
													statusMap.put(Constants.statusFail,
															"Track Royalties by income type Stream Source  and Stream Destination donot match");
												} else if (map.getValue().equalsIgnoreCase("UI Field value is null")) {
													statusMap.put(Constants.statusFail,
															"Track Royalties by income type Stream UI is null");
												}
												break;

											}
										}

									} else if (map.getKey().matches(Constants.statusFail)) {
										if (map.getValue().equalsIgnoreCase(Constants.errMessage1)) {
											statusMap.put(Constants.statusFail,
													"Track Units by income type download UI and Download Destination donot match");
										} else if (map.getValue().equalsIgnoreCase(
												"source and destiantion value is null but UI value is not null")) {
											statusMap.put(Constants.statusFail,
													"Track Units by income type download Source  and Download Destination is null but Royalty by Download UI is not null");
										} else if (map.getValue().equalsIgnoreCase(Constants.errMessage3)) {
											statusMap.put(Constants.statusFail,
													"Track Units by income type download Source  and Download Destination donot match");
										} else if (map.getValue().equalsIgnoreCase("UI Field value is null")) {
											statusMap.put(Constants.statusFail,
													"Track Units by income type download UI is null");
										}
										break;

									}
								}

							} else if (map.getKey().matches(Constants.statusFail)) {
								if (map.getValue().equalsIgnoreCase(Constants.errMessage1)) {
									statusMap.put(Constants.statusFail,
											"Track Royalties by income type download UI and Download Destination donot match");
								} else if (map.getValue().equalsIgnoreCase(
										"source and destiantion value is null but UI value is not null")) {
									statusMap.put(Constants.statusFail,
											"Track Royalties by income type download Source  and Download Destination is null but Royalty by Download UI is not null");
								} else if (map.getValue().equalsIgnoreCase(Constants.errMessage3)) {
									statusMap.put(Constants.statusFail,
											"Track Royalties by income type download Source  and Download Destination donot match");
								} else if (map.getValue().equalsIgnoreCase("UI Field value is null")) {
									statusMap.put(Constants.statusFail,
											"Track Royalties by income type download UI is null");
								}
								break;

							}

						}
					} else {
						statusMap.put(Constants.statusFail,
								"Track Isrc code  from Destination   and Source donot match");
					}

				} else {
					statusMap.put(Constants.statusFail, "Track Isrc code  from UI  and Destiantion donot match");
				}

			} else {
				statusMap.put(Constants.statusFail, "Track title from Destination  and Source donot match");
			}

		} else {
			statusMap.put(Constants.statusFail, "Track title from UI and destiantion donot match");
		}

		return statusMap;
	}

	// @Then("^fetch Data from Db$")
	public void fetchDataFromDBForTopTracks(String client, ArrayList<String> scrossClientList, String site,
			String crossClientId, String periodTopTrackUI2)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		try {
			mapTopTracksRoyaltyByDownload = new HashMap<>();
			mapTopTracksRoyaltyByStream = new HashMap<>();
			mapTopTracksUnitsByDownload = new HashMap<>();
			mapTopTracksUnitsByStream = new HashMap<>();
			maptopTrackProductTitleDest = new HashMap<>();
			mapTopTracksRoyaltyDownloadSource = new HashMap<>();
			mapTopTracksRoyaltyStreamSource = new HashMap<>();
			mapTopTracksUnitsStreamSource = new HashMap<>();
			maptopTrackProductTitleSource = new HashMap<>();
			mapTopTracksUnitDownloadSource = new HashMap<>();
			topTracksProductIsrcCode = new ArrayList<String>();
			topTracksProductIsrcCodeSource = new ArrayList<String>();

			dbQueries_TopTracks.DB_Query(site, client, scrossClientList, crossClientId, periodTopTrackUI2);
			topTracksProductIsrcCode = dbQueries_TopTracks.TopTracksProductIsrcCode;
			mapTopTracksRoyaltyByDownload = dbQueries_TopTracks.mapTopTracksRoyaltyByDownload;
			mapTopTracksRoyaltyByStream = dbQueries_TopTracks.mapTopTracksRoyaltyByStream;
			mapTopTracksUnitsByDownload = dbQueries_TopTracks.mapTopTracksUnitsByDownload;

			mapTopTracksUnitsByStream = dbQueries_TopTracks.mapTopTracksUnitsByStream;
			topTracksProductIsrcCodeSource = dbQueries_TopTracks.TopTracksProductIsrcCodeSource;
			mapTopTracksRoyaltyDownloadSource = dbQueries_TopTracks.mapTopTracksRoyaltyDownloadSource;
			mapTopTracksUnitDownloadSource = dbQueries_TopTracks.mapTopTracksUnitDownloadSource;
			mapTopTracksRoyaltyStreamSource = dbQueries_TopTracks.mapTopTracksRoyaltyStreamSource;
			mapTopTracksUnitsStreamSource = dbQueries_TopTracks.mapTopTracksUnitsStreamSource;
			maptopTrackProductTitleDest = dbQueries_TopTracks.mapTopTracksProductTitle;
			maptopTrackProductTitleSource = dbQueries_TopTracks.mapTopTracksProductTitleSource;
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchDataFromDBForTopTracks :" + e);
		}
	}

	// @Given("^fetch New Statement Due from UI$")
	public void fetchNewStatementDueFromUIandDb(String client, String clientName, ArrayList<String> scrossClientList,
			ArrayList<String> crossClientNamesList, String crossClientName, String crossClientId, String site)
			throws InterruptedException, ClassNotFoundException, SQLException, IOException {
		try {
			newStatementDueUI = null;
			newStatementDueUI = userSelectClient.newStatementDueUI();
			fetchDataFromDBForNewStatementDue(client, scrossClientList, site, crossClientId);

			ArrayList<String> newStatementDue = new ArrayList<String>();

			newStatementDue.add(site);

			if (crossClientId != null) {
				newStatementDue.add(client + "," + crossClientId.toString().replace("'", ""));
				newStatementDue.add(clientName + "," + crossClientName.toString().replace("'", ""));
			} else if (!scrossClientList.isEmpty()) {
				newStatementDue.add(client + ","
						+ scrossClientList.toString().replace("['", "").replace("']", "").replace("'", ""));
				newStatementDue.add(clientName + ","
						+ crossClientNamesList.toString().replace("['", "").replace("']", "").replace("'", ""));
			} else {
				newStatementDue.add(client);
				newStatementDue.add(clientName);
			}
			newStatementDue.add(newStatementDueUI);
			newStatementDue.add(newStatementDueDest);
			newStatementDue.add(newStatementDueSource);
			String arr[] = null;
			arr = newStatementDueUI.split(" ");
			String newStatementForUI = arr[0];

			Map<String, String> statusMap = new HashMap<String, String>();

			statusMap = getTestCaseStatus(newStatementForUI, newStatementDueDest, newStatementDueSource);

			for (Map.Entry<String, String> map : statusMap.entrySet()) {
				if (map.getKey().matches(Constants.statusPass)) {
					newStatementDue.add(Constants.statusPass);
					newStatementDue.add(map.getValue());
					break;

				} else if (map.getKey().matches(Constants.statusFail)) {
					newStatementDue.add(Constants.statusFail);
					newStatementDue.add(map.getValue());
					break;

				}

			}

			newStatementDueFinalResult.add(newStatementDue);
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchNewStatementDueFromUIandDb :" + e);
		}
	}

	// @Given("^fetch New Statement Due from DB$")
	private void fetchDataFromDBForNewStatementDue(String client, ArrayList<String> scrossClientList, String site,
			String crossClientId) throws SQLException, InterruptedException, IOException {
		try {
			newStatementDueDest = null;
			newStatementDueSource = null;
			dbQueries_NewStatementDue.DB_Query_NewStatementDue(site, client, scrossClientList, crossClientId);
			newStatementDueDest = dbQueries_NewStatementDue.newStatementDueDest;
			newStatementDueSource = dbQueries_NewStatementDue.newStatementDueSource;

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchDataFromDBForNewStatementDue :" + e);
		}

	}

	// @Given("^fetch Pipeline Royalty For Recording from UI$")
	public void fetchPipelineRoyaltyRecordFromUIandDb(String client, ArrayList<String> scrossClientList,
			String crossClientId, String site)
			throws InterruptedException, ClassNotFoundException, SQLException, IOException {
		try {

			pipelineRoyaltyRecordUI = null;
			pipelineRoyaltyRecordDest = null;
			pipelineRoyaltyRecordSource = null;
			periodPipelineRoyaltyRecordUI = null;
			pipelineRoyaltyCurrencyUI = null;
			userSelectClient.pipelineRoyaltyRecordUI();
			periodPipelineRoyaltyRecordUI = userSelectClient.periodPipelineRoyaltyRecordUI;
			pipelineRoyaltyRecordUI = userSelectClient.pipelineRoyaltyRecordUI;
			pipelineRoyaltyCurrencyUI = userSelectClient.pipelineRoyaltyCurrencyUI;

			fetchDataFromDBForPipelineRoyaltyRecord(client, scrossClientList, site, crossClientId);

			ArrayList<String> pipelineRoylRecord = new ArrayList<String>();

			pipelineRoylRecord.add(site);

			if (crossClientId != null) {
				pipelineRoylRecord.add(client + "," + crossClientId);
			} else if (!scrossClientList.isEmpty()) {
				pipelineRoylRecord.add(client + "," + scrossClientList.toString().replace("[", "").replace("]", ""));
			} else {
				pipelineRoylRecord.add(client);
			}

			pipelineRoylRecord.add(periodPipelineRoyaltyRecordUI);
			pipelineRoylRecord.add(pipelineRoyaltyCurrencyUI);
			pipelineRoylRecord.add(pipelineRoyaltyRecordUI);

			if (pipelineRoyaltyRecordDest != null && pipelineRoyaltyRecordDest != "") {
				pipelineRoylRecord.add(pipelineRoyltyPeriodRecordDest);

				pipelineRoylRecord.add(pipelineRoyaltyRecordDest);

			} else {
				pipelineRoylRecord.add(null);
			}

			if (pipelineRoyaltyRecordSource != null && pipelineRoyaltyRecordSource != "") {
				pipelineRoylRecord.add(pipelineRoyltyPeriodRecordSource);

				pipelineRoylRecord.add(pipelineRoyaltyRecordSource);
			} else {
				pipelineRoylRecord.add(null);
			}

			Map<String, String> statusMap = new HashMap<String, String>();

			statusMap = getTestCaseStatus(pipelineRoyaltyRecordUI, pipelineRoyaltyRecordDest,
					pipelineRoyaltyRecordSource);

			for (Map.Entry<String, String> map : statusMap.entrySet()) {
				if (map.getKey().matches(Constants.statusPass)) {
					pipelineRoylRecord.add(Constants.statusPass);
					pipelineRoylRecord.add(map.getValue());
					break;

				} else if (map.getKey().matches(Constants.statusFail)) {
					pipelineRoylRecord.add(Constants.statusFail);
					pipelineRoylRecord.add(map.getValue());
					break;

				}

			}

			pipelineRoyltyRecordFinalResult.add(pipelineRoylRecord);
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchPipelineRoyaltyRecordFromUIandDb: " + e.getLocalizedMessage());
		}

	}

	// @Given("^fetch New Statement Due from DB$")
	private void fetchDataFromDBForPipelineRoyaltyRecord(String client, ArrayList<String> scrossClientList, String site,
			String crossClientId) {
		try {
			pipelineRoyaltyRecordDest = null;
			pipelineRoyaltyRecordSource = null;
			pipelineRoyltyPeriodRecordDest = null;
			pipelineRoyltyPeriodRecordSource = null;
			dbQueries_PipelineRoyalty_Recording.DB_Query_PipelineRoyalty_Record(site, client, scrossClientList,
					crossClientId);
			pipelineRoyaltyRecordDest = dbQueries_PipelineRoyalty_Recording.pipelineRoyltyRecordDest;
			pipelineRoyaltyRecordSource = dbQueries_PipelineRoyalty_Recording.pipelineRoyltyRecordSource;
			pipelineRoyltyPeriodRecordDest = dbQueries_PipelineRoyalty_Recording.pipelineRoyltyPeriodRecordDest;
			pipelineRoyltyPeriodRecordSource = dbQueries_PipelineRoyalty_Recording.pipelineRoyltyPeriodRecordSource;

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Methods_Class.fetchDataFromDBForPipelineRoyaltyRecord :" + e);
		}

	}

	public void fetchRoyaltyTrendLicensing(String client, String clientName, ArrayList<String> scrossClientList,
			ArrayList<String> crossClientNamesList, String crossClientName, String crossClientId, String site,
			String year, List<String> allYears) {
		try {
			royaltyTrendLicensingUI = null;
			royaltyTrendLicensingDestDb = null;
			royaltyTrendLicensingSourceDb = null;

			userSelectClient.fetchRoyaltyTrendLicensingFromUI(year);
			royaltyTrendLicensingUI = userSelectClient.royaltyTrendLicensingUI;
			dbConnectionForRoyaltyTrend.DB_Query_RoyaltyTrendLicensing(client, scrossClientList, crossClientId, site,
					year);
			royaltyTrendLicensingDestDb = dbConnectionForRoyaltyTrend.royaltyTrendLicensingDestDb;
			royaltyTrendLicensingSourceDb = dbConnectionForRoyaltyTrend.royaltyTrendLicensingSourceDb;

			ArrayList<String> royaltyTrendLicensingList = new ArrayList<String>();
			royaltyTrendLicensingList.add(site);
			if (crossClientId != null) {
				royaltyTrendLicensingList.add(client + "," + crossClientId.toString().replace("'", ""));
				royaltyTrendLicensingList.add(clientName + "," + crossClientName.toString().replace("'", ""));
			} else if (!scrossClientList.isEmpty()) {
				royaltyTrendLicensingList
						.add(client + "," + scrossClientList.toString().replace("['", "").replace("']", ""));
				royaltyTrendLicensingList
						.add(clientName + "," + crossClientNamesList.toString().replace("['", "").replace("']", ""));
			} else {
				royaltyTrendLicensingList.add(client);
				royaltyTrendLicensingList.add(clientName);
			}
			royaltyTrendLicensingList.add(year);
			String currency[] = null;
			currency = royaltyTrendLicensingUI.split(" ");
			royaltyTrendLicensingList.add(currency[0]);
			royaltyTrendLicensingList.add(currency[1]);
			// royaltyTrendLicensingList.add(royaltyTrendLicensingUI);
			royaltyTrendLicensingList.add(royaltyTrendLicensingDestDb);
			royaltyTrendLicensingList.add(royaltyTrendLicensingSourceDb);
			royaltyTrendLicensingFinalList.add(royaltyTrendLicensingList);
		} catch (Exception ex) {
			ex.printStackTrace();

		}

	}

	public void fetchRoyaltyTrendSync(String client, String clientName, ArrayList<String> scrossClientList,
			ArrayList<String> crossClientNamesList, String crossClientName, String crossClientId, String site,
			String year, List<String> allYears) {
		try {
			royaltyTrendSyncUI = null;
			royaltyTrendSyncDestDb = null;
			royaltyTrendSyncSourceDb = null;

			userSelectClient.fetchRoyaltyTrendSyncFromUI(year);
			royaltyTrendSyncUI = userSelectClient.royaltyTrendSyncUI;
			dbConnectionForRoyaltyTrend.DB_Query_RoyaltyTrendSync(client, scrossClientList, crossClientId, site, year);
			royaltyTrendSyncDestDb = dbConnectionForRoyaltyTrend.royaltyTrendSyncDestDb;
			royaltyTrendSyncSourceDb = dbConnectionForRoyaltyTrend.royaltyTrendSyncSourceDb;

			ArrayList<String> royaltyTrendSyncList = new ArrayList<String>();
			royaltyTrendSyncList.add(site);
			if (crossClientId != null) {
				royaltyTrendSyncList.add(client + "," + crossClientId.toString().replace("'", ""));
				royaltyTrendSyncList.add(clientName + "," + crossClientName.toString().replace("'", ""));
			} else if (!scrossClientList.isEmpty()) {
				royaltyTrendSyncList
						.add(client + "," + scrossClientList.toString().replace("['", "").replace("']", ""));
				royaltyTrendSyncList
						.add(clientName + "," + crossClientNamesList.toString().replace("['", "").replace("']", ""));
			} else {
				royaltyTrendSyncList.add(client);
				royaltyTrendSyncList.add(clientName);
			}
			royaltyTrendSyncList.add(year);
			String currency[] = null;
			currency = royaltyTrendSyncUI.split(" ");
			royaltyTrendSyncList.add(currency[0]);
			royaltyTrendSyncList.add(currency[1]);
			// royaltyTrendSyncList.add(royaltyTrendSyncUI);
			royaltyTrendSyncList.add(royaltyTrendSyncDestDb);
			royaltyTrendSyncList.add(royaltyTrendSyncSourceDb);
			royaltyTrendSyncFinalList.add(royaltyTrendSyncList);
		} catch (Exception ex) {
			ex.printStackTrace();

		}
	}

	public void fetchRoyaltyTrendPublicPerformance(String client, String clientName, ArrayList<String> scrossClientList,
			ArrayList<String> crossClientNamesList, String crossClientName, String crossClientId, String site,
			String year, List<String> allYears) {
		try {
			royaltyTrendPublicPerformanceUI = null;
			royaltyTrendPublicPerformanceDestDb = null;
			royaltyTrendPublicPerformanceSourceDb = null;
			userSelectClient.fetchRoyaltyTrendPublicPerformanceFromUI(year);
			royaltyTrendPublicPerformanceUI = userSelectClient.royaltyTrendPublicPerformanceUI;
			dbConnectionForRoyaltyTrend.DB_Query_RoyaltyTrendPublicPerformance(client, scrossClientList, crossClientId,
					site, year);
			royaltyTrendPublicPerformanceDestDb = dbConnectionForRoyaltyTrend.royaltyTrendPublicPerformanceDestDb;
			royaltyTrendPublicPerformanceSourceDb = dbConnectionForRoyaltyTrend.royaltyTrendPublicPerformanceSourceDb;

			ArrayList<String> royaltyTrendPublicPerformanceList = new ArrayList<String>();
			royaltyTrendPublicPerformanceList.add(site);
			if (crossClientId != null) {
				royaltyTrendPublicPerformanceList.add(client + "," + crossClientId.toString().replace("'", ""));
				royaltyTrendPublicPerformanceList.add(clientName + "," + crossClientName.toString().replace("'", ""));
			} else if (!scrossClientList.isEmpty()) {
				royaltyTrendPublicPerformanceList
						.add(client + "," + scrossClientList.toString().replace("['", "").replace("']", ""));
				royaltyTrendPublicPerformanceList
						.add(clientName + "," + crossClientNamesList.toString().replace("['", "").replace("']", ""));
			} else {
				royaltyTrendPublicPerformanceList.add(client);
				royaltyTrendPublicPerformanceList.add(clientName);
			}
			royaltyTrendPublicPerformanceList.add(year);
			String currency[] = null;
			currency = royaltyTrendPublicPerformanceUI.split(" ");
			royaltyTrendPublicPerformanceList.add(currency[0]);
			royaltyTrendPublicPerformanceList.add(currency[1]);
			// royaltyTrendPublicPerformanceList.add(royaltyTrendPublicPerformanceUI);
			royaltyTrendPublicPerformanceList.add(royaltyTrendPublicPerformanceDestDb);
			royaltyTrendPublicPerformanceList.add(royaltyTrendPublicPerformanceSourceDb);
			royaltyTrendPublicPerformanceFinalList.add(royaltyTrendPublicPerformanceList);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void fetchRoyaltyTrendMerchandise(String client, String clientName, ArrayList<String> scrossClientList,
			ArrayList<String> crossClientNamesList, String crossClientName, String crossClientId, String site,
			String year, List<String> allYears) {
		try {
			royaltyTrendMerchandiseUI = null;
			royaltyTrendMerchandiseDestDb = null;
			royaltyTrendMerchandiseSourceDb = null;
			userSelectClient.fetchRoyaltyTrendMerchandiseFromUI(year);
			royaltyTrendMerchandiseUI = userSelectClient.royaltyTrendMerchandiseUI;
			dbConnectionForRoyaltyTrend.DB_Query_RoyaltyTrendMerchandise(client, scrossClientList, crossClientId, site,
					year);
			royaltyTrendMerchandiseDestDb = dbConnectionForRoyaltyTrend.royaltyTrendMerchandiseDestDb;
			royaltyTrendMerchandiseSourceDb = dbConnectionForRoyaltyTrend.royaltyTrendMerchandiseSourceDb;

			ArrayList<String> royaltyTrendMerchandiseList = new ArrayList<String>();
			royaltyTrendMerchandiseList.add(site);
			if (crossClientId != null) {
				royaltyTrendMerchandiseList.add(client + "," + crossClientId.toString().replace("'", ""));
				royaltyTrendMerchandiseList.add(clientName + "," + crossClientName.toString().replace("'", ""));
			} else if (!scrossClientList.isEmpty()) {
				royaltyTrendMerchandiseList
						.add(client + "," + scrossClientList.toString().replace("['", "").replace("']", ""));
				royaltyTrendMerchandiseList
						.add(clientName + "," + crossClientNamesList.toString().replace("['", "").replace("']", ""));
			} else {
				royaltyTrendMerchandiseList.add(client);
				royaltyTrendMerchandiseList.add(clientName);
			}
			royaltyTrendMerchandiseList.add(year);
			String currency[] = null;
			currency = royaltyTrendMerchandiseUI.split(" ");
			royaltyTrendMerchandiseList.add(currency[0]);
			royaltyTrendMerchandiseList.add(currency[1]);
			// royaltyTrendMerchandiseList.add(royaltyTrendMerchandiseUI);
			royaltyTrendMerchandiseList.add(royaltyTrendMerchandiseDestDb);
			royaltyTrendMerchandiseList.add(royaltyTrendMerchandiseSourceDb);
			royaltyTrendMerchandiseFinalList.add(royaltyTrendMerchandiseList);
		} catch (Exception ex) {
			ex.printStackTrace();

		}
	}

	public void fetchPipelineRoyaltyPublishingFromUIandDb(String client, String clientName,
			ArrayList<String> scrossClientList, ArrayList<String> crossClientNamesList, String crossClientName,
			String crossClientId, String site) {
		try {
			pipelineRoyaltyPublishingUI = null;
			pipelineRoyaltyCurrencyPublishingUI = null;
			periodPipelineRoyaltyPublishingUI = null;
			pipelineRoyaltyCurrencyPublishingUI = null;
			userSelectClient.pipelineRoyaltyPublishingUI();
			periodPipelineRoyaltyPublishingUI = userSelectClient.periodPipelineRoyaltyPublishingUI;
			pipelineRoyaltyPublishingUI = userSelectClient.pipelineRoyaltyPublishingUI;
			pipelineRoyaltyCurrencyPublishingUI = userSelectClient.pipelineRoyaltyCurrencyPublishingUI;

			fetchDataFromDBForPipelineRoyaltyPub(client, scrossClientList, site, crossClientId);

			ArrayList<String> pipelineRoylPub = new ArrayList<String>();

			pipelineRoylPub.add(site);

			if (crossClientId != null) {
				pipelineRoylPub.add(client + "," + crossClientId.toString().replace("'", ""));
				pipelineRoylPub.add(clientName + "," + crossClientName.toString().replace("'", ""));
			} else if (!scrossClientList.isEmpty()) {
				pipelineRoylPub.add(client + "," + scrossClientList.toString().replace("['", "").replace("']", ""));
				pipelineRoylPub
						.add(clientName + "," + crossClientNamesList.toString().replace("['", "").replace("']", ""));
			} else {
				pipelineRoylPub.add(client);
				pipelineRoylPub.add(clientName);
			}

			pipelineRoylPub.add(periodPipelineRoyaltyPublishingUI);
			pipelineRoylPub.add(pipelineRoyaltyCurrencyPublishingUI);
			pipelineRoylPub.add(pipelineRoyaltyPublishingUI);

			Map<String, String> statusMap = new HashMap<String, String>();

			statusMap = getTestCaseStatus(pipelineRoyaltyPublishingUI, pipelineRoyaltyPublishingDest1,
					pipelineRoyaltyPublishingSource1);

			for (Map.Entry<String, String> map : statusMap.entrySet()) {
				if (map.getKey().matches(Constants.statusPass)) {

					pipelineRoylPub.add(pipelineRoyltyPeriodPublishingDest1);

					pipelineRoylPub.add(pipelineRoyaltyPublishingDest1);

					pipelineRoylPub.add(pipelineRoyltyPeriodPublishingSource1);
					pipelineRoylPub.add(pipelineRoyaltyPublishingSource1);

					pipelineRoylPub.add("");
					pipelineRoylPub.add("");

					pipelineRoylPub.add(Constants.statusPass);
					pipelineRoylPub.add(map.getValue());
					break;

				} else if (map.getKey().matches(Constants.statusFail)) {

					Map<String, String> statusMap1 = new HashMap<String, String>();

					statusMap1 = getTestCaseStatus(pipelineRoyaltyPublishingUI, pipelineRoyaltyPublishingDest2,
							pipelineRoyaltyPublishingSource1);

					for (Map.Entry<String, String> map1 : statusMap1.entrySet()) {
						if (map1.getKey().matches(Constants.statusPass)) {

							pipelineRoylPub.add(pipelineRoyltyPeriodPublishingDest2);

							pipelineRoylPub.add(pipelineRoyaltyPublishingDest2);

							pipelineRoylPub.add(pipelineRoyltyPeriodPublishingSource1);
							pipelineRoylPub.add(pipelineRoyaltyPublishingSource1);

							pipelineRoylPub.add(Constants.statusPass);
							pipelineRoylPub.add(map.getValue());
							break;
						} else if (map1.getKey().matches(Constants.statusFail)) {
							Map<String, String> statusMap2 = new HashMap<String, String>();

							statusMap2 = getTestCaseStatus(pipelineRoyaltyPublishingUI, pipelineRoyaltyPublishingDest3,
									pipelineRoyaltyPublishingSource1);

							for (Map.Entry<String, String> map2 : statusMap.entrySet()) {
								if (map2.getKey().matches(Constants.statusPass)) {

									pipelineRoylPub.add(pipelineRoyltyPeriodPublishingDest3);

									pipelineRoylPub.add(pipelineRoyaltyPublishingDest3);

									pipelineRoylPub.add(pipelineRoyltyPeriodPublishingSource1);
									pipelineRoylPub.add(pipelineRoyaltyPublishingSource1);

									pipelineRoylPub.add(Constants.statusPass);
									pipelineRoylPub.add(map.getValue());
									break;

								} else if (map2.getKey().matches(Constants.statusFail)) {
									Map<String, String> statusMap3 = new HashMap<String, String>();

									statusMap3 = getTestCaseStatus(pipelineRoyaltyPublishingUI,
											pipelineRoyaltyPublishingDest1, pipelineRoyaltyPublishingSource2);

									for (Map.Entry<String, String> map3 : statusMap.entrySet()) {
										if (map3.getKey().matches(Constants.statusPass)) {
											pipelineRoylPub.add(pipelineRoyltyPeriodPublishingDest1);

											pipelineRoylPub.add(pipelineRoyaltyPublishingDest1);

											pipelineRoylPub.add(pipelineRoyltyPeriodPublishingSource2);
											pipelineRoylPub.add(pipelineRoyaltyPublishingSource2);

											pipelineRoylPub.add(Constants.statusPass);
											pipelineRoylPub.add(map.getValue());
											break;

										} else if (map3.getKey().matches(Constants.statusFail)) {
											Map<String, String> statusMap4 = new HashMap<String, String>();

											statusMap4 = getTestCaseStatus(pipelineRoyaltyPublishingUI,
													pipelineRoyaltyPublishingDest2, pipelineRoyaltyPublishingSource2);

											for (Map.Entry<String, String> map4 : statusMap.entrySet()) {
												if (map4.getKey().matches(Constants.statusPass)) {
													pipelineRoylPub.add(pipelineRoyltyPeriodPublishingDest2);

													pipelineRoylPub.add(pipelineRoyaltyPublishingDest2);

													pipelineRoylPub.add(pipelineRoyltyPeriodPublishingSource2);
													pipelineRoylPub.add(pipelineRoyaltyPublishingSource2);

													pipelineRoylPub.add(Constants.statusPass);
													pipelineRoylPub.add(map.getValue());
													break;
												} else if (map4.getKey().matches(Constants.statusFail)) {
													Map<String, String> statusMap5 = new HashMap<String, String>();

													statusMap5 = getTestCaseStatus(pipelineRoyaltyPublishingUI,
															pipelineRoyaltyPublishingDest3,
															pipelineRoyaltyPublishingSource2);

													for (Map.Entry<String, String> map5 : statusMap.entrySet()) {
														if (map5.getKey().matches(Constants.statusPass)) {
															pipelineRoylPub.add(pipelineRoyltyPeriodPublishingDest3);

															pipelineRoylPub.add(pipelineRoyaltyPublishingDest3);

															pipelineRoylPub.add(pipelineRoyltyPeriodPublishingSource2);
															pipelineRoylPub.add(pipelineRoyaltyPublishingSource2);

															pipelineRoylPub.add(Constants.statusPass);
															pipelineRoylPub.add(map.getValue());
															break;

														} else if (map5.getKey().matches(Constants.statusFail)) {
															pipelineRoylPub.add(pipelineRoyltyPeriodPublishingDest3);

															pipelineRoylPub.add(pipelineRoyaltyPublishingDest3);

															pipelineRoylPub.add(pipelineRoyltyPeriodPublishingSource2);
															pipelineRoylPub.add(pipelineRoyaltyPublishingSource2);

															pipelineRoylPub.add(Constants.statusFail);
															pipelineRoylPub.add(map5.getValue());
															break;
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}

					pipelineRoylPub.add(Constants.statusFail);
					pipelineRoylPub.add(map.getValue());
					break;

				}

			}

			pipelineRoyltyPublishFinalResult.add(pipelineRoylPub);

		} catch (

		Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchPipelineRoyaltyRecordFromUIandDb: " + e.getLocalizedMessage());
		}

	}

	// @Given("^fetch pipeline Royalty for Publishing from DB$")
	private void fetchDataFromDBForPipelineRoyaltyPub(String client, ArrayList<String> scrossClientList, String site,
			String crossClientId) {
		try {
			pipelineRoyaltyRecordDest = null;
			pipelineRoyaltyRecordSource = null;
			pipelineRoyltyPeriodRecordDest = null;
			pipelineRoyltyPeriodRecordSource = null;
			dbQueries_PipelineRoyalty_Recording.DB_Query_PipelineRoyalty_Record(site, client, scrossClientList,
					crossClientId);
			pipelineRoyaltyRecordDest = dbQueries_PipelineRoyalty_Recording.pipelineRoyltyRecordDest;
			pipelineRoyaltyRecordSource = dbQueries_PipelineRoyalty_Recording.pipelineRoyltyRecordSource;
			pipelineRoyltyPeriodRecordDest = dbQueries_PipelineRoyalty_Recording.pipelineRoyltyPeriodRecordDest;
			pipelineRoyltyPeriodRecordSource = dbQueries_PipelineRoyalty_Recording.pipelineRoyltyPeriodRecordSource;
			pipelineRoyaltyPublishingDest1 = null;
			pipelineRoyaltyPublishingSource1 = null;
			pipelineRoyltyPeriodPublishingDest1 = null;
			pipelineRoyltyPeriodPublishingSource1 = null;
			pipelineRoyaltyPublishingSource2 = null;
			pipelineRoyltyPeriodPublishingSource2 = null;
			pipelineRoyltyPeriodPublishingDest2 = null;
			pipelineRoyaltyPublishingDest2 = null;
			pipelineRoyltyPeriodPublishingDest3 = null;
			pipelineRoyaltyPublishingDest3 = null;

			dbQueries_PipelineRoyalty_Publishing.DB_Query_PipelineRoyalty_Publishing(site, client, scrossClientList,
					crossClientId);
			pipelineRoyaltyPublishingDest1 = dbQueries_PipelineRoyalty_Publishing.pipelineRoyaltyPublishingDest1;
			pipelineRoyltyPeriodPublishingDest1 = dbQueries_PipelineRoyalty_Publishing.pipelineRoyltyPeriodPublishingDest1;
			pipelineRoyaltyPublishingDest2 = dbQueries_PipelineRoyalty_Publishing.pipelineRoyaltyPublishingDest2;
			pipelineRoyltyPeriodPublishingDest2 = dbQueries_PipelineRoyalty_Publishing.pipelineRoyltyPeriodPublishingDest2;
			pipelineRoyaltyPublishingDest3 = dbQueries_PipelineRoyalty_Publishing.pipelineRoyaltyPublishingDest3;
			pipelineRoyltyPeriodPublishingDest3 = dbQueries_PipelineRoyalty_Publishing.pipelineRoyltyPeriodPublishingDest3;
			pipelineRoyaltyPublishingSource1 = dbQueries_PipelineRoyalty_Publishing.pipelineRoyaltyPublishingSource1;
			pipelineRoyltyPeriodPublishingSource1 = dbQueries_PipelineRoyalty_Publishing.pipelineRoyltyPeriodPublishingSource1;
			pipelineRoyaltyPublishingSource2 = dbQueries_PipelineRoyalty_Publishing.pipelineRoyaltyPublishingSource2;
			pipelineRoyltyPeriodPublishingSource2 = dbQueries_PipelineRoyalty_Publishing.pipelineRoyltyPeriodPublishingSource2;

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchDataFromDBForPipelineRoyaltyPub: " + e.getLocalizedMessage());
		}

	}

	// @Given("^fetch Top Licensing Products from UI$")
	public void fetchTopLicensingProductsFromUIandDb(String client, ArrayList<String> scrossClientList,
			String crossClientId, String site, String clientName)
			throws InterruptedException, ClassNotFoundException, SQLException, IOException {
		try {
			periodTopLicensingUI = null;
			userSelectClient.getTopLicensingProductsUI();
			periodTopLicensingUI = userSelectClient.periodTopLicensingProducts;
			List<List<String>> topLicensingProductList = new ArrayList<List<String>>();
			if (periodTopLicensingUI != null) {
				topLicensingProductList = userSelectClient.topLicensingProductList;
				fetchDataFromDBForTopLicensingProducts(client, scrossClientList, site, crossClientId,
						periodTopLicensingUI);
				for (List<String> t : topLicensingProductList) {
					ArrayList<String> topLicensingChildList = new ArrayList<String>();

					topLicensingChildList.add(site);
					if (crossClientId != null) {
						String crossClient[] = crossClientId.split(",");
						topLicensingChildList.add(client + "," + crossClient[0].toString().replace("'", ""));
						topLicensingChildList.add(clientName + "," + crossClient[1].toString().replace("'", ""));
					} else if (!scrossClientList.isEmpty()) {
						topLicensingChildList.add(
								client + "," + scrossClientList.get(0).toString().replace("['", "").replace("']", ""));
						topLicensingChildList.add(clientName + ","
								+ scrossClientList.get(1).toString().replace("['", "").replace("']", ""));
					} else {
						topLicensingChildList.add(client);
						topLicensingChildList.add(clientName);
					}

//				if (crossClientId != null) {
//					topLicensingChildList.add(client + "," + crossClientId);
//				} else if (!scrossClientList.isEmpty()) {
//					topLicensingChildList
//							.add(client + "," + scrossClientList.toString().replace("[", "").replace("]", ""));
//				} else {
//					topLicensingChildList.add(client);
//				}
//				
//				topLicensingChildList.add(clientName);

					topLicensingChildList.add(periodTopLicensingUI);
					topLicensingChildList.add(t.get(0));
					String[] arr = null;
					arr = t.get(1).split(":");
					String configuration = arr[1];
					topLicensingChildList.add(configuration);
					String[] arr1 = null;
					arr1 = t.get(2).split(":");
					String productUserCodeUI = arr1[1];
					topLicensingChildList.add(productUserCodeUI);
					String arr2[] = null;
					arr2 = t.get(3).split(" ");
					String currencyUI = arr2[0];
					String royaltyUI = arr2[1];
					topLicensingChildList.add(currencyUI);
					topLicensingChildList.add(royaltyUI);

					Boolean valuePresent = false;

					for (Map.Entry<String, String> map : mapTopLicensingPeriodDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topLicensingChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topLicensingChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopLicensingProductTitleDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topLicensingChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topLicensingChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopLicensingConfigDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topLicensingChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topLicensingChildList.add("");
						valuePresent = false;
					}
					for (String isrc : topLicensingProductCodeDest) {
						if (isrc.trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topLicensingChildList.add(isrc);
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topLicensingChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopLicensingRoyaltyDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topLicensingChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topLicensingChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopLicensingPeriodSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topLicensingChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topLicensingChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopLicensingProductTitleSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topLicensingChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topLicensingChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopLicensingConfigSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topLicensingChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topLicensingChildList.add("");
						valuePresent = false;
					}
					for (String isrc : topLicensingProductCodeSource) {
						if (isrc.trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topLicensingChildList.add(isrc);
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topLicensingChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopLicensingRoyaltySource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topLicensingChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topLicensingChildList.add("");
						valuePresent = false;
					}

					Map<String, String> statusMap = new HashMap<String, String>();
					statusMap = getStatusMapWidget(topLicensingChildList);

					for (Map.Entry<String, String> map3 : statusMap.entrySet()) {
						if (map3.getKey().matches(Constants.statusPass)) {
							topLicensingChildList.add(Constants.statusPass);
							topLicensingChildList.add("");
						} else if (map3.getKey().matches(Constants.statusFail)) {
							topLicensingChildList.add(Constants.statusFail);
							topLicensingChildList.add(map3.getValue());
						}
					}
					topLicensingProductFinalList.add(topLicensingChildList);
				}
			} else {
				ArrayList<String> topLicensingChildList = new ArrayList<String>();

				topLicensingChildList.add(site);

				if (crossClientId != null) {
					topLicensingChildList.add(client + "," + crossClientId);
				} else if (!scrossClientList.isEmpty()) {
					topLicensingChildList
							.add(client + "," + scrossClientList.toString().replace("[", "").replace("]", ""));
				} else {
					topLicensingChildList.add(client);
				}
				topLicensingChildList.add(clientName);
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add(Constants.noDataOnUIMessage);
				topLicensingProductFinalList.add(topLicensingChildList);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Methods_Class.fetchDataFromDBForPipelineRoyaltyRecord :" + e);
		}

	}

	public String formatUIValue(String value) {

		String finalPI = "";
		String finalPI2 = "";

		if (value.contains(",")) {
			String pipUI = "";

			String pip2 = "";
			String[] arr = null;
			arr = value.split(",");

			for (int i = 0; i < arr.length; i++) {
				pipUI = pipUI + arr[i];
				finalPI = pipUI;
			}

			if (pipUI.contains(".")) {
				String[] arr1 = null;

				arr1 = pipUI.split("\\.");
				for (int i = 0; i < arr1.length; i++) {
					pip2 = pip2 + arr1[i];
					finalPI = pip2;
				}
			}
			LOGGER.info(finalPI);
		} else if (value.contains(".")) {
			String pipUI = "";

			String pip2 = "";
			String[] arr = null;
			arr = value.split("\\.");

			for (int i = 0; i < arr.length; i++) {
				pipUI = pipUI + arr[i];
				finalPI = pipUI;
			}

			if (pipUI.contains(",")) {
				String[] arr1 = null;

				arr1 = pipUI.split(",");
				for (int i = 0; i < arr1.length; i++) {
					pip2 = pip2 + arr1[i];
					finalPI = pip2;
				}
			}
			LOGGER.info(finalPI);
		} else {
			finalPI = value;
		}

		if (finalPI.contains("K")) {
			String arr3[] = null;
			arr3 = finalPI.split("K");

			finalPI2 = arr3[0];

		} else {
			finalPI2 = finalPI;
		}

		LOGGER.info("UI value :" + finalPI2);
		return finalPI2;

	}

	public String formatDestValue(String value) {

		String finalPIDest = "";
		String finalPI2Dest = "";
		if (value.contains(",")) {
			String pipUI = "";

			String pip2 = "";
			String[] arr = null;
			arr = value.split(",");

			for (int i = 0; i < arr.length; i++) {
				pipUI = pipUI + arr[i];
				finalPIDest = pipUI;
			}

			if (pipUI.contains(".")) {
				String[] arr1 = null;

				arr1 = pipUI.split("\\.");
				for (int i = 0; i < arr1.length; i++) {
					pip2 = pip2 + arr1[i];
					finalPIDest = pip2;
				}
			}
			LOGGER.info(finalPIDest);
		} else if (value.contains(".")) {
			String pipUI = "";

			String pip2 = "";
			String[] arr = null;
			arr = value.split("\\.");

			for (int i = 0; i < arr.length; i++) {
				pipUI = pipUI + arr[i];
				finalPIDest = pipUI;
			}

			if (pipUI.contains(",")) {
				String[] arr1 = null;

				arr1 = pipUI.split(",");
				for (int i = 0; i < arr1.length; i++) {
					pip2 = pip2 + arr1[i];
					finalPIDest = pip2;
				}
			}
			LOGGER.info(finalPIDest);
		} else {
			finalPIDest = value;
		}

		if (finalPIDest.contains("K")) {
			String arr3[] = null;
			arr3 = finalPIDest.split("K");

			finalPI2Dest = arr3[0];

		} else {
			finalPI2Dest = finalPIDest;
		}
		return finalPI2Dest;
	}

	// @Given("^fetch Top Sync Products from UI$")
	public void fetchTopSyncProductsFromUIandDb(String client, ArrayList<String> scrossClientList, String crossClientId,
			String site, String clientName)
			throws InterruptedException, ClassNotFoundException, SQLException, IOException {
		try {
			periodTopSyncUI = null;
			userSelectClient.getTopSyncProductsUI();
			periodTopSyncUI = userSelectClient.periodTopSyncProducts;
			List<List<String>> topSyncProductList = new ArrayList<List<String>>();
			if (periodTopSyncUI != null) {
				topSyncProductList = userSelectClient.topSyncProductList;
				fetchDataFromDBForTopSyncProducts(client, scrossClientList, site, crossClientId, periodTopSyncUI);
				for (List<String> t : topSyncProductList) {
					ArrayList<String> topSyncChildList = new ArrayList<String>();

					topSyncChildList.add(site);
					if (crossClientId != null) {
						String crossClient[] = crossClientId.split(",");
						topSyncChildList.add(client + "," + crossClient[0].toString().replace("'", ""));
						topSyncChildList.add(clientName + "," + crossClient[1].toString().replace("'", ""));
					} else if (!scrossClientList.isEmpty()) {
						topSyncChildList.add(
								client + "," + scrossClientList.get(0).toString().replace("['", "").replace("']", ""));
						topSyncChildList.add(clientName + ","
								+ scrossClientList.get(1).toString().replace("['", "").replace("']", ""));
					} else {
						topSyncChildList.add(client);
						topSyncChildList.add(clientName);
					}

//				if (crossClientId != null) {
//					topLicensingChildList.add(client + "," + crossClientId);
//				} else if (!scrossClientList.isEmpty()) {
//					topLicensingChildList
//							.add(client + "," + scrossClientList.toString().replace("[", "").replace("]", ""));
//				} else {
//					topLicensingChildList.add(client);
//				}
//				
//				topLicensingChildList.add(clientName);

					topSyncChildList.add(periodTopSyncUI);
					topSyncChildList.add(t.get(0));
					String[] arr = null;
					arr = t.get(1).split(":");
					String configuration = arr[1];
					topSyncChildList.add(configuration);
					String[] arr1 = null;
					arr1 = t.get(2).split(":");
					String productUserCodeUI = arr1[1];
					topSyncChildList.add(productUserCodeUI);
					String arr2[] = null;
					arr2 = t.get(3).split(" ");
					String currencyUI = arr2[0];
					String royaltyUI = arr2[1];
					topSyncChildList.add(currencyUI);
					topSyncChildList.add(royaltyUI);

					Boolean valuePresent = false;

					for (Map.Entry<String, String> map : mapTopSyncPeriodDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopSyncProductTitleDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopSyncConfigDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}
					for (String isrc : topSyncProductCodeDest) {
						if (isrc.trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(isrc);
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopSyncRoyaltyDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopSyncPeriodSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopSyncProductTitleSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopSyncConfigSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}
					for (String isrc : topSyncProductCodeSource) {
						if (isrc.trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(isrc);
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public Map<String, String> getTestCaseStatus(String uiValue, String destValue, String SourceValue) {
		Map<String, String> statusMap = new HashMap<String, String>();
		try {
			if ((uiValue == null || uiValue == "0.00") && SourceValue == null && destValue == null) {
				statusMap.put(Constants.statusPass, "");

			} else if (destValue != null && SourceValue != null) {
				double dest = 0.0;
				double source = 0.0;
				dest = Double.parseDouble(destValue);
				DecimalFormat df = new DecimalFormat("#.##");
				dest = Double.valueOf(df.format(dest));

				source = Double.parseDouble(SourceValue);
				DecimalFormat df2 = new DecimalFormat("#.##");
				source = Double.valueOf(df2.format(source));

				if (dest == source) {

					if (uiValue != null) {
						Double UIDoubleValue = 0.0;
						Double destDoubleValue = 0.0;
						String destValueFormated = formatDestValue(destValue);
						String uiValueFormated = formatUIValue(uiValue);
						LOGGER.info("destination fromatted value lenght: " + destValueFormated.length());
						LOGGER.info("UI fromatted value lenght: " + uiValueFormated.length());

						if (destValueFormated.length() == uiValueFormated.length()) {
							UIDoubleValue = Double.parseDouble(uiValueFormated);
							destDoubleValue = Double.parseDouble(destValueFormated);
							if (.05 > (destDoubleValue - UIDoubleValue) && (UIDoubleValue - destDoubleValue) > -.05) {
								statusMap.put(Constants.statusPass, "");
							} else {
								statusMap.put(Constants.statusFail, Constants.errMessage1);

							}
						} else {
							int uilenght = uiValueFormated.length();
							destValueFormated = destValueFormated.substring(0, uilenght);
							LOGGER.info("destiantion value after substring" + destValueFormated);
							UIDoubleValue = Double.parseDouble(uiValueFormated);
							destDoubleValue = Double.parseDouble(destValueFormated);
							if (.05 > (destDoubleValue - UIDoubleValue) && (UIDoubleValue - destDoubleValue) > -.05) {
								statusMap.put(Constants.statusPass, "");
							} else {
								statusMap.put(Constants.statusFail, Constants.errMessage1);

							}
						}

					} else {
						statusMap.put(Constants.statusFail, Constants.errMessage2);

					}

				} else {
					statusMap.put(Constants.statusFail, Constants.errMessage3);

				}
			} else if ((uiValue != "0.0" || uiValue != "0") && destValue == null && SourceValue == null) {
				statusMap.put(Constants.statusFail, "source and destiantion value is null but UI value is not null");
			} else if (((uiValue != "0.0" || uiValue != "0") && destValue == null && SourceValue != null)) {
				statusMap.put(Constants.statusFail, Constants.errMessage4);
			} else if (((uiValue != "0.0" || uiValue != "0") && destValue != null && SourceValue == null)) {
				statusMap.put(Constants.statusFail, Constants.errMessage5);
			} else {
				statusMap.put(Constants.statusPass, "");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.getTestCaseStatus :" + e);
		}
		return statusMap;
	}

	// @Given("^fetch Top Licensing Products from UI$")
	public void fetchTopLicensingProductsFromUIandDb(String client, String clientName,
			ArrayList<String> scrossClientList, ArrayList<String> crossClientNamesList, String crossClientName,
			String crossClientId, String site)
			throws InterruptedException, ClassNotFoundException, SQLException, IOException {
		try {
			periodTopLicensingUI = null;
			userSelectClient.getTopLicensingProductsUI();
			periodTopLicensingUI = userSelectClient.periodTopLicensingProducts;
			List<List<String>> topLicensingProductList = new ArrayList<List<String>>();
			if (periodTopLicensingUI != null) {
				topLicensingProductList = userSelectClient.topLicensingProductList;
				fetchDataFromDBForTopLicensingProducts(client, scrossClientList, site, crossClientId,
						periodTopLicensingUI);
				for (List<String> t : topLicensingProductList) {
					ArrayList<String> topLicensingChildList = new ArrayList<String>();

					topLicensingChildList.add(site);
					if (crossClientId != null) {
						topLicensingChildList.add(client + "," + crossClientId.toString().replace("'", ""));
						topLicensingChildList.add(clientName + "," + crossClientName.toString().replace("'", ""));
					} else if (!scrossClientList.isEmpty()) {
						topLicensingChildList
								.add(client + "," + scrossClientList.toString().replace("['", "").replace("']", ""));
						topLicensingChildList.add(
								clientName + "," + crossClientNamesList.toString().replace("['", "").replace("']", ""));
					} else {
						topLicensingChildList.add(client);
						topLicensingChildList.add(clientName);
					}

					topLicensingChildList.add(t.get(0));
					String[] arr = null;
					arr = t.get(1).split(":");
					String configuration = arr[1];
					topLicensingChildList.add(configuration);
					String[] arr1 = null;
					arr1 = t.get(2).split(":");
					String productUserCodeUI = arr1[1];
					topLicensingChildList.add(productUserCodeUI);

					Boolean valuePresent = false;
					for (Map.Entry<String, String> map : mapTopSyncRoyaltySource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topLicensingChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topLicensingChildList.add("");
						valuePresent = false;
					}

					Map<String, String> statusMap = new HashMap<String, String>();
					statusMap = getStatusMapWidget(topLicensingChildList);

					for (Map.Entry<String, String> map3 : statusMap.entrySet()) {
						if (map3.getKey().matches(Constants.statusPass)) {
							topLicensingChildList.add(Constants.statusPass);
							topLicensingChildList.add("");
						} else if (map3.getKey().matches(Constants.statusFail)) {
							topLicensingChildList.add(Constants.statusFail);
							topLicensingChildList.add(map3.getValue());
						}
					}
					topLicensingProductFinalList.addAll(topSyncChildList);
				}
			} else {
				ArrayList<String> topSyncChildList = new ArrayList<String>();

				topSyncChildList.add(site);
				if (crossClientId != null) {
					String crossClient[] = crossClientId.split(",");
					topSyncChildList.add(client + "," + crossClient[0].toString().replace("'", ""));
					topSyncChildList.add(clientName + "," + crossClient[1].toString().replace("'", ""));
				} else if (!scrossClientList.isEmpty()) {
					topSyncChildList
							.add(client + "," + scrossClientList.get(0).toString().replace("['", "").replace("']", ""));
					topSyncChildList.add(
							clientName + "," + scrossClientList.get(1).toString().replace("['", "").replace("']", ""));
				} else {
					topSyncChildList.add(client);
					topSyncChildList.add(clientName);
				}

				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add(Constants.noDataOnUIMessage);
				topSyncProductFinalList.add(topSyncChildList);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchTopTracks :" + e);
		}

	}

	// @Then("^fetch Data from Db$")
	public void fetchDataFromDBForTopSyncProducts(String client, ArrayList<String> scrossClientList, String site,
			String crossClientId, String periodTopTrackUI2)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		try {
			mapTopSyncConfigDest = new HashMap<>();
			mapTopSyncConfigSource = new HashMap<>();
			mapTopSyncPeriodDest = new HashMap<>();
			mapTopSyncPeriodSource = new HashMap<>();
			mapTopSyncProductTitleDest = new HashMap<>();
			mapTopSyncProductTitleSource = new HashMap<>();
			mapTopSyncRoyaltyDest = new HashMap<>();
			mapTopSyncRoyaltySource = new HashMap<>();
			topSyncProductCodeSource = new ArrayList<String>();
			topSyncProductCodeDest = new ArrayList<String>();

			dbQueries_TopSyncProducts.DB_Query(site, client, scrossClientList, crossClientId, periodTopTrackUI2);
			mapTopSyncPeriodDest = dbQueries_TopSyncProducts.mapTopSyncPeriodDest;
			mapTopSyncProductTitleDest = dbQueries_TopSyncProducts.mapTopSyncProductTitleDest;
			mapTopSyncConfigDest = dbQueries_TopSyncProducts.mapTopSyncConfigDest;
			topSyncProductCodeDest = dbQueries_TopSyncProducts.topSyncProductCodeDest;
			mapTopSyncRoyaltyDest = dbQueries_TopSyncProducts.mapTopSyncRoyaltyDest;

			mapTopSyncPeriodSource = dbQueries_TopSyncProducts.mapTopSyncPeriodSource;
			mapTopSyncProductTitleSource = dbQueries_TopSyncProducts.mapTopSyncProductTitleSource;
			mapTopSyncConfigSource = dbQueries_TopSyncProducts.mapTopSyncConfigSource;
			topSyncProductCodeSource = dbQueries_TopSyncProducts.topSyncProductCodeSource;
			mapTopSyncRoyaltySource = dbQueries_TopSyncProducts.mapTopSyncRoyaltySource;

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchDataFromDBForTopLicensingProducts :" + e);
		}
	}

	// @Given("^fetch Top Public Performance Products from UI$")
	public void fetchTopPPProductsFromUIandDb(String client, ArrayList<String> scrossClientList, String crossClientId,
			String site, String clientName)
			throws InterruptedException, ClassNotFoundException, SQLException, IOException {
		try {
			periodTopPPUI = null;
			userSelectClient.getTopPPProductsUI();
			periodTopPPUI = userSelectClient.periodTopPPProducts;
			List<List<String>> topPPProductList = new ArrayList<List<String>>();
			if (periodTopPPUI != null) {
				topPPProductList = userSelectClient.topPPProductList;
				fetchDataFromDBForTopPPProducts(client, scrossClientList, site, crossClientId, periodTopSyncUI);
				for (List<String> t : topPPProductList) {
					ArrayList<String> topPPChildList = new ArrayList<String>();

					topPPChildList.add(site);
					if (crossClientId != null) {
						String crossClient[] = crossClientId.split(",");
						topPPChildList.add(client + "," + crossClient[0].toString().replace("'", ""));
						topPPChildList.add(clientName + "," + crossClient[1].toString().replace("'", ""));
					} else if (!scrossClientList.isEmpty()) {
						topPPChildList.add(
								client + "," + scrossClientList.get(0).toString().replace("['", "").replace("']", ""));
						topPPChildList.add(clientName + ","
								+ scrossClientList.get(1).toString().replace("['", "").replace("']", ""));
					} else {
						topPPChildList.add(client);
						topPPChildList.add(clientName);
					}

					topPPChildList.add(periodTopSyncUI);
					topPPChildList.add(t.get(0));
					String[] arr = null;
					arr = t.get(1).split(":");
					String configuration = arr[1];
					topPPChildList.add(configuration);
					String[] arr1 = null;
					arr1 = t.get(2).split(":");
					String productUserCodeUI = arr1[1];
					topPPChildList.add(productUserCodeUI);

					String arr2[] = null;
					arr2 = t.get(3).split(" ");
					String currencyUI = arr2[0];
					String royaltyUI = arr2[1];

					topPPChildList.add(currencyUI);
					topPPChildList.add(royaltyUI);

					Boolean valuePresent = false;

					for (Map.Entry<String, String> map : mapTopPPPeriodDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());

							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopPPProductTitleDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());

							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {

						topPPChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopPPConfigDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());

							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {

						topPPChildList.add("");
						valuePresent = false;
					}
					for (String isrc : topPPProductCodeDest) {
						if (isrc.trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(isrc);

							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopPPRoyaltyDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {

						topPPChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopPPPeriodSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopPPProductTitleSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopPPConfigSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}
					for (String isrc : topPPProductCodeSource) {
						if (isrc.trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(isrc);
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopPPRoyaltySource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}

					Map<String, String> statusMap = new HashMap<String, String>();

					statusMap = getStatusMapWidget(topLicensingChildList);

					for (Map.Entry<String, String> map3 : statusMap.entrySet()) {
						if (map3.getKey().matches(Constants.statusPass)) {
							topLicensingChildList.add(Constants.statusPass);
							topLicensingChildList.add("");
						} else if (map3.getKey().matches(Constants.statusFail)) {
							topLicensingChildList.add(Constants.statusFail);
							topLicensingChildList.add(map3.getValue());
						}
					}
					topLicensingProductFinalList.add(topLicensingChildList);
				}
			} else {
				ArrayList<String> topLicensingChildList = new ArrayList<String>();

				topLicensingChildList.add(site);

				if (crossClientId != null) {
					topLicensingChildList.add(client + "," + crossClientId);
				} else if (!scrossClientList.isEmpty()) {
					topLicensingChildList
							.add(client + "," + scrossClientList.toString().replace("[", "").replace("]", ""));
				} else {
					topLicensingChildList.add(client);
				}
				topLicensingChildList.add(clientName);
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add("");
				topLicensingChildList.add(Constants.noDataOnUIMessage);
				topLicensingProductFinalList.add(topLicensingChildList);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchTopTracks :" + e);
		}

	}

	private Map<String, String> getStatusMapWidget(ArrayList<String> topWidgetList) {
		Map<String, String> statusMap = new HashMap<String, String>();
		if (topWidgetList.get(10).trim().equalsIgnoreCase(topWidgetList.get(15).trim())) {
			if (topWidgetList.get(10).trim().equalsIgnoreCase(topWidgetList.get(4).trim())) {
				if (topWidgetList.get(11).trim().equalsIgnoreCase(topWidgetList.get(16).trim())) {
					if (topWidgetList.get(11).trim().equalsIgnoreCase(topWidgetList.get(5).trim())) {
						if (topWidgetList.get(12).trim().equalsIgnoreCase(topWidgetList.get(17).trim())) {
							if (topWidgetList.get(12).trim().equalsIgnoreCase(topWidgetList.get(6).trim())) {
								statusMap = getTestCaseStatus(topWidgetList.get(8).trim(), topWidgetList.get(13).trim(),
										topWidgetList.get(18).trim());
							} else {
								statusMap.put("fail", Constants.productCodeErrMessage2);
							}

						} else {
							statusMap.put("fail", Constants.productCodeErrMessage1);
						}

					} else {
						statusMap.put("fail", Constants.configErrMessage2);
					}

				} else {
					statusMap.put("fail", Constants.configErrMessage1);
				}
			} else {
				statusMap.put("fail", Constants.errMessage1);
			}

		} else {
			statusMap.put("fail", Constants.errMessage3);
		}
		return statusMap;
	}

	// @Then("^fetch Data from Db$")
	public void fetchDataFromDBForTopLicensingProducts(String client, ArrayList<String> scrossClientList, String site,
			String crossClientId, String periodTopTrackUI2)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		try {
			mapTopLicensingConfigDest = new HashMap<>();
			mapTopLicensingConfigSource = new HashMap<>();
			mapTopLicensingPeriodDest = new HashMap<>();
			mapTopLicensingPeriodSource = new HashMap<>();
			mapTopLicensingProductTitleDest = new HashMap<>();
			mapTopLicensingProductTitleSource = new HashMap<>();
			mapTopLicensingRoyaltyDest = new HashMap<>();
			mapTopLicensingRoyaltySource = new HashMap<>();
			topLicensingProductCodeSource = new ArrayList<String>();
			topLicensingProductCodeDest = new ArrayList<String>();

			dbQueries_TopLicensingProducts.DB_Query(site, client, scrossClientList, crossClientId, periodTopTrackUI2);
			mapTopLicensingPeriodDest = dbQueries_TopLicensingProducts.mapTopLicensingPeriodDest;
			mapTopLicensingProductTitleDest = dbQueries_TopLicensingProducts.mapTopLicensingProductTitleDest;
			mapTopLicensingConfigDest = dbQueries_TopLicensingProducts.mapTopLicensingConfigDest;
			topLicensingProductCodeDest = dbQueries_TopLicensingProducts.topLicensingProductCodeDest;
			mapTopLicensingRoyaltyDest = dbQueries_TopLicensingProducts.mapTopLicensingRoyaltyDest;

			mapTopLicensingPeriodSource = dbQueries_TopLicensingProducts.mapTopLicensingPeriodSource;
			mapTopLicensingProductTitleSource = dbQueries_TopLicensingProducts.mapTopLicensingProductTitleSource;
			mapTopLicensingConfigSource = dbQueries_TopLicensingProducts.mapTopLicensingConfigSource;
			topLicensingProductCodeSource = dbQueries_TopLicensingProducts.topLicensingProductCodeSource;
			mapTopLicensingRoyaltySource = dbQueries_TopLicensingProducts.mapTopLicensingRoyaltySource;

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchDataFromDBForTopLicensingProducts :" + e);
		}
	}

	// @Given("^fetch Top Sync Products from UI$")
	public void fetchTopSyncProductsFromUIandDb(String client, String clientName, ArrayList<String> scrossClientList,
			ArrayList<String> crossClientNamesList, String crossClientName, String crossClientId, String site)
			throws InterruptedException, ClassNotFoundException, SQLException, IOException {
		try {
			periodTopSyncUI = null;
			userSelectClient.getTopSyncProductsUI();
			periodTopSyncUI = userSelectClient.periodTopSyncProducts;
			List<List<String>> topSyncProductList = new ArrayList<List<String>>();
			if (periodTopSyncUI != null) {
				topSyncProductList = userSelectClient.topSyncProductList;
				fetchDataFromDBForTopSyncProducts(client, scrossClientList, site, crossClientId, periodTopSyncUI);
				for (List<String> t : topSyncProductList) {
					ArrayList<String> topSyncChildList = new ArrayList<String>();

					topSyncChildList.add(site);
					if (crossClientId != null) {
						topSyncChildList.add(client + "," + crossClientId.toString().replace("'", ""));
						topSyncChildList.add(clientName + "," + crossClientName.toString().replace("'", ""));
					} else if (!scrossClientList.isEmpty()) {
						topSyncChildList
								.add(client + "," + scrossClientList.toString().replace("['", "").replace("']", ""));
						topSyncChildList.add(
								clientName + "," + crossClientNamesList.toString().replace("['", "").replace("']", ""));
					} else {
						topSyncChildList.add(client);
						topSyncChildList.add(clientName);
					}

//				if (crossClientId != null) {
//					topLicensingChildList.add(client + "," + crossClientId);
//				} else if (!scrossClientList.isEmpty()) {
//					topLicensingChildList
//							.add(client + "," + scrossClientList.toString().replace("[", "").replace("]", ""));
//				} else {
//					topLicensingChildList.add(client);
//				}
//				
//				topLicensingChildList.add(clientName);

					topSyncChildList.add(periodTopSyncUI);
					topSyncChildList.add(t.get(0));
					String[] arr = null;
					arr = t.get(1).split(":");
					String configuration = arr[1];
					topSyncChildList.add(configuration);
					String[] arr1 = null;
					arr1 = t.get(2).split(":");
					String productUserCodeUI = arr1[1];
					topSyncChildList.add(productUserCodeUI);
					String arr2[] = null;
					arr2 = t.get(3).split(" ");
					String currencyUI = arr2[0];
					String royaltyUI = arr2[1];
					topSyncChildList.add(currencyUI);
					topSyncChildList.add(royaltyUI);

					Boolean valuePresent = false;

					for (Map.Entry<String, String> map : mapTopSyncPeriodDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopSyncProductTitleDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopSyncConfigDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}
					for (String isrc : topSyncProductCodeDest) {
						if (isrc.trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(isrc);
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopSyncRoyaltyDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopSyncPeriodSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopSyncProductTitleSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopSyncConfigSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}
					for (String isrc : topSyncProductCodeSource) {
						if (isrc.trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(isrc);
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopSyncRoyaltySource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topSyncChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topSyncChildList.add("");
						valuePresent = false;
					}

					Map<String, String> statusMap = new HashMap<String, String>();
					statusMap = getStatusMapWidget(topSyncChildList);

					for (Map.Entry<String, String> map3 : statusMap.entrySet()) {
						if (map3.getKey().matches(Constants.statusPass)) {
							topSyncChildList.add(Constants.statusPass);
							topSyncChildList.add("");
						} else if (map3.getKey().matches(Constants.statusFail)) {
							topSyncChildList.add(Constants.statusFail);
							topSyncChildList.add(map3.getValue());
						}
					}
					topSyncProductFinalList.add(topSyncChildList);
				}
			} else {
				ArrayList<String> topSyncChildList = new ArrayList<String>();

				topSyncChildList.add(site);
				if (crossClientId != null) {
					String crossClient[] = crossClientId.split(",");
					topSyncChildList.add(client + "," + crossClient[0].toString().replace("'", ""));
					topSyncChildList.add(clientName + "," + crossClient[1].toString().replace("'", ""));
				} else if (!scrossClientList.isEmpty()) {
					topSyncChildList
							.add(client + "," + scrossClientList.get(0).toString().replace("['", "").replace("']", ""));
					topSyncChildList.add(
							clientName + "," + scrossClientList.get(1).toString().replace("['", "").replace("']", ""));
				} else {
					topSyncChildList.add(client);
					topSyncChildList.add(clientName);
				}

				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add("");
				topSyncChildList.add(Constants.noDataOnUIMessage);
				topSyncProductFinalList.add(topSyncChildList);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchTopTracks :" + e);
		}

	}

	// @Given("^fetch Top Public Performance Products from UI$")
	public void fetchTopPPProductsFromUIandDb(String client, String clientName, ArrayList<String> scrossClientList,
			ArrayList<String> crossClientNamesList, String crossClientName, String crossClientId, String site)
			throws InterruptedException, ClassNotFoundException, SQLException, IOException {
		try {
			periodTopPPUI = null;
			userSelectClient.getTopPPProductsUI();
			periodTopPPUI = userSelectClient.periodTopPPProducts;
			List<List<String>> topPPProductList = new ArrayList<List<String>>();
			if (periodTopPPUI != null) {
				topPPProductList = userSelectClient.topPPProductList;
				fetchDataFromDBForTopPPProducts(client, scrossClientList, site, crossClientId, periodTopSyncUI);
				for (List<String> t : topPPProductList) {
					ArrayList<String> topPPChildList = new ArrayList<String>();

					topPPChildList.add(site);
					if (crossClientId != null) {
						topPPChildList.add(client + "," + crossClientId.toString().replace("'", ""));
						topPPChildList.add(clientName + "," + crossClientName.toString().replace("'", ""));
					} else if (!scrossClientList.isEmpty()) {
						topPPChildList
								.add(client + "," + scrossClientList.toString().replace("['", "").replace("']", ""));
						topPPChildList.add(
								clientName + "," + crossClientNamesList.toString().replace("['", "").replace("']", ""));
					} else {
						topPPChildList.add(client);
						topPPChildList.add(clientName);
					}

//				if (crossClientId != null) {
//					topLicensingChildList.add(client + "," + crossClientId);
//				} else if (!scrossClientList.isEmpty()) {
//					topLicensingChildList
//							.add(client + "," + scrossClientList.toString().replace("[", "").replace("]", ""));
//				} else {
//					topLicensingChildList.add(client);
//				}
//				
//				topLicensingChildList.add(clientName);

					topPPChildList.add(periodTopSyncUI);
					topPPChildList.add(t.get(0));
					String[] arr = null;
					arr = t.get(1).split(":");
					String configuration = arr[1];
					topPPChildList.add(configuration);
					String[] arr1 = null;
					arr1 = t.get(2).split(":");
					String productUserCodeUI = arr1[1];
					topPPChildList.add(productUserCodeUI);
					String arr2[] = null;
					arr2 = t.get(3).split(" ");
					String currencyUI = arr2[0];
					String royaltyUI = arr2[1];
					topPPChildList.add(currencyUI);
					topPPChildList.add(royaltyUI);

					Boolean valuePresent = false;

					for (Map.Entry<String, String> map : mapTopPPPeriodDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopPPProductTitleDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopPPConfigDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}
					for (String isrc : topPPProductCodeDest) {
						if (isrc.trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(isrc);
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopPPRoyaltyDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopPPPeriodSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopPPProductTitleSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopPPConfigSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}
					for (String isrc : topPPProductCodeSource) {
						if (isrc.trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(isrc);
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopPPRoyaltySource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topPPChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPPChildList.add("");
						valuePresent = false;
					}

					Map<String, String> statusMap = new HashMap<String, String>();
					statusMap = getStatusMapWidget(topPPChildList);

					for (Map.Entry<String, String> map3 : statusMap.entrySet()) {
						if (map3.getKey().matches(Constants.statusPass)) {
							topPPChildList.add(Constants.statusPass);
							topPPChildList.add("");
						} else if (map3.getKey().matches(Constants.statusFail)) {
							topPPChildList.add(Constants.statusFail);
							topPPChildList.add(map3.getValue());
						}
					}
					topPPProductFinalList.add(topPPChildList);
				}
			} else {
				ArrayList<String> topPPChildList = new ArrayList<String>();

				topPPChildList.add(site);
				if (crossClientId != null) {
					String crossClient[] = crossClientId.split(",");
					topPPChildList.add(client + "," + crossClient[0].toString().replace("'", ""));
					topPPChildList.add(clientName + "," + crossClient[1].toString().replace("'", ""));
				} else if (!scrossClientList.isEmpty()) {
					topPPChildList
							.add(client + "," + scrossClientList.get(0).toString().replace("['", "").replace("']", ""));
					topPPChildList.add(
							clientName + "," + scrossClientList.get(1).toString().replace("['", "").replace("']", ""));
				} else {
					topPPChildList.add(client);
					topPPChildList.add(clientName);
				}

				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add("");
				topPPChildList.add(Constants.noDataOnUIMessage);
				topPPProductFinalList.add(topPPChildList);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchTopPPProducts :" + e);
		}

	}

	// @Then("^fetch Data from Db$")
	public void fetchDataFromDBForTopPPProducts(String client, ArrayList<String> scrossClientList, String site,
			String crossClientId, String periodTopTrackUI2)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		try {
			mapTopPPConfigDest = new HashMap<>();
			mapTopPPConfigSource = new HashMap<>();
			mapTopPPPeriodDest = new HashMap<>();
			mapTopPPPeriodSource = new HashMap<>();
			mapTopPPProductTitleDest = new HashMap<>();
			mapTopPPProductTitleSource = new HashMap<>();
			mapTopPPRoyaltyDest = new HashMap<>();
			mapTopPPRoyaltySource = new HashMap<>();
			topPPProductCodeSource = new ArrayList<String>();
			topPPProductCodeDest = new ArrayList<String>();

			dBQueries_Top_PP_Products.DB_Query(site, client, scrossClientList, crossClientId, periodTopTrackUI2);
			mapTopPPPeriodDest = dBQueries_Top_PP_Products.mapTopPPPeriodDest;
			mapTopPPProductTitleDest = dBQueries_Top_PP_Products.mapTopPPProductTitleDest;
			mapTopPPConfigDest = dBQueries_Top_PP_Products.mapTopPPConfigDest;
			topPPProductCodeDest = dBQueries_Top_PP_Products.topPPProductCodeDest;
			mapTopPPRoyaltyDest = dBQueries_Top_PP_Products.mapTopPPRoyaltyDest;

			mapTopPPPeriodSource = dBQueries_Top_PP_Products.mapTopPPPeriodSource;
			mapTopPPProductTitleSource = dBQueries_Top_PP_Products.mapTopPPProductTitleSource;
			mapTopPPConfigSource = dBQueries_Top_PP_Products.mapTopPPConfigSource;
			topPPProductCodeSource = dBQueries_Top_PP_Products.topPPProductCodeSource;
			mapTopPPRoyaltySource = dBQueries_Top_PP_Products.mapTopPPRoyaltySource;

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchDataFromDBForTopPPProducts :" + e);
		}
	}

	// @Given("^fetch Top Merchandise Products from UI$")
	public void fetchTopMrchndiseProductsFromUIandDb(String client, String clientName,
			ArrayList<String> scrossClientList, ArrayList<String> crossClientNamesList, String crossClientName,
			String crossClientId, String site)
			throws InterruptedException, ClassNotFoundException, SQLException, IOException {
		try {
			periodTopMrchndiseUI = null;
			userSelectClient.getTopMrchndiseProductsUI();
			periodTopMrchndiseUI = userSelectClient.periodTopMrchndiseProducts;
			List<List<String>> topMrchndiseProductList = new ArrayList<List<String>>();
			if (periodTopMrchndiseUI != null) {
				topMrchndiseProductList = userSelectClient.topMrchndiseProductList;
				fetchDataFromDBForTopMrchndiseProducts(client, scrossClientList, site, crossClientId,
						periodTopMrchndiseUI);
				for (List<String> t : topMrchndiseProductList) {
					ArrayList<String> topMrchndiseChildList = new ArrayList<String>();

					topMrchndiseChildList.add(site);
					if (crossClientId != null) {
						topMrchndiseChildList.add(client + "," + crossClientId.toString().replace("'", ""));
						topMrchndiseChildList.add(clientName + "," + crossClientName.toString().replace("'", ""));
					} else if (!scrossClientList.isEmpty()) {
						topMrchndiseChildList
								.add(client + "," + scrossClientList.toString().replace("['", "").replace("']", ""));
						topMrchndiseChildList.add(
								clientName + "," + crossClientNamesList.toString().replace("['", "").replace("']", ""));
					} else {
						topMrchndiseChildList.add(client);
						topMrchndiseChildList.add(clientName);
					}

//				if (crossClientId != null) {
//					topLicensingChildList.add(client + "," + crossClientId);
//				} else if (!scrossClientList.isEmpty()) {
//					topLicensingChildList
//							.add(client + "," + scrossClientList.toString().replace("[", "").replace("]", ""));
//				} else {
//					topLicensingChildList.add(client);
//				}
//				
//				topLicensingChildList.add(clientName);

					topMrchndiseChildList.add(periodTopMrchndiseUI);
					topMrchndiseChildList.add(t.get(0));
					String[] arr = null;
					arr = t.get(1).split(":");
					String configuration = arr[1];
					topMrchndiseChildList.add(configuration);
					String[] arr1 = null;
					arr1 = t.get(2).split(":");
					String productUserCodeUI = arr1[1];
					topMrchndiseChildList.add(productUserCodeUI);
					String arr2[] = null;
					arr2 = t.get(3).split(" ");
					String currencyUI = arr2[0];
					String royaltyUI = arr2[1];
					topMrchndiseChildList.add(currencyUI);
					topMrchndiseChildList.add(royaltyUI);

					Boolean valuePresent = false;

					for (Map.Entry<String, String> map : mapTopMrchndisePeriodDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topMrchndiseChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topMrchndiseChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopMrchndiseProductTitleDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topMrchndiseChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topMrchndiseChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopMrchndiseConfigDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topMrchndiseChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topMrchndiseChildList.add("");
						valuePresent = false;
					}
					for (String isrc : topMrchndiseProductCodeDest) {
						if (isrc.trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topMrchndiseChildList.add(isrc);
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topMrchndiseChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopMrchndiseRoyaltyDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topMrchndiseChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topMrchndiseChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopMrchndisePeriodSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topMrchndiseChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topMrchndiseChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopMrchndiseProductTitleSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topMrchndiseChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topMrchndiseChildList.add("");
						valuePresent = false;
					}
					for (Map.Entry<String, String> map : mapTopMrchndiseConfigSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topMrchndiseChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topMrchndiseChildList.add("");
						valuePresent = false;
					}
					for (String isrc : topMrchndiseProductCodeSource) {
						if (isrc.trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topMrchndiseChildList.add(isrc);
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topMrchndiseChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopMrchndiseRoyaltySource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(productUserCodeUI.trim())) {
							topMrchndiseChildList.add(map.getValue());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topMrchndiseChildList.add("");
						valuePresent = false;
					}

					Map<String, String> statusMap = new HashMap<String, String>();
					statusMap = getStatusMapWidget(topMrchndiseChildList);

					for (Map.Entry<String, String> map3 : statusMap.entrySet()) {
						if (map3.getKey().matches(Constants.statusPass)) {
							topMrchndiseChildList.add(Constants.statusPass);
							topMrchndiseChildList.add("");
						} else if (map3.getKey().matches(Constants.statusFail)) {
							topMrchndiseChildList.add(Constants.statusFail);
							topMrchndiseChildList.add(map3.getValue());
						}
					}
					topMrchndiseProductFinalList.add(topMrchndiseChildList);
				}
			} else {
				ArrayList<String> topMrchndiseChildList = new ArrayList<String>();

				topMrchndiseChildList.add(site);
				if (crossClientId != null) {
					String crossClient[] = crossClientId.split(",");
					topMrchndiseChildList.add(client + "," + crossClient[0].toString().replace("'", ""));
					topMrchndiseChildList.add(clientName + "," + crossClient[1].toString().replace("'", ""));
				} else if (!scrossClientList.isEmpty()) {
					topMrchndiseChildList
							.add(client + "," + scrossClientList.get(0).toString().replace("['", "").replace("']", ""));
					topMrchndiseChildList.add(
							clientName + "," + scrossClientList.get(1).toString().replace("['", "").replace("']", ""));
				} else {
					topMrchndiseChildList.add(client);
					topMrchndiseChildList.add(clientName);
				}

				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add("");
				topMrchndiseChildList.add(Constants.noDataOnUIMessage);
				topMrchndiseProductFinalList.add(topMrchndiseChildList);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchTopMerchandise Products" + e);
		}

	}

	// @Then("^fetch Data from Db$")
	public void fetchDataFromDBForTopMrchndiseProducts(String client, ArrayList<String> scrossClientList, String site,
			String crossClientId, String periodTopTrackUI2)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		try {
			mapTopMrchndiseConfigDest = new HashMap<>();
			mapTopMrchndiseConfigSource = new HashMap<>();
			mapTopMrchndisePeriodDest = new HashMap<>();
			mapTopMrchndisePeriodSource = new HashMap<>();
			mapTopMrchndiseProductTitleDest = new HashMap<>();
			mapTopMrchndiseProductTitleSource = new HashMap<>();
			mapTopMrchndiseRoyaltyDest = new HashMap<>();
			mapTopMrchndiseRoyaltySource = new HashMap<>();
			topMrchndiseProductCodeSource = new ArrayList<String>();
			topMrchndiseProductCodeDest = new ArrayList<String>();

			dbQueries_Top_Mrchndise_Products.DB_Query(site, client, scrossClientList, crossClientId, periodTopTrackUI2);
			mapTopMrchndisePeriodDest = dbQueries_Top_Mrchndise_Products.mapTopMrchndisePeriodDest;
			mapTopMrchndiseProductTitleDest = dbQueries_Top_Mrchndise_Products.mapTopMrchndiseProductTitleDest;
			mapTopMrchndiseConfigDest = dbQueries_Top_Mrchndise_Products.mapTopMrchndiseConfigDest;
			topMrchndiseProductCodeDest = dbQueries_Top_Mrchndise_Products.topMrchndiseProductCodeDest;
			mapTopMrchndiseRoyaltyDest = dbQueries_Top_Mrchndise_Products.mapTopMrchndiseRoyaltyDest;

			mapTopMrchndisePeriodSource = dbQueries_Top_Mrchndise_Products.mapTopMrchndisePeriodSource;
			mapTopMrchndiseProductTitleSource = dbQueries_Top_Mrchndise_Products.mapTopMrchndiseProductTitleSource;
			mapTopMrchndiseConfigSource = dbQueries_Top_Mrchndise_Products.mapTopMrchndiseConfigSource;
			topMrchndiseProductCodeSource = dbQueries_Top_Mrchndise_Products.topMrchndiseProductCodeSource;
			mapTopMrchndiseRoyaltySource = dbQueries_Top_Mrchndise_Products.mapTopMrchndiseRoyaltySource;

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchDataFromDBForTopMrchndiseProducts :" + e);
		}
	}

	// @Given("^fetch Top Performing Territories from UI$")
	public void fetchTopPerformTerrFromUIandDb(String client, String clientName, ArrayList<String> scrossClientList,
			ArrayList<String> crossClientNamesList, String crossClientName, String crossClientId, String site)
			throws InterruptedException, ClassNotFoundException, SQLException, IOException {
		try {
			periodTopPerformTerrUI = null;
			userSelectClient.getTopPerformTerrUI();
			periodTopPerformTerrUI = userSelectClient.periodTopPerformTerr;
			List<List<String>> topPerformTerrList = new ArrayList<List<String>>();
			if (periodTopPerformTerrUI != null) {
				topPerformTerrList = userSelectClient.topPerformTerrList;
				fetchDataFromDBForTopPerformTerr(client, scrossClientList, site, crossClientId, periodTopMrchndiseUI);
				for (List<String> t : topPerformTerrList) {
					ArrayList<String> topPerformTerrChildList = new ArrayList<String>();

					topPerformTerrChildList.add(site);
					if (crossClientId != null) {
						topPerformTerrChildList.add(client + "," + crossClientId.toString().replace("'", ""));
						topPerformTerrChildList.add(clientName + "," + crossClientName.toString().replace("'", ""));
					} else if (!scrossClientList.isEmpty()) {
						topPerformTerrChildList
								.add(client + "," + scrossClientList.toString().replace("['", "").replace("']", ""));
						topPerformTerrChildList.add(
								clientName + "," + crossClientNamesList.toString().replace("['", "").replace("']", ""));
					} else {
						topPerformTerrChildList.add(client);
						topPerformTerrChildList.add(clientName);
					}

//				if (crossClientId != null) {
//					topLicensingChildList.add(client + "," + crossClientId);
//				} else if (!scrossClientList.isEmpty()) {
//					topLicensingChildList
//							.add(client + "," + scrossClientList.toString().replace("[", "").replace("]", ""));
//				} else {
//					topLicensingChildList.add(client);
//				}
//				
//				topLicensingChildList.add(clientName);

					topPerformTerrChildList.add(periodTopPerformTerrUI);
					String territoryNameUI = t.get(0);
					topPerformTerrChildList.add(territoryNameUI);
					String[] arr = null;
					arr = t.get(1).split(" ");
					String currency = arr[0];
					String royaltyUI = arr[1];
					topPerformTerrChildList.add(currency);
					topPerformTerrChildList.add(royaltyUI);

					Boolean valuePresent = false;

					for (Map.Entry<String, String> map : mapTopPerformTerrPeriodDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(territoryNameUI.trim())) {
							topPerformTerrChildList.add(map.getValue().trim());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPerformTerrChildList.add("");
						valuePresent = false;
					}

					for (String territoryName : mapTopPerformTerrNameDest) {
						if (territoryName.trim().equalsIgnoreCase(territoryNameUI.trim())) {
							topPerformTerrChildList.add(territoryName.trim());
							valuePresent = true;
							break;
						}
					}

					if (valuePresent != true) {
						topPerformTerrChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopPerformTerrRoyaltyDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(territoryNameUI.trim())) {
							topPerformTerrChildList.add(map.getValue().trim());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPerformTerrChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapTopPerformTerrPeriodSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(territoryNameUI.trim())) {
							topPerformTerrChildList.add(map.getValue().trim());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPerformTerrChildList.add("");
						valuePresent = false;
					}
					for (String isrc : mapTopPerformTerrNameSource) {
						if (isrc.trim().equalsIgnoreCase(territoryNameUI.trim())) {
							topPerformTerrChildList.add(isrc);
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						topPerformTerrChildList.add("");
						valuePresent = false;
					}

					Map<String, String> statusMap = new HashMap<String, String>();
					statusMap = getStatusTopPerformingTerr(topPerformTerrChildList);

					for (Map.Entry<String, String> map3 : statusMap.entrySet()) {
						if (map3.getKey().matches(Constants.statusPass)) {
							topPerformTerrChildList.add(Constants.statusPass);
							topPerformTerrChildList.add("");
						} else if (map3.getKey().matches(Constants.statusFail)) {
							topPerformTerrChildList.add(Constants.statusFail);
							topPerformTerrChildList.add(map3.getValue());
						}
					}
					topPerfromTerrFinalList.add(topPerformTerrChildList);
				}
			} else {
				ArrayList<String> topPerformTerrChildList = new ArrayList<String>();

				topPerformTerrChildList.add(site);
				if (crossClientId != null) {
					String crossClient[] = crossClientId.split(",");
					topPerformTerrChildList.add(client + "," + crossClient[0].toString().replace("'", ""));
					topPerformTerrChildList.add(clientName + "," + crossClient[1].toString().replace("'", ""));
				} else if (!scrossClientList.isEmpty()) {
					topPerformTerrChildList
							.add(client + "," + scrossClientList.get(0).toString().replace("['", "").replace("']", ""));
					topPerformTerrChildList.add(
							clientName + "," + scrossClientList.get(1).toString().replace("['", "").replace("']", ""));
				} else {
					topPerformTerrChildList.add(client);
					topPerformTerrChildList.add(clientName);
				}

				topPerformTerrChildList.add("");
				topPerformTerrChildList.add("");
				topPerformTerrChildList.add("");
				topPerformTerrChildList.add("");
				topPerformTerrChildList.add("");
				topPerformTerrChildList.add("");
				topPerformTerrChildList.add("");
				topPerformTerrChildList.add("");
				topPerformTerrChildList.add("");
				topPerformTerrChildList.add("");

				topPerformTerrChildList.add(Constants.noDataOnUIMessage);
				topPerfromTerrFinalList.add(topPerformTerrChildList);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchTopPerfromTerr" + e);
		}

	}

	private Map<String, String> getStatusTopPerformingTerr(ArrayList<String> topPerformTerrChildList) {
		Map<String, String> statusMap = new HashMap<String, String>();
		if (topPerformTerrChildList.get(8).trim().equalsIgnoreCase(topPerformTerrChildList.get(11).trim())) {
			if (topPerformTerrChildList.get(8).trim().equalsIgnoreCase(topPerformTerrChildList.get(4).trim())) {
				statusMap = getTestCaseStatus(topPerformTerrChildList.get(6).trim(),
						topPerformTerrChildList.get(9).trim(), topPerformTerrChildList.get(12).trim());
			} else {
				statusMap.put(Constants.statusFail, Constants.terrNameErrMsg2);
			}

		} else {
			statusMap.put(Constants.statusFail, Constants.terrNameErrMsg1);
		}

		return statusMap;
	}

	// @Then("^fetch Data from Db$")
	public void fetchDataFromDBForTopPerformTerr(String client, ArrayList<String> scrossClientList, String site,
			String crossClientId, String periodTopTrackUI2)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		try {
			mapTopPerformTerrRoyaltyDest = new HashMap<>();
			mapTopPerformTerrNameDest = new ArrayList<String>();
			mapTopPerformTerrRoyaltySource = new HashMap<>();
			mapTopPerformTerrNameSource = new ArrayList<String>();

			dbQueries_TopPerformingTerritories.DB_Query(site, client, scrossClientList, crossClientId,
					periodTopTrackUI2);
			mapTopPerformTerrRoyaltyDest = dbQueries_TopPerformingTerritories.mapTopPerformTerrRoyaltyDest;
			mapTopPerformTerrNameDest = dbQueries_TopPerformingTerritories.mapTopPerformTerrNameDest;
			mapTopPerformTerrRoyaltySource = dbQueries_TopPerformingTerritories.mapTopPerformTerrRoyaltySource;
			mapTopPerformTerrNameSource = dbQueries_TopPerformingTerritories.mapTopPerformTerrNameSource;
			mapTopPerformTerrPeriodDest = dbQueries_TopPerformingTerritories.mapTopPerformTerrPeriodDest;
			mapTopPerformTerrPeriodSource = dbQueries_TopPerformingTerritories.mapTopPerformTerrPeriodSource;

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchDataFromDBForTopMrchndiseProducts :" + e);
		}
	}

	// @Given("^fetch Latest Statement from UI$")
	public void fetchLatestStatementFromUIandDb(String client, String clientName, ArrayList<String> scrossClientList,
			ArrayList<String> crossClientNamesList, String crossClientName, String crossClientId, String site)
			throws InterruptedException, ClassNotFoundException, SQLException, IOException {
		try {
			periodLatestStatementUI = null;
			userSelectClient.getTopPerformTerrUI();
			periodLatestStatementUI = userSelectClient.periodTopPerformTerr;
			List<List<String>> latestStmntList = new ArrayList<List<String>>();
			if (periodLatestStatementUI != null) {
				latestStmntList = userSelectClient.latestStatementList;
				fetchDataFromDBForTopPerformTerr(client, scrossClientList, site, crossClientId,
						periodLatestStatementUI);
				for (List<String> t : latestStmntList) {
					ArrayList<String> latestStmntListChildList = new ArrayList<String>();

					latestStmntListChildList.add(site);
					if (crossClientId != null) {
						latestStmntListChildList.add(client + "," + crossClientId.toString().replace("'", ""));
						latestStmntListChildList.add(clientName + "," + crossClientName.toString().replace("'", ""));
					} else if (!scrossClientList.isEmpty()) {
						latestStmntListChildList
								.add(client + "," + scrossClientList.toString().replace("['", "").replace("']", ""));
						latestStmntListChildList.add(
								clientName + "," + crossClientNamesList.toString().replace("['", "").replace("']", ""));
					} else {
						latestStmntListChildList.add(client);
						latestStmntListChildList.add(clientName);
					}

//				if (crossClientId != null) {
//					topLicensingChildList.add(client + "," + crossClientId);
//				} else if (!scrossClientList.isEmpty()) {
//					topLicensingChildList
//							.add(client + "," + scrossClientList.toString().replace("[", "").replace("]", ""));
//				} else {
//					topLicensingChildList.add(client);
//				}
//				
//				topLicensingChildList.add(clientName);

					latestStmntListChildList.add(periodLatestStatementUI);
					String code = t.get(0);
					latestStmntListChildList.add(code);
					latestStmntListChildList.add(t.get(1));
					latestStmntListChildList.add(t.get(2));
					latestStmntListChildList.add(t.get(3));

					Boolean valuePresent = false;

					for (Map.Entry<String, String> map : mapLatestStatementPeriodDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(code.trim())) {
							latestStmntListChildList.add(map.getValue().trim());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						latestStmntListChildList.add("");
						valuePresent = false;
					}

					for (String royaltorCode : latestStatmntRoyaltorCodeDest) {
						if (royaltorCode.trim().equalsIgnoreCase(code.trim())) {
							latestStmntListChildList.add(royaltorCode.trim());
							valuePresent = true;
							break;
						}
					}

					if (valuePresent != true) {
						latestStmntListChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapLatestStatementRoyaltorNameDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(code.trim())) {
							latestStmntListChildList.add(map.getValue().trim());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						latestStmntListChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapLatestStatementRoyaltyDest.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(code.trim())) {
							latestStmntListChildList.add(map.getValue().trim());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						latestStmntListChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapLatestStatementPeriodSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(code.trim())) {
							latestStmntListChildList.add(map.getValue().trim());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						latestStmntListChildList.add("");
						valuePresent = false;
					}

					for (String royaltorCode : latestStatmntRoyaltorCodeSource) {
						if (royaltorCode.trim().equalsIgnoreCase(code.trim())) {
							latestStmntListChildList.add(royaltorCode.trim());
							valuePresent = true;
							break;
						}
					}

					if (valuePresent != true) {
						latestStmntListChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapLatestStatementRoyaltorNameSource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(code.trim())) {
							latestStmntListChildList.add(map.getValue().trim());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						latestStmntListChildList.add("");
						valuePresent = false;
					}

					for (Map.Entry<String, String> map : mapLatestStatementRoyaltySource.entrySet()) {
						if (map.getKey().trim().equalsIgnoreCase(code.trim())) {
							latestStmntListChildList.add(map.getValue().trim());
							valuePresent = true;
							break;
						}
					}
					if (valuePresent != true) {
						latestStmntListChildList.add("");
						valuePresent = false;
					}

					Map<String, String> statusMap = new HashMap<String, String>();
					statusMap = getStatusLatestStatmnt(latestStmntListChildList);

					for (Map.Entry<String, String> map3 : statusMap.entrySet()) {
						if (map3.getKey().matches(Constants.statusPass)) {
							latestStmntListChildList.add(Constants.statusPass);
							latestStmntListChildList.add("");
						} else if (map3.getKey().matches(Constants.statusFail)) {
							latestStmntListChildList.add(Constants.statusFail);
							latestStmntListChildList.add(map3.getValue());
						}
					}
					latestStatementFinalList.add(latestStmntListChildList);
				}
			} else {
				ArrayList<String> latestStmntListChildList = new ArrayList<String>();

				latestStmntListChildList.add(site);
				if (crossClientId != null) {
					String crossClient[] = crossClientId.split(",");
					latestStmntListChildList.add(client + "," + crossClient[0].toString().replace("'", ""));
					latestStmntListChildList.add(clientName + "," + crossClient[1].toString().replace("'", ""));
				} else if (!scrossClientList.isEmpty()) {
					latestStmntListChildList
							.add(client + "," + scrossClientList.get(0).toString().replace("['", "").replace("']", ""));
					latestStmntListChildList.add(
							clientName + "," + scrossClientList.get(1).toString().replace("['", "").replace("']", ""));
				} else {
					latestStmntListChildList.add(client);
					latestStmntListChildList.add(clientName);
				}

				latestStmntListChildList.add("");
				latestStmntListChildList.add("");
				latestStmntListChildList.add("");
				latestStmntListChildList.add("");
				latestStmntListChildList.add("");
				latestStmntListChildList.add("");
				latestStmntListChildList.add("");
				latestStmntListChildList.add("");
				latestStmntListChildList.add("");
				latestStmntListChildList.add("");
				latestStmntListChildList.add("");
				latestStmntListChildList.add("");
				latestStmntListChildList.add(Constants.noDataOnUIMessage);
				latestStatementFinalList.add(latestStmntListChildList);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchTopPerfromTerr" + e);
		}

	}

	private Map<String, String> getStatusLatestStatmnt(ArrayList<String> latestStmntListChildList) {
		Map<String, String> statusMap = new HashMap<String, String>();

		if (latestStmntListChildList.get(9).trim().equalsIgnoreCase(latestStmntListChildList.get(13).trim())) {
			if (latestStmntListChildList.get(9).trim().equalsIgnoreCase(latestStmntListChildList.get(4).trim())) {
				if (latestStmntListChildList.get(10).trim().equalsIgnoreCase(latestStmntListChildList.get(14).trim())) {
					if (latestStmntListChildList.get(10).trim()
							.equalsIgnoreCase(latestStmntListChildList.get(5).trim())) {
						statusMap = getTestCaseStatus(latestStmntListChildList.get(7).trim(),
								latestStmntListChildList.get(11).trim(), latestStmntListChildList.get(15).trim());
					} else {
						statusMap.put(Constants.statusFail, Constants.royaltorNameErrMsg2);
					}
				} else {
					statusMap.put(Constants.statusFail, Constants.royaltorNameErrMsg1);
				}
			} else {
				statusMap.put(Constants.statusFail, Constants.clientCodeErrMsg1);
			}
		} else {
			statusMap.put(Constants.statusFail, Constants.clientCodeErrMsg1);
		}
		return statusMap;
	}

	// @Then("^fetch Data from Db$")
	public void fetchDataFromDBForLatestStatement(String client, ArrayList<String> scrossClientList, String site,
			String crossClientId, String periodTopTrackUI2)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		try {
			mapLatestStatementRoyaltyDest = new HashMap<String, String>();
			mapLatestStatementRoyaltorNameDest = new HashMap<String, String>();
			mapLatestStatementPeriodDest = new HashMap<String, String>();
			latestStatmntRoyaltorCodeDest = new ArrayList<String>();
			latestStatmntRoyaltorCodeSource = new ArrayList<String>();
			mapLatestStatementPeriodSource = new HashMap<String, String>();
			mapLatestStatementRoyaltySource = new HashMap<String, String>();
			mapLatestStatementRoyaltorNameSource = new HashMap<String, String>();
			dbQueries_LatestStatement.DB_Query(site, client, scrossClientList, crossClientId, periodTopTrackUI2);
			latestStatmntRoyaltorCodeDest = dbQueries_LatestStatement.latestStatmntRoyaltorCodeDest;
			mapLatestStatementPeriodDest = dbQueries_LatestStatement.mapLatestStatementPeriodDest;
			mapLatestStatementRoyaltorNameDest = dbQueries_LatestStatement.mapLatestStatementRoyaltorNameDest;
			mapLatestStatementRoyaltyDest = dbQueries_LatestStatement.mapLatestStatementRoyaltyDest;
			latestStatmntRoyaltorCodeSource = dbQueries_LatestStatement.latestStatmntRoyaltorCodeSource;
			mapLatestStatementPeriodSource = dbQueries_LatestStatement.mapLatestStatementPeriodSource;
			mapLatestStatementRoyaltorNameSource = dbQueries_LatestStatement.mapLatestStatementRoyaltorNameSource;
			mapLatestStatementRoyaltySource = dbQueries_LatestStatement.mapLatestStatementRoyaltySource;

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchDataFromDBForTopMrchndiseProducts :" + e);
		}
	}

	public void fetchStatementBalanceFromUIandDb(String client, String clientName, ArrayList<String> scrossClientList,
			ArrayList<String> crossClientNamesList, String crossClientName, String crossClientId, String site)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {

		try {
			String statementCurrency = null;
			String statementAmount = null;
			String statementPeriod = null;

			userSelectClient.fetchStatementBalanceFromUI(client);
			statementCurrency = userSelectClient.statementCurrency;
			statementPeriod = userSelectClient.statementPeriod;
			statementAmount = userSelectClient.statementAmount;
			ArrayList<String> statementDetailsUI = new ArrayList<String>();

			statementDetailsUI.add(site);

			if (crossClientId != null) {
				statementDetailsUI.add(client + "," + crossClientId.toString().replace("'", ""));
				statementDetailsUI.add(clientName + "," + crossClientName.toString().replace("'", ""));
			} else if (!scrossClientList.isEmpty()) {
				statementDetailsUI.add(client + ","
						+ scrossClientList.toString().replace("['", "").replace("']", "").replace("'", ""));
				statementDetailsUI.add(clientName + ","
						+ crossClientNamesList.toString().replace("['", "").replace("']", "").replace("'", ""));
			} else {
				statementDetailsUI.add(client);
				statementDetailsUI.add(clientName);
			}
			DecimalFormat df = new DecimalFormat("#.000000000000");

			statementDetailsUI.add(statementPeriod);
			statementDetailsUI.add(statementCurrency);
			statementDetailsUI.add(statementAmount);

			fetchDataFromDB(client, scrossClientList, site, crossClientId);
			statementDetailsUI.add(closingPeriodDestiantionDb);
			statementDetailsUI.add(currencyDestinationDb);
			statementDetailsUI.add(df.format(closingBalanceDestinationDb));
			statementDetailsUI.add(periodSourceDb);
			statementDetailsUI.add(df.format(closingBalanceSourceDb) + "");
			Map<String, String> statusMap = new HashMap<String, String>();

			statusMap = getTestCaseStatus(statementAmount, Double.toString(closingBalanceDestinationDb),
					Double.toString(closingBalanceSourceDb));

			for (Map.Entry<String, String> map : statusMap.entrySet()) {
				if (map.getKey().matches(Constants.statusPass)) {
					statementDetailsUI.add(Constants.statusPass);
					statementDetailsUI.add(map.getValue());
					break;

				} else if (map.getKey().matches(Constants.statusFail)) {
					statementDetailsUI.add(Constants.statusFail);
					statementDetailsUI.add(map.getValue());
					break;

				}

			}
			Log.info("Clients: " + statementDetailsUI.get(1));
			Log.info("StatementBalance UI: " + statementCurrency + " " + statementAmount);
			Log.info("StatementBalance Destination DB: " + currencyDestinationDb + " "
					+ df.format(closingBalanceDestinationDb));
			Log.info("StatementBalance Source DB: " + df.format(closingBalanceSourceDb) + "");
			Log.info("StatementBalnce Period UI: " + statementPeriod);
			Log.info("StatementBalance Period Destination DB : " + closingPeriodDestiantionDb);
			Log.info("StatementBalance Period Source DB: " + periodSourceDb);
			statementDataUI.add(statementDetailsUI);
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in Common_Method_Class.fetchStatementBalanceFromUIandDb :" + e);
		}
	}

	public void fetchRoyaltyTrendDonutGraph(String client, String clientName,
			ArrayList<String> scrossClientList, ArrayList<String> crossClientNamesList, String crossClientName,
			String crossClientId, String site, String year, List<String> allYears) {
		try {
			ArrayList<String> royaltyTrendDonutGraphList = null;
			userSelectClient.fetchRoyaltyTrendDonutGraphFromUI(year);
			String totalRoyaltyUI = "";
			ArrayList<String> listDonutGraphUI = new ArrayList<>();
			ArrayList<String> listTitle = new ArrayList<>();
			dbConnectionForRoyaltyTrend.DB_Query_RoyaltyTrendDonutGraph(client, scrossClientList, crossClientId, site,
					year, listTitle);

			for (String uiValue : listDonutGraphUI) {

				royaltyTrendDonutGraphList = new ArrayList<String>();
				royaltyTrendDonutGraphList.add(site);
				if (crossClientId != null) {
					String crossClient[] = crossClientId.split(",");
					royaltyTrendDonutGraphList.add(client + "," + crossClient[0].toString().replace("'", ""));
					royaltyTrendDonutGraphList.add(clientName + "," + crossClient[1].toString().replace("'", ""));
				} else if (!scrossClientList.isEmpty()) {
					royaltyTrendDonutGraphList
							.add(client + "," + scrossClientList.get(0).toString().replace("['", "").replace("']", ""));
					royaltyTrendDonutGraphList.add(
							clientName + "," + scrossClientList.get(1).toString().replace("['", "").replace("']", ""));
				} else {
					royaltyTrendDonutGraphList.add(client);
					royaltyTrendDonutGraphList.add(clientName);
				}

				royaltyTrendDonutGraphList.add(year);

				if (uiValue.contains("Total Royalty")) {
					royaltyTrendDonutGraphList.add("Total Royalty");
					String arr[] = null;
					arr = totalRoyaltyUI.split(" ");
					royaltyTrendDonutGraphList.add(arr[0]);
					royaltyTrendDonutGraphList.add(arr[1]);
					// royaltyTrendDonutGraphList.add(totalRoyaltyUI);
					royaltyTrendDonutGraphList.add(null);
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.totalValueDestDb));
					royaltyTrendDonutGraphList.add(null);
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.totalValueSrcDb));
					royaltyTrendDonutGraphList.add(null);
				} else if (uiValue.contains("Physical")) {
					int i = listDonutGraphUI.indexOf("Physical");
					royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i));
					String arr[] = null;
					arr = listDonutGraphUI.get(i + 1).split(" ");
					royaltyTrendDonutGraphList.add(arr[0]);
					royaltyTrendDonutGraphList.add(arr[1]);
					// royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i+1));
					royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i + 2));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.phyValueDestDb));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.percentagePhyDestDb));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.phyValueSourceDb));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.percentagePhySourceDb));
				} else if (uiValue.contains("Digital")) {
					int i = listDonutGraphUI.indexOf("Digital");
					royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i));
					String arr[] = null;
					arr = listDonutGraphUI.get(i + 1).split(" ");
					royaltyTrendDonutGraphList.add(arr[0]);
					royaltyTrendDonutGraphList.add(arr[1]);
					// royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i+1));
					royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i + 2));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.digitalValueDestDb));
					royaltyTrendDonutGraphList
							.add(Double.toString(dbConnectionForRoyaltyTrend.percentageDigitalDestDb));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.digitalValueSourceDb));
					royaltyTrendDonutGraphList
							.add(Double.toString(dbConnectionForRoyaltyTrend.percentageDigitalSourceDb));
				} else if (uiValue.contains("Licensing")) {
					int i = listDonutGraphUI.indexOf("Licensing");
					royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i));
					String arr[] = null;
					arr = listDonutGraphUI.get(i + 1).split(" ");
					royaltyTrendDonutGraphList.add(arr[0]);
					royaltyTrendDonutGraphList.add(arr[1]);
					// royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i+1));
					royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i + 2));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.licensingValueDestDb));
					royaltyTrendDonutGraphList
							.add(Double.toString(dbConnectionForRoyaltyTrend.percentageLicensingDestDb));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.licensingValueSourceDb));
					royaltyTrendDonutGraphList
							.add(Double.toString(dbConnectionForRoyaltyTrend.percentageLicensingSourceDb));
				} else if (uiValue.contains("Sync")) {
					int i = listDonutGraphUI.indexOf("Sync");
					royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i));
					String arr[] = null;
					arr = listDonutGraphUI.get(i + 1).split(" ");
					royaltyTrendDonutGraphList.add(arr[0]);
					royaltyTrendDonutGraphList.add(arr[1]);
					// royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i+1));
					royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i + 2));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.syncValueDestDb));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.percentageSyncDestDb));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.syncValueSourceDb));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.percentageSyncSourceDb));
				} else if (uiValue.contains("Public Performance")) {
					int i = listDonutGraphUI.indexOf("Public Performance");
					royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i));
					String arr[] = null;
					arr = listDonutGraphUI.get(i + 1).split(" ");
					royaltyTrendDonutGraphList.add(arr[0]);
					royaltyTrendDonutGraphList.add(arr[1]);
					// royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i+1));
					royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i + 2));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.ppValueDestDb));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.percentagePPDestDb));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.ppValueSourceDb));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.percentagePPSourceDb));
				} else if (uiValue.contains("Merchandise")) {
					int i = listDonutGraphUI.indexOf("Merchandise");
					royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i));
					String arr[] = null;
					arr = listDonutGraphUI.get(i + 1).split(" ");
					royaltyTrendDonutGraphList.add(arr[0]);
					royaltyTrendDonutGraphList.add(arr[1]);
					// royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i+1));
					royaltyTrendDonutGraphList.add(listDonutGraphUI.get(i + 2));
					royaltyTrendDonutGraphList.add(Double.toString(dbConnectionForRoyaltyTrend.merchandiseValueDestDb));
					royaltyTrendDonutGraphList
							.add(Double.toString(dbConnectionForRoyaltyTrend.percentageMerchandiseDestDb));
					royaltyTrendDonutGraphList
							.add(Double.toString(dbConnectionForRoyaltyTrend.merchandiseValueSourceDb));
					royaltyTrendDonutGraphList
							.add(Double.toString(dbConnectionForRoyaltyTrend.percentageMerchandiseSourceDb));
				}
				if (uiValue.matches("^([^0-9]*)$")) {
					royaltyTrendDonutGraphFinalList.add(royaltyTrendDonutGraphList);
					LOGGER.info("donut graph list:" + royaltyTrendDonutGraphFinalList);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	public void showComparison(String fileLocation,String testCaseNo,String dataFieldForComparison)
			throws IOException {
	
		try {
		if (dataFieldForComparison.equalsIgnoreCase(Constants.statementComparison)) {
		
			readExcel.writeExcel(fileLocation,testCaseNo, statementDataUI,dataFieldForComparison);
		} else if (dataFieldForComparison.equalsIgnoreCase(Constants.topTerrComparison)) {
			readExcel.writeExcel(fileLocation,testCaseNo, statementDataUI2,dataFieldForComparison);
		}else if (dataFieldForComparison.equalsIgnoreCase(Constants.totalNoOfAlbumSingleComparision)) {
			readExcel.writeExcel(fileLocation,testCaseNo, totalAlbumSingleList,dataFieldForComparison);
		}else if (dataFieldForComparison.equalsIgnoreCase(Constants.RoyaltyTrendPhysical)) {
			readExcel.writeExcel(fileLocation,testCaseNo, royaltyTrendPhyList,dataFieldForComparison);
		}else if (dataFieldForComparison.equalsIgnoreCase(Constants.RoyaltyTrendDigital)) {
			readExcel.writeExcel(fileLocation,testCaseNo, royaltyTrendDigitalFinalList,dataFieldForComparison);
		}else if (dataFieldForComparison.equalsIgnoreCase(Constants.RoyaltyTrendTotalRoyalty)) {
			readExcel.writeExcel(fileLocation,testCaseNo, royaltyTrendTotalRoyaltyFinalList,dataFieldForComparison);
		}else if (dataFieldForComparison.equalsIgnoreCase(Constants.RoyaltyTrendLicensing)) {
			readExcel.writeExcel(fileLocation,testCaseNo,royaltyTrendLicensingFinalList ,dataFieldForComparison);
		}else if (dataFieldForComparison.equalsIgnoreCase(Constants.RoyaltyTrendSync)) {
			readExcel.writeExcel(fileLocation,testCaseNo,royaltyTrendSyncFinalList ,dataFieldForComparison);
		}else if (dataFieldForComparison.equalsIgnoreCase(Constants.RoyaltyTrendPublicPerformance)) {
			readExcel.writeExcel(fileLocation,testCaseNo,royaltyTrendPublicPerformanceFinalList ,dataFieldForComparison);
		}else if (dataFieldForComparison.equalsIgnoreCase(Constants.RoyaltyTrendMerchandise)) {
			readExcel.writeExcel(fileLocation,testCaseNo,royaltyTrendMerchandiseFinalList ,dataFieldForComparison);
		}else if (dataFieldForComparison.equalsIgnoreCase(Constants.RoyaltyTrendDonutGraph)) {
			readExcel.writeExcel(fileLocation,testCaseNo,royaltyTrendDonutGraphFinalList ,dataFieldForComparison);
		}else if (dataFieldForComparison.matches(Constants.newStatementDueComparison)) {
			readExcel.writeExcel(fileLocation,testCaseNo, newStatementDueFinalResult, Constants.newStatementDueComparison);
		}else if (dataFieldForComparison.matches(Constants.pipelineRoyltyRecordComparsion)) {
			readExcel.writeExcel(fileLocation,testCaseNo, pipelineRoyltyRecordFinalResult, Constants.pipelineRoyltyRecordComparsion);
		}
		else if (dataFieldForComparison.matches(Constants.newStatementDueComparison)) {
				readExcel.writeExcel(fileLocation,testCaseNo, newStatementDueFinalResult, Constants.newStatementDueComparison);
			}

			else if (dataFieldForComparison.matches(Constants.pipelineRoyltyRecordComparsion)) {
				readExcel.writeExcel(fileLocation,testCaseNo, pipelineRoyltyRecordFinalResult,
						Constants.pipelineRoyltyRecordComparsion);

			}

			else if (dataFieldForComparison.matches(Constants.topTracks)) {
				readExcel.writeExcel(fileLocation,testCaseNo, statementDataUI3, Constants.topTracks);

			}

			else if (dataFieldForComparison.matches(Constants.pipelineRoyltyPublishingComparsion)) {
				readExcel.writeExcel(fileLocation,testCaseNo, pipelineRoyltyPublishFinalResult,
						Constants.pipelineRoyltyPublishingComparsion);
			}
			
			else if (dataFieldForComparison.matches(Constants.topLicensingProducts)) {
				readExcel.writeExcel(fileLocation,testCaseNo, topLicensingProductFinalList,
						Constants.topLicensingProducts);
			}
			
			else if (dataFieldForComparison.matches(Constants.topSyncProducts)) {
				readExcel.writeExcel(fileLocation,testCaseNo, topSyncProductFinalList,
						Constants.topSyncProducts);
			}
			else if (dataFieldForComparison.matches(Constants.topMrchndiseProducts)) {
				readExcel.writeExcel(fileLocation,testCaseNo, topMrchndiseProductFinalList,
						Constants.topMrchndiseProducts);
			}
			
			else if (dataFieldForComparison.matches(Constants.topPerformingTerritories)) {
				readExcel.writeExcel(fileLocation,testCaseNo, topPerfromTerrFinalList,
						Constants.topPerformingTerritories);
			}

			else if (dataFieldForComparison.matches(Constants.latestStatement)) {
				readExcel.writeExcel(fileLocation,testCaseNo, latestStatementFinalList,
						Constants.latestStatement);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.info("Error in Common_Method_Class.show Comparsion :" + e);
		}
	}
}
