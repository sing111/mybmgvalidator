package com.infosys.bmg.MyBMGDataValidations.manager;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
//import com.relevantcodes.extentreports.ExtentReports;
//import com.relevantcodes.extentreports.ExtentTest;
//import com.relevantcodes.extentreports.LogStatus; 
//import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.infosys.bmg.MyBMGDataValidations.beans.CrossClient;
import com.infosys.bmg.MyBMGDataValidations.beans.ExcelData;
import com.infosys.bmg.MyBMGDataValidations.beans.SingleClient;
import com.infosys.bmg.MyBMGDataValidations.beans.TestCase;
import com.infosys.bmg.MyBMGDataValidations.beans.TestDataBean;
import com.infosys.bmg.MyBMGDataValidations.utility.Log;
import com.infosys.bmg.MyBMGDataValidations.utility.Objectwait;
import com.infosys.bmg.MyBMGDataValidations.utility.ReadExcelData;
import com.infosys.bmg.MyBMGDataValidations.utility.UIElementLocator;

@Service
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class TestCaseExecute {

	private final static Logger LOGGER = LoggerFactory.getLogger(TestCaseExecute.class);

	ReadExcelData readExcel = new ReadExcelData();

	WebDriver driver = null;;

	public void deleteAllCookiesExample() {
		driver = new ChromeDriver();
		String URL = "https://stagingrec.mybmg.com/mybmg/login";
		driver.navigate().to(URL);
		driver.manage().deleteAllCookies();
	}

	public ExcelData readExcel(TestDataBean currBean) {
		{
			ExcelData excelData = null;
			try {

				String excelFileLocation = currBean.getFilePath() + "/" + currBean.getFinalFileName();
				LOGGER.info("reading excel " + excelFileLocation);
				List<String> creds = readExcel.userpwdArr(excelFileLocation);
				if (creds == null || creds.size() < 3) {
					return null;
				} else {
					excelData = new ExcelData();
					excelData.setSiteURL(creds.get(1));
					excelData.setUserName(creds.get(3));
					excelData.setPassword(creds.get(5));
				}
				LOGGER.info("creds read for " + excelFileLocation + " and now reading test cases");

				List<TestCase> testCases = readExcel.readTestCases(excelFileLocation);
				if (testCases == null || testCases.size() < 0) {
					return null;
				} else {
					excelData.setTestCases(testCases);
				}
				LOGGER.info("test cases read for " + excelFileLocation + " and now reading single clients");

				List<SingleClient> singlClients = readExcel.readSingleClientData(excelFileLocation);
				if (singlClients == null || singlClients.size() < 0) {
					return null;
				} else {
					excelData.setSinglsClient(singlClients);
				}

				LOGGER.info("single clients read for " + excelFileLocation + " and now reading cross clients");

				List<CrossClient> crossClients = readExcel.readCrossClientData(excelFileLocation);
				if (crossClients == null || crossClients.size() < 0) {
					return null;
				} else {
					excelData.setCrossClient(crossClients);
				}
				LOGGER.info("cross client read for " + excelFileLocation);
			} catch (Exception e) {
				LOGGER.error("Error in readExcel :" + ValidatorManager.getExceptionTrace(e));
				return null;

			}
			return excelData;
		}
	}

	public void createResultFile(TestDataBean testData, String resultDirectory) throws Exception {
		try {
			String resultFileName = "Results_" + testData.getFinalFileName();
			String filename = resultDirectory + "/" + resultFileName;
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("TestFileDetails");

			HSSFRow rowhead = sheet.createRow((short) 0);
			rowhead.createCell(0).setCellValue("FileName");
			rowhead.createCell(1).setCellValue(testData.getBaseFileName());

			FileOutputStream fileOut = new FileOutputStream(filename);
			workbook.write(fileOut);
			fileOut.close();
			testData.setResultFileLocation(resultDirectory);
			testData.setResultFileName(resultFileName);
			testData.setResultFileNameWithPath(filename);
			LOGGER.info("excel file has been generated!");

		} catch (Exception e) {
			LOGGER.error("Error in creating result File :" + ValidatorManager.getExceptionTrace(e));
			throw e;
		}
	}

	public void close() throws IOException {
		if (driver != null) {
			driver.quit();
		}
	}

	public void executeTestCasebatch(ExcelData excelData, TestDataBean currBean) {
		List<TestCase> totalCasesToExec = new ArrayList<>();
		for (TestCase testCase : excelData.getTestCases()) {
			if (testCase.getRunStatus()) {
				totalCasesToExec.add(testCase);
			}
		}
		currBean.setTotalValidations(totalCasesToExec.size());
		int casesCompleted = 0;
		for (TestCase testCase : totalCasesToExec) {
			executeTestCase(testCase, excelData, currBean);
			casesCompleted++;
			currBean.setValProcessed(casesCompleted);
		}
	}

	public void executeTestCase(TestCase testCase, ExcelData excelData, TestDataBean currBean) {
		LOGGER.info("TestCase Execution started for scan Name: " + testCase.getTestCase());

		try {
			openandlogin(testCase.getModule(), excelData, currBean);
			LOGGER.info("browser opened and logged in for testcase" + testCase.getTestCase());
			TestCases testCases = new TestCases(driver, readExcel, currBean, excelData, testCase);
			switch (testCase.getTestCaseNumber().toUpperCase().trim()) {
			case "TC01":
				testCases.TC01_Validate_StatementBalance_Single_Client_Selection();
				break;
			case "TC02":
				testCases.TC02_Validate_StatementBalance_All_Client_Selection();
				break;
			case "TC03":
				testCases.TC03_Validate_StatementBalance_Different_CrossClient_Payee_Selection();
				break;
			case "TC04":
				testCases.TC04_Validate_TopTerr_Single_Client_Selection();
				break;
			case "TC05":
				testCases.TC05_Validate_TopTerr_Single_Client_Selection();
				break;
			case "TC06":
				testCases.TC06_Validate_TopTerr_Different_CrossClient_Payee_Selection();
				break;
			case "TC07":
				// testCases.TC07
				break;
			case "TC08":
				// testCases.TC08
				break;
			case "TC09":
				// testCases.TC09
				break;
			case "TC10":
				testCases.TC10_Validate_TotalNumOfAlbumorSingles_Single_Client_Selection();
				break;
			case "TC11":
				testCases.TC11_Validate_TopNumOfAlbumSingles_All_Client_Selection();
				break;
			case "TC12":
				testCases.TC12_Validate_TopNumOfAlbumSingles_Different_CrossClient_Payee_Selection();
				break;
			case "TC13":
				testCases.TC13_Validate_Royalties_trend_Physical_Single_Client_Selection();
				break;
			case "TC14":
				testCases.TC14_Validate_Royalties_trend_Physical_All_Client_Selection();
				break;
			case "TC15":
				testCases.TC15_Validate_Royalties_trend_Physical_Different_CrossClient_Payee_Selection();
				break;
			case "TC16":
				testCases.TC16_Validate_Royalties_trend_Digital_Single_Client_Selection();
				break;
			case "TC17":
				testCases.TC17_Validate_Royalties_trend_Digital_All_Client_Selection();
				break;
			case "TC18":
				testCases.TC18_Validate_Royalties_trend_Digital_Different_CrossClient_Payee_Selection();
				break;
			case "TC19":
				testCases.TC19_Validate_Royalties_trend_TotalRoyalty_Single_Client_Selection();
				break;
			case "TC20":
				testCases.TC20_Validate_Royalties_trend_TotalRoyalty_All_Client_Selection();
				break;
			case "TC21":
				testCases.TC21_Validate_Royalties_trend_TotalRoyalty_Different_CrossClient_Payee_Selection();
				break;
			case "TC22":
				// testCases.TC22
				break;
			case "TC23":
				testCases.TC23_Validate_TopTracks_Single_Client_Selection();
				break;
			case "TC24":
				testCases.TC24_Validate_TopTracks_All_Client_Selection();
				break;
			case "TC25":
				testCases.TC25_Validate_TopTracks_Different_CrossClient_Payee_Selection();
				break;
			case "TC27":
				testCases.TC27_Validate_NewStatementDue_All_Client_Selection();
				break;
			case "TC28":
				testCases.TC28_Validate_NewStatementDue_Diff_CrossClient_Payee_Selection();
				break;
			case "TC29":
				testCases.TC29_Validate_Pipeline_Royalty_Recording_Diff_CrossClient_Payee_Selection();
				break;
			case "TC30":
				testCases.TC30_Validate_Pipeline_Royalty_Recording_All_Client_Selection();
				break;
			case "TC31":
				testCases.TC31_Validate_Pipeline_Royalty_Recording_Single_Client_Selection();
				break;
			case "TC32":
				testCases.TC32_Validate_Royalties_trend_Licensing_Single_Client_Selection();
				break;
			case "TC33":
				testCases.TC33_Validate_Royalties_trend_Licensing_All_Client_Selection();
				break;
			case "TC34":
				testCases.TC34_Validate_Royalties_trend_Licensing_Different_CrossClient_Payee_Selection();
				break;
			case "TC35":
				testCases.TC35_Validate_Royalties_trend_Sync_Single_Client_Selection();
				break;
			case "TC36":
				testCases.TC36_Validate_Royalties_trend_Sync_All_Client_Selection();
				break;
			case "TC37":
				testCases.TC37_Validate_Royalties_trend_Sync_Different_CrossClient_Payee_Selection();
				break;
			case "TC38":
				testCases.TC38_Validate_Royalties_trend_PublicPerformance_Single_Client_Selection();
				break;
			case "TC39":
				testCases.TC39_Validate_Royalties_trend_PublicPerformance_All_Client_Selection();
				break;
			case "TC40":
				testCases.TC40_Validate_Royalties_trend_PublicPerfomance_Different_CrossClient_Payee_Selection();
				break;
			case "TC41":
				testCases.TC41_Validate_Royalties_trend_Merchandise_Single_Client_Selection();
				break;
			case "TC42":
				testCases.TC42_Validate_Royalties_trend_Merchandise_All_Client_Selection();
				break;
			case "TC43":
				testCases.TC43_Validate_Royalties_trend_Merchandise_Different_CrossClient_Payee_Selection();
				break;
			case "TC44":
				testCases.TC44_Validate_Royalties_trend_DonutGraph_Single_Client_Selection();
				break;
			case "TC45":
				testCases.TC45_Validate_Royalties_trend_DonutGraph_All_Client_Selection();
				break;
			case "TC46":
				testCases.TC46_Validate_Royalties_trend_DonutGraph_Different_CrossClient_Payee_Selection();
				break;

			}

		} catch (Exception e) {
			LOGGER.error("TestCase Execution failed for scan Name: " + testCase.getTestCase() + " with error "
					+ ValidatorManager.getExceptionTrace(e));
		} finally {
			LOGGER.info("TestCase Execution completed for scan Name: " + testCase.getTestCase());
			System.gc();
			if (driver != null) {
				try {
					driver.close();
				} catch (Exception e) {
				} finally {
				}
			}
		}
	}

	public void openandlogin(String Module, ExcelData excelData, TestDataBean currBean) throws Exception {
		try {
			System.setProperty("webdriver.chrome.driver", "D:/MyBMGDeepanshu/Data/ChromeDriver/chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-popup-blocking");
			options.addArguments("--disable-web-security");
			options.addArguments("disable-infobars");
			options.addArguments("--disable-save-password-bubble");
			options.addArguments("--no-sandbox");
			options.addArguments("�disable-dev-shm-usage");
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.password_manager_enabled", false);
			options.setExperimentalOption("prefs", prefs);
			DesiredCapabilities caps = DesiredCapabilities.chrome();
			caps.setCapability(ChromeOptions.CAPABILITY, options);
			caps.setCapability("chrome.switches", Arrays.asList("--disable-javascript"));
			driver = new ChromeDriver(caps);
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.HOURS);
			driver.manage().window().maximize();
			driver.get(excelData.getSiteURL());
			Objectwait.elementWait();

			driver.findElement(By.id(UIElementLocator.UserName)).sendKeys(excelData.getUserName());
			Log.info("Username entered in the Username text box");
			driver.findElement(By.id(UIElementLocator.Password)).sendKeys(excelData.getPassword());
			Log.info("password  entered in the password text box");
			driver.findElement(By.name(UIElementLocator.LoginBTN)).click();
			Objectwait.elementWait();
			WebElement Element = driver.findElement(By.className(UIElementLocator.Greeting));
			if (!(Element.isDisplayed())) {
				throw new Exception("User is not logged in");

			}

			if (Module.equalsIgnoreCase("Recording")) {
				driver.findElement(By.xpath(UIElementLocator.ModuleSelection)).click();
				Objectwait.elementWait();
			}
			if (Module.equalsIgnoreCase("Analysis")) {
				WebElement element = driver.findElement(By.xpath(UIElementLocator.AnalysisModuleSelection));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
				Thread.sleep(500);
				driver.findElement(By.xpath(UIElementLocator.AnalysisModuleSelection)).click();
				Objectwait.elementWait();
			}
		} catch (Exception e) {
			LOGGER.error("Unable to open and login the browser " + ValidatorManager.getExceptionTrace(e));
			throw e;
		}
	}

}
