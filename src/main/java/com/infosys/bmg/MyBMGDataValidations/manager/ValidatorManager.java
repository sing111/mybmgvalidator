package com.infosys.bmg.MyBMGDataValidations.manager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.infosys.bmg.MyBMGDataValidations.beans.ExcelData;
import com.infosys.bmg.MyBMGDataValidations.beans.FileDatBean;
import com.infosys.bmg.MyBMGDataValidations.beans.TestDataBean;
import com.infosys.bmg.MyBMGDataValidations.utility.Constants;
import com.infosys.bmg.MyBMGDataValidations.utility.StatusMessage;

@Service
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ValidatorManager {
	private final static Logger LOGGER = LoggerFactory.getLogger(ValidatorManager.class);

	@Value("${upload.fileDirectory}")
	private String fileUploadDirectory;

	@Value("${download.templatePathWithName}")
	private String templatePathWithName;

	@Value("${download.resultDirectory}")
	private String resultDirectory;

	@Autowired
	TestDataProgressBar progressBarManager;

	@Autowired
	TestCaseExecute testDataExecute;

	public TestDataBean uploadFile(MultipartFile file) {
		{
			LOGGER.info("inside saveUploadedFiles");
			TestDataBean currBean = progressBarManager.populateProgressBar();
			StringTokenizer tokenizer = null;
			int tokens;
			String name = "";
			try {
				String timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new java.util.Date());
				tokenizer = new StringTokenizer(file.getOriginalFilename(), "\\");
				tokens = tokenizer.countTokens();

				for (int i = 1; i <= tokens; i++) {
					name = tokenizer.nextToken();
				}
				currBean.setBaseFileName(name);
				File theDir = new File(fileUploadDirectory);

				if (!theDir.exists()) {

					try {
						theDir.mkdir();
						LOGGER.info("directory created");
					} catch (SecurityException se) {
						LOGGER.error("exception occured in  saveUploadedFiles" + se);
						throw se;
					}
				}
				byte[] bytes = file.getBytes();
				String[] fileName = name.split("\\.");
				String finalFileName = fileName[0] + "_" + timeStamp + "." + fileName[1];
				currBean.setFinalFileName(finalFileName);
				currBean.setFilePath(fileUploadDirectory);
				Path path = Paths.get(fileUploadDirectory + "/" + finalFileName);
				Files.write(path, bytes);
				LOGGER.info("file uploaded in directory " + finalFileName);

				currBean.setStatusMessage(StatusMessage.FILE_UPLOAD_COMPLETED.getMessage());
				currBean.setStatusCode(StatusMessage.FILE_UPLOAD_COMPLETED.getCode());

			} catch (Exception e) {
				currBean.setStatusMessage(StatusMessage.FILE_UPLOAD_FAILED.getMessage());
				currBean.setStatusCode(StatusMessage.FILE_UPLOAD_FAILED.getCode());
				LOGGER.error("Error in saveUploadedFiles :" + getExceptionTrace(e));

			}
			return currBean;
		}
	}

	public static String getExceptionTrace(Exception e) {
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			e.printStackTrace(new PrintStream(os));
			return new String(os.toByteArray());
		} catch (Exception ex) {
			LOGGER.error("Unable to generate exception string " + e + " due to " + ex);
			return null;
		}
	}

	public String getDownloadFilePath(Integer pId) {
		TestDataBean currBean = progressBarManager.getProgressBar(pId);
		if (pId == -1) {
			return templatePathWithName;
		}
		if (currBean == null) {
			return null;
		} else {
			return currBean.getResultFileLocation() + "/" + currBean.getResultFileName();
		}
	}

	public String getStatus(Integer pId) {
		TestDataBean currBean = progressBarManager.getProgressBar(pId);
		if (currBean == null) {
			return null;
		} else {
			return currBean.getValProcessed() + " test Cases executed out of " + currBean.getTotalValidations();
		}
	}

	public Map<String, String> executeTestCases(int pId) {
		{
			Map<String, String> jsonobj = new HashMap<>();
			TestDataBean currBean = null;
			try {
				currBean = progressBarManager.getProgressBar(pId);
				if (currBean == null) {
					jsonobj.put(Constants.CODE, String.valueOf(StatusMessage.TEST_CASES_INITIATED.getCode()));
					jsonobj.put(Constants.MESSAGE, StatusMessage.TEST_CASES_INITIATED.getMessage());
					return jsonobj;
				}
				currBean.setStatusMessage(StatusMessage.READ_EXCEL_STARTED.getMessage());
				currBean.setStatusCode(StatusMessage.READ_EXCEL_STARTED.getCode());
				ExcelData excelData = testDataExecute.readExcel(currBean);
				if (excelData == null) {
					currBean.setStatusMessage(StatusMessage.READ_EXCEL_FAILED.getMessage());
					currBean.setStatusCode(StatusMessage.READ_EXCEL_FAILED.getCode());
					jsonobj.put(Constants.CODE, String.valueOf(StatusMessage.READ_EXCEL_FAILED.getCode()));
					jsonobj.put(Constants.MESSAGE, StatusMessage.READ_EXCEL_FAILED.getMessage());
				} else {
					currBean.setStatusMessage(StatusMessage.READ_EXCEL_SUCCESS.getMessage());
					currBean.setStatusCode(StatusMessage.READ_EXCEL_SUCCESS.getCode());
				}

				LOGGER.info(" create result excel file for " + currBean.getFinalFileName());
				testDataExecute.createResultFile(currBean, resultDirectory);
				jsonobj.put(Constants.CODE, String.valueOf(StatusMessage.TEST_CASES_STARTED.getCode()));
				jsonobj.put(Constants.MESSAGE, StatusMessage.TEST_CASES_STARTED.getMessage());
				LOGGER.info("Inside executeTestCases " + excelData);
				// testDataExecute.openBrowser();
				// execute test cases
				testDataExecute.executeTestCasebatch(excelData, currBean);
				// Thread.sleep(10000);

				testDataExecute.close();
				currBean.setStatusMessage(StatusMessage.TEST_CASES_PROCESS_COMPLETED.getMessage());
				currBean.setStatusCode(StatusMessage.TEST_CASES_PROCESS_COMPLETED.getCode());

			} catch (Exception e) {
				if (currBean != null) {
					currBean.setStatusMessage(StatusMessage.TEST_CASES_FAILED.getMessage());
					currBean.setStatusCode(StatusMessage.TEST_CASES_FAILED.getCode());
				}
				LOGGER.error("Error in saveUploadedFiles :" + getExceptionTrace(e));

			} finally {
				currBean.setCompletedDate(new Date());
			}
			return jsonobj;
		}
	}

	public FileDatBean loadFileAsResource(Integer pId) {
		try {
			FileDatBean data = new FileDatBean();
			String encodedString = null;
			String fileLoc = getDownloadFilePath(pId);
			if (fileLoc == null) {
				return null;
			}

			File file = new File(fileLoc);
			byte[] bytes;

			bytes = loadFile(file);
			byte[] encoded = Base64.encodeBase64(bytes);
			encodedString = new String(encoded);
			data.file = encodedString;
			data.filePath = fileLoc;
			if (pId == -1) {
				data.fileName = "TestData.xlsx";
			} else {
				TestDataBean currBean = progressBarManager.getProgressBar(pId);
				if (currBean == null) {
					data.fileName = null;
				} else {
					data.fileName = currBean.getResultFileName();
				}
			}
			return data;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	private byte[] loadFile(File file) throws IOException {
		int offset = 0;
		int numRead = 0;
		InputStream is = new FileInputStream(file);
		byte[] bytes = new byte[(int) file.length()];
		while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}
		if (offset < bytes.length) {
			throw new IOException("Could not completely read file " + file.getName());
		}

		is.close();
		return bytes;
	}

}
