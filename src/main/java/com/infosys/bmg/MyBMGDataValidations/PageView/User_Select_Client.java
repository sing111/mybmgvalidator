package com.infosys.bmg.MyBMGDataValidations.PageView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.infosys.bmg.MyBMGDataValidations.utility.Log;
import com.infosys.bmg.MyBMGDataValidations.utility.Objectwait;
import com.infosys.bmg.MyBMGDataValidations.utility.UIElementLocator;

public class User_Select_Client {

	public  String statementCurrency = null;
	public  String statementAmount = null;
	public  String statementPeriod = null;
	public  List<String> allPeriodsUI = new ArrayList<>();
	public  String periodUI = null;;
	public  String grandTotalForTopTerrUI = null;
	public  String territoryName = null;;
	public  List<List<String>> territoriesUI = new ArrayList<List<String>>();
	public  List<List<String>> territoriesProduct = new ArrayList<List<String>>();
	public  List<String> terrList=new ArrayList<>();
	public  Integer totalNumOfAlbumSingle;
	public  String periodTopTracksUI = null;

	public  List<String> allYearsUI = new ArrayList<>();
	public  String RoyaltyTrendPhysUI;
	public  String RoyaltyTrendPhysDest;
	public  String RoyaltyTrendPhysSource;
	public  String royaltyTrendDigitalUI;
	public  String royaltyTrendTotalRoyUI;

	public  String newStatementDueUI = null;
	public  String pipelineRoyaltyRecordUI = null;
	public  String pipelineRoyaltyCurrencyUI = null;
	public  String periodPipelineRoyaltyRecordUI = null;

	
	public  String periodLatestStaementUI=null;
    //For Top Merchandise Products Dashboard
	public  String periodTopMrchndiseProducts = null;
	public  List<List<String>> topMrchndiseProductList = new ArrayList<List<String>>();

	 //For Top Performing Terr  Dashboard
	public  String periodTopPerformTerr = null;
	public  List<List<String>> topPerformTerrList = new ArrayList<List<String>>();
	

	 //For LatestStatement  Dashboard
	//CHECK	public  String periodLatestStaementUI= null;
//CHECK	public  List<List<String>> latestStatementList = new ArrayList<List<String>>();
		

	public  String pipelineRoyaltyPublishingUI = null;
	public  String periodPipelineRoyaltyPublishingUI = null;
	public  String pipelineRoyaltyCurrencyPublishingUI = null;
	public  String pipelineRoyaltyPublishingDest = null;
	public  String pipelineRoyaltyPublishingSource = null;
	public  String pipelineRoyltyPeriodPublishingDest = null;
	public  String pipelineRoyltyPeriodPublishingSource = null;
	public  List<List<String>> topTracksList=new ArrayList<List<String>>();
	public  String royaltyTrendLicensingUI;
	public  String royaltyTrendSyncUI;
	public  String royaltyTrendPublicPerformanceUI;
	public  String royaltyTrendMerchandiseUI;
	public  String royaltyTrendDonutGraphUI;
	public  String totalRoyaltyUI;
		// For Top Licensing Products Dashboard
	public  String periodTopLicensingProducts = null;
	public  List<List<String>> topLicensingProductList = new ArrayList<List<String>>();
	
	//For Top Sync Products Dashboard
	public  String periodTopSyncProducts = null;
	public  List<List<String>> topSyncProductList = new ArrayList<List<String>>();
	
	//For Top Public Performance Products Dashboard
	public  String periodTopPPProducts = null;
	public  List<List<String>> topPPProductList = new ArrayList<List<String>>();
	public  ArrayList<String> listUI=new ArrayList<>();
	public  ArrayList<String> listTitle=new ArrayList<>();
	
	public  ArrayList<List<String>> latestStatementList=new ArrayList<>();
	
	WebDriver driver;
	public User_Select_Client(WebDriver driver) {
		 this.driver=driver;
	}

	public  void user_select_client(String Site, String currency, String id, boolean selectAllClients,
			String royaltorName) throws Exception {
		Boolean showData = false;
		try {
			Objectwait.elementWait();
			Objectwait.elementWait();


			/* newly added wait */

			new WebDriverWait(driver, 30).pollingEvery(5, TimeUnit.SECONDS).withTimeout(60, TimeUnit.SECONDS)
					.ignoring(Exception.class)
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(UIElementLocator.CreateNewList)));
			Objectwait.elementWait();
			Objectwait.elementWait();

			driver.findElement(By.xpath(UIElementLocator.CreateNewList)).click();
//			Objectwait.elementWait();
//			Objectwait.elementWait();
//			Objectwait.elementWait();
//			Objectwait.elementVisibility("//input[@id='clients-input']");
			Objectwait.elementWait();
			Objectwait.elementWait();

			driver.findElement(By.xpath(UIElementLocator.EnterClient)).sendKeys(Site);
//			Objectwait.elementVisibility("//*[@id='available-clients']");
//			Objectwait.elementWait();
//			Objectwait.elementWait();
//			Objectwait.elementWait();
//			Objectwait.elementWait();
//			WebElement table = driver.findElement(By.xpath(UIElementLocator.SearchedClientTable));
//
//			Objectwait.elementWait();
			Objectwait.elementWait();
			Objectwait.elementWait();
			
			List<WebElement> e3 = driver.findElements(By.xpath("//table[@id='available-clients']/tbody/tr/td/div"));
			List<String>  clientlist= new ArrayList<String>();
			for(WebElement e1 :e3) {
				clientlist.add(e1.getText());
			}
		
			
			for(String client :clientlist) {
				if(client.toLowerCase().equals(royaltorName.toLowerCase())){
					royaltorName=client;
				}
			}
			
			
			JavascriptExecutor js = (JavascriptExecutor) driver;
			
//			WebDriverWait wait = new WebDriverWait(driver, 300);
//			WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@id='available-clients']/tbody/tr[./td[contains(text(), '" + Site + "')] and ./td/div[contains(text(), \""+royaltorName+"\")]]")));
//			
//			driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
//            
//	        (new WebDriverWait(driver, 150)).withMessage("Waited too long for Element").until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@id='available-clients']/tbody/tr[./td[contains(text(), '" + Site + "')] and ./td/div[contains(text(), \""+royaltorName+"\")]]")));
			
	        		
			    
            //driver.findElement(By.xpath("//tr[./td[contains(text(), '" + Site + "')] and ./td/div[contains(text(),'"+royaltorName+"')]]"));

			WebElement client = driver.findElement(By.xpath("//table[@id='available-clients']/tbody/tr[./td[contains(text(), '" + Site + "')] and ./td/div[contains(text(), \""+royaltorName+"\")]]"));
			Objectwait.elementWait();
			Objectwait.elementWait();
			js.executeScript("arguments[0].scrollIntoView();", client);
			Objectwait.elementWait();
			Objectwait.elementWait();
			js.executeScript("arguments[0].scrollIntoView();", client);
			Objectwait.elementWait();
			client.click();
			Objectwait.elementWait();
			 //--- WebDriverWait 
//			        new WebDriverWait(driver, 25)
//			        .pollingEvery(10, TimeUnit.SECONDS)
//			        .withTimeout(50, TimeUnit.SECONDS)
//			        .ignoring(Exception.class)
//			        .until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//tr[./td[contains(text(), '" + Site + "')] and ./td/div[contains(text(),'"+royaltorName+"')]]"))));
		  if(selectAllClients==true){
			    Objectwait.elementWait();
				driver.findElement(By.xpath(UIElementLocator.ShowData)).click();
				showData = true;
				Objectwait.elementWait();
		  }else{

				Objectwait.elementVisibility("//*[@id='client-selection-tabs']/li[2]/button",driver);

				driver.findElement(By.xpath("//*[@id='client-selection-tabs']/li[2]/button")).click();
				Objectwait.elementWait();

				new WebDriverWait(driver, 10).pollingEvery(5, TimeUnit.SECONDS)
						.withTimeout(10, TimeUnit.SECONDS).ignoring(Exception.class).until(ExpectedConditions
								.visibilityOf(driver.findElement(By.xpath("//*[@id='selected-clients']"))));
				WebElement selectedClientTable = driver.findElement(By.xpath("//*[@id='selected-clients']"));
				Objectwait.elementWait();
				Objectwait.elementWait();
				Objectwait.elementWait();
				List<WebElement> rows = driver
						.findElements(By.xpath("//table[@id='selected-clients']/tbody/tr"));

				System.out.println(rows.size());
				Objectwait.elementWait();
				List<String> activeDateListAsString = new ArrayList<String>();

				for (WebElement e : rows) {
					String[] arr = null;
					arr = e.getText().split("\n");
					activeDateListAsString.add(arr[0]);
				}

				Objectwait.elementWait();
				if (id != null) {
					id = id.toString().replace("'", "").replace("'", "");
					for (String ids : activeDateListAsString) {
						if (!ids.equalsIgnoreCase(id) && !ids.equalsIgnoreCase(Site)) {

						selectedClientTable.findElement(By.xpath("//tr/td[contains(text(), '" + ids + "')]")).click();
						 Objectwait.elementWait();
						 Objectwait.elementWait();
						 Objectwait.elementWait();
					}
				}
							
				}else{
					for (String ids : activeDateListAsString) {
						if (!ids.equalsIgnoreCase(Site)) {

							selectedClientTable.findElement(By.xpath("//tr/td[contains(text(), '" + ids + "')]")).click();
							 Objectwait.elementWait();
						}
					}
				}
				Objectwait.elementWait();
				Objectwait.elementWait();
				driver.findElement(By.xpath(UIElementLocator.ShowData)).click();
				Objectwait.elementWait();
			}
		} catch (Exception e) {
			if (showData == false) {
				Objectwait.elementWait();
				driver.findElement(By.xpath(UIElementLocator.ShowData)).click();
				showData = true;
				Objectwait.elementWait();
			}
			e.printStackTrace();
		}
	}

	public  void fetchStatementBalanceFromUI(String Client) {
		statementCurrency = null;
		statementAmount = null;
		statementPeriod = null;
		try {
			statementCurrency = driver.findElement(By.xpath(UIElementLocator.StatementCurrency)).getText();
			statementAmount = driver.findElement(By.xpath(UIElementLocator.StatementBalance)).getText();
			statementPeriod = driver.findElement(By.xpath(UIElementLocator.StatementPeriod)).getText();
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error in User_Select_Client.fetchStatemntBalanceFromUI : " + e);
		}
	}

	public  List<String> fetchPeriodFromUI() {
		try {
			allPeriodsUI = new ArrayList<>();
			// driver.findElement(By.xpath(UIElementLocator.periodPrevArrowScroll)).click();

			List<WebElement> listPeriod = driver.findElements(By.xpath(
					"//div[@class='financial__period-selection__carousel']/div[@class='slick-initialized slick-slider']/div[@class='slick-list draggable']/div/div/a/span"));
			for (WebElement e : listPeriod) {
				// System.out.println(e.getAttribute("innerText") +":periodsUI");
				allPeriodsUI.add(e.getAttribute("innerText"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return allPeriodsUI;		
	}

	public  void fetchTopTerrFromUI(String period) throws InterruptedException {
		Boolean isPresent = null;
		grandTotalForTopTerrUI = null;
		territoriesUI.clear();
		Boolean scrollButton = driver.findElements(By.xpath("//button[@class='btn btn-clean financial__period-selection__carousel__prev slick-arrow']")).size() != 0;
		if(scrollButton){
			 driver.findElement(By.xpath(UIElementLocator.periodPrevArrowScroll)).click();
		}
		Objectwait.elementWait();
		 isPresent = driver.findElements(By.xpath("//div[@class='slick-slide slick-current slick-active']/a/span[contains(text(), '" + period + "')]")).size() != 0;
		 if(!isPresent){
			Boolean present = driver.findElements(By.xpath("//div[@class='slick-slide slick-active']/a/span[contains(text(), '" + period + "')]")).size() != 0;
				if(!present){
					Boolean scrollNextButton = driver.findElements(By.xpath("//button[@class='btn btn-clean financial__period-selection__carousel__next slick-arrow']")).size() != 0;
					if(scrollNextButton){
						driver.findElement(By.xpath(UIElementLocator.periodNxtArrowScroll)).click();
						Objectwait.elementWait();
					}
				}
				Boolean webElmntPresent = driver.findElements(By.xpath("//div[@class='slick-slide slick-active']/a/span[contains(text(), '" + period + "')]")).size() != 0;
				if(webElmntPresent){
					 driver.findElement(By.xpath("//div[@class='slick-slide slick-active']/a/span[contains(text(), '" + period + "')]")).click();
				}
			
		 }else{
			 Boolean webElmntPresent = driver.findElements(By.xpath("//div[@class='slick-slide slick-current slick-active']/a/span[contains(text(), '" + period + "')]")).size() != 0;
				if(webElmntPresent){
					 driver.findElement(By.xpath("//div[@class='slick-slide slick-current slick-active']/a/span[contains(text(), '" + period + "')]")).click(); 
				}
			
		 }
		 System.out.println("//div[@class='slick-slide slick-current slick-active']/a/span[contains(text(), '" + period + "')]");
		Objectwait.elementWait();
		Objectwait.elementWait();
		Boolean webElmntPresence = driver.findElements(By.xpath(UIElementLocator.grandtotalForTerr)).size() != 0;
		
		if(webElmntPresence){
			grandTotalForTopTerrUI=driver.findElement(By.xpath(UIElementLocator.grandtotalForTerr)).getText();
			System.out.println("grandTotalForPeriodUI: "+grandTotalForTopTerrUI);
			List<WebElement> rows = driver
					.findElements(By.xpath("//div/section[@class='financial__view financial__view--territory financial__view--rec__territory']/div[1]/ul/li"));
			for (WebElement e : rows) {
				ArrayList<String> activeDateListAsString = new ArrayList<String>();
				String[] arr = null;
				String[] arr1=null;
				//arr = e.getText().split("\n");
				arr1=e.getAttribute("innerText").split("\n");
					if (!arr1[0].isEmpty()) {
						activeDateListAsString.add(arr1[0]);
						activeDateListAsString.add(arr1[1]);
						territoriesUI.add(activeDateListAsString);
					}
			}
		}
		System.out.println(territoriesUI+"::territoriesUI");
	}

	public  void fetchTopTerrFromUIAllPeriods() {
		grandTotalForTopTerrUI=null;
		grandTotalForTopTerrUI=driver.findElement(By.xpath(UIElementLocator.grandtotalForTerr)).getText();
		System.out.println("grandTotalForPeriodUIAllPeriod: "+grandTotalForTopTerrUI);
		List<WebElement> rows = driver
				.findElements(By.xpath("//div/section[@class='financial__view financial__view--territory financial__view--rec__territory']/div[1]/ul/li"));
		for (WebElement e : rows) {
			ArrayList<String> activeDateListAsString = new ArrayList<String>();
			String[] arr = null;
			String[] arr1 = null;
			// arr = e.getText().split("\n");
			arr1 = e.getAttribute("innerText").split("\n");
			if (!arr1[0].isEmpty()) {
				activeDateListAsString.add(arr1[0]);
				activeDateListAsString.add(arr1[1]);
				territoriesUI.add(activeDateListAsString);
			}
		}
		System.out.println(territoriesUI + "::territoriesUIAllPeriods");
	}

	public  void fetchTopTerrProductsFromUI(String period, List<String> allPeriods, boolean allTerr,
			String terrName, List<String> territoryList) {
		try {
			territoriesProduct = new ArrayList<List<String>>();

			if (allPeriods == null) {

				territoriesUI.clear();

			if(allTerr){
				
				territoryName=driver.findElement(By.xpath(UIElementLocator.terrName)).getText();
				System.out.println(territoryName);
				
				if(territoryName.equalsIgnoreCase("All Territories")){
					terrList=territoryList;
					List<WebElement> rows = driver
							.findElements(By.xpath(UIElementLocator.topProductsByTerritory));
					for (WebElement e : rows) {
						ArrayList<String> activeDateListAsString = new ArrayList<String>();
					
						String[] arr1=null;
						arr1=e.getAttribute("innerText").split("\n");
							if (!arr1[0].isEmpty()) {
								activeDateListAsString.add(arr1[0]);
								activeDateListAsString.add(arr1[1]);
								activeDateListAsString.add(arr1[2]);
								activeDateListAsString.add(arr1[3]);
								territoriesProduct.add(activeDateListAsString);
							}
						}
						System.out.println(territoriesProduct + ":territoriesProduct");
					}
				} else {
					//terr = terr.replace("'", "");
					// List<WebElement> s = driver.findElements(By.xpath("//div[@class='financial__list-charted__entry__name']/div[contains(text(), '" + terr + "')]"));
					// String countryNameUI=driver.findElement(By.xpath("/ul[@class='ps-container ps-theme-default ps-active-y']/li/div[1][contains(text(), '" + terr + "')]"));
						 for(int i=0;i<=territoryList.size();i++){
						        if(territoryList.get(i).equalsIgnoreCase(terrName)){
						        	 WebElement element=driver.findElement(By.xpath("//ul[@class='ps-container ps-theme-default ps-active-y']/li[@data-territory-rank="+i+"]"));
						        	 //WebElement element = driver.findElement(By.id("id_of_element"));
						        	 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
						        	 Thread.sleep(500); 
						        	 
						        	 Actions action = new Actions(driver);
								        action.moveToElement(element).perform(); 
						        
						        List<WebElement> rows1 = driver.findElements(By.xpath(UIElementLocator.topProductsByTerritory));
								for (WebElement e : rows1) {
									ArrayList<String> activeDateListAsString = new ArrayList<String>();
									String[] arr1=null;
									arr1=e.getAttribute("innerText").split("\n");
										if (!arr1[0].isEmpty()) {
											activeDateListAsString.add(arr1[0]);
											activeDateListAsString.add(arr1[1]);
											activeDateListAsString.add(arr1[2]);
											activeDateListAsString.add(arr1[3]);
											territoriesProduct.add(activeDateListAsString);
										}
								}
								
								System.out.println(territoriesProduct+":territoriesProduct");
						 }
							 
						 }
		}
		}else{
			
		}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public  List<String> fetchTerrListByPeriodFromUI(String period) {
		try{
		terrList=new ArrayList<>();
		Boolean isPresent=null;
		Boolean scrollButton = driver.findElements(By.xpath("//button[@class='btn btn-clean financial__period-selection__carousel__prev slick-arrow']")).size() != 0;
		if(scrollButton){
			 driver.findElement(By.xpath(UIElementLocator.periodPrevArrowScroll)).click();
		}
		Objectwait.elementWait();
		 isPresent = driver.findElements(By.xpath("//div[@class='slick-slide slick-current slick-active']/a/span[contains(text(), '" + period + "')]")).size() != 0;
		 if(!isPresent){
			Boolean present = driver.findElements(By.xpath("//div[@class='slick-slide slick-active']/a/span[contains(text(), '" + period + "')]")).size() != 0;
				if(!present){
					Boolean scrollNextButton = driver.findElements(By.xpath("//button[@class='btn btn-clean financial__period-selection__carousel__next slick-arrow']")).size() != 0;
					if(scrollNextButton){
						driver.findElement(By.xpath(UIElementLocator.periodNxtArrowScroll)).click();
						Objectwait.elementWait();
					}
				}
				Boolean webElmntPresent = driver.findElements(By.xpath("//div[@class='slick-slide slick-active']/a/span[contains(text(), '" + period + "')]")).size() != 0;
				if(webElmntPresent){
					 driver.findElement(By.xpath("//div[@class='slick-slide slick-active']/a/span[contains(text(), '" + period + "')]")).click();
				}
			
		 }else{
			 Boolean webElmntPresent = driver.findElements(By.xpath("//div[@class='slick-slide slick-current slick-active']/a/span[contains(text(), '" + period + "')]")).size() != 0;
				if(webElmntPresent){
					 driver.findElement(By.xpath("//div[@class='slick-slide slick-current slick-active']/a/span[contains(text(), '" + period + "')]")).click(); 
				}
			
		 }
		 System.out.println("//div[@class='slick-slide slick-current slick-active']/a/span[contains(text(), '" + period + "')]");
		Objectwait.elementWait();
		Objectwait.elementWait();
		
		
		List<WebElement> rows = driver
				.findElements(By.xpath("//div/section[@class='financial__view financial__view--territory financial__view--rec__territory']/div[1]/ul/li"));
		for (WebElement e : rows) {
			ArrayList<String> activeDateListAsString = new ArrayList<String>();
			String[] arr = null;
			String[] arr1=null;
			//arr = e.getText().split("\n");
			arr1=e.getAttribute("innerText").split("\n");
				if (!arr1[0].isEmpty()) {
					activeDateListAsString.add("'"+arr1[0]+"'");
					terrList.addAll(activeDateListAsString);
				}
		}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return terrList;
	}

	public  void fetchTotalAlbumSingleFromUI(String period,boolean allPeriod) {
		try{
			totalNumOfAlbumSingle=0;
			 List<String> totalAlbumSingleList=new ArrayList<>();
			//if(!allPeriod){
				Boolean isPresent=null;
				Boolean scrollButton = driver.findElements(By.xpath("//button[@class='btn btn-clean financial__period-selection__carousel__prev slick-arrow']")).size() != 0;
				if(scrollButton){
					 driver.findElement(By.xpath(UIElementLocator.periodPrevArrowScroll)).click();
				}
				Objectwait.elementWait();
				 isPresent = driver.findElements(By.xpath("//div[@class='slick-slide slick-current slick-active']/a/span[contains(text(), '" + period + "')]")).size() != 0;
				 Objectwait.elementWait();
				 if(!isPresent){
					Boolean present = driver.findElements(By.xpath("//div[@class='slick-slide slick-active']/a/span[contains(text(), '" + period + "')]")).size() != 0;
						if(!present){
							Boolean scrollNextButton = driver.findElements(By.xpath("//button[@class='btn btn-clean financial__period-selection__carousel__next slick-arrow']")).size() != 0;
							if(scrollNextButton){
								driver.findElement(By.xpath(UIElementLocator.periodNxtArrowScroll)).click();
								Objectwait.elementWait();
							}
						}
						Boolean webElmntPresent = driver.findElements(By.xpath("//div[@class='slick-slide slick-active']/a/span[contains(text(), '" + period + "')]")).size() != 0;
						if(webElmntPresent){
							 driver.findElement(By.xpath("//div[@class='slick-slide slick-active']/a/span[contains(text(), '" + period + "')]")).click();
							 Objectwait.elementWait();
						}
					
				 }else{
					 Boolean webElmntPresent = driver.findElements(By.xpath("//div[@class='slick-slide slick-current slick-active']/a/span[contains(text(), '" + period + "')]")).size() != 0;
						if(webElmntPresent){
							 driver.findElement(By.xpath("//div[@class='slick-slide slick-current slick-active']/a/span[contains(text(), '" + period + "')]")).click(); 
							 Objectwait.elementWait();
						}
					
				 }
				 System.out.println("//div[@class='slick-slide slick-current slick-active']/a/span[contains(text(), '" + period + "')]");
				Objectwait.elementWait();
				Objectwait.elementWait();	
			//}
			
			
			List<WebElement> rows=driver.findElements(By.xpath(UIElementLocator.TotalAlbumSingleRowCount));
			for (WebElement e : rows) {
				ArrayList<String> activeDateListAsString = new ArrayList<String>();
				String[] arr1=null;
				arr1=e.getAttribute("innerText").split("\n");
					if (!arr1[0].isEmpty()) {
						activeDateListAsString.add("'"+arr1[0]+"'");
						totalAlbumSingleList.addAll(activeDateListAsString);
					}
			}
			//int size= driver.findElements(By.xpath(UIElementLocator.TotalAlbumSingleRowCount)).size();
			int size=totalAlbumSingleList.size();
			totalNumOfAlbumSingle=size;
			System.out.println("Total No Of Album single from UI for period :"+period+" is :"+totalNumOfAlbumSingle);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	public  void clickAlbumSingleTab() {
		try{
			driver.findElement(By.xpath(UIElementLocator.AlbumSingleTab)).click();
			Objectwait.elementWait();
			Objectwait.elementWait();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}

	public  List<String> fetchYearsFromUI() {
		allYearsUI=new ArrayList<>();
		List<WebElement> elementList=driver.findElements(By.xpath(UIElementLocator.AllYearsFromUI));
		for(WebElement e:elementList){
			allYearsUI.add(e.getAttribute("innerText"));
		}
		return allYearsUI;
	}

	public  void fetchRoyaltyTrendPhysAllPeriodsFromUI(){
		RoyaltyTrendPhysUI=null;
		String physicalRoyalty=driver.findElement(By.xpath("//ul[@class='widget-stats-list']/li[1]/div[@class='value-absolute']")).getAttribute("innerText");
		System.out.println(physicalRoyalty+":physicalRoyalty");
		RoyaltyTrendPhysUI=physicalRoyalty;
		
	}

	public  void fetchRoyaltyTrendPhysFromUI(String year) {
		RoyaltyTrendPhysUI=null;
		String physicalRoyalty=null;
		try{
			Objectwait.elementWait();	
			Objectwait.elementWait();	
			WebElement element=driver.findElement(By.xpath("//div[@class='highcharts-axis-labels highcharts-xaxis-labels ']/span/span[contains(text(), '" + year + "')]"));
			Objectwait.elementWait();	
			element.click();
			Objectwait.elementWait();	
			Objectwait.elementWait();
			List<WebElement> list=driver.findElements(By.xpath("//div[@class='chart-area-info']/ul/li/div[1]"));
			 String arr1=null;
			 int i=1;
				for(WebElement e:list){
					arr1=e.getAttribute("innerText");
					if(arr1.equalsIgnoreCase("Physical")){
						physicalRoyalty=driver.findElement(By.xpath("//div[@class='chart-area-info']/ul/li["+i+"]/div[2]")).getAttribute("innerText");	
						System.out.println(physicalRoyalty+":physicalRoyalty");
					}
					i++;
				}
			RoyaltyTrendPhysUI=physicalRoyalty;
		}catch(Exception ex){
			ex.printStackTrace();
		}		
	}

	public  void fetchRoyaltyTrendDigitalFromUI(String year) {
		try{
			royaltyTrendDigitalUI=null; String arr1=null;String digitalRoyalty=null;
		Objectwait.elementWait();	
		Objectwait.elementWait();	
		WebElement element=driver.findElement(By.xpath("//div[@class='highcharts-axis-labels highcharts-xaxis-labels ']/span/span[contains(text(), '" + year + "')]"));
		element.click();
		
		List<WebElement> list=driver.findElements(By.xpath("//div[@class='chart-area-info']/ul/li/div[1]"));
		
		 int i=1;
			for(WebElement e:list){
				arr1=e.getAttribute("innerText");
				if(arr1.equalsIgnoreCase("Digital")){
					digitalRoyalty=driver.findElement(By.xpath("//div[@class='chart-area-info']/ul/li["+i+"]/div[2]")).getAttribute("innerText");	
					System.out.println(digitalRoyalty+":digitalRoyalty");
				}
				i++;
			}
		royaltyTrendDigitalUI=digitalRoyalty;
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public  void fetchRoyaltyTrendTotalRoyAllPeriodsFromUI()  {
		try{
			royaltyTrendTotalRoyUI=null;
			Objectwait.elementWait();	
			WebElement element=driver.findElement(By.xpath("//div[@class='chart-area-info']/div[@class='value']"));
			Objectwait.elementWait();	
			royaltyTrendTotalRoyUI=element.getAttribute("innerText");
			System.out.println(royaltyTrendTotalRoyUI+"::royaltyTrendTotalRoyUI::");
		}catch(Exception ex){
			ex.printStackTrace();
		}		
	}

	public  void fetchRoyaltyTrendTotalRoyByYearFromUI(String year) {
		try{
			royaltyTrendTotalRoyUI=null;
			Objectwait.elementWait();	
			WebElement element=driver.findElement(By.xpath("//div[@class='highcharts-axis-labels highcharts-xaxis-labels ']/span/span[contains(text(), '" + year + "')]"));
			element.click();
			Objectwait.elementWait();	
			WebElement totRoy=driver.findElement(By.xpath("//div[@class='chart-area-info']/div[@class='value']"));
			Objectwait.elementWait();
			royaltyTrendTotalRoyUI=totRoy.getAttribute("innerText");
			System.out.println(royaltyTrendTotalRoyUI+"::royaltyTrendTotalRoyUI::");
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}

	public  void getTopTracksUI() throws InterruptedException{
	     periodTopTracksUI=driver.findElement(By.xpath("//div[@class='widget widget-trend-list widget-top-track-sales widget--full']/div/div[@class='widget-header-period']/div[@class='period']")).getText();
	     Objectwait.elementWait();
	     Objectwait.elementWait();
	     List<WebElement> rows=driver.findElements(By.xpath("//div[@class='widget widget-trend-list widget-top-track-sales widget--full']/div[@class='widget-trend-list-container column-count-5']/ul/li"));
	     topTracksList=new ArrayList<List<String>>();
	     
	     for(WebElement e:rows){
	    	 ArrayList<String>  toptrcklst=new ArrayList<String>();
	           String[] arr=null;
	           arr=e.getText().split("\n");
	           toptrcklst.add(arr[1]);
	           toptrcklst.add(arr[2]);
	           toptrcklst.add(arr[3]);
	           toptrcklst.add(arr[4]);
	           toptrcklst.add(arr[5]);
	           toptrcklst.add(arr[6]);
	           topTracksList.add(toptrcklst);
	        }
	
	}

	public  String newStatementDueUI() {
		 List<WebElement> rows=driver.findElements(By.xpath("//div[@class='widget-general-stats col-md-4']/div[@class='value']"));
		 newStatementDueUI=rows.get(2).getText();
		 return newStatementDueUI;		
	}

	public  void pipelineRoyaltyRecordUI() throws InterruptedException {
		
		 Objectwait.elementWait();
	     Objectwait.elementWait();
		 List<WebElement> rows=driver.findElements(By.xpath("//div[@class='widget-general-stats col-md-4']/div[@class='value']/span"));
		   if(rows.size()>2){
		 if(rows.get(3).getText().contains(",")){
			 String[] arr=null;
	           arr=rows.get(3).getText().split(",");     
		 pipelineRoyaltyRecordUI=arr[0]+arr[1]; 
		 }else{
			 pipelineRoyaltyRecordUI=rows.get(3).getText();
		 }
		   
		   pipelineRoyaltyCurrencyUI=rows.get(2).getText();
		   }
		 Objectwait.elementWait();
	     Objectwait.elementWait();
		 List<WebElement> period=driver.findElements(By.xpath("//div[@class='widget-general-stats col-md-4']/div[@class='period']"));
		 if(period.size()>1){
		 periodPipelineRoyaltyRecordUI=period.get(1).getText();
		 }
		
	}
	
	public  void fetchRoyaltyTrendLicensingFromUI(String year) {
		try{
			royaltyTrendLicensingUI=null;
			String licensingRoyalty=null;
			 String arr1=null;
		Objectwait.elementWait();	
		Objectwait.elementWait();	
		WebElement element=driver.findElement(By.xpath("//div[@class='highcharts-axis-labels highcharts-xaxis-labels ']/span/span[contains(text(), '" + year + "')]"));
		element.click();
		 int i=1;
		 List<WebElement> list=driver.findElements(By.xpath("//div[@class='chart-area-info']/ul/li/div[1]"));
			for(WebElement e:list){
				arr1=e.getAttribute("innerText");
				if(arr1.equalsIgnoreCase("Licensing")){
					licensingRoyalty=driver.findElement(By.xpath("//div[@class='chart-area-info']/ul/li["+i+"]/div[2]")).getAttribute("innerText");	
					System.out.println(licensingRoyalty+":licensingRoyalty");
				}
				i++;
			}
		royaltyTrendLicensingUI=licensingRoyalty;
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public  void fetchRoyaltyTrendSyncFromUI(String year) {
		try{
			royaltyTrendSyncUI=null;
		Objectwait.elementWait();	
		Objectwait.elementWait();	
		WebElement element=driver.findElement(By.xpath("//div[@class='highcharts-axis-labels highcharts-xaxis-labels ']/span/span[contains(text(), '" + year + "')]"));
		element.click();
		String syncRoyalty=null;
		 int i=1;
		 String arr1=null;
		 List<WebElement> list=driver.findElements(By.xpath("//div[@class='chart-area-info']/ul/li/div[1]"));
			for(WebElement e:list){
				arr1=e.getAttribute("innerText");
				if(arr1.equalsIgnoreCase("Sync")){
					syncRoyalty=driver.findElement(By.xpath("//div[@class='chart-area-info']/ul/li["+i+"]/div[2]")).getAttribute("innerText");	
					System.out.println(syncRoyalty+":syncRoyalty");
				}
				i++;
			}
		royaltyTrendSyncUI=syncRoyalty;
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public  void fetchRoyaltyTrendPublicPerformanceFromUI(String year) {
		try{
		Objectwait.elementWait();	
		Objectwait.elementWait();	
		String publicPerformanceRoyalty=null;
		WebElement element=driver.findElement(By.xpath("//div[@class='highcharts-axis-labels highcharts-xaxis-labels ']/span/span[contains(text(), '" + year + "')]"));
		element.click();
		 int i=1;
		 String arr1=null;
		 List<WebElement> list=driver.findElements(By.xpath("//div[@class='chart-area-info']/ul/li/div[1]"));
			for(WebElement e:list){
				arr1=e.getAttribute("innerText");
				if(arr1.equalsIgnoreCase("Public Performance")){
					publicPerformanceRoyalty=driver.findElement(By.xpath("//div[@class='chart-area-info']/ul/li["+i+"]/div[2]")).getAttribute("innerText");	
					
				}
				i++;
			}
			if(publicPerformanceRoyalty!=null){
			 royaltyTrendPublicPerformanceUI=publicPerformanceRoyalty;	
			}else{
			royaltyTrendPublicPerformanceUI="NULL";
			}
			System.out.println(publicPerformanceRoyalty+":publicPerformanceRoyaltyUI");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public  void fetchRoyaltyTrendMerchandiseFromUI(String year) {
		try{
		Objectwait.elementWait();	
		Objectwait.elementWait();	
		WebElement element=driver.findElement(By.xpath("//div[@class='highcharts-axis-labels highcharts-xaxis-labels ']/span/span[contains(text(), '" + year + "')]"));
		element.click();
		String merchandiseRoyalty=null;
		 int i=1;
		 String arr1=null;
		 List<WebElement> list=driver.findElements(By.xpath("//div[@class='chart-area-info']/ul/li/di[1]"));
			for(WebElement e:list){
				arr1=e.getAttribute("innerText");
				if(arr1.equalsIgnoreCase("Merchandise")){
					merchandiseRoyalty=driver.findElement(By.xpath("//div[@class='chart-area-info']/ul/li["+i+"]/div[2]")).getAttribute("innerText");	
					
				}
				i++;
			}
			if(merchandiseRoyalty!=null){
			 royaltyTrendMerchandiseUI=merchandiseRoyalty;	
			}else{
			royaltyTrendMerchandiseUI="NULL";
			}
		royaltyTrendMerchandiseUI=merchandiseRoyalty;
		System.out.println(merchandiseRoyalty+":merchandiseRoyalty");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public  void fetchRoyaltyTrendDonutGraphFromUI(String year) {
		try{
		Objectwait.elementWait();	
		Objectwait.elementWait();	
		driver.findElement(By.xpath("//button[@id='royaltiesTrendArea']")).click();
		Objectwait.elementWait();	
		WebElement element=driver.findElement(By.xpath("//div[@class='highcharts-axis-labels highcharts-xaxis-labels ']/span/span[contains(text(), '" + year + "')]"));
		element.click();
		driver.findElement(By.xpath("//button[@id='royaltiesTrendPie']")).click();
		Objectwait.elementWait();	
		totalRoyaltyUI=driver.findElement(By.xpath("//div[@class='chart-label']/div[2]")).getAttribute("innerText");
		System.out.println("total Royalty UI:" +totalRoyaltyUI);
		Objectwait.elementWait();	
		 listUI=new ArrayList<>();
		 listUI.add("Total Royalty");
		 listUI.add(totalRoyaltyUI);
		List<WebElement> list1=driver.findElements(By.xpath("//div[@class='chart-info col-md-6']/ul/li/div[1]"));
		String arr=null;
		listTitle=new ArrayList<>();
		for(WebElement e:list1){
			arr=e.getAttribute("innerText");
			listTitle.add(arr);
		}
		
		List<WebElement> list=driver.findElements(By.xpath("//div[@class='chart-info col-md-6']/ul/li/div"));
		 String arr1=null;
		for(WebElement e:list){
			arr1=e.getAttribute("innerText");
			listUI.add(arr1);
		}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public  void pipelineRoyaltyPublishingUI() throws InterruptedException {

		Objectwait.elementWait();
		Objectwait.elementWait();
		List<WebElement> rows = driver
				.findElements(By.xpath("//div[@class='widget-general-stats col-md-4']/div[@class='value']/span"));
		if (rows.size() > 2) {
			if (rows.get(3).getText().contains(",")) {
				String[] arr = null;
				arr = rows.get(3).getText().split(",");

				pipelineRoyaltyPublishingUI = arr[0] + arr[1];
			} else {
				pipelineRoyaltyPublishingUI = rows.get(3).getText();
			}

			pipelineRoyaltyCurrencyPublishingUI = rows.get(2).getText();
		}
		Objectwait.elementWait();
		Objectwait.elementWait();
		List<WebElement> period = driver
				.findElements(By.xpath("//div[@class='widget-general-stats col-md-4']/div[@class='period']"));
		if (period.size() > 1) {
			periodPipelineRoyaltyPublishingUI = period.get(1).getText();
		}

	}


	public  void getTopLicensingProductsUI() throws InterruptedException {
		try {
		periodTopLicensingProducts = driver
				.findElement(By.xpath(UIElementLocator.periodTopLicensingProducts)).getText();
		Objectwait.elementWait();
		Objectwait.elementWait();

		List<WebElement> rows = driver.findElements(By.xpath(UIElementLocator.topLicensingProducts));
		topLicensingProductList = new ArrayList<List<String>>();

		for (WebElement e : rows) {
			ArrayList<String> toptlicensinglst = new ArrayList<String>();
			String[] arr = null;
			arr = e.getText().split("\n");
			toptlicensinglst.add(arr[1]);
			toptlicensinglst.add(arr[2]);
			toptlicensinglst.add(arr[3]);
			toptlicensinglst.add(arr[4]);
		
			topLicensingProductList.add(toptlicensinglst);
		}
		}catch(Exception e) {
			e.printStackTrace();
		}

	}
	
	public  void getTopSyncProductsUI() throws InterruptedException {
		try {
		periodTopSyncProducts = driver
				.findElement(By.xpath(UIElementLocator.periodTopSyncProducts)).getText();
		Objectwait.elementWait();
		Objectwait.elementWait();

		List<WebElement> rows = driver.findElements(By.xpath(UIElementLocator.topSyncProducts));
		topSyncProductList = new ArrayList<List<String>>();

		for (WebElement e : rows) {
			ArrayList<String> toptlicensinglst = new ArrayList<String>();
			String[] arr = null;
			arr = e.getText().split("\n");
			toptlicensinglst.add(arr[1]);
			toptlicensinglst.add(arr[2]);
			toptlicensinglst.add(arr[3]);
			toptlicensinglst.add(arr[4]);
		
			topSyncProductList.add(toptlicensinglst);

		}
		}catch(Exception e) {
			e.printStackTrace();
		}

	}
	
	public  void getTopPPProductsUI() throws InterruptedException {
		try {
		periodTopPPProducts = driver
				.findElement(By.xpath(UIElementLocator.periodTopPPProducts)).getText();
		Objectwait.elementWait();
		Objectwait.elementWait();

		List<WebElement> rows = driver.findElements(By.xpath(UIElementLocator.topPPProducts));
		topPPProductList = new ArrayList<List<String>>();

		for (WebElement e : rows) {
			ArrayList<String> topPPlst = new ArrayList<String>();
			String[] arr = null;
			arr = e.getText().split("\n");
			topPPlst.add(arr[1]);
			topPPlst.add(arr[2]);
			topPPlst.add(arr[3]);
			topPPlst.add(arr[4]);
		
			topPPProductList.add(topPPlst);
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public  void getTopMrchndiseProductsUI() throws InterruptedException {
		try {
			periodTopMrchndiseProducts = driver
				.findElement(By.xpath(UIElementLocator.periodTopMrchndiseProducts)).getText();
		Objectwait.elementWait();
		Objectwait.elementWait();

		List<WebElement> rows = driver.findElements(By.xpath(UIElementLocator.topMrchndiseProducts));
		topMrchndiseProductList = new ArrayList<List<String>>();

		for (WebElement e : rows) {
			ArrayList<String> topMrchnlst = new ArrayList<String>();
			String[] arr = null;
			arr = e.getText().split("\n");
			topMrchnlst.add(arr[1]);
			topMrchnlst.add(arr[2]);
			topMrchnlst.add(arr[3]);
			topMrchnlst.add(arr[4]);
		
			topMrchndiseProductList.add(topMrchnlst);
		}
		}catch(Exception e) {
			e.printStackTrace();
		}

	}
	
	public  void getTopPerformTerrUI() throws InterruptedException {
		try {
			periodTopPerformTerr = driver
				.findElement(By.xpath(UIElementLocator.periodTopPerformTerr)).getText();
		Objectwait.elementWait();
		Objectwait.elementWait();

		List<WebElement> rows = driver.findElements(By.xpath(UIElementLocator.topPerformTerr));
		topPerformTerrList = new ArrayList<List<String>>();

		for (WebElement e : rows) {
			ArrayList<String> topPerformTerrlst = new ArrayList<String>();
			String[] arr = null;
			arr = e.getText().split("\n");
			topPerformTerrlst.add(arr[0]);
			topPerformTerrlst.add(arr[1]);
		
		
			topPerformTerrList.add(topPerformTerrlst);
		}
		}catch(Exception e) {
			e.printStackTrace();
		}

	}
	
	
	public  void getLatestStatementUI() throws InterruptedException {
		try {
			periodLatestStaementUI = driver
				.findElement(By.xpath(UIElementLocator.periodLatestStatemnt)).getText();
		Objectwait.elementWait();
		Objectwait.elementWait();

		List<WebElement> rows = driver.findElements(By.xpath(UIElementLocator.latestStatemnts));
		latestStatementList = new ArrayList<List<String>>();

		for (WebElement e : rows) {
			ArrayList<String> topStatementlist = new ArrayList<String>();
			String[] arr = null;
			arr = e.getText().split("\n");
			String clinetName=arr[0];
			String[] arr1=null;
			arr1=clinetName.split("(");
			String clientNameFinal=arr1[0];
			String clientCode=arr1[1].replace(")", "");
			String  royalty=arr[1];
			String arr2[]=null;
			arr2=royalty.split(" ");
			String currency=arr2[0];
			topStatementlist.add(clientCode);
			topStatementlist.add(clientNameFinal);
			topStatementlist.add(currency);
			topStatementlist.add(arr2[1]);
			latestStatementList.add(topStatementlist);
		}
		}catch(Exception e) {
			e.printStackTrace();
		}

	}

}
