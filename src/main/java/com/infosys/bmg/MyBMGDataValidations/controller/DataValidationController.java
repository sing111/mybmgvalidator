package com.infosys.bmg.MyBMGDataValidations.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.infosys.bmg.MyBMGDataValidations.beans.FileDatBean;
import com.infosys.bmg.MyBMGDataValidations.beans.TestDataBean;
import com.infosys.bmg.MyBMGDataValidations.manager.ValidatorManager;
import com.infosys.bmg.MyBMGDataValidations.utility.Constants;
import com.infosys.bmg.MyBMGDataValidations.utility.StatusMessage;

import io.swagger.annotations.Api;

@Controller
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
@RequestMapping(Constants.MY_BMG_BASE)
@Api(value = "dataValidation")
public class DataValidationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataValidationController.class);
	@Autowired
	ValidatorManager validatorManager;

	@RequestMapping(value = Constants.START_TEST, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, String> startTest(@PathVariable Integer pId) {
		return validatorManager.executeTestCases(pId);

	}

	@RequestMapping(value = Constants.UPLOAD_FILE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public @ResponseBody TestDataBean uploadFile(HttpServletRequest request, HttpServletResponse response) {

		MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request;
		MultipartFile file = mpr.getFile("myfile");
		// return
		return validatorManager.uploadFile(file);

	}

	@RequestMapping(value = "/temp", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, String> downloadFilePath(@PathVariable Integer pId) {

		LOGGER.info("Inside  downloadFile() starts");
		Map<String, String> jsonobj = new HashMap<>();
		try {
			String path = validatorManager.getDownloadFilePath(pId);
			if (path == null) {
				jsonobj.put(Constants.CODE, String.valueOf(StatusMessage.INVALID_PROCESS_ID.getCode()));
				jsonobj.put(Constants.MESSAGE, StatusMessage.INVALID_PROCESS_ID.getMessage());
				return jsonobj;
			} else {
				jsonobj.put(Constants.CODE, String.valueOf(StatusMessage.PATH_RECIEVED_SUCCESFULLY.getCode()));
				jsonobj.put(Constants.MESSAGE, StatusMessage.PATH_RECIEVED_SUCCESFULLY.getMessage());
				jsonobj.put(Constants.FILE_PATH, path);
			}
		} catch (final Exception e) {
			LOGGER.error("Exception in downloadFiles method : " + e.getMessage());
			jsonobj.put(Constants.CODE, String.valueOf(StatusMessage.FILE_DOWNLOAD_ERROR.getCode()));
			jsonobj.put(Constants.MESSAGE, StatusMessage.FILE_DOWNLOAD_ERROR.getMessage());

		}
		return jsonobj;

	}

	@RequestMapping(value = Constants.GET_STATUS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, String> getStatus(@PathVariable Integer pId) {

		LOGGER.info("Inside  GET_STATUS starts");
		Map<String, String> jsonobj = new HashMap<>();
		try {
			String status = validatorManager.getStatus(pId);
			if (status == null) {
				jsonobj.put(Constants.CODE, String.valueOf(StatusMessage.UNABLE_TO_GET_STATUS.getCode()));
				jsonobj.put(Constants.MESSAGE, StatusMessage.UNABLE_TO_GET_STATUS.getMessage());
			} else {
				jsonobj.put(Constants.CODE, String.valueOf(StatusMessage.STATUS_RECIEVED_SUCCESFULLY.getCode()));
				jsonobj.put(Constants.MESSAGE, StatusMessage.STATUS_RECIEVED_SUCCESFULLY.getMessage());
				jsonobj.put(Constants.STATUS, status);
			}
		} catch (final Exception e) {
			LOGGER.error("Exception in getStatus method : " + e.getMessage());
			jsonobj.put(Constants.CODE, String.valueOf(StatusMessage.UNABLE_TO_GET_STATUS.getCode()));
			jsonobj.put(Constants.MESSAGE, StatusMessage.UNABLE_TO_GET_STATUS.getMessage());
		}
		return jsonobj;

	}

	@RequestMapping(value = Constants.DOWNLOAD_FILE, method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody FileDatBean downloadFile(@PathVariable Integer pId) {
		// Load file as Resource
		return  validatorManager.loadFileAsResource(pId);


	}
}