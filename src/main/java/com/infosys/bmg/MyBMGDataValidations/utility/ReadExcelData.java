package com.infosys.bmg.MyBMGDataValidations.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.infosys.bmg.MyBMGDataValidations.beans.CrossClient;
import com.infosys.bmg.MyBMGDataValidations.beans.SingleClient;
import com.infosys.bmg.MyBMGDataValidations.beans.TestCase;
import com.infosys.bmg.MyBMGDataValidations.manager.ValidatorManager;

public class ReadExcelData {
	private final static Logger LOGGER = LoggerFactory.getLogger(ReadExcelData.class);

	public List<String> userpwdArr(String location) throws Exception {
		List<String> arr1 = new ArrayList<>();
		FileInputStream inputStream = null;
		try {
			inputStream = new FileInputStream(new File(location));
			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = firstSheet.iterator();
			while (iterator.hasNext()) {
				Row nextRow = iterator.next();
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					if (cell.getCellType() == 1) {
						arr1.add(cell.getStringCellValue());
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("error while reading excel for creds" + ValidatorManager.getExceptionTrace(e));
			return null;
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
		}
		return arr1;

	}

	public List<TestCase> readTestCases(String location) throws Exception {
		List<TestCase> testCases = null;
		FileInputStream inputStream = null;
		try {
			inputStream = new FileInputStream(new File(location));
			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(1);
			Iterator<Row> iterator = firstSheet.iterator();
			testCases = new ArrayList<>();
			TestCase testCase;
			if (iterator.hasNext()) {
				iterator.next();
			}
			while (iterator.hasNext()) {
				testCase = new TestCase();
				Row nextRow = iterator.next();
				testCase.setTestCaseNumber(nextRow.getCell(0) == null ? "" : nextRow.getCell(0).toString());

				testCase.setTestCase(nextRow.getCell(1) == null ? "" : nextRow.getCell(1).toString());
				String testCaseStatus = nextRow.getCell(2) == null ? "" : nextRow.getCell(2).toString();
				testCase.setModule(nextRow.getCell(3) == null ? "" : nextRow.getCell(3).toString());
				if (testCaseStatus.equalsIgnoreCase("yes")) {
					testCase.setRunStatus(true);
				} else {
					testCase.setRunStatus(false);
				}
				testCases.add(testCase);
			}
			inputStream.close();
		} catch (Exception e) {
			LOGGER.error("error while reading excel for test cases" + ValidatorManager.getExceptionTrace(e));
			return null;
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
		}
		return testCases;
	}

	public List<SingleClient> readSingleClientData(String location) throws Exception {
		List<SingleClient> singleClients = null;
		FileInputStream inputStream = null;
		try {
			inputStream = new FileInputStream(new File(location));
			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(2);
			Iterator<Row> iterator = firstSheet.iterator();
			singleClients = new ArrayList<>();
			SingleClient singleClient;
			if (iterator.hasNext()) {
				iterator.next();
			}
			while (iterator.hasNext()) {
				singleClient = new SingleClient();
				Row nextRow = iterator.next();
				singleClient.setRoyaltorSite(nextRow.getCell(0) == null ? "" : nextRow.getCell(0).toString());
				String code = nextRow.getCell(1) == null ||nextRow.getCell(1).toString().trim().isEmpty()? "0" : nextRow.getCell(1).toString();
				singleClient.setRoyaltorName(nextRow.getCell(2) == null ? "" : nextRow.getCell(2).toString());
				singleClient.setPayeeOrCrossClient(nextRow.getCell(3) == null ? "" : nextRow.getCell(3).toString());
				singleClient.setCurrency_Desc(nextRow.getCell(4) == null ? "" : nextRow.getCell(4).toString());
				 Double codeDouble = Double.valueOf(code);
				 singleClient.setRoyaltorCode(String.valueOf(codeDouble.intValue()));

				if (!singleClient.getRoyaltorSite().isEmpty()) {
					singleClients.add(singleClient);
				}
			}

		} catch (Exception e) {
			LOGGER.error("error while reading excel for singleClients" + ValidatorManager.getExceptionTrace(e));
			return null;
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
		}
		return singleClients;
	}

	public List<CrossClient> readCrossClientData(String location) throws Exception {
		List<CrossClient> crossClients = null;
		FileInputStream inputStream = null;
		try {
			inputStream = new FileInputStream(new File(location));
			Workbook workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(3);
			Iterator<Row> iterator = firstSheet.iterator();
			crossClients = new ArrayList<>();
			CrossClient crossClient;
			if (iterator.hasNext()) {
				iterator.next();
			}
			while (iterator.hasNext()) {
				crossClient = new CrossClient();
				Row nextRow = iterator.next();
				crossClient.setRoyaltorSite(nextRow.getCell(0) == null ? "" : nextRow.getCell(0).toString());
				String code = nextRow.getCell(1) == null ||nextRow.getCell(1).toString().trim().isEmpty()? "0" : nextRow.getCell(1).toString();
				crossClient.setRoyaltorName(nextRow.getCell(2) == null ? "" : nextRow.getCell(2).toString());
				crossClient.setPayeeOrCrossClient(nextRow.getCell(3) == null ? "" : nextRow.getCell(3).toString());
				crossClient.setCurrency_Desc(nextRow.getCell(4) == null ? "" : nextRow.getCell(4).toString());
				Double codeDouble = Double.valueOf(code);
				crossClient.setRoyaltorCode(String.valueOf(codeDouble.intValue()));

				if (!crossClient.getRoyaltorSite().isEmpty()) {
					crossClients.add(crossClient);
				}
			}

		} catch (Exception e) {
			LOGGER.error("error while reading excel for crossClients" + ValidatorManager.getExceptionTrace(e));
			return null;
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
		}
		return crossClients;
	}

	public void writeExcel(String fileLocation, String sheetName, List<List<String>> statementDataUI,
			String dataPointHeader) throws IOException {

		try {
			File file = new File(fileLocation);
			FileInputStream inputStream = new FileInputStream(file);
			// Workbook workbook = WorkbookFactory.create(inputStream);
			XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
			removeSheetIfExists(workbook, sheetName);
			Sheet sheet = workbook.createSheet(sheetName);
			Row headerRow = sheet.createRow(0);
			String[] column = getHeaders(dataPointHeader);

			for (int k = 0; k < column.length; k++) {
				Cell cell1 = headerRow.createCell(k);
				cell1.setCellValue(column[k]);
			}

			int i = 0, rowCount = 1;
			for (List<String> strList : statementDataUI) {
				Row newRow = sheet.createRow(rowCount++);
				i = 0;
				for (String str : strList) {
					Cell cell = newRow.createCell(i);
					cell.setCellType(Cell.CELL_TYPE_STRING);
					cell.setCellValue(str);
					LOGGER.info(str);
					i++;
				}
			}

			FileOutputStream outputStream = new FileOutputStream(file);
			workbook.write(outputStream);
			inputStream.close();
			outputStream.close();

		} catch (Exception e) {
			LOGGER.error("error while writing data" + ValidatorManager.getExceptionTrace(e));

		}
	}

	public void removeSheetIfExists(XSSFWorkbook book, String sheetName) {
		for (int i = 0; i < book.getNumberOfSheets(); i++) {
			XSSFSheet tmpSheet = book.getSheetAt(i);
			if (tmpSheet.getSheetName().equals(sheetName)) {
				int sheetIndex = book.getSheetIndex(sheetName);
				book.removeSheetAt(sheetIndex);
			}
		}

	}

	public String[] getHeaders(String dataPointHeader) {
		String[] column = null;
		switch (dataPointHeader) {

		case Constants.statementComparison:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.currency, Constants.statementBalanceUI, Constants.statementPeriodDestinationDB,
					"Statement Currency Dest", Constants.destiantionDB, Constants.statmentPeriodSourceDB,
					"Currency Source", Constants.sourceDb, "Status", "Notes" };
			break;
		case Constants.topTerrComparison:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.topTerrUI, Constants.currency, Constants.terrAmountUI, Constants.terrAmountDestDb,
					Constants.terrAmountSourceDb, Constants.grandTotalUI, Constants.grandTotalDestDb,
					Constants.grandTotalSourceDb };
			break;
		case Constants.totalNoOfAlbumSingleComparision:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.totalNoOfAlbumSingleUI, Constants.totalNoOfAlbumSingleDestDb,
					Constants.totalNoOfAlbumSingleSrcDb, };
			break;
		/*
		 * check case else case Constants. topTerrProductsBreakDownComparision: column =
		 * new String[] { Constants.site, Constants.client,
		 * Constants.periodUI,Constants.terrBreakDownByProductsUI
		 * ,Constants.terrBreakDownByProductsDestDb,
		 * Constants.terrBreakDownByProductsSrcDb}; } else
		 */
		case Constants.RoyaltyTrendPhysical:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.currency, Constants.royaltyTrendPhyUI, Constants.royaltyTrendPhyDestDb,
					Constants.royaltyTrendPhySrcDb, };
			break;
		case Constants.RoyaltyTrendDigital:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.currency, Constants.royaltyTrendDigitalUI, Constants.royaltyTrendDigitalDestDb,
					Constants.royaltyTrendDigitalSrcDb, };
			break;
		case Constants.RoyaltyTrendTotalRoyalty:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.currency, Constants.royaltyTrendTotalRoyaltyUI, Constants.royaltyTrendTotalRoyaltyDestDb,
					Constants.royaltyTrendTotalRoyaltySrcDb };
			break;
		case Constants.RoyaltyTrendLicensing:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.currency, Constants.royaltyTrendLicensingUI, Constants.royaltyTrendLicensingDestDb,
					Constants.royaltyTrendLicensingSrcDb };
			break;
		case Constants.RoyaltyTrendSync:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.currency, Constants.royaltyTrendSyncUI, Constants.royaltyTrendSyncDestDb,
					Constants.royaltyTrendSyncSrcDb };
			break;
		case Constants.RoyaltyTrendPublicPerformance:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.currency, Constants.royaltyTrendPublicPerformanceUI,
					Constants.royaltyTrendPublicPerformanceDestDb, Constants.royaltyTrendPublicPerformanceSrcDb };
			break;
		case Constants.RoyaltyTrendMerchandise:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.currency, Constants.royaltyTrendMerchandiseUI, Constants.royaltyTrendMerchandiseDestDb,
					Constants.royaltyTrendMerchandiseSrcDb };
			break;
		case Constants.RoyaltyTrendDonutGraph:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.currency, Constants.DoNutGraph, Constants.royaltyTrendDonutGraphUI,
					Constants.royaltyTrendPercentageUI, Constants.royaltyTrendDonutGraphDestDb,
					Constants.royaltyTrendPercentageDestDb, Constants.royaltyTrendDonutGraphSrcDb,
					Constants.royaltyTrendPercentageSrcDb };
			break;
		case Constants.newStatementDueComparison:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.UI,
					Constants.Destination, Constants.Source, Constants.status, Constants.Notes };

			break;
		case Constants.pipelineRoyltyRecordComparsion:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.period,
					"Currency UI", Constants.UI, "Period Dest", Constants.Destination, "Period Source",
					Constants.Source, Constants.status, Constants.Notes };
			break;
		case Constants.topTracks:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					"Track title UI", "Track Isrc code UI", Constants.currency, "Track Royalty By Download UI",
					"Track Units By Download UI", "Track Royalty By Stream UI", "Track Unit By Stream UI",
					"Track title Dest", "Track Isrc code Dest", "Track Royalty By Download Dest",
					"Track Units By Download Dest", "Track Royalty By Stream Dest", "Track Unit By Stream Dest",
					"Track title Source", "Track Isrc code Source", "Track Royalty By Download Source",
					"Track Units By Download Source", "Track Royalty By Stream Source", "Track Unit By Stream Source",
					"Status", "Notes" };
			break;
		case Constants.pipelineRoyltyPublishingComparsion:

			column = new String[] { Constants.site, Constants.client, Constants.period, "Currency UI", Constants.UI,
					"Period Dest", Constants.Destination, "Period Source", Constants.Source, Constants.status,
					Constants.Notes };
			break;
		case Constants.topLicensingProducts:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.productTitleUI, Constants.productConfigUI, Constants.productCodeUI, Constants.currencyUI,
					Constants.royaltyUI, Constants.periodDest, Constants.productTitleDest, Constants.productConfigDest,
					Constants.productCodeDest, Constants.royaltyDest, Constants.periodSource,
					Constants.productTitleSource, Constants.productConfigSource, Constants.productCodeSource,
					Constants.royaltySource, Constants.status, Constants.Notes };
			break;
		case Constants.topSyncProducts:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.productTitleUI, Constants.productConfigUI, Constants.productCodeUI, Constants.currencyUI,
					Constants.royaltyUI, Constants.periodDest, Constants.productTitleDest, Constants.productConfigDest,
					Constants.productCodeDest, Constants.royaltyDest, Constants.periodSource,
					Constants.productTitleSource, Constants.productConfigSource, Constants.productCodeSource,
					Constants.royaltySource, Constants.status, Constants.Notes };

			break;
		case Constants.topMrchndiseProducts:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.productTitleUI, Constants.productConfigUI, Constants.productCodeUI, Constants.currencyUI,
					Constants.royaltyUI, Constants.periodDest, Constants.productTitleDest, Constants.productConfigDest,
					Constants.productCodeDest, Constants.royaltyDest, Constants.periodSource,
					Constants.productTitleSource, Constants.productConfigSource, Constants.productCodeSource,
					Constants.royaltySource, Constants.status, Constants.Notes };
			break;
		case Constants.topPerformingTerritories:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.territoryNameUI, Constants.currencyUI, Constants.royaltyUI, Constants.periodDest,
					Constants.territoryNameDest, Constants.royaltyDest, Constants.periodSource,
					Constants.territoryNameSource, Constants.royaltySource, Constants.status, Constants.Notes };
			break;
		case Constants.latestStatement:
			column = new String[] { Constants.site, Constants.client, Constants.clientName, Constants.periodUI,
					Constants.royaltorCodeUI, Constants.royaltorNameUI, Constants.currencyUI, Constants.royaltyUI,
					Constants.periodDest, Constants.royaltorCodeDest, Constants.royaltorNameDest, Constants.royaltyDest,
					Constants.periodSource, Constants.royaltorCodeSource, Constants.royaltorNameSource,
					Constants.royaltySource, Constants.status, Constants.Notes };
		}
		return column;
	}
}
