package com.infosys.bmg.MyBMGDataValidations.utility;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Objectwait {
	public static void elementWait() throws InterruptedException{
		Thread.sleep(4000);
	}
	
	
	public static  void elementVisibility(String locator,WebDriver driver) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
		Thread.sleep(3000);
	}


}
