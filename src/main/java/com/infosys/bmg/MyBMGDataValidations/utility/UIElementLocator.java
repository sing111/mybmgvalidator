package com.infosys.bmg.MyBMGDataValidations.utility;

public class UIElementLocator {
	public static String UserName = "field-username";
	public static String Password = "field-password";
	public static String LoginBTN = "submitButton";
	public static String Greeting="greeting";
	public static String ModuleSelection="//span[text()='RECORDING']";
	public static String CreateNewList="//span[text()='Create new list']";
	public static String EnterClient ="//div[@class='client-sel-input-container']/input[@id='clients-input']";
	public static String SearchedClientTable="//*[@id='available-clients']";
	public static String ShowData="//div[@class='client-sel-footer']/button[@id='clientSelectionApply']";
	public static String SelectedClients="//button[text()=All clients'] ";
	public static String AllCurrencySelector="//button[text()='All Currencies'] ";
	public static String StatementCurrency="//div[@class='value']/span[@class='currency']";
	public static String StatementBalance="//div[@class='value']/span[@class='amount']";
    public static String StatementPeriod="//div[@class='period']";
    public static String SelectedClientTable="//*[@id='selected-clients']";
    public static String NoOfClients="//table[@id='selected-clients']/tbody/tr";
    
    public static String AnalysisModuleSelection="//div[@class='rec360-nav']/ul/li[2]/a/span[text()='ANALYSIS']";
    public static String getPeriod="//div[@class='slick-slide slick-current slick-active']/a/span";
    public static String grandtotalForTerr="//h1[@class='financial__title']/span[2]";
    public static String territories="//div/section[@class='financial__view financial__view--territory financial__view--rec__territory']/div[1]/ul/li";
    
    public static String periodPrevArrowScroll="//button[@class='btn btn-clean financial__period-selection__carousel__prev slick-arrow']";
    public static String periodNxtArrowScroll="//button[@class='btn btn-clean financial__period-selection__carousel__next slick-arrow']";
	public static String terrName="//span[@class='top-five-products-totalRoyalties']";
	public static String topProductsByTerritory="//div[@class='widget-territory-top-products financial__chart-pie__chart clearfix']/ul/li/div[@class='title-values-wrapper']";
   
	public static final String AlbumSingleTab = "//ul[@class='clearfix']/li[3]";
	public static final String TotalAlbumSingleRowCount = "//table[@id='financial-album-list']/tbody/tr";
	public static final String AllYearsFromUI = "//div[@class='highcharts-axis-labels highcharts-xaxis-labels ']/span";
	public static final String clickYear = "//div[@class='highcharts-axis-labels highcharts-xaxis-labels ']/span/span[@class='label-wrapper']";
	
	//top Licensing Products 
	public static final String topLicensingProducts="//div[@class='widget widget-trend-list widget-top-licence-incomes widget--small']/div[@class='widget-trend-list-container column-count-3']/ul/li";
    public static final String periodTopLicensingProducts="//div[@class='widget widget-trend-list widget-top-licence-incomes widget--small']/div[@class='widget-header-period']/div[@class='period']";

    //top Sync Products
    public static final String topSyncProducts="//div[@class='widget widget-trend-list widget-top-sync-incomes widget--small']/div[@class='widget-trend-list-container column-count-3']/ul/li";
    public static final String periodTopSyncProducts="//div[@class='widget widget-trend-list widget-top-sync-incomes widget--small']/div[@class='widget-header-period']/div[@class='period']";
    
    
    //top Sync Products
    public static final String topPPProducts="//div[@class='widget widget-trend-list widget-top-performance-incomes widget--small']/div[@class='widget-trend-list-container column-count-3']/ul/li";
    public static final String periodTopPPProducts="//div[@class='widget widget-trend-list widget-top-performance-incomes widget--small']/div[@class='widget-header-period']/div[@class='period']";
  
    //top Sync Products
    public static final String topMrchndiseProducts="//div[@class='widget widget-trend-list widget-top-merchandise-incomes widget--small']/div[@class='widget-trend-list-container column-count-3']/ul/li";
    public static final String periodTopMrchndiseProducts="//div[@class='widget widget-trend-list widget-top-merchandise-incomes widget--small']/div[@class='widget-header-period']/div[@class='period']";
    
    //top Perform Terr
    public static final String topPerformTerr="//div[@class='widget widget-top-territories']/ul/li/div[@class='col-50']";
    public static final String periodTopPerformTerr="//div[@class='widget widget-top-territories']/div[@class='widget-header-period']/div[@class='period']";
    
    //Latest Statements dashboard
    public static final String periodLatestStatemnt="//div[@class='widget widget-latest-statements widget--small']/div[@class='widget-header-period']/div[@class='period']";
    public static final String latestStatemnts="//div[@class='widget widget-latest-statements widget--small']/div/ul/li";
}
