package com.infosys.bmg.MyBMGDataValidations.utility;

public enum StatusMessage {
TEST_CASES_PROCESS_COMPLETED(200,"TEST_CASES_PROCESS_COMPLETED"),
FILE_UPLOAD_STARTED(301,"Uploading started"),
FILE_UPLOAD_COMPLETED(302,"Uploading Completed"),
STATUS_RECIEVED_SUCCESFULLY(303,"Status recieved succesfully"),
PATH_RECIEVED_SUCCESFULLY(304,"Path recieved succesfully"),
TEST_CASES_STARTED(305,"TEST_CASES_STARTED"),
TEST_CASES_COMPLETED(306,"Uploading started"),
READ_EXCEL_SUCCESS(307,"READ_EXCEL_SUCCESS"),
TEST_CASES_INITIATED(308,"TEST_CASES_INITIATED"),
READ_EXCEL_STARTED(309,"READ_EXCEL_STARTED"),


FILE_DOWNLOAD_ERROR(401,"Unable to download the file"),
UNABLE_TO_GET_STATUS(402,"Unable to get the status"),
FILE_UPLOAD_FAILED(403,"Uploading Failed"),
UNABLE_TO_GET_PATH(404,"Unable to get file path"),
INVALID_PROCESS_ID(405,"INVALID_PROCESS_ID"),
TEST_CASES_FAILED(406,"TEST_CASES_COMPLETED"),
READ_EXCEL_FAILED(407,"READ_EXCEL_FAILED"),
	


	;

	private Integer code;
	private String message;

	private StatusMessage(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
