package com.infosys.bmg.MyBMGDataValidations.utility;

public class Constants {
	public static final String statementComparison="StatementBalance";
	public static final String newStatementDueComparison="New Statement Due";
	public static final String pipelineRoyltyRecordComparsion="Pipeline Royalty Recording";
	public static final String pipelineRoyltyPublishingComparsion="Pipeline Royalty Publishing";
	public static final String client="Client";
	public static final String periodUI="Period UI";
	public static final String statementBalanceUI="Statement Balance UI";
	public static final String statmentPeriodSourceDB=" Period Source DB";
	public static final String statementPeriodDestinationDB="Period myBmg DB";
	public static final String currencyDestiantionDB="Currency myBmg DB";
	public static final String destiantionDB="Statement Balance Destination DB";
	public static final String sourceDb="Statement Balance Source DB";
	public static final String site="Site";
	public static final String topTerrComparison="Top Territories";
	public static final String topTerrUI="Top Territories UI";
	public static final String grandTotalUI="Total Amount UI";
	public static final String terrAmountUI="Terr Amount UI";
	public static final String terrAmountDestDb="Terr Amount Destination DB";
	public static final String terrAmountSourceDb="Terr Amount Source DB";
	public static final String topTerrDestDb="Top Territories Destination DB";
	public static final String grandTotalDestDb="Total Amount Destination DB";
	public static final String grandTotalSourceDb="Total Amount Source DB";
	public static final String periodDestDB="Period Destination DB";
	public static final String topTerrProductsBreakDownComparision="Terr Breakdown by Products";
	public static final String totalNoOfAlbumSingleComparision="Total Number of Album/Single";
	public static final String totalNoOfAlbumSingleUI="Total Album/Single UI ";
	public static final String totalNoOfAlbumSingleDestDb="Total Album/Single DestDb";
	public static final String totalNoOfAlbumSingleSrcDb="Total Album/Single SrcDb";
	public static final String RoyaltyTrendDigital = "Royalty Trend-Digital";
	public static final String RoyaltyTrendTotalRoyalty = "Royalty Trend-Total Royalty";
	public static final String RoyaltyTrendLicensing = "Royalty Trend-Licensing";
	public static final String RoyaltyTrendMerchandise = "Royalty Trend-Merchandise";
	public static final String RoyaltyTrendDonutGraph = "Royalty Trend-DonutGraph";
	public static final String RoyaltyTrendSync = "Royalty Trend-Sync";
	public static final String RoyaltyTrendPublicPerformance = "Royalty Trend-Public Performance";
	public static final String RoyaltyTrendPhysical = "Royalty Trend-Physical";
	public static final String royaltyTrendPhyUI="Royalty Trend-Physical UI";
	public static final String royaltyTrendPhyDestDb="Royalty Trend-Physical SrcDb";
	public static final String royaltyTrendPhySrcDb="Royalty Trend-Physical DestDb";
	public static final String royaltyTrendDigitalSrcDb="Royalty Trend-Digital SrcDb";
	public static final String royaltyTrendDigitalUI="Royalty Trend-Digital UI";
	public static final String royaltyTrendDigitalDestDb="Royalty Trend-Digital DestDb";
	public static final String royaltyTrendTotalRoyaltyUI="Royalty Trend-TotalRoyalty UI";
	public static final String royaltyTrendTotalRoyaltyDestDb="Royalty Trend-TotalRoyalty DestDb";
	public static final String royaltyTrendTotalRoyaltySrcDb="Royalty Trend-TotalRoyalty SrcDb";
	public static final String royaltyTrendPercentageUI = "% UI";
	public static final String royaltyTrendPercentageDestDb = "% Dest Db";
	public static final String royaltyTrendPercentageSrcDb = "% Src Db";
	public static final String UI="UI";
	public static final String Source="Source";
	public static final String Destination="Destination";
	public static final String period="Period";
	public static final String topTracks="Top Tracks";
	public static final String royaltyTrendLicensingUI="Royalty Trend-Licensing UI";
	public static final String royaltyTrendLicensingDestDb="Royalty Trend-Licensing DestDb";
	public static final String royaltyTrendLicensingSrcDb="Royalty Trend-Licensing SrcDb";
	public static final String royaltyTrendPublicPerformanceUI="Royalty Trend-PublicPerformance UI";
	public static final String royaltyTrendPublicPerformanceDestDb="Royalty Trend-PublicPerformance DestDb";
	public static final String royaltyTrendPublicPerformanceSrcDb="Royalty Trend-PublicPerformance SrcDb";
	public static final String royaltyTrendSyncUI="Royalty Trend-Sync UI";
	public static final String royaltyTrendSyncDestDb="Royalty Trend-Sync DestDb";
	public static final String royaltyTrendSyncSrcDb="Royalty Trend-Sync SrcDb";
	public static final String royaltyTrendMerchandiseUI="Royalty Trend-Merchandise UI";
	public static final String royaltyTrendMerchandiseDestDb="Royalty Trend-Merchandise DestDb";
	public static final String royaltyTrendMerchandiseSrcDb="Royalty Trend-Merchandise SrcDb";
	public static final String royaltyTrendDonutGraphUI="Royalty Trend-DonutGraph UI";
	public static final String royaltyTrendDonutGraphDestDb="Royalty Trend-donutGraph DestDb";
	public static final String royaltyTrendDonutGraphSrcDb="Royalty Trend-DonutGraph SrcDb";
	public static final String DoNutGraph="DoNut Graph";
	public static final String statusPass="pass";
	public static final String statusFail="fail";
	public static final String errMessage1="UI value and dest value donot match";
	public static final String errMessage2="UI value is null but Destination and Source values are not null";
    public static final String errMessage3="Source and Destination values do not match";
    public static final String errMessage4="Destination value is null but UI and Source values are not null";
    public static final String errMessage5="Source value is null but UI and Destination value are not null";
	public static final String currency="Currency";
    //Dashboard widgets
    public static final String topLicensingProducts="Top Licensing Products";
    public static final String noDataOnUIMessage="No Data for this Client selection available On UI and Backend";
    public static final String configErrMessage1="Config Desc from Destination and Source Donot Match";
    public static final String configErrMessage2="Config Desc from UI and Destination Donot Match";
    public static final String productCodeErrMessage1="Product Code  from Destination and Source Donot Match";
    public static final String productCodeErrMessage2="Product Code  from UI and Destination Donot Match";
    public static final String royaltyErrMessage1="Royalty  from Destination and Source Donot Match";
    public static final String royaltyErrMessage2="Royalty from UI and Destination Donot Match";
    public static final String clientName="Client Name";
    public static final String productTitleUI="Product Title UI";
    public static final String productConfigUI="Config Desc UI";
    public static final String productCodeUI="Product Code UI";
    public static final String currencyUI="Currency";
    public static final String royaltyUI="Total Royalty UI";
    public static final String periodDest="Period Dest";
    public static final String productTitleDest="Product Title Dest";
    public static final String productConfigDest="Config Desc Dest";
    public static final String productCodeDest="Product Code Dest";
    public static final String royaltyDest="Total Royalty Dest";
    public static final String periodSource="Period Source";
    public static final String productTitleSource="Product Title Source";
    public static final String productConfigSource="Config Desc Source";
    public static final String productCodeSource="Product Code Source";
    public static final String royaltySource="Total Royalty Sources";
    public static final String topSyncProducts="Top Sync Products";   
    public static final String topPPProducts="Top Public Performace Products";
    public static final String topMrchndiseProducts="Top Merchandise Products";  
    
    //Top Perfroming Terr
    public static final String terrNameErrMsg1="Territory Name in Destination  and Source donot match";
    public static final String terrNameErrMsg2="Territory Name in UI and Destinaton donot match";
    
    public static final String terrRoyaltyErrMsg1="Territory Royalty in Destination  and Source donot match";
    public static final String terrRoyaltyErrMsg2="Territory Royalty in UI and Destinaton donot match";
    public static final String topPerformingTerritories="Top Performing Territories";
    public static final String territoryNameUI="Territory Name UI";
    public static final String territoryNameDest="Territory Name Dest";
    public static final String territoryNameSource="Territory Name Source";
    public static final String status="Status";
    public static final String Notes="Notes";
    
    
    //Latest Statemnt
    public static final String clientCodeErrMsg1="Client Code in Destination  and Source donot match";
      public static final String clientCodeErrMsg2="Client Code in UI and Destinaton donot match";
    
    public static final String royaltorNameErrMsg1="Royaltor Name in Destination  and Source donot match";
    public static final String royaltorNameErrMsg2="Royaltor Name in UI and Destinaton donot match";
    
    public static final String royaltyLatestStmntErrMsg1="Royalty Value in Destination  and Source donot match";
    public static final String royaltyLatestStmntErrMsg2="Royalty Value in UI and Destinaton donot match";
    public static final String latestStatement="Latest Statement";
    public static final String royaltorNameUI="Royaltor Name UI";
    public static final String royaltorNameDest="Royaltor Name Dest";
    public static final String royaltorNameSource="Royaltor Name Source";		
    public static final String royaltorCodeUI="Royaltor Code UI";
    public static final String royaltorCodeDest="Royaltor Code Dest";
    public static final String royaltorCodeSource="Royaltor Code Source";	
	
	
	
   	public static final String MY_BMG_BASE="/dataValidation";
	public static final String UPLOAD_FILE="/uploadFile";
	public static final String DOWNLOAD_FILE="/downloadFile/{pId}";
	public static final String GET_STATUS="/getStatus/{pId}";
	public static final String START_TEST="/startTest/{pId}";
	public static final String FILE_PATH="Path";
	public static final String STATUS="status";
	public static final String CODE="code";
	public static final String MESSAGE="message";
	
 

}
