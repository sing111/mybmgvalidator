/**
 * Copyright (C) : 2017 BMG
 *
 * Modification Log
 * Date           Author    Remarks
 * May 09, 2017   Infosys   Created.
 *
 */
package com.infosys.bmg.MyBMGDataValidations.logging.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 *This is aspect for Logging purpose.
 * @author Infosys
 */

@Aspect
@Component
public class LoggingAspect {

  /**
   * @param pjp
   * @return log
   * @throws Throwable
   */
  @Around("execution(public * com.infosys.bmg.MyBMGDataValidations..*(..))")
  public Object log(final ProceedingJoinPoint pjp) throws Throwable {
   final Logger log = LoggerFactory.getLogger(pjp.getTarget().getClass());
		final Object[] args = pjp.getArgs();
		final String methodName = pjp.getSignature().getName();
		log.info(" Entering {}: {}", methodName , args);
		final Object result = pjp.proceed();
		log.info("Leaving {}():{}: result={}", methodName ,args,result);
		return result;
  }

  

}
