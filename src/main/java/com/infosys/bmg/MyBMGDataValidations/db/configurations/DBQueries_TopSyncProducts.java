package com.infosys.bmg.MyBMGDataValidations.db.configurations;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.infosys.bmg.MyBMGDataValidations.utility.Log;


public class DBQueries_TopSyncProducts {


		public  Map<String, String> mapTopSyncRoyaltyDest = new HashMap<>();
		public  Map<String, String> mapTopSyncConfigDest = new HashMap<>();
		public  Map<String, String> mapTopSyncProductTitleDest = new HashMap<>();
		public  Map<String, String> mapTopSyncPeriodDest = new HashMap<>();
		public  List<String> topSyncProductCodeDest = new ArrayList<String>();
		public  Map<String, String> mapTopSyncRoyaltySource = new HashMap<>();
		public  Map<String, String> mapTopSyncProductTitleSource = new HashMap<>();
		public  Map<String, String> mapTopSyncConfigSource = new HashMap<>();
		public  Map<String, String> mapTopSyncPeriodSource = new HashMap<>();
		public  List<String> topSyncProductCodeSource = new ArrayList<String>();
	
		public  List<List<String>> topSyncProductFinalList = new ArrayList<List<String>>();


		public  void DB_Query(String site, String client, ArrayList<String> scrossClientList, String crossClientId,
				 String period)
				throws ClassNotFoundException, SQLException, InterruptedException, IOException {
			mapTopSyncConfigDest= new HashMap<>();
			mapTopSyncConfigSource = new HashMap<>();
			mapTopSyncPeriodDest = new HashMap<>();
			mapTopSyncPeriodSource= new HashMap<>();
			mapTopSyncProductTitleDest = new HashMap<>();
			mapTopSyncProductTitleSource = new HashMap<>();
			mapTopSyncRoyaltyDest = new HashMap<>();
			mapTopSyncRoyaltySource = new HashMap<>();
			topSyncProductCodeSource = new ArrayList<String>();
			topSyncProductCodeDest = new ArrayList<String>();
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=mybmg4_stage_rec";
			
			Connection conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007",
					"Si2RIbVdVkDyCIVVBmbA");

			Statement stmtDestDb = conDestinationDb.createStatement();

			
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dbUrlBmgPrimeLandingLaye = "jdbc:sqlserver://10.6.238.202:1433;databaseName=BMGPrime_LandingLayer";
			Connection connBmgPrime = DriverManager.getConnection(dbUrlBmgPrimeLandingLaye, "batr007",
					"Si2RIbVdVkDyCIVVBmbA");
			Statement stmtBmgPrime = connBmgPrime.createStatement();
			try {
			List<String> clientIdList = new ArrayList<String>();
			clientIdList.add("'" + client + "'");
			if (crossClientId != null) {
				clientIdList.add(crossClientId);
			} else if (!scrossClientList.isEmpty()) {
				String cId = "";
				cId = scrossClientList.toString().replace("[", "").replace("]", "");
				clientIdList.add(cId);

			}
			String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
			String month = null;
			String year = null;
	        if(period!=null) {
			StringBuilder sb = new StringBuilder();
			System.out.println(period);
			period = period.toString().replace("(", "").replace(")", "");
			String s[] = period.split("/");
			month = s[0];
			year = s[1];
			sb.append(year + "" + month);
			period = sb.toString();
			System.out.println(period + ":period");

			String queryToGetTopSyncProductsDest = "SELECT TOP 5 "+
					  "P.Product_Title, "+
					  " P.Product_UserCode, "+
					  " PC.Config_Desc, "+
					  " SUM(RE.Earnings_Amount) AS totalRoyalty "+
					 "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "+
					   "INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "+
					   "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "+
					  "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "+
					   "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "+
					   "INNER JOIN Client CL ON RE.Client_Key = CL.client_key "+
					 "WHERE "+
					   "CL.Client_Code IN (" + clientIds + ")  AND CL.Client_Site='" + site + "'" +
					   "AND PR.Period IN ('"+period+"') "+
					   "AND DC.DistChannel_Category = 'Synchronisation' "+
					 "GROUP BY P.Product_Key, P.Product_Title,P.Product_UserCode, "+
					  " PC.Config_Desc "+
					 "ORDER BY totalRoyalty DESC";

			ResultSet topSyncProductListDest = stmtDestDb.executeQuery(queryToGetTopSyncProductsDest);

				while (topSyncProductListDest.next()) {
				String productTitle = topSyncProductListDest.getString("Product_Title");
				String productIsrcCode = topSyncProductListDest.getString("Product_UserCode");
				String configDesc=topSyncProductListDest.getString("Config_Desc");
				String totalRoyalty=topSyncProductListDest.getString("totalRoyalty");
				
				topSyncProductCodeDest.add(productIsrcCode);
				mapTopSyncProductTitleDest.put(productIsrcCode, productTitle);
				mapTopSyncConfigDest.put(productIsrcCode, configDesc);
				mapTopSyncRoyaltyDest.put(productIsrcCode, totalRoyalty);
				mapTopSyncPeriodDest.put(productIsrcCode, period);
			}
			
	        

		

			String queryToGetTopSyncProductsSource = "select top 5 "+
					"PD.PRODUCT_TITLE, "+
					"PD.Product_UserCode, "+
					"PC.Config_Desc, "+
					" SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT "+
					 "from  "+
					"  (select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ, "+
					 "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID "+
					 " from tbl_STG_RoycalcPayee dcrx "+
					" inner join "+
					"(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ , "+
					" RP.roycalcpayee_bfbalance,COUNT(*) as reccount "+
					" from ( "+
					" select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code "+
					 "from tbl_STG_RoycalcPayee  "+
					"	JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Contract where  CONTRACT_NAME IS NOT NULL AND NVL(CONTTYPE_CODE,'''') not in (''JVAT'',''PAGC'')')) CO  "+
					 "    ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE "+
					"					 ) rp "+
									
					 " group by  "+
					 "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ, "+
					" RP.roycalcpayee_bfbalance) DUP "+
					" on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "+
					 "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "+
					" group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ, "+
					 "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP  "+
					 "  JOIN tbl_STG_SalesWorkArc "+
					 "   SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ  "+
					  "  JOIN tbl_STG_Calclog CL "+
					 "    ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F' "+
					 "    AND CL.CALCLOG_PERIODTYPE<>'A'   "+   
					 "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD "+
					 "    ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "+
					 "    INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF "+
					  "   ON CF.CONFIG_CODE=SWA.CONFIG_CODE "+
					"INNER JOIN   (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC "+
						"				ON PC.CONFIG_CODE=PD.CONFIG_CODE   "+
					   " where SWA.DISTCHAN_CODE IN ('LI','MSTL') "+
					  "   and rcp.royaltor_code in (" + clientIds + ") and rcp.royaltor_site='" + site + "'" +
					  "   and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) +  substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)=('"+period+"') "+
										
					  "   GROUP BY  "+
					  "   PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE, PD.Product_UserCode, "+
					"PC.Config_Desc "+
					" ORDER BY SUM(SWA.SALESWORKARC_EARNINGS)  DESC ";


			ResultSet topSyncProductsSource = stmtBmgPrime.executeQuery(queryToGetTopSyncProductsSource);
		
			while (topSyncProductsSource.next()) {
				String productTitle = topSyncProductsSource.getString("PRODUCT_TITLE");
				String productIsrcCode = topSyncProductsSource.getString("Product_UserCode");
				String configDesc=topSyncProductsSource.getString("Config_Desc");
				String totalRoyalty=topSyncProductsSource.getString("AMOUNT");
				
				topSyncProductCodeSource.add(productIsrcCode);
				mapTopSyncProductTitleSource.put(productIsrcCode, productTitle);
				mapTopSyncConfigSource.put(productIsrcCode, configDesc);
				mapTopSyncRoyaltySource.put(productIsrcCode, totalRoyalty);
				mapTopSyncPeriodSource.put(productIsrcCode, period);
			}

	        }
		}catch(Exception e) {
			e.printStackTrace();
			Log.error("Error in DBQueries_Top_SyncProducts: "+e);
			
		}finally {
			stmtDestDb.close();
			conDestinationDb.close();
			stmtBmgPrime.close();
			conDestinationDb.close();
			
		}
		
	}	}
