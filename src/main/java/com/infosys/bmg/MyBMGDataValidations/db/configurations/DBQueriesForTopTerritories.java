package com.infosys.bmg.MyBMGDataValidations.db.configurations;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBQueriesForTopTerritories {
	
	
	public  String grandtotalDestDb = null;
	public  String grandtotalSourceDb = null;
	public  String grandtotalSourceRemaDb = null;
	public  String grandtotalSourcePrimeDb = null;
	public  Map<String,String> terrAmmtDetailsDestDb =new HashMap<>();
	public  Map<String,String> terrAmmtDetailsSourceDb =new HashMap<>();
	public  Map<String,String> terrAmmtDetailsSourceRemaDb =new HashMap<>();
	public  Map<String,String> terrAmmtDetailsSourcePrimeDb =new HashMap<>();
	
	
	
	public  void DB_Query_Terr(String site, String client,String period ,
			ArrayList<String> scrossClientList, String crossClientId,List<String> allPeriods)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
	
		//String period=trrSinglePeriod;
		String month = null,year=null;
		String periodforRema=null;
		String periodforPrime=null;
		StringBuilder sb=new StringBuilder();
		StringBuilder sb1=new StringBuilder();
		String prefix="" ,prefix1= "";
		
		if(period!=null){
			String s[]=period.split("/");
				month=s[0];
				year=s[1];
				sb.append(year+""+month);
				period=sb.toString();
				System.out.println(period+":period");
		}else{/*
			//default all periods
			boolean flag=false;
			for(String periods:allPeriods){
				String pe[]=periods.split("/");
				month=pe[0];
				year=pe[1];
				sb.append(prefix);
				prefix = ",";
				sb.append("'"+year+""+month+"'");
				period=sb.toString();
				System.out.println(period);
				int yr=Integer.parseInt(year);
				if(yr<=2016){
					periodforRema=sb.toString();
					
				}else if(yr>2016){
					sb1.append(prefix1);
					prefix1 = ",";
					sb1.append("'"+year+""+month+"'");
					periodforPrime=sb1.toString();
				}
				
			}
			
		*/}
		
		
		
		
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		//String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.243:1433;databaseName=mybmg4_prod_rec";
		String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=Mybmg4_stage_rec";
		List<String> clientIdList = new ArrayList<String>();
		clientIdList.add("'" + client + "'");
		if (crossClientId != null) {
			clientIdList.add(crossClientId);
		} else if (!scrossClientList.isEmpty()) {
			String cId = "";
			cId = scrossClientList.toString().replace("[", "").replace("]", "");
			clientIdList.add(cId);

		}
		String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
		String queryToGetTopTerr = "SELECT Territory.Territory_Name,sum(Rec_Earnings.Earnings_Amount) as Amount,"
				+ " sum(sum(Rec_Earnings.Earnings_Amount)) over () as grandtotal "
				+ " FROM [mybmg4_stage_rec].[dbo].[Products] "
				+ " INNER JOIN"
				+ "  [mybmg4_stage_rec].[dbo].[Rec_Earnings] ON Products.Product_Key = Rec_Earnings.Product_Key "
				+ " INNER JOIN"
				+ "  [mybmg4_stage_rec].[dbo].[Client] ON Rec_Earnings.Client_Key = Client.Client_Key"
				+ " INNER JOIN"
				+ "  [mybmg4_stage_rec].[dbo].[Period] ON Rec_Earnings.Period_Key = Period.Period_Key"
				+ " INNER JOIN"
				+ "  [mybmg4_stage_rec].[dbo].[Territory] ON Rec_Earnings.Territory_Key = Territory.Territory_Key"
				+ " WHERE     (Period.Period IN("+period+")) AND (Client.Client_Site = '"+site+"') AND"
				+ " (Client.Client_Code IN ("+clientIds+"))"
				+ " group by Territory.Territory_Name  order by sum(Rec_Earnings.Earnings_Amount) desc";

		Connection conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");

		Statement stmtDestDb = conDestinationDb.createStatement();
		ResultSet topTerrList = stmtDestDb.executeQuery(queryToGetTopTerr);
		Map<String,String> map=new HashMap<>();
		//List<String> l=new ArrayList<>();
		//List<String> l1=new ArrayList<>();
		//ArrayList<List<String>> list=new ArrayList<>();
		while (topTerrList.next()) {
			for (String clientId : clientIdList) {
				clientId = clientId.toString().replace("'", "").replace("'", "");
				String terrName = topTerrList.getString("Territory_Name");
				//l.add(terrName);
				String amount= topTerrList.getString("Amount");
				//l1.add(amount);
				map.put(terrName, amount);
				//System.out.println(map);
		}
			//list.add(l);
			//list.add(l1);
			grandtotalDestDb= topTerrList.getString("grandtotal");
	}
		terrAmmtDetailsDestDb=map;
		System.out.println(grandtotalDestDb+"::grandtotalDestDb");
		System.out.println(terrAmmtDetailsDestDb+":terrAmmtDetails");
		
		if(allPeriods!=null){
			if(periodforRema!=null)
				dbQueryForRema(period,periodforRema,site,clientIds,clientIdList);
			/*if(periodforPrime!=null)
				dbQueryForPrime(period,periodforPrime,site,clientIds,clientIdList);	
			if(periodforRema!=null && periodforPrime!=null){
				grandtotalSourceDb=grandtotalSourceRemaDb+grandtotalSourcePrimeDb;
			}*/
			
		}else{
			int yr=Integer.parseInt(year);
			if(yr<=2016){
				dbQueryForRema(period,periodforRema,site,clientIds,clientIdList);
			}else{
				dbQueryForPrime(period,periodforPrime,site,clientIds,clientIdList);	
			}
			
		}
		
	}


	private  void dbQueryForRema(String period,String periodsforRema,String site,String clientIds,List<String> clientIdList) {
		grandtotalSourceDb=null;
		terrAmmtDetailsSourceDb=null;
		try {
			/*if(periodsforRema!=null){
				period=periodsforRema;
			}
			*/
			String queryToGetTopTerrRema="select TRR.TERRITORY_NAME, TRR.TERRITORY_CODE,SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT ,sum(sum(SWA.SALESWORKARC_EARNINGS)) over () as grandtotal"
					+ " from  (select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
					+ " dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID"
					+ " from RMAESTRO_USER.RoycalcPayee dcrx"
					+ " inner join"
					+ "(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,"
					+ " RP.roycalcpayee_bfbalance,COUNT(*) as reccount from "
					+ " (select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code"
					+ " from RMAESTRO_USER.RoycalcPayee ) rp group by "
					+ " RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,"
					+ " RP.roycalcpayee_bfbalance) DUP "
					+ " on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ"
					+ " where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1)"
					+ " group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
					+ " dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP "
					+ " JOIN RMAESTRO_USER.SalesWorkArc "
					+ "SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ "
					+ "JOIN RMAESTRO_USER.CALCLOG CL"
					+ " ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F'"
					+ " AND CL.CALCLOG_PERIODTYPE<>'A' JOIN RMAESTRO_USER.CONTRACT CO"
					+ " ON CO.CONTRACT_CODE=RCP.RCONTRACT_CODE AND CO.CONTRACT_SITE=RCP.RCONTRACT_SITE AND NVL(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC') "
					+ " INNER JOIN"
					+ " RMAESTRO_USER.TERRITORY TRR "
					+ " ON "
					+ " LTRIM(RTRIM(TRR.TERRITORY_CODE))=CASE WHEN LTRIM(RTRIM(SWA.TERRITORY_CODE))='EX' THEN '**' ELSE LTRIM(RTRIM(SWA.TERRITORY_CODE)) END  "
					+ " where rcp.royaltor_code in("+clientIds+") and rcp.royaltor_site='"+site+"' "
					+ " and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) ||  SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2) In("+period+")"
					+ "GROUP BY "
					+ "TRR.TERRITORY_NAME, TRR.TERRITORY_CODE order by "
					+ "SUM(SWA.SALESWORKARC_EARNINGS) DESC";
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con=DriverManager.getConnection("jdbc:oracle:thin:@10.6.238.172:1480:DBOPRIM1","RMAESTRO_USER","counterp");  
			Statement stmtRematDb=con.createStatement();  
			ResultSet topTerrList = stmtRematDb.executeQuery(queryToGetTopTerrRema);	
			//List<String> terrList=new ArrayList<>();
			//List<String> terrAmnt=new ArrayList<>();
			Map<String,String> map=new HashMap<>();
			while (topTerrList.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					String terrName = topTerrList.getString("Territory_Name");
					//terrList.add(terrName);
					String amount= topTerrList.getString("Amount");
					//terrAmnt.add(amount);
					map.put(terrName, amount);
					//System.out.println(map);
			}
				grandtotalSourceDb= topTerrList.getString("grandtotal");
			
		} 
			terrAmmtDetailsSourceDb=map;
			
			System.out.println(grandtotalSourceDb+"::grandtotalSourceDb");
			System.out.println(terrAmmtDetailsSourceDb+":terrAmmtDetailsSourceDb");
			/*if(periodsforRema==null){
				grandtotalSourceDb=grandtotalSourceRemaDb;
				terrAmmtDetailsSourceDb=terrAmmtDetailsSourceRemaDb;
			}
			*/
		}catch (ClassNotFoundException e ) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}   
		
	}
	
	

	private  void dbQueryForPrime(String period,String periodsforPrime, String site, String clientIds, List<String> clientIdList) {
		grandtotalSourceDb=null;
		terrAmmtDetailsSourceDb=null;
		try{
			/*if(periodsforPrime!=null){
				period=periodsforPrime;
			}
			*/
			String queryToGetTopTerrPrime="select TRR.TERRITORY_NAME, TRR.TERRITORY_CODE,SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT ,sum(sum(SWA.SALESWORKARC_EARNINGS)) over () as grandtotal "
				+ " from  (select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
				+ " dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID from tbl_STG_RoycalcPayee dcrx"
				+ " inner join "
				+ "(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,RP.roycalcpayee_bfbalance,COUNT(*) as reccount from ("
				+ " select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code from tbl_STG_RoycalcPayee ) rp"
				+ " group by  RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,RP.roycalcpayee_bfbalance) DUP on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ"
				+ " where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
				+ " group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
				+ " dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP "
				+ " JOIN tbl_STG_SalesWorkArc SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ "
				+ " JOIN tbl_STG_Calclog CL "
				+ " ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F' AND CL.CALCLOG_PERIODTYPE<>'A'"
				+ " JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Contract where NVL(CONTTYPE_CODE,'''') not in (''JVAT'',''PAGC'') ')) CO "
				+ " ON CO.CONTRACT_CODE=RCP.RCONTRACT_CODE AND CO.CONTRACT_SITE=RCP.RCONTRACT_SITE "
				+ " INNER JOIN "
				+ " (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Territory')) TRR ON "
				+ " TRR.TERRITORY_CODE=CASE WHEN LTRIM(RTRIM(SWA.TERRITORY_CODE))='EX' THEN '**' ELSE LTRIM(RTRIM(SWA.TERRITORY_CODE)) END  "
				+ " where rcp.royaltor_code IN ("+clientIds+")  and rcp.royaltor_site='"+site+"' "
				+ " and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) +  substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)="+period
				+ " GROUP BY TRR.TERRITORY_NAME, TRR.TERRITORY_CODE "
				+ " ORDER BY  SUM(SWA.SALESWORKARC_EARNINGS) DESC";
		
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dbUrlBmgPrimeLandingLayer = "jdbc:sqlserver://10.6.238.202:1433;databaseName=BMGPrime_LandingLayer";
			Connection connBmgPrime = DriverManager.getConnection(dbUrlBmgPrimeLandingLayer, "batr007","Si2RIbVdVkDyCIVVBmbA");
			Statement stmtRematDb=connBmgPrime.createStatement();  
			ResultSet topTerrList = stmtRematDb.executeQuery(queryToGetTopTerrPrime);	
			//List<String> terrList=new ArrayList<>();
			//List<String> terrAmnt=new ArrayList<>();
			Map<String,String> map=new HashMap<>();

		while (topTerrList.next()) {
			for (String clientId : clientIdList) {
				clientId = clientId.toString().replace("'", "").replace("'", "");
				String terrName = topTerrList.getString("Territory_Name");
			//	terrList.add(terrName);
				String amount= topTerrList.getString("Amount");
			//	terrAmnt.add(amount);
				map.put(terrName, amount);
			//	System.out.println(map);
		}
		grandtotalSourceDb= topTerrList.getString("grandtotal");
	} 
		terrAmmtDetailsSourceDb=map;
		System.out.println(grandtotalSourceDb+"::grandtotalSourceDb");
		System.out.println(terrAmmtDetailsSourceDb+":terrAmmtDetailsSourceDb");
		/*if(periodsforPrime==null){
			grandtotalSourceDb=grandtotalSourceRemaDb;
			terrAmmtDetailsSourceDb=terrAmmtDetailsSourceRemaDb;
		}*/
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	/*String s[]=allPeriods.split("\n");
	for(String p:s){
		String pe[]=p.split("/");
		month=pe[0];
		year=pe[1];
		sb.append(prefix);
		prefix = ",";
		sb.append("'"+year+""+month+"'");
		period=sb.toString();
		int yr=Integer.parseInt(year);
		if(yr<=2016){
			periodforRema=sb.toString();
			
		}else if(yr>2016){
			sb1.append(prefix1);
			prefix1 = ",";
			sb1.append("'"+year+""+month+"'");
			periodforPrime=sb1.toString();
		}
		
	}
	System.out.println(period+":periodForDest");
	System.out.println(periodforRema+":periodforRema");
	System.out.println(periodforPrime+":periodforPrime");*/
	
	
}


