package com.infosys.bmg.MyBMGDataValidations.db.configurations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBQueries_PipelineRoyalty_Publishing {

	public  String pipelineRoyaltyPublishingDest1 = null;
	public  String pipelineRoyaltyPublishingSource1 = null;
	public  String pipelineRoyltyPeriodPublishingDest1 = null;
	public  String pipelineRoyltyPeriodPublishingSource1 = null;
	public  String pipelineRoyaltyPublishingSource2 = null;
	public  String pipelineRoyltyPeriodPublishingSource2 = null;
	public  String pipelineRoyltyPeriodPublishingDest2 = null;
	 public  String pipelineRoyaltyPublishingDest2 = null;
	 public  String pipelineRoyltyPeriodPublishingDest3 = null;
	 public  String pipelineRoyaltyPublishingDest3 = null;

	public  void DB_Query_PipelineRoyalty_Publishing(String site, String client,
			ArrayList<String> scrossClientList, String crossClientId) throws SQLException, ClassNotFoundException {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.243:1433;databaseName=mybmg4_prod_pub";

		Connection conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");

		Statement stmtDestDb = conDestinationDb.createStatement();

		Class.forName("com.ibm.as400.access.AS400JDBCDriver");
		String dbUrlRecroProd = "jdbc:as400://10.6.245.238:8070/MPUBFILE;transaction isolation=none";
		Connection conRecroProdDb = DriverManager.getConnection(dbUrlRecroProd, "GGMONUSRP", "bmg123");

		Statement stmtRecroProdDb = conRecroProdDb.createStatement();

		try {
			pipelineRoyaltyPublishingDest1 = null;
			pipelineRoyaltyPublishingSource1 = null;
			pipelineRoyltyPeriodPublishingDest1 = null;
			pipelineRoyltyPeriodPublishingSource1= null;
			pipelineRoyaltyPublishingSource2 = null;
			pipelineRoyltyPeriodPublishingSource2 = null;
			 pipelineRoyltyPeriodPublishingDest2 = null;
			pipelineRoyaltyPublishingDest2 = null;
		 pipelineRoyltyPeriodPublishingDest3 = null;
			pipelineRoyaltyPublishingDest3 = null;
			List<String> clientIdList = new ArrayList<String>();
			clientIdList.add("'" + client + "'");
			if (crossClientId != null) {
				clientIdList.add(crossClientId);
			} else if (!scrossClientList.isEmpty()) {
				String cId = "";
				cId = scrossClientList.toString().replace("[", "").replace("]", "");
				clientIdList.add(cId);

			}
			String clientIds = clientIdList.toString().replace("[", "").replace("]", "");

			String queryToFetchPipleLineRoyltyDest1 = "SELECT  [CLIENT_ID] "+
				   " ,[AMOUNT]  ,[periodID] "+ 
		 "FROM [mybmg4_prod_pub].[dbo].[MUMA_PIPELINE] where client_id in (" + clientIds + ")  "+
		"and periodid=(Select max(periodId) from [mybmg4_prod_pub].[dbo].[MUMA_PIPELINE] where client_id in (" + clientIds + "))";

			ResultSet pipleLineRoyltyDestDB1 = stmtDestDb.executeQuery(queryToFetchPipleLineRoyltyDest1);

			while (pipleLineRoyltyDestDB1.next()) {
				pipelineRoyaltyPublishingDest1 = pipleLineRoyltyDestDB1.getString("AMOUNT");
				pipelineRoyltyPeriodPublishingDest1 = pipleLineRoyltyDestDB1.getString("periodID");
			}
			
			String queryToFetchPipleLineRoyltyDest2 = "SELECT  [CLIENT_ID] "+
					   " ,[AMOUNT]  ,[periodID] "+ 
			 "FROM [mybmg4_prod_pub].[dbo].[MUMA_PIPELINE1] where client_id in (" + clientIds + ")  "+
			"and periodid=(Select max(periodId) from [mybmg4_prod_pub].[dbo].[MUMA_PIPELINE1] where client_id in (" + clientIds + "))";

				ResultSet pipleLineRoyltyDestDB2 = stmtDestDb.executeQuery(queryToFetchPipleLineRoyltyDest2);

				while (pipleLineRoyltyDestDB2.next()) {
					pipelineRoyaltyPublishingDest2 = pipleLineRoyltyDestDB2.getString("AMOUNT");
					pipelineRoyltyPeriodPublishingDest2 = pipleLineRoyltyDestDB2.getString("periodID");
				}
				
				String queryToFetchPipleLineRoyltyDest3 = "SELECT  [CLIENT_ID] "+
						   " ,[AMOUNT]  ,[periodID] "+ 
				 "FROM [mybmg4_prod_pub].[dbo].[MUMA_PIPELINE2] where client_id in (" + clientIds + ")  "+
				"and periodid=(Select max(periodId) from [mybmg4_prod_pub].[dbo].[MUMA_PIPELINE2] where client_id in (" + clientIds + "))";

					ResultSet pipleLineRoyltyDestDB3 = stmtDestDb.executeQuery(queryToFetchPipleLineRoyltyDest3);

					while (pipleLineRoyltyDestDB3.next()) {
						pipelineRoyaltyPublishingDest3 = pipleLineRoyltyDestDB3.getString("AMOUNT");
						pipelineRoyltyPeriodPublishingDest3 = pipleLineRoyltyDestDB3.getString("periodID");
					}	

			String queryToFetchPipleLineRoyltySource1 =  
					 "select sum(dFAA_Royalty) as Royalty,dfaa_client_code as Client,(dfaa_month ||'-'|| dfaa_year) as Period " +
							 "from mpubfile.IMdFAAFL where dfaa_year=(Select max(dfaa_year) from mpubfile.IMdFAAFL where dfaa_client_code in (" + clientIds + "))  "+
							 "and dfaa_month=(Select max(dfaa_month) from mpubfile.IMdFAAFL where dfaa_client_code in (" + clientIds + ")) and dfaa_client_code  in (" + clientIds + ")  "+
							 "group by dfaa_client_code,dfaa_month ||'-'|| dfaa_year ";

			ResultSet pipleLineRoyltySourceDb1 = stmtRecroProdDb.executeQuery(queryToFetchPipleLineRoyltySource1);

			while (pipleLineRoyltySourceDb1.next()) {
				pipelineRoyaltyPublishingSource1 = pipleLineRoyltySourceDb1.getString("Royalty");
				pipelineRoyltyPeriodPublishingSource1 = pipleLineRoyltySourceDb1.getString("Period");
			}
			
			
			String queryToFetchPipleLineRoyltySource2 =  
					 "select sum(CFAA_Royalty) as Royalty,Cfaa_client_code as Client,(Cfaa_month ||'-'|| Cfaa_year) as Period " +
							 "from mpubfile.IMCFAAFL where Cfaa_year=(Select max(cfaa_year) from mpubfile.IMCFAAFL where cfaa_client_code in (" + clientIds + "))  "+
							 "and cfaa_month=(Select max(cfaa_month) from mpubfile.IMCFAAFL where cfaa_client_code in (" + clientIds + ")) and cfaa_client_code  in (" + clientIds + ")  "+
							 "group by cfaa_client_code,cfaa_month ||'-'|| cfaa_year ";

			ResultSet pipleLineRoyltySourceDb2 = stmtRecroProdDb.executeQuery(queryToFetchPipleLineRoyltySource2);

			while (pipleLineRoyltySourceDb2.next()) {
				pipelineRoyaltyPublishingSource2 = pipleLineRoyltySourceDb2.getString("Royalty");
				pipelineRoyltyPeriodPublishingSource2 = pipleLineRoyltySourceDb2.getString("Period");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			stmtDestDb.close();
			conDestinationDb.close();
			stmtRecroProdDb.close();
			conRecroProdDb.close();
		}

	}

}
