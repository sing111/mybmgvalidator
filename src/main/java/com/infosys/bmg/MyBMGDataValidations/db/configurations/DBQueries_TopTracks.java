package com.infosys.bmg.MyBMGDataValidations.db.configurations;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.infosys.bmg.MyBMGDataValidations.utility.Log;

public class DBQueries_TopTracks {
	public  Map<String, String> mapTopTracksRoyaltyByDownload = new HashMap<>();
	public  Map<String, String> mapTopTracksRoyaltyByStream = new HashMap<>();
	public  Map<String, String> mapTopTracksUnitsByDownload = new HashMap<>();
	public  Map<String, String> mapTopTracksUnitsByStream = new HashMap<>();
	public  Map<String, String> mapTopTracksProductTitle = new HashMap<>();
	public  Map<String, String> mapTopTracksUnitDownloadSource = new HashMap<>();
	public  Map<String, String> mapTopTracksRoyaltyDownloadSource = new HashMap<>();
	public  Map<String, String> mapTopTracksRoyaltyStreamSource = new HashMap<>();
	public  Map<String, String> mapTopTracksUnitsStreamSource = new HashMap<>();
	public  List<String> TopTracksProductIsrcCode = new ArrayList<String>();
	public  List<String> TopTracksProductIsrcCodeSource = new ArrayList<String>();
	public  Map<String, String> mapTopTracksProductTitleSource = new HashMap<>();
	public  String currencyDestinationDb = null;

	public  void DB_Query(String site, String client, ArrayList<String> scrossClientList, String crossClientId,
			 String period)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		mapTopTracksRoyaltyByDownload = new HashMap<>();
		mapTopTracksRoyaltyByStream = new HashMap<>();
		mapTopTracksUnitsByDownload = new HashMap<>();
		mapTopTracksUnitsByStream = new HashMap<>();
		TopTracksProductIsrcCode = new ArrayList<String>();
		mapTopTracksUnitDownloadSource = new HashMap<>();
		mapTopTracksRoyaltyDownloadSource = new HashMap<>();
		mapTopTracksRoyaltyStreamSource = new HashMap<>();
		mapTopTracksUnitsStreamSource = new HashMap<>();
		TopTracksProductIsrcCodeSource = new ArrayList<String>();
		mapTopTracksProductTitle = new HashMap<>();
		mapTopTracksProductTitleSource = new HashMap<>();
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=mybmg4_stage_rec";
		
		Connection conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");

		Statement stmtDestDb = conDestinationDb.createStatement();

		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dbUrlRecroProd = "jdbc:sqlserver://10.6.238.158:5555;databaseName=recroy_prod";
		Connection connRecroProd = DriverManager.getConnection(dbUrlRecroProd, "batr007", "Si2RIbVdVkDyCIVVBmbA");

		Statement stmtRecroProd = connRecroProd.createStatement();

		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dbUrlBmgPrimeLandingLaye = "jdbc:sqlserver://10.6.238.202:1433;databaseName=BMGPrime_LandingLayer";
		Connection connBmgPrime = DriverManager.getConnection(dbUrlBmgPrimeLandingLaye, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");
		Statement stmtBmgPrime = connBmgPrime.createStatement();
		try {
		List<String> clientIdList = new ArrayList<String>();
		clientIdList.add("'" + client + "'");
		if (crossClientId != null) {
			clientIdList.add(crossClientId);
		} else if (!scrossClientList.isEmpty()) {
			String cId = "";
			cId = scrossClientList.toString().replace("[", "").replace("]", "");
			clientIdList.add(cId);

		}
		String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
		String month = null;
		String year = null;

		StringBuilder sb = new StringBuilder();
		System.out.println(period);
		period = period.toString().replace("(", "").replace(")", "");
		String s[] = period.split("/");
		month = s[0];
		year = s[1];
		sb.append(year + "" + month);
		period = sb.toString();
		System.out.println(period + ":period");

		String queryToGetProductCodes = "SELECT TOP 5 " + "P.Product_Key, " + "P.Product_Title, "
				+ "P.Product_UserCode ," + " SUM(RE.Earnings_Amount) AS totalRoyalty "
				+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
				+ "INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
				+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
				+ " INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
				+ " INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
				+ "INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + " CL.Client_Code IN (" + clientIds
				+ ") AND CL.Client_Site='" + site + "'" + " AND PR.Period IN ('201806') "
				+ "AND DC.DistChannel_Category = 'Sales' " + " AND EC.Config_ProductType IN ('Track') "
				+ "GROUP BY P.Product_Key, P.Product_Title ,P.Product_UserCode " + "ORDER BY totalRoyalty DESC;";

		ResultSet productCodeList = stmtDestDb.executeQuery(queryToGetProductCodes);

		List<String> pcodeList = new ArrayList<String>();

		while (productCodeList.next()) {
			String pcode = productCodeList.getString("Product_Key");
			String productTitle = productCodeList.getString("Product_Title");
			String productIsrcCode = productCodeList.getString("Product_UserCode");
			pcodeList.add(pcode);
			TopTracksProductIsrcCode.add(productIsrcCode);
			mapTopTracksProductTitle.put(productIsrcCode, productTitle);
		}
		if (!pcodeList.isEmpty()) {
			String pcodel = pcodeList.toString().replace("[", "").replace("]", "");
			String queryToGetTopTracksByIncomeSubtypeDownload = "SELECT TOP 5 " + "P.Product_Key, "
					+ "P.Product_Title, " + "PC.Config_Desc, " + "P.Product_UserCode, "
					+ "SUM(RE.Earnings_Amount) AS totalRoyalty "
					+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
					+ " INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
					+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
					+ "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
					+ "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
					+ "	INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + "  CL.Client_Code IN ("
					+ clientIds + ") AND CL.Client_Site='" + site + "'" + "AND PR.Period IN ('201806') "
					+ "AND DC.DistChannel_Category = 'Sales' " + "AND EC.Config_ProductType IN ('Track') "
					+ "	and EC.Income_SubType='Download' " + "	and P.Product_Key in  (" + pcodel + ") "
					+ "GROUP BY P.Product_Key, P.Product_Title, PC.Config_Desc, P.Product_UserCode, P.Product_Barcode "
					+ "ORDER BY totalRoyalty DESC;";

			ResultSet topTracksDownload = stmtDestDb.executeQuery(queryToGetTopTracksByIncomeSubtypeDownload);

			while (topTracksDownload.next()) {
				// ArrayList<String> topTracksList=new ArrayList<String>();
				String productTitle = topTracksDownload.getString("Product_Title");
				String totalRoyalty = topTracksDownload.getString("totalRoyalty");
				String productUserCode = topTracksDownload.getString("Product_UserCode");
				mapTopTracksRoyaltyByDownload.put(productUserCode, totalRoyalty);
			}

			String queryToGetTopTracksByIncomeSubtypeStream = "SELECT TOP 5 " + "P.Product_Key, " + "P.Product_Title, "
					+ "PC.Config_Desc, " + "P.Product_UserCode, " + "SUM(RE.Earnings_Amount) AS totalRoyalty "
					+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
					+ " INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
					+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
					+ "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
					+ "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
					+ "	INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + "  CL.Client_Code IN ("
					+ clientIds + ") AND CL.Client_Site='" + site + "'" + "AND PR.Period IN ('201806') "
					+ "AND DC.DistChannel_Category = 'Sales' " + "AND EC.Config_ProductType IN ('Track') "
					+ "	and EC.Income_SubType='Stream' " + "	and P.Product_Key in  (" + pcodel + ") "
					+ "GROUP BY P.Product_Key, P.Product_Title, PC.Config_Desc, P.Product_UserCode, P.Product_Barcode "
					+ "ORDER BY totalRoyalty DESC;";

			ResultSet topTrackStream = stmtDestDb.executeQuery(queryToGetTopTracksByIncomeSubtypeStream);
			while (topTrackStream.next()) {
				String productTitle = topTrackStream.getString("Product_Title");
				String totalRoyalty = topTrackStream.getString("totalRoyalty");
				String productUserCode = topTrackStream.getString("Product_UserCode");
				mapTopTracksRoyaltyByStream.put(productUserCode, totalRoyalty);
			}
			String queryToGetTopTracksUnitsByDownload = "SELECT TOP 5 " + "P.Product_Key, " + "P.Product_Title, "
					+ "PC.Config_Desc, " + "P.Product_UserCode, " + "SUM(COALESCE(ERE.Units, 0))     AS Units  "
					+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
					+ " INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
					+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
					+ "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
					+ "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
					+ "INNER JOIN ExtnRecEarnings ERE ON RE.RecEarnings_Key = ERE.RecEarnings_Key "
					+ "	INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + "  CL.Client_Code IN ("
					+ clientIds + ") AND CL.Client_Site='" + site + "'" + "AND PR.Period IN ('201806') "
					+ "AND DC.DistChannel_Category = 'Sales' " + "AND EC.Config_ProductType IN ('Track') "
					+ "	and EC.Income_SubType='Download' " + "	and P.Product_Key in  (" + pcodel + ") "
					+ "GROUP BY P.Product_Key, P.Product_Title, PC.Config_Desc, P.Product_UserCode, P.Product_Barcode "
					+ "ORDER BY Units  DESC;";

			ResultSet topTrackUnitDownload = stmtDestDb.executeQuery(queryToGetTopTracksUnitsByDownload);
			while (topTrackUnitDownload.next()) {
				String productTitle = topTrackUnitDownload.getString("Product_Title");
				String totalUnits = topTrackUnitDownload.getString("Units");
				String productUserCode = topTrackUnitDownload.getString("Product_UserCode");
				mapTopTracksUnitsByDownload.put(productUserCode, totalUnits);
			}

			String queryToGetTopTracksUnitsByStream = "SELECT TOP 5 " + "P.Product_Key, "
					+ "P.Product_Title, P.Product_UserCode ," +

					"SUM(COALESCE(ERE.Units, 0))     AS Units  "
					+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
					+ " INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
					+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
					+ "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
					+ "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
					+ "INNER JOIN ExtnRecEarnings ERE ON RE.RecEarnings_Key = ERE.RecEarnings_Key "
					+ "	INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + "  CL.Client_Code IN ("
					+ clientIds + ") AND CL.Client_Site='" + site + "'" + "AND PR.Period IN ('201806') "
					+ "AND DC.DistChannel_Category = 'Sales' " + "AND EC.Config_ProductType IN ('Track') "
					+ "	and EC.Income_SubType='Stream' " + "	and P.Product_Key in  (" + pcodel + ") "
					+ "GROUP BY P.Product_Key, P.Product_Title, PC.Config_Desc, P.Product_UserCode, P.Product_Barcode "
					+ "ORDER BY Units  DESC;";

			ResultSet topTrackUnitStream = stmtDestDb.executeQuery(queryToGetTopTracksUnitsByStream);
			while (topTrackUnitStream.next()) {
				String productTitle = topTrackUnitStream.getString("Product_Title");
				String totalUnits = topTrackUnitStream.getString("Units");
				String productUserCode = topTrackUnitStream.getString("Product_UserCode");
				mapTopTracksUnitsByStream.put(productUserCode, totalUnits);
			}
		}

		String queryToGetCurrencyCodeDestDbtop = "Select currency_code from  [mybmg4_stage_rec].[dbo].[Currency] where currency_key =("
				+ "Select siteCurrency_Key from mybmg4_stage_rec.[dbo].[Site] WHERE site_code='" + site + "')";
		ResultSet currencyListtop = stmtDestDb.executeQuery(queryToGetCurrencyCodeDestDbtop);
		while (currencyListtop.next()) {
			currencyDestinationDb = currencyListtop.getString("currency_code");
		}

		String queryToFetchProductCodeTopTracks = "select top 5 "
				+ "PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC,PD.Product_UserCode,  "
				+ "SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT " + "from  "
				+ " (select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ, "
				+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID "
				+ "from tbl_STG_RoycalcPayee dcrx " + "inner join "
				+ "(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ , "
				+ " RP.roycalcpayee_bfbalance,COUNT(*) as reccount " + "from ( "
				+ "select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code "
				+ "from tbl_STG_RoycalcPayee  "
				+ "	JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Contract')) CO  "
				+ "     ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND isnull(CO.CONTTYPE_CODE,'')  "
				+ "   not in('JVAT','PAGC') " + "					AND CO.CONTRACT_NAME IS NOT NULL "
				+ "					 ) rp " + " group by "
				+ "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ, "
				+ "RP.roycalcpayee_bfbalance) DUP "
				+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "
				+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
				+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ, "
				+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP  " + "  JOIN tbl_STG_SalesWorkArc "
				+ "  SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ  "
				+ " JOIN tbl_STG_Calclog CL "
				+ " ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F' "
				+ "AND CL.CALCLOG_PERIODTYPE<>'A'      "
				+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD  "
				+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "
				+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF "
				+ "ON CF.CONFIG_CODE=SWA.CONFIG_CODE "
				+ "			INNER JOIN   (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC  "
				+ "			ON PC.CONFIG_CODE=PD.CONFIG_CODE   "
				+ " where CF.CONFIG_CODE in('OTH','DD','DAS','DVS','DVT','MT','RB','VOIC','VT','SUB','RT','STR','DST','REAL','TNDU','PR','ST','DCLO','TD')  "
				+ "			AND SWA.DISTCHAN_CODE IN ('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO')  "
				+ " and rcp.royaltor_code in(" + clientIds + ") and rcp.royaltor_site='" + site + "'"
				+ "and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) +  substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)='"
				+ period + "'  " +

				" GROUP BY  " + "PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC,  "
				+ "PD.Product_UserCode,PD.PRODUCT_BARCODE  " + "ORDER BY SUM(SWA.SALESWORKARC_EARNINGS)  DESC  ";
		ResultSet topTrackProductCodeSource = stmtBmgPrime.executeQuery(queryToFetchProductCodeTopTracks);
		List<String> prodCodeList = new ArrayList<String>();
		while (topTrackProductCodeSource.next()) {
			String productCode = "'" + topTrackProductCodeSource.getString("PRODUCT_CODE") + "'";
			prodCodeList.add(productCode);
			String productTitle = topTrackProductCodeSource.getString("PRODUCT_TITLE");
			String productIsrcCode = topTrackProductCodeSource.getString("Product_UserCode");
			TopTracksProductIsrcCodeSource.add(productIsrcCode);
			mapTopTracksProductTitleSource.put(productIsrcCode, productTitle);
		}

		if (!prodCodeList.isEmpty()) {

			String pcodeSource = prodCodeList.toString().replace("[", "").replace("]", "");
			String querytopTracksUnitsDownloadSource = "select "
					+ "PD.PRODUCT_TITLE,PC.CONFIG_DESC,PD.Product_UserCode, SUM(SWA.SALESWORKARC_UNITS) UNITS  "
					+ "from  "
					+ "(select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,  "
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID  "
					+ "  from tbl_STG_RoycalcPayee dcrx   " + " inner join   "
					+ "(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,   "
					+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount  " + "from (   "
					+ "select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code   "
					+ "from tbl_STG_RoycalcPayee    "
					+ "JOIN(select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Contract')) CO    "
					+ " ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND "
					+ "isnull(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')   "
					+ "					AND CO.CONTRACT_NAME IS NOT NULL   " + "					 ) rp   "
					+ " group by   "
					+ "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,   "
					+ "RP.roycalcpayee_bfbalance) DUP   "
					+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ   "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1)   "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,   "
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP    " + " JOIN tbl_STG_SalesWorkArc   "
					+ " SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ    "
					+ "JOIN tbl_STG_Calclog CL   "
					+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F'   "
					+ "AND CL.CALCLOG_PERIODTYPE<>'A'        "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD   "
					+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE   "
//					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF   "
//					+ "ON CF.CONFIG_CODE=SWA.CONFIG_CODE   "
					+ " 			INNER JOIN  (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC   "
					+ "			ON PC.CONFIG_CODE=PD.CONFIG_CODE    "
					+ "where SWA.CONFIG_CODE in('OTH','DD','DAS','DVS','DVT','MT','RB','VOIC','VT','RT','DST','REAL','TNDU','TD')   "
					+ "			AND SWA.DISTCHAN_CODE IN ('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO')   "
					+ "and rcp.royaltor_code in(" + clientIds + ")and rcp.royaltor_site='" + site + "'"
					+ "and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  + substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)='"
					+ period + "'  " + "and PD.product_code in (" + pcodeSource + ") " + "GROUP BY    "
					+ "PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC,   "
					+ "PD.Product_UserCode,PD.PRODUCT_BARCODE   " + "ORDER BY SUM(SWA.SALESWORKARC_UNITS)  DESC ";

			ResultSet topTrackUnitDownloadSource = stmtBmgPrime.executeQuery(querytopTracksUnitsDownloadSource);
			while (topTrackUnitDownloadSource.next()) {
				String productTitle = topTrackUnitDownloadSource.getString("PRODUCT_TITLE");
				String totalUnits = topTrackUnitDownloadSource.getString("UNITS");
				String productIsrcCode = topTrackUnitDownloadSource.getString("Product_UserCode");
				mapTopTracksUnitDownloadSource.put(productIsrcCode, totalUnits);

			}

			String querytopTrackRoyaltyDownloadSource = "select "
					+ "PD.PRODUCT_TITLE,PC.CONFIG_DESC,PD.Product_UserCode, SUM(SWA.SALESWORKARC_Earnings) Amount  "
					+ "from  "
					+ "(select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,  "
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID  "
					+ "  from tbl_STG_RoycalcPayee dcrx   " + " inner join   "
					+ "(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,   "
					+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount  " + "from (   "
					+ "select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code   "
					+ "from tbl_STG_RoycalcPayee    "
					+ "JOIN(select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Contract')) CO    "
					+ " ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND "
					+ "isnull(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')   "
					+ "					AND CO.CONTRACT_NAME IS NOT NULL   " + "					 ) rp   "
					+ " group by   "
					+ "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,   "
					+ "RP.roycalcpayee_bfbalance) DUP   "
					+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ   "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1)   "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,   "
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP    " + " JOIN tbl_STG_SalesWorkArc   "
					+ " SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ    "
					+ "JOIN tbl_STG_Calclog CL   "
					+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F'   "
					+ "AND CL.CALCLOG_PERIODTYPE<>'A'        "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD   "
					+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE   "
//					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF   "
//					+ "ON CF.CONFIG_CODE=SWA.CONFIG_CODE   "
					+ " 			INNER JOIN  (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC   "
					+ "			ON PC.CONFIG_CODE=PD.CONFIG_CODE    "
					+ "where SWA.CONFIG_CODE in('OTH','DD','DAS','DVS','DVT','MT','RB','VOIC','VT','RT','DST','REAL','TNDU','TD')   "
					+ "			AND SWA.DISTCHAN_CODE IN ('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO')   "
					+ "and rcp.royaltor_code in(" + clientIds + ")and rcp.royaltor_site='" + site + "'"
					+ "and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  + substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)='"
					+ period + "'  " + "and PD.product_code in (" + pcodeSource + ") " + "GROUP BY    "
					+ "PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC,   "
					+ "PD.Product_UserCode,PD.PRODUCT_BARCODE   " + "ORDER BY SUM(SWA.SALESWORKARC_Earnings)  DESC ";

			ResultSet topTrackRoyaltyDownloadSource = stmtBmgPrime.executeQuery(querytopTrackRoyaltyDownloadSource);
			List<String> topTracksProductTitleDownload = new ArrayList<String>();
			while (topTrackRoyaltyDownloadSource.next()) {
				String productTitle = topTrackRoyaltyDownloadSource.getString("PRODUCT_TITLE");
				String totalRoyalty = topTrackRoyaltyDownloadSource.getString("Amount");
				String productIsrcCode = topTrackRoyaltyDownloadSource.getString("Product_UserCode");
				mapTopTracksRoyaltyDownloadSource.put(productIsrcCode, totalRoyalty);
				topTracksProductTitleDownload.add(productTitle);
			}

			String querytopTracksUnitsStreamSource = "select "
					+ "PD.PRODUCT_TITLE,PC.CONFIG_DESC,PD.Product_UserCode, SUM(SWA.SALESWORKARC_UNITS) UNITS  "
					+ "from  "
					+ "(select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,  "
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID  "
					+ "  from tbl_STG_RoycalcPayee dcrx   " + " inner join   "
					+ "(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,   "
					+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount  " + "from (   "
					+ "select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code   "
					+ "from tbl_STG_RoycalcPayee    "
					+ "JOIN(select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Contract')) CO    "
					+ " ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND "
					+ "isnull(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')   "
					+ "					AND CO.CONTRACT_NAME IS NOT NULL   " + "					 ) rp   "
					+ " group by   "
					+ "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,   "
					+ "RP.roycalcpayee_bfbalance) DUP   "
					+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ   "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1)   "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,   "
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP    " + " JOIN tbl_STG_SalesWorkArc   "
					+ " SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ    "
					+ "JOIN tbl_STG_Calclog CL   "
					+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F'   "
					+ "AND CL.CALCLOG_PERIODTYPE<>'A'        "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD   "
					+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE   "
//					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF   "
//					+ "ON CF.CONFIG_CODE=SWA.CONFIG_CODE   "
					+ " 			INNER JOIN  (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC   "
					+ "			ON PC.CONFIG_CODE=PD.CONFIG_CODE    "
					+ "where SWA.CONFIG_CODE in('SUB','STR','PR','ST','DCLO')   "
					+ "			AND SWA.DISTCHAN_CODE IN ('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO')   "
					+ "and rcp.royaltor_code in(" + clientIds + ")and rcp.royaltor_site='" + site + "'"
					+ "and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  + substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)='"
					+ period + "'  " + "and PD.product_code in (" + pcodeSource + ") " + "GROUP BY    "
					+ "PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC,   "
					+ "PD.Product_UserCode,PD.PRODUCT_BARCODE   " + "ORDER BY SUM(SWA.SALESWORKARC_UNITS)  DESC ";
			ResultSet topTrackUnitStreamSource = stmtBmgPrime.executeQuery(querytopTracksUnitsStreamSource);
			while (topTrackUnitStreamSource.next()) {
				String productTitle = topTrackUnitStreamSource.getString("PRODUCT_TITLE");
				String totalUnits = topTrackUnitStreamSource.getString("UNITS");
				String productIsrcCode = topTrackUnitStreamSource.getString("Product_UserCode");
				mapTopTracksUnitsStreamSource.put(productIsrcCode, totalUnits);

			}

			String querytopTracksRoyaltyStreamSource = "select "
					+ "PD.PRODUCT_TITLE,PC.CONFIG_DESC,PD.Product_UserCode, SUM(SWA.SALESWORKARC_Earnings) Amount  "
					+ "from  "
					+ "(select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,  "
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID  "
					+ "  from tbl_STG_RoycalcPayee dcrx   " + " inner join   "
					+ "(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,   "
					+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount  " + "from (   "
					+ "select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code   "
					+ "from tbl_STG_RoycalcPayee    "
					+ "JOIN(select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Contract')) CO    "
					+ " ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND "
					+ "isnull(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')   "
					+ "					AND CO.CONTRACT_NAME IS NOT NULL   " + "					 ) rp   "
					+ " group by   "
					+ "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,   "
					+ "RP.roycalcpayee_bfbalance) DUP   "
					+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ   "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1)   "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,   "
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP    " + " JOIN tbl_STG_SalesWorkArc   "
					+ " SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ    "
					+ "JOIN tbl_STG_Calclog CL   "
					+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F'   "
					+ "AND CL.CALCLOG_PERIODTYPE<>'A'        "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD   "
					+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE   "
//					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF   "
//					+ "ON CF.CONFIG_CODE=SWA.CONFIG_CODE   "
					+ " 			INNER JOIN  (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC   "
					+ "			ON PC.CONFIG_CODE=PD.CONFIG_CODE    "
					+ "where SWA.CONFIG_CODE in('SUB','STR','PR','ST','DCLO')   "
					+ "			AND SWA.DISTCHAN_CODE IN ('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO')   "
					+ "and rcp.royaltor_code in(" + clientIds + ")and rcp.royaltor_site='" + site + "'"
					+ "and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  + substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)='"
					+ period + "'  " + "and PD.product_code in (" + pcodeSource + ") " + "GROUP BY    "
					+ "PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC,   "
					+ "PD.Product_UserCode,PD.PRODUCT_BARCODE   " + "ORDER BY SUM(SWA.SALESWORKARC_Earnings)  DESC ";

			ResultSet topTrackRoyaltyStreamSource = stmtBmgPrime.executeQuery(querytopTracksRoyaltyStreamSource);
			while (topTrackRoyaltyStreamSource.next()) {
				String productTitle = topTrackRoyaltyStreamSource.getString("PRODUCT_TITLE");
				String totalRoyalty = topTrackRoyaltyStreamSource.getString("Amount");
				String productIsrcCode = topTrackRoyaltyStreamSource.getString("Product_UserCode");
				mapTopTracksRoyaltyStreamSource.put(productIsrcCode, totalRoyalty);

			}
		}
	}catch(Exception e) {
		e.printStackTrace();
		Log.error("Error in DBQueries_Top_Tracks: "+e);
		
	}finally {
		stmtDestDb.close();
		conDestinationDb.close();
		stmtBmgPrime.close();
		conDestinationDb.close();
		stmtRecroProd.close();
		connRecroProd.close();
	}
	}
}
