package com.infosys.bmg.MyBMGDataValidations.db.configurations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class DBConnectionForRoyaltyTrend {

	public String royaltyTrendPhyDestDb;
	public String royaltyTrendPhySourceDb;
	public String royaltyTrendDigitalDestDb;
	public String royaltyTrendDigitalSourceDb;
	public String royaltyTrendLicensingDestDb;
	public String royaltyTrendLicensingSourceDb;
	public String royaltyTrendSyncDestDb;
	public String royaltyTrendSyncSourceDb;
	public String royaltyTrendPublicPerformanceDestDb;
	public String royaltyTrendPublicPerformanceSourceDb;
	public String royaltyTrendMerchandiseDestDb;
	public String royaltyTrendMerchandiseSourceDb;
	public String royaltyTrendDonutGraphDestDb;
	public String royaltyTrendDonutGraphSourceDb;
	public String royaltyTrendTotalRoyaltyDestDb;
	public String royaltyTrendTotalRoyaltySourceDb;

	public double totalValueDestDb;
	public double phyValueDestDb;
	public double percentagePhyDestDb;

	public double digitalValueDestDb;
	public double percentageDigitalDestDb;

	public double licensingValueDestDb;
	public double percentageLicensingDestDb;

	public double merchandiseValueDestDb;
	public double percentageMerchandiseDestDb;

	public double ppValueDestDb;
	public double percentagePPDestDb;

	public double syncValueDestDb;
	public double percentageSyncDestDb;

	// For source db
	public double totalValueSrcDb;
	public double phyValueSourceDb;
	public double percentagePhySourceDb;

	public double digitalValueSourceDb;
	public double percentageDigitalSourceDb;

	public double licensingValueSourceDb;
	public double percentageLicensingSourceDb;

	public double syncValueSourceDb;
	public double percentageSyncSourceDb;

	public double ppValueSourceDb;
	public double percentagePPSourceDb;

	public double merchandiseValueSourceDb;
	public double percentageMerchandiseSourceDb;

	public List<Double> listDest = new ArrayList<>();
	public List<Double> listSrc = new ArrayList<>();

	public void DB_Query_RoyaltyTrendPhysical(String client, ArrayList<String> scrossClientList, String crossClientId,
			String site, String year) {
		try {
			List<String> clientIdList = new ArrayList<String>();
			clientIdList.add("'" + client + "'");
			if (crossClientId != null) {
				clientIdList.add(crossClientId);
			} else if (!scrossClientList.isEmpty()) {
				String cId = "";
				cId = scrossClientList.toString().replace("[", "").replace("]", "");
				clientIdList.add(cId);
			}

			String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
			dbQueryDestDbForRoyTrendPhy(year, site, clientIds, clientIdList);
			int yr = Integer.parseInt(year);
			if (yr <= 2016) {
				dbQueryForRemaRoyaltyTrendPhy(year, site, clientIds, clientIdList);
			} else {
				dbQueryForPrimeRoyaltyTrendPhy(year, site, clientIds, clientIdList);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void dbQueryDestDbForRoyTrendPhy(String year, String site, String clientIds, List<String> clientIdList)
			throws SQLException {
		Connection conDestinationDb = null;
		Statement stmtDestDb = null;
		royaltyTrendPhyDestDb = null;
		try {
			String queryforRoyaltyTrendPhyDestDb = "SELECT SUM(RE.Earnings_Amount) AS totalRoyalty, substring(cast (PR.Period as varchar) ,1,4) as year "
					+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
					+ "INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
					+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
					+ "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
					+ "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
					+ "INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + "CL.Client_Code IN ("
					+ clientIds + ") AND CL.Client_Site='" + site + "' ";
			String prd = null;
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int yr = cal.get(Calendar.YEAR);
			if (year.equalsIgnoreCase(String.valueOf(yr))) {
				int month = cal.get(Calendar.MONTH);
				if (month > 06) {
					prd = "'" + yr + "06'";
					queryforRoyaltyTrendPhyDestDb = queryforRoyaltyTrendPhyDestDb + "AND  PR.Period =" + prd;
				}
			} else {
				queryforRoyaltyTrendPhyDestDb = queryforRoyaltyTrendPhyDestDb
						+ "AND  substring(cast (PR.Period as varchar) ,1,4)='" + year + "' ";
			}

			queryforRoyaltyTrendPhyDestDb = queryforRoyaltyTrendPhyDestDb
					+ "AND DC.DistChannel_Category = 'Sales' AND EC.Income_Type  in ('Physical') "
					+ "group by  substring(cast (PR.Period as varchar) ,1,4)";

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=Mybmg4_stage_rec";
			conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007", "Si2RIbVdVkDyCIVVBmbA");
			stmtDestDb = conDestinationDb.createStatement();
			ResultSet royaltyTrendPhy = stmtDestDb.executeQuery(queryforRoyaltyTrendPhyDestDb);
			String totalRoyalty = null;
			while (royaltyTrendPhy.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendPhy.getString("totalRoyalty");
					// year = royaltyTrendPhy.getString("year");
				}
			}
			royaltyTrendPhyDestDb = totalRoyalty;
			System.out.println("Royalty trend physical of dest db for  :" + year + " is : " + royaltyTrendPhyDestDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtDestDb.close();
			conDestinationDb.close();
		}
	}

	private void dbQueryForPrimeRoyaltyTrendPhy(String year, String site, String clientIds, List<String> clientIdList)
			throws SQLException {
		Connection connBmgPrime = null;
		Statement stmtRematDb = null;
		royaltyTrendPhySourceDb = null;
		try {

			String queryforRoyaltyTrendPhyPrimeDb = "select substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  year,SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT "
					+ "from (select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID from tbl_STG_RoycalcPayee dcrx "
					+ "inner join(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,"
					+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount from (select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code "
					+ "from tbl_STG_RoycalcPayee  "
					+ "JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.CONTRACT')) CO "
					+ "ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE "
					+ "AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND isnull(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC') AND CO.CONTRACT_NAME IS NOT NULL) rp "
					+ "group by RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,RP.roycalcpayee_bfbalance) DUP "
					+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ, "
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP  " + "JOIN tbl_STG_SalesWorkArc "
					+ "SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ "
					+ "JOIN tbl_STG_Calclog CL "
					+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F'"
					+ " AND CL.CALCLOG_PERIODTYPE<>'A' "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD "
					+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF "
					+ "ON CF.CONFIG_CODE=SWA.CONFIG_CODE  "
					+ "where CF.CONFIG_CODE in('ECDE','MCLP','LP33','CD','MC','LP','10IL','10IS','12I','3ICD','7I','7IEP','8TR','BOD','CDEP','CDRM','CDS','CROM','DCD','DDL','DMC','DMIX','DROM','DVAS','DVD','DVDP','DVDS','DVVS','ECD','ECDS','EMAX','EMD','LD','MAXI','MCEP','MCS','MD','MEM','SACD','USB','VCD','VHS','VHSS','VS','CDE','12HB','12PD','12SB','12ST','7HB','7PD','7SB','7ST','CDS5','CDSP','MCSP'"
					+ ",'SCSP','SGLM','VIGF','VISP','CDVD','10EP','12EP','12CD','BOX','12SP','CDDV','BRAY','VISS') "
					+ "AND SWA.DISTCHAN_CODE IN ('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO') "
					+ "and rcp.royaltor_code in(" + clientIds + ") and rcp.royaltor_site='" + site + "' "
					+ "AND  substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)='" + year + "'"
					+ "GROUP BY substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)";

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dbUrlBmgPrimeLandingLayer = "jdbc:sqlserver://10.6.238.202:1433;databaseName=BMGPrime_LandingLayer";
			connBmgPrime = DriverManager.getConnection(dbUrlBmgPrimeLandingLayer, "batr007", "Si2RIbVdVkDyCIVVBmbA");
			stmtRematDb = connBmgPrime.createStatement();
			ResultSet royaltyTrendPhy = stmtRematDb.executeQuery(queryforRoyaltyTrendPhyPrimeDb);
			String totalRoyalty = null;
			while (royaltyTrendPhy.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendPhy.getString("AMOUNT");
				}
			}
			royaltyTrendPhySourceDb = totalRoyalty;
			System.out.println("Royalty trend physical for Prime db :" + year + " is : " + royaltyTrendPhySourceDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtRematDb.close();
			connBmgPrime.close();
		}

	}

	private void dbQueryForRemaRoyaltyTrendPhy(String year, String site, String clientIds, List<String> clientIdList)
			throws SQLException {
		Connection con = null;
		Statement stmtRematDb = null;
		royaltyTrendPhySourceDb = null;
		try {

			String queryForRoyaltyTrendPhyRema = "select SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) year,"
					+ "SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT from (select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,"
					+ "dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID "
					+ "from RMAESTRO_USER.RoycalcPayee dcrx "
					+ "inner join (select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ , RP.roycalcpayee_bfbalance,COUNT(*) as reccount "
					+ "from (select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code "
					+ "from RMAESTRO_USER.RoycalcPayee  " + "JOIN RMAESTRO_USER.CONTRACT CO "
					+ "ON CO.CONTRACT_CODE=RMAESTRO_USER.RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=RMAESTRO_USER.RoycalcPayee.RCONTRACT_SITE"
					+ " AND NVL(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')) rp" + " group by "
					+ " RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,"
					+ "RP.roycalcpayee_bfbalance) DUP "
					+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ, "
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP " + "  JOIN RMAESTRO_USER.SalesWorkArc "
					+ "  SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ "
					+ "  JOIN RMAESTRO_USER.Calclog CL"
					+ "   ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F'"
					+ "   AND CL.CALCLOG_PERIODTYPE<>'A' "
					+ "   and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) || SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)>='201401' "
					+ "  and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) || SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)<='201703' "
					+ "  INNER JOIN RMAESTRO_USER.productS PD"
					+ "   ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE"
					+ "   INNER JOIN RMAESTRO_USER.CONFIG CF" + "   ON CF.CONFIG_CODE=SWA.CONFIG_CODE "
					+ "INNER JOIN   RMAESTRO_USER.CONFIG PC" + "	ON PC.CONFIG_CODE=PD.CONFIG_CODE"
					+ " where CF.CONFIG_CODE in('ECDE','MCLP','LP33','CD','MC','LP','10IL','10IS','12I','3ICD','7I','7IEP','8TR','BOD',"
					+ "'CDEP','CDRM','CDS','CROM','DCD','DDL','DMC','DMIX','DROM','DVAS','DVD','DVDP','DVDS','DVVS','ECD','ECDS','EMAX','EMD','LD',"
					+ "'MAXI','MCEP','MCS','MD','MEM','SACD','USB','VCD','VHS','VHSS','VS','CDE','12HB','12PD','12SB','12ST','7HB','7PD','7SB','7ST',"
					+ "'CDS5','CDSP','MCSP','SCSP','SGLM','VIGF','VISP','CDVD','10EP','12EP','12CD','BOX','12SP','CDDV','BRAY','VISS')"
					+ "	AND SWA.DISTCHAN_CODE IN ('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO')"
					+ " and rcp.royaltor_code in(" + clientIds + ") and rcp.royaltor_site='" + site + "'"
					+ " and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) ='" + year + "'"
					+ " GROUP BY SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)";

			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection("jdbc:oracle:thin:@10.6.238.172:1480:DBOPRIM1", "RMAESTRO_USER",
					"counterp");
			stmtRematDb = con.createStatement();
			ResultSet royaltyTrendPhy = stmtRematDb.executeQuery(queryForRoyaltyTrendPhyRema);
			String totalRoyalty = null;
			while (royaltyTrendPhy.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendPhy.getString("AMOUNT");
				}
			}
			royaltyTrendPhySourceDb = totalRoyalty;
			System.out.println(
					"Royalty trend physical of REMA db for  year:" + year + " is : " + royaltyTrendPhySourceDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtRematDb.close();
			con.close();

		}

	}

	// *******************************************************************************************
	// ************************ ROYALTY TREND ------ DIGITAL
	// ********************************** *
	// *******************************************************************************************

	public void DB_Query_RoyaltyTrendDigital(String client, ArrayList<String> scrossClientList, String crossClientId,
			String site, String year) throws SQLException {
		try {
			List<String> clientIdList = new ArrayList<String>();
			clientIdList.add("'" + client + "'");
			if (crossClientId != null) {
				clientIdList.add(crossClientId);
			} else if (!scrossClientList.isEmpty()) {
				String cId = "";
				cId = scrossClientList.toString().replace("[", "").replace("]", "");
				clientIdList.add(cId);
			}
			String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
			dbQueryForDestRoyaTrendDigital(year, site, clientIds, clientIdList);
			int yr = Integer.parseInt(year);
			if (yr <= 2016) {
				dbQueryForRemaRoyaTrendDigital(year, site, clientIds, clientIdList);
			} else {
				dbQueryForPrimeRoyaTrendDigital(year, site, clientIds, clientIdList);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void dbQueryForDestRoyaTrendDigital(String year, String site, String clientIds, List<String> clientIdList)
			throws SQLException {
		Connection conDestinationDb = null;
		Statement stmtDestDb = null;
		royaltyTrendDigitalDestDb = null;
		try {
			String queryforRoyaltyTrendDigitalDestDb = "SELECT SUM(RE.Earnings_Amount) AS totalRoyalty, substring(cast (PR.Period as varchar) ,1,4) as year "
					+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
					+ "INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
					+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
					+ "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
					+ "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
					+ "INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + "CL.Client_Code IN ("
					+ clientIds + ") AND CL.Client_Site='" + site + "' ";

			String prd = null;
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int yr = cal.get(Calendar.YEAR);
			if (year.equalsIgnoreCase(String.valueOf(yr))) {
				int month = cal.get(Calendar.MONTH);
				if (month > 06) {
					prd = "'" + yr + "06'";
					queryforRoyaltyTrendDigitalDestDb = queryforRoyaltyTrendDigitalDestDb + "AND  PR.Period =" + prd;
				}
			} else {
				queryforRoyaltyTrendDigitalDestDb = queryforRoyaltyTrendDigitalDestDb
						+ "AND  substring(cast (PR.Period as varchar) ,1,4)='" + year + "' ";
			}

			queryforRoyaltyTrendDigitalDestDb = queryforRoyaltyTrendDigitalDestDb
					+ "AND DC.DistChannel_Category = 'Sales' AND EC.Income_Type  in ('Digital') "
					+ "group by  substring(cast (PR.Period as varchar) ,1,4)";

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=Mybmg4_stage_rec";
			conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007", "Si2RIbVdVkDyCIVVBmbA");
			stmtDestDb = conDestinationDb.createStatement();
			ResultSet royaltyTrendDigital = stmtDestDb.executeQuery(queryforRoyaltyTrendDigitalDestDb);
			String totalRoyalty = null;
			while (royaltyTrendDigital.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendDigital.getString("totalRoyalty");
					// year = royaltyTrendPhy.getString("year");
				}
			}
			royaltyTrendDigitalDestDb = totalRoyalty;
			System.out.println("Royalty trend Digital od dest db for  :" + year + " is : " + royaltyTrendDigitalDestDb);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtDestDb.close();
			conDestinationDb.close();
		}

	}

	private void dbQueryForPrimeRoyaTrendDigital(String year, String site, String clientIds, List<String> clientIdList)
			throws SQLException {
		Connection connBmgPrime = null;
		Statement stmtRematDb = null;
		royaltyTrendDigitalSourceDb = null;
		try {

			String queryforRoyaltyTrendDigitalPrimeDb = "select substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  year,"
					+ "SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT from  "
					+ "(select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,"
					+ "dcrx.CALCLOG_SEQ,dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID "
					+ "from tbl_STG_RoycalcPayee dcrx inner join(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,"
					+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount from (select distinct ROYALTOR_CODE,"
					+ "ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code from tbl_STG_RoycalcPayee "
					+ "JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.CONTRACT')) CO "
					+ "ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE "
					+ "AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND "
					+ "isnull(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC') AND CO.CONTRACT_NAME IS NOT NULL) rp"
					+ " group by RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,"
					+ "RP.roycalcpayee_bfbalance) DUP on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and "
					+ "DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP " + "JOIN tbl_STG_SalesWorkArc "
					+ "SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ "
					+ "JOIN tbl_STG_Calclog CL "
					+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F' "
					+ "AND CL.CALCLOG_PERIODTYPE<>'A'  "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD "
					+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF "
					+ "ON CF.CONFIG_CODE=SWA.CONFIG_CODE "
					+ "where CF.CONFIG_CODE in('OTH','DD','DAB','DAEP','DAL','DAS','DCMB','DCMC','DVG','DVL','DVS',"
					+ "'DVT','MT','RB','VOIC','VT','DABV','DS','SUB','RT','STR','DA','DST','REAL','ANDU','APU','TNDU',"
					+ "'DLP','PR','ST','DCLO','DGAB','VOD','TD') AND SWA.DISTCHAN_CODE IN "
					+ "('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO') "
					+ "and rcp.royaltor_code in(" + clientIds + ") and rcp.royaltor_site='" + site + "' "
					+ "and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) ='" + year + "' "
					+ "GROUP BY substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)";

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dbUrlBmgPrimeLandingLayer = "jdbc:sqlserver://10.6.238.202:1433;databaseName=BMGPrime_LandingLayer";
			connBmgPrime = DriverManager.getConnection(dbUrlBmgPrimeLandingLayer, "batr007", "Si2RIbVdVkDyCIVVBmbA");
			stmtRematDb = connBmgPrime.createStatement();
			ResultSet royaltyTrendDigital = stmtRematDb.executeQuery(queryforRoyaltyTrendDigitalPrimeDb);
			String totalRoyalty = null;
			while (royaltyTrendDigital.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendDigital.getString("AMOUNT");
				}
			}
			royaltyTrendDigitalSourceDb = totalRoyalty;
			System.out.println("Royalty trend Digital for Prime db :" + year + " is : " + royaltyTrendDigitalSourceDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtRematDb.close();
			connBmgPrime.close();
		}

	}

	private void dbQueryForRemaRoyaTrendDigital(String year, String site, String clientIds, List<String> clientIdList)
			throws SQLException {
		Connection con = null;
		Statement stmtRematDb = null;
		try {

			String queryForRoyaltyTrendDigitalRema = "select SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) year,"
					+ "SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT from (select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,"
					+ "dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID "
					+ "from RMAESTRO_USER.RoycalcPayee dcrx inner join"
					+ "(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,"
					+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount "
					+ "from (select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,"
					+ "roycalcpayee_bfbalance,Payee_Code from RMAESTRO_USER.RoycalcPayee JOIN RMAESTRO_USER.CONTRACT CO "
					+ "ON CO.CONTRACT_CODE=RMAESTRO_USER.RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE="
					+ "RMAESTRO_USER.RoycalcPayee.RCONTRACT_SITE AND NVL(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')) rp "
					+ "group by RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,"
					+ "RP.roycalcpayee_bfbalance) DUP on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and "
					+ "DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP JOIN RMAESTRO_USER.SalesWorkArc "
					+ "SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ "
					+ "JOIN RMAESTRO_USER.Calclog CL "
					+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F' "
					+ "AND CL.CALCLOG_PERIODTYPE<>'A' and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) || "
					+ "SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)>='201401' and SUBSTR(NVL(CL.CALCLOG_STATTO, "
					+ "CL.CALCLOG_TO),4,4) || SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)<='201703' "
					+ "INNER JOIN RMAESTRO_USER.productS PD "
					+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "
					+ "INNER JOIN RMAESTRO_USER.CONFIG CF ON CF.CONFIG_CODE=SWA.CONFIG_CODE "
					+ "INNER JOIN RMAESTRO_USER.CONFIG PC ON PC.CONFIG_CODE=PD.CONFIG_CODE where "
					+ "CF.CONFIG_CODE in('OTH','DD','DAB','DAEP','DAL','DAS','DCMB','DCMC','DVG','DVL','DVS',"
					+ "'DVT','MT','RB','VOIC','VT','DABV','DS','SUB','RT','STR','DA','DST','REAL','ANDU','APU',"
					+ "'TNDU','DLP','PR','ST','DCLO','DGAB','VOD','TD') AND SWA.DISTCHAN_CODE IN"
					+ " ('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO') and "
					+ "rcp.royaltor_code in(" + clientIds + ") and rcp.royaltor_site='" + site
					+ "' and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) ='" + year + "' "
					+ "GROUP BY SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)";

			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection("jdbc:oracle:thin:@10.6.238.172:1480:DBOPRIM1", "RMAESTRO_USER",
					"counterp");
			stmtRematDb = con.createStatement();
			ResultSet royaltyTrendDigital = stmtRematDb.executeQuery(queryForRoyaltyTrendDigitalRema);
			String totalRoyalty = null;
			while (royaltyTrendDigital.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendDigital.getString("AMOUNT");
				}
			}
			royaltyTrendDigitalSourceDb = totalRoyalty;
			System.out.println(
					"Royalty trend Digital of REMA db for  year:" + year + " is : " + royaltyTrendDigitalSourceDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtRematDb.close();
			con.close();

		}

	}

	// *******************************************************************************************
	// ************************ ROYALTY TREND ------ Total Royalty
	// **************************** *
	// *******************************************************************************************

	public void DB_Query_RoyaltyTrendTotalRoyalty(String client, ArrayList<String> scrossClientList,
			String crossClientId, String site, String year, List<String> allYears) throws SQLException {
		try {
			List<String> clientIdList = new ArrayList<String>();
			clientIdList.add("'" + client + "'");
			if (crossClientId != null) {
				clientIdList.add(crossClientId);
			} else if (!scrossClientList.isEmpty()) {
				String cId = "";
				cId = scrossClientList.toString().replace("[", "").replace("]", "");
				clientIdList.add(cId);
			}
			String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
			dbQueryForRoyaTrendTotalRoyDestDb(year, site, clientIds, clientIdList, allYears);
			dbQueryForRoyaTrendTotalRoyRecroy(year, site, clientIds, clientIdList, allYears);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void dbQueryForRoyaTrendTotalRoyDestDb(String year, String site, String clientIds,
			List<String> clientIdList, List<String> allYears) throws SQLException {
		Connection conDestinationDb = null;
		Statement stmtDestDb = null;
		royaltyTrendTotalRoyaltyDestDb = null;
		try {
			String queryforRoyaltyTrendTotalRoyaDestDb = null;
			if (allYears != null) {
				queryforRoyaltyTrendTotalRoyaDestDb = "select sum(royalty_total) as royaltyTotal "
						+ "from Rec_Balances rb join client c on rb.client_key=c.client_key "
						+ "where c.client_code IN (" + clientIds + ") and client_site='" + site + "'";
			} else {
				queryforRoyaltyTrendTotalRoyaDestDb = "select sum(royalty_total) as royaltyTotal,substring(cast (p.Period as varchar) ,1,4) as year from period  p "
						+ "inner join Rec_Balances rb on p.period_key=rb.period_key "
						+ "inner join client c on rb.client_key=c.client_key "
						+ "where   substring(cast (p.Period as varchar) ,1,4)='" + year + "' "
						+ "and c.client_code in (" + clientIds + ") " + "and client_site='" + site + "' "
						+ "Group by substring(cast (p.Period as varchar) ,1,4)";
			}
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=Mybmg4_stage_rec";
			conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007", "Si2RIbVdVkDyCIVVBmbA");
			stmtDestDb = conDestinationDb.createStatement();
			ResultSet royaltyTrendTotalRoy = stmtDestDb.executeQuery(queryforRoyaltyTrendTotalRoyaDestDb);
			String totalRoyalty = null;
			while (royaltyTrendTotalRoy.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendTotalRoy.getString("royaltyTotal");
				}
			}
			royaltyTrendTotalRoyaltyDestDb = totalRoyalty;
			if (year == null) {
				System.out.println("Royalty trend total Royalty of dest db for  :" + allYears + " is : "
						+ royaltyTrendTotalRoyaltyDestDb);
			} else {
				System.out.println("Royalty trend total Royalty of dest db for  :" + year + " is : "
						+ royaltyTrendTotalRoyaltyDestDb);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtDestDb.close();
			conDestinationDb.close();
		}

	}

	/*
	 * *********************************** RECROY SOURCE DB
	 * ***********************************
	 */
	private void dbQueryForRoyaTrendTotalRoyRecroy(String year, String site, String clientIds,
			List<String> clientIdList, List<String> allYears) throws SQLException {

		Connection conSourceDb = null;
		Statement stmtSourceDb = null;
		royaltyTrendTotalRoyaltySourceDb = null;
		String queryforRoyaltyTrendTotalRoyaSrcDb = null;
		List<Integer> intList = new ArrayList<>();
		try {
			if (allYears != null) {
				queryforRoyaltyTrendTotalRoyaSrcDb = "select SUM(CONTOPTACT_RECOUPAMT) AS ROYALTY_TOTAL "
						+ "from (select royaltor_code,royaltor_site,calclog_finalized,CONTOPTACT_RECOUPAMT,transtype_code,contract_code,contract_site "
						+ "from recroy_prod..CONTOPTACT) rt "
						+ "inner join [dbo].[tblRecordmaestroETL_CONTRACT_Table1]  C on rt.CONTRACT_CODE=C.CONTRACT_CODE AND rt.CONTRACT_SITE=C.CONTRACT_SITE AND  isnull(C.CONTTYPE_CODE,'') not in ('JVAT','PAGC') "
						+ "inner join(select substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),4,4) + substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),1,2) as period , calclog_seq  "
						+ "from [dbo].[CALCLOG]) CL on CL.calclog_seq=rt.calclog_finalized WHERE "
						+ "TRANSTYPE_CODE='ROYS' AND royaltor_code IN (" + clientIds + ") AND ROYALTOR_SITE='" + site
						+ "'";
				for (String s : allYears) {
					intList.add(Integer.valueOf(s));
				}
				StringBuilder prd = new StringBuilder();
				Integer maxYear = Collections.max(intList);
				for (int yr : intList) {
					if (yr < maxYear) {
						prd.append("'" + yr + "06','" + yr + "12'");
						prd.append(",");
					}
					if (yr == maxYear) {
						Calendar cal = Calendar.getInstance();
						cal.setTime(new Date());
						int month = cal.get(Calendar.MONTH);
						if (month > 06) {
							prd.append("'" + yr + "06'");
						}
					}
				}

				queryforRoyaltyTrendTotalRoyaSrcDb = queryforRoyaltyTrendTotalRoyaSrcDb + "AND  Period IN(" + prd + ")";
			} else {
				queryforRoyaltyTrendTotalRoyaSrcDb = "select  substring(cast (Period as varchar) ,1,4) as year,SUM(CONTOPTACT_RECOUPAMT) AS ROYALTY_TOTAL "
						+ "from(select royaltor_code,royaltor_site,calclog_finalized,CONTOPTACT_RECOUPAMT,transtype_code,contract_code,contract_site "
						+ "from recroy_prod..CONTOPTACT) rt "
						+ "inner join [dbo].[tblRecordmaestroETL_CONTRACT_Table1] C on rt.CONTRACT_CODE=C.CONTRACT_CODE AND rt.CONTRACT_SITE=C.CONTRACT_SITE AND  isnull(C.CONTTYPE_CODE,'') not in ('JVAT','PAGC') "
						+ "inner join (select substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),4,4) + substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),1,2) as period , calclog_seq  from [dbo].[CALCLOG]) CL "
						+ "on CL.calclog_seq=rt.calclog_finalized WHERE "
						+ "TRANSTYPE_CODE='ROYS' AND royaltor_code IN (" + clientIds + ") AND ROYALTOR_SITE='" + site
						+ "' ";

				String prd = null;
				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				int yr = cal.get(Calendar.YEAR);
				if (year.equalsIgnoreCase(String.valueOf(yr))) {
					int month = cal.get(Calendar.MONTH);
					if (month > 06) {
						prd = "'" + yr + "06'";
						queryforRoyaltyTrendTotalRoyaSrcDb = queryforRoyaltyTrendTotalRoyaSrcDb + "AND  Period =" + prd;
					}

				} else {
					queryforRoyaltyTrendTotalRoyaSrcDb = queryforRoyaltyTrendTotalRoyaSrcDb
							+ "AND  substring(cast (Period as varchar) ,1,4)='" + year + "' ";
				}
				queryforRoyaltyTrendTotalRoyaSrcDb = queryforRoyaltyTrendTotalRoyaSrcDb
						+ "group by  substring(cast (Period as varchar) ,1,4)";
			}
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dbUrlMyBmgSource = "jdbc:sqlserver://10.6.238.158:5555;databaseName=recroy_prod";
			conSourceDb = DriverManager.getConnection(dbUrlMyBmgSource, "batr007", "Si2RIbVdVkDyCIVVBmbA");
			stmtSourceDb = conSourceDb.createStatement();
			ResultSet royaltyTrendTotalRoy = stmtSourceDb.executeQuery(queryforRoyaltyTrendTotalRoyaSrcDb);
			String totalRoyalty = null;
			while (royaltyTrendTotalRoy.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendTotalRoy.getString("ROYALTY_TOTAL");
				}
			}
			royaltyTrendTotalRoyaltySourceDb = totalRoyalty;
			if (year == null) {
				System.out.println("Royalty trend Total Royalty of Source db for  :" + allYears + " is : "
						+ royaltyTrendTotalRoyaltySourceDb);
			} else {
				System.out.println("Royalty trend Total Royalty of Source db for  :" + year + " is : "
						+ royaltyTrendTotalRoyaltySourceDb);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtSourceDb.close();
			conSourceDb.close();
		}
	}

	// *******************************************************************************************
	// ************************ ROYALTY TREND ------ LICENSING
	// ********************************** *
	// *******************************************************************************************

	public void DB_Query_RoyaltyTrendLicensing(String client, ArrayList<String> scrossClientList, String crossClientId,
			String site, String year) throws SQLException {
		try {
			List<String> clientIdList = new ArrayList<String>();
			clientIdList.add("'" + client + "'");
			if (crossClientId != null) {
				clientIdList.add(crossClientId);
			} else if (!scrossClientList.isEmpty()) {
				String cId = "";
				cId = scrossClientList.toString().replace("[", "").replace("]", "");
				clientIdList.add(cId);
			}
			String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
			dbQueryForDestRoyaTrendLicensing(year, site, clientIds, clientIdList);
			int yr = Integer.parseInt(year);
			if (yr <= 2016) {
				dbQueryForRemaRoyaTrendLicensing(year, site, clientIds, clientIdList);
			} else {
				dbQueryForPrimeRoyaTrendLicensing(year, site, clientIds, clientIdList);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void dbQueryForDestRoyaTrendLicensing(String year, String site, String clientIds, List<String> clientIdList)
			throws SQLException {
		Connection conDestinationDb = null;
		Statement stmtDestDb = null;
		try {
			String queryforRoyaltyTrendLicensingDestDb = "SELECT SUM(RE.Earnings_Amount) AS totalRoyalty, substring(cast (PR.Period as varchar) ,1,4) as year "
					+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
					+ "INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
					+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
					+ "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
					+ "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
					+ "INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + "CL.Client_Code IN ("
					+ clientIds + ") AND CL.Client_Site='" + site + "' ";

			String prd = null;
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int yr = cal.get(Calendar.YEAR);
			if (year.equalsIgnoreCase(String.valueOf(yr))) {
				int month = cal.get(Calendar.MONTH);
				if (month > 06) {
					prd = "'" + yr + "06'";
					queryforRoyaltyTrendLicensingDestDb = queryforRoyaltyTrendLicensingDestDb + "AND  PR.Period ="
							+ prd;
				}
			} else {
				queryforRoyaltyTrendLicensingDestDb = queryforRoyaltyTrendLicensingDestDb
						+ "AND  substring(cast (PR.Period as varchar) ,1,4)='" + year + "' ";
			}
			queryforRoyaltyTrendLicensingDestDb = queryforRoyaltyTrendLicensingDestDb
					+ "AND DC.DistChannel_Category = 'License' "
					+ "group by  substring(cast (PR.Period as varchar) ,1,4)";

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=Mybmg4_stage_rec";
			conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007", "Si2RIbVdVkDyCIVVBmbA");
			stmtDestDb = conDestinationDb.createStatement();
			ResultSet royaltyTrendLicensing = stmtDestDb.executeQuery(queryforRoyaltyTrendLicensingDestDb);
			String totalRoyalty = null;
			while (royaltyTrendLicensing.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendLicensing.getString("totalRoyalty");
				}
			}
			royaltyTrendLicensingDestDb = totalRoyalty;
			System.out.println(
					"Royalty trend Licensing of dest db for  :" + year + " is : " + royaltyTrendLicensingDestDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtDestDb.close();
			conDestinationDb.close();
		}
	}

	private void dbQueryForPrimeRoyaTrendLicensing(String year, String site, String clientIds,
			List<String> clientIdList) throws SQLException {
		Connection connBmgPrime = null;
		Statement stmtRematDb = null;
		try {

			String queryforRoyaltyTrendLicensingPrimeDb = "select substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  year,"
					+ "SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT from  "
					+ "(select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,"
					+ "dcrx.CALCLOG_SEQ,dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID "
					+ "from tbl_STG_RoycalcPayee dcrx inner join(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,"
					+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount from (select distinct ROYALTOR_CODE,"
					+ "ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code from tbl_STG_RoycalcPayee "
					+ "JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.CONTRACT')) CO "
					+ "ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE "
					+ "AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND "
					+ "isnull(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC') AND CO.CONTRACT_NAME IS NOT NULL) rp"
					+ " group by RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,"
					+ "RP.roycalcpayee_bfbalance) DUP on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and "
					+ "DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP " + "JOIN tbl_STG_SalesWorkArc "
					+ "SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ "
					+ "JOIN tbl_STG_Calclog CL "
					+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F' "
					+ "AND CL.CALCLOG_PERIODTYPE<>'A'  "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD "
					+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF "
					+ "ON CF.CONFIG_CODE=SWA.CONFIG_CODE "
					+ "INNER JOIN   (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC ON PC.CONFIG_CODE=PD.CONFIG_CODE "
					+ "where SWA.DISTCHAN_CODE IN ('LI','MSTL')" + "and rcp.royaltor_code in(" + clientIds
					+ ") and rcp.royaltor_site='" + site + "' "
					+ "and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) ='" + year + "' "
					+ "GROUP BY substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)";

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dbUrlBmgPrimeLandingLayer = "jdbc:sqlserver://10.6.238.202:1433;databaseName=BMGPrime_LandingLayer";
			connBmgPrime = DriverManager.getConnection(dbUrlBmgPrimeLandingLayer, "batr007", "Si2RIbVdVkDyCIVVBmbA");
			stmtRematDb = connBmgPrime.createStatement();
			ResultSet royaltyTrendLicensing = stmtRematDb.executeQuery(queryforRoyaltyTrendLicensingPrimeDb);
			String totalRoyalty = null;
			while (royaltyTrendLicensing.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendLicensing.getString("AMOUNT");
				}
			}
			royaltyTrendLicensingSourceDb = totalRoyalty;
			System.out.println(
					"Royalty trend Licensing for Prime db :" + year + " is : " + royaltyTrendLicensingSourceDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtRematDb.close();
			connBmgPrime.close();
		}

	}

	private void dbQueryForRemaRoyaTrendLicensing(String year, String site, String clientIds, List<String> clientIdList)
			throws SQLException {
		Connection con = null;
		Statement stmtRematDb = null;
		try {

			String queryForRoyaltyTrendLicensingRema = "select SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  as year,SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT "
					+ "from (select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID from RMAESTRO_USER.RoycalcPayee dcrx "
					+ "inner join(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,RP.roycalcpayee_bfbalance,"
					+ "COUNT(*) as reccount from (select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,"
					+ "RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code from RMAESTRO_USER.RoycalcPayee JOIN RMAESTRO_USER.CONTRACT CO "
					+ " ON CO.CONTRACT_CODE=RMAESTRO_USER.RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=RMAESTRO_USER."
					+ "RoycalcPayee.RCONTRACT_SITE AND NVL(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')) rp group by "
					+ "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,RP.roycalcpayee_bfbalance) DUP "
					+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,dcrx.roycalcpayee_bfbalance,"
					+ "dcrx.payee_code) RCP JOIN RMAESTRO_USER.SalesWorkArc SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ "
					+ "JOIN RMAESTRO_USER.Calclog CL ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' "
					+ "AND CL.CALCLOG_RUNTYPE='F' AND CL.CALCLOG_PERIODTYPE<>'A' and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) || SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)>='201401'  "
					+ "and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) || SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)<='201703' "
					+ "INNER JOIN RMAESTRO_USER.productS PD ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "
					+ "INNER JOIN RMAESTRO_USER.CONFIG CF ON CF.CONFIG_CODE=SWA.CONFIG_CODE "
					+ "INNER JOIN   RMAESTRO_USER.CONFIG PC ON PC.CONFIG_CODE=PD.CONFIG_CODE where SWA.DISTCHAN_CODE IN ('LI','MSTL') "
					+ "and rcp.royaltor_code in(" + clientIds + ") and rcp.royaltor_site='" + site
					+ "' and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) ='" + year + "' "
					+ "GROUP BY SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)";

			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection("jdbc:oracle:thin:@10.6.238.172:1480:DBOPRIM1", "RMAESTRO_USER",
					"counterp");
			stmtRematDb = con.createStatement();
			ResultSet royaltyTrendLicensing = stmtRematDb.executeQuery(queryForRoyaltyTrendLicensingRema);
			String totalRoyalty = null;
			while (royaltyTrendLicensing.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendLicensing.getString("AMOUNT");
				}
			}
			royaltyTrendLicensingSourceDb = totalRoyalty;
			System.out.println(
					"Royalty trend Licensing of REMA db for  year:" + year + " is : " + royaltyTrendLicensingSourceDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtRematDb.close();
			con.close();

		}

	}

	// *******************************************************************************************
	// ************************ ROYALTY TREND ------ Sync
	// ********************************** *
	// *******************************************************************************************

	public void DB_Query_RoyaltyTrendSync(String client, ArrayList<String> scrossClientList, String crossClientId,
			String site, String year) throws SQLException {
		try {
			List<String> clientIdList = new ArrayList<String>();
			clientIdList.add("'" + client + "'");
			if (crossClientId != null) {
				clientIdList.add(crossClientId);
			} else if (!scrossClientList.isEmpty()) {
				String cId = "";
				cId = scrossClientList.toString().replace("[", "").replace("]", "");
				clientIdList.add(cId);
			}
			String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
			dbQueryForDestRoyaTrendSync(year, site, clientIds, clientIdList);
			int yr = Integer.parseInt(year);
			if (yr <= 2016) {
				dbQueryForRemaRoyaTrendSync(year, site, clientIds, clientIdList);
			} else {
				dbQueryForPrimeRoyaTrendSync(year, site, clientIds, clientIdList);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void dbQueryForDestRoyaTrendSync(String year, String site, String clientIds, List<String> clientIdList)
			throws SQLException {
		Connection conDestinationDb = null;
		Statement stmtDestDb = null;
		try {
			String queryforRoyaltyTrendSyncDestDb = "SELECT SUM(RE.Earnings_Amount) AS totalRoyalty, substring(cast (PR.Period as varchar) ,1,4) as year "
					+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
					+ "INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
					+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
					+ "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
					+ "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
					+ "INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + "CL.Client_Code IN ("
					+ clientIds + ") AND CL.Client_Site='" + site + "' ";

			String prd = null;
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int yr = cal.get(Calendar.YEAR);
			if (year.equalsIgnoreCase(String.valueOf(yr))) {
				int month = cal.get(Calendar.MONTH);
				if (month > 06) {
					prd = "'" + yr + "06'";
					queryforRoyaltyTrendSyncDestDb = queryforRoyaltyTrendSyncDestDb + "AND  PR.Period =" + prd;
				}
			} else {
				queryforRoyaltyTrendSyncDestDb = queryforRoyaltyTrendSyncDestDb
						+ "AND  substring(cast (PR.Period as varchar) ,1,4)='" + year + "' ";
			}

			queryforRoyaltyTrendSyncDestDb = queryforRoyaltyTrendSyncDestDb
					+ "AND DC.DistChannel_Category = 'Synchronisation' "
					+ "group by  substring(cast (PR.Period as varchar) ,1,4)";

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=Mybmg4_stage_rec";
			conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007", "Si2RIbVdVkDyCIVVBmbA");
			stmtDestDb = conDestinationDb.createStatement();
			ResultSet royaltyTrendSync = stmtDestDb.executeQuery(queryforRoyaltyTrendSyncDestDb);
			String totalRoyalty = null;
			while (royaltyTrendSync.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendSync.getString("totalRoyalty");
					// year = royaltyTrendPhy.getString("year");
				}
			}
			royaltyTrendSyncDestDb = totalRoyalty;
			System.out.println("Royalty trend Sync of dest db for  :" + year + " is : " + royaltyTrendSyncDestDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtDestDb.close();
			conDestinationDb.close();
		}
	}

	private void dbQueryForPrimeRoyaTrendSync(String year, String site, String clientIds, List<String> clientIdList)
			throws SQLException {
		Connection connBmgPrime = null;
		Statement stmtRematDb = null;
		try {

			String queryforRoyaltyTrendSyncPrimeDb = "select substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  year,"
					+ "SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT from  "
					+ "(select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,"
					+ "dcrx.CALCLOG_SEQ,dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID "
					+ "from tbl_STG_RoycalcPayee dcrx inner join(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,"
					+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount from (select distinct ROYALTOR_CODE,"
					+ "ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code from tbl_STG_RoycalcPayee "
					+ "JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.CONTRACT')) CO "
					+ "ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE "
					+ "AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND "
					+ "isnull(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC') AND CO.CONTRACT_NAME IS NOT NULL) rp"
					+ " group by RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,"
					+ "RP.roycalcpayee_bfbalance) DUP on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and "
					+ "DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP " + "JOIN tbl_STG_SalesWorkArc "
					+ "SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ "
					+ "JOIN tbl_STG_Calclog CL "
					+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F' "
					+ "AND CL.CALCLOG_PERIODTYPE<>'A'  "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD "
					+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "
					+ "INNER JOIN   (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC ON PC.CONFIG_CODE=PD.CONFIG_CODE "
					+ "where SWA.DISTCHAN_CODE IN ('SYNC')" + "and rcp.royaltor_code in(" + clientIds
					+ ") and rcp.royaltor_site='" + site + "' "
					+ "and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) ='" + year + "' "
					+ "GROUP BY substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)";

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dbUrlBmgPrimeLandingLayer = "jdbc:sqlserver://10.6.238.202:1433;databaseName=BMGPrime_LandingLayer";
			connBmgPrime = DriverManager.getConnection(dbUrlBmgPrimeLandingLayer, "batr007", "Si2RIbVdVkDyCIVVBmbA");
			stmtRematDb = connBmgPrime.createStatement();
			ResultSet royaltyTrendSync = stmtRematDb.executeQuery(queryforRoyaltyTrendSyncPrimeDb);
			String totalRoyalty = null;
			while (royaltyTrendSync.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendSync.getString("AMOUNT");
				}
			}
			royaltyTrendSyncSourceDb = totalRoyalty;
			System.out.println("Royalty trend Sync for Prime db :" + year + " is : " + royaltyTrendSyncSourceDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtRematDb.close();
			connBmgPrime.close();
		}

	}

	private void dbQueryForRemaRoyaTrendSync(String year, String site, String clientIds, List<String> clientIdList)
			throws SQLException {
		Connection con = null;
		Statement stmtRematDb = null;
		try {

			String queryForRoyaltyTrendSyncRema = "select SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  as year,SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT "
					+ "from (select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID from RMAESTRO_USER.RoycalcPayee dcrx "
					+ "inner join(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,RP.roycalcpayee_bfbalance,"
					+ "COUNT(*) as reccount from (select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,"
					+ "RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code from RMAESTRO_USER.RoycalcPayee JOIN RMAESTRO_USER.CONTRACT CO "
					+ " ON CO.CONTRACT_CODE=RMAESTRO_USER.RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=RMAESTRO_USER."
					+ "RoycalcPayee.RCONTRACT_SITE AND NVL(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')) rp group by "
					+ "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,RP.roycalcpayee_bfbalance) DUP "
					+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,dcrx.roycalcpayee_bfbalance,"
					+ "dcrx.payee_code) RCP JOIN RMAESTRO_USER.SalesWorkArc SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ "
					+ "JOIN RMAESTRO_USER.Calclog CL ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' "
					+ "AND CL.CALCLOG_RUNTYPE='F' AND CL.CALCLOG_PERIODTYPE<>'A' and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) || SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)>='201401'  "
					+ "and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) || SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)<='201703' "
					+ "INNER JOIN RMAESTRO_USER.productS PD ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "
					+ "INNER JOIN RMAESTRO_USER.CONFIG CF ON CF.CONFIG_CODE=SWA.CONFIG_CODE "
					+ "INNER JOIN   RMAESTRO_USER.CONFIG PC ON PC.CONFIG_CODE=PD.CONFIG_CODE where SWA.DISTCHAN_CODE IN ('SYNC') "
					+ "and rcp.royaltor_code in(" + clientIds + ") and rcp.royaltor_site='" + site
					+ "' and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) ='" + year + "' "
					+ "GROUP BY SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)";

			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection("jdbc:oracle:thin:@10.6.238.172:1480:DBOPRIM1", "RMAESTRO_USER",
					"counterp");
			stmtRematDb = con.createStatement();
			ResultSet royaltyTrendSync = stmtRematDb.executeQuery(queryForRoyaltyTrendSyncRema);
			String totalRoyalty = null;
			while (royaltyTrendSync.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendSync.getString("AMOUNT");
				}
			}
			royaltyTrendSyncSourceDb = totalRoyalty;
			System.out.println("Royalty trend Sync of REMA db for  year:" + year + " is : " + royaltyTrendSyncSourceDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtRematDb.close();
			con.close();
		}
	}

	// *******************************************************************************************
	// * ******************** ROYALTY TREND ------ Public Performance
	// ************************* *
	// *******************************************************************************************

	public void DB_Query_RoyaltyTrendPublicPerformance(String client, ArrayList<String> scrossClientList,
			String crossClientId, String site, String year) throws SQLException {
		try {
			List<String> clientIdList = new ArrayList<String>();
			clientIdList.add("'" + client + "'");
			if (crossClientId != null) {
				clientIdList.add(crossClientId);
			} else if (!scrossClientList.isEmpty()) {
				String cId = "";
				cId = scrossClientList.toString().replace("[", "").replace("]", "");
				clientIdList.add(cId);
			}
			String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
			dbQueryForDestRoyaTrendPublicPerformance(year, site, clientIds, clientIdList);
			int yr = Integer.parseInt(year);
			if (yr <= 2016) {
				dbQueryForRemaRoyaTrendPublicPerformance(year, site, clientIds, clientIdList);
			} else {
				dbQueryForPrimeRoyaTrendPublicPerformance(year, site, clientIds, clientIdList);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void dbQueryForDestRoyaTrendPublicPerformance(String year, String site, String clientIds,
			List<String> clientIdList) throws SQLException {
		Connection conDestinationDb = null;
		Statement stmtDestDb = null;
		try {
			String queryforRoyaltyTrendPublicPerformanceDestDb = "SELECT SUM(RE.Earnings_Amount) AS totalRoyalty, substring(cast (PR.Period as varchar) ,1,4) as year "
					+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
					+ "INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
					+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
					+ "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
					+ "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
					+ "INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + "CL.Client_Code IN ("
					+ clientIds + ") AND CL.Client_Site='" + site + "' ";
			String prd = null;
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int yr = cal.get(Calendar.YEAR);
			if (year.equalsIgnoreCase(String.valueOf(yr))) {
				int month = cal.get(Calendar.MONTH);
				if (month > 06) {
					prd = "'" + yr + "06'";
					queryforRoyaltyTrendPublicPerformanceDestDb = queryforRoyaltyTrendPublicPerformanceDestDb
							+ "AND  PR.Period =" + prd;
				}
			} else {
				queryforRoyaltyTrendPublicPerformanceDestDb = queryforRoyaltyTrendPublicPerformanceDestDb
						+ "AND  substring(cast (PR.Period as varchar) ,1,4)='" + year + "' ";
			}
			queryforRoyaltyTrendPublicPerformanceDestDb = queryforRoyaltyTrendPublicPerformanceDestDb
					+ "AND DC.DistChannel_Category = 'Public_Performance' "
					+ "group by  substring(cast (PR.Period as varchar) ,1,4)";

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=Mybmg4_stage_rec";
			conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007", "Si2RIbVdVkDyCIVVBmbA");
			stmtDestDb = conDestinationDb.createStatement();
			ResultSet royaltyTrendPublicPerformance = stmtDestDb
					.executeQuery(queryforRoyaltyTrendPublicPerformanceDestDb);
			String totalRoyalty = null;
			while (royaltyTrendPublicPerformance.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendPublicPerformance.getString("totalRoyalty");
					// year = royaltyTrendPhy.getString("year");
				}
			}
			royaltyTrendPublicPerformanceDestDb = totalRoyalty;
			System.out.println("Royalty trend PublicPerformance of dest db for  :" + year + " is : "
					+ royaltyTrendPublicPerformanceDestDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtDestDb.close();
			conDestinationDb.close();
		}

	}

	private void dbQueryForPrimeRoyaTrendPublicPerformance(String year, String site, String clientIds,
			List<String> clientIdList) throws SQLException {
		Connection connBmgPrime = null;
		Statement stmtRematDb = null;
		try {

			String queryforRoyaltyTrendPublicPerformancePrimeDb = "select substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  year,"
					+ "SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT from  "
					+ "(select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,"
					+ "dcrx.CALCLOG_SEQ,dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID "
					+ "from tbl_STG_RoycalcPayee dcrx inner join(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,"
					+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount from (select distinct ROYALTOR_CODE,"
					+ "ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code from tbl_STG_RoycalcPayee "
					+ "JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.CONTRACT')) CO "
					+ "ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE "
					+ "AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND "
					+ "isnull(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC') AND CO.CONTRACT_NAME IS NOT NULL) rp"
					+ " group by RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,"
					+ "RP.roycalcpayee_bfbalance) DUP on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and "
					+ "DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP " + "JOIN tbl_STG_SalesWorkArc "
					+ "SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ "
					+ "JOIN tbl_STG_Calclog CL "
					+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F' "
					+ "AND CL.CALCLOG_PERIODTYPE<>'A'  "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD "
					+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "
					+ "INNER JOIN   (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC ON PC.CONFIG_CODE=PD.CONFIG_CODE "
					+ "where SWA.DISTCHAN_CODE IN ('PPI')" + "and rcp.royaltor_code in(" + clientIds
					+ ") and rcp.royaltor_site='" + site + "' "
					+ "and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) ='" + year + "' "
					+ "GROUP BY substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)";

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dbUrlBmgPrimeLandingLayer = "jdbc:sqlserver://10.6.238.202:1433;databaseName=BMGPrime_LandingLayer";
			connBmgPrime = DriverManager.getConnection(dbUrlBmgPrimeLandingLayer, "batr007", "Si2RIbVdVkDyCIVVBmbA");
			stmtRematDb = connBmgPrime.createStatement();
			ResultSet royaltyTrendPublicPerformance = stmtRematDb
					.executeQuery(queryforRoyaltyTrendPublicPerformancePrimeDb);
			String totalRoyalty = null;
			while (royaltyTrendPublicPerformance.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendPublicPerformance.getString("AMOUNT");
				}
			}
			royaltyTrendPublicPerformanceSourceDb = totalRoyalty;
			System.out.println("Royalty trend PublicPerformance for Prime db :" + year + " is : "
					+ royaltyTrendPublicPerformanceSourceDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtRematDb.close();
			connBmgPrime.close();
		}

	}

	private void dbQueryForRemaRoyaTrendPublicPerformance(String year, String site, String clientIds,
			List<String> clientIdList) throws SQLException {
		Connection con = null;
		Statement stmtRematDb = null;
		try {

			String queryForRoyaltyTrendPublicPerformanceRema = "select SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  as year,SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT "
					+ "from (select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID from RMAESTRO_USER.RoycalcPayee dcrx "
					+ "inner join(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,RP.roycalcpayee_bfbalance,"
					+ "COUNT(*) as reccount from (select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,"
					+ "RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code from RMAESTRO_USER.RoycalcPayee JOIN RMAESTRO_USER.CONTRACT CO "
					+ " ON CO.CONTRACT_CODE=RMAESTRO_USER.RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=RMAESTRO_USER."
					+ "RoycalcPayee.RCONTRACT_SITE AND NVL(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')) rp group by "
					+ "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,RP.roycalcpayee_bfbalance) DUP "
					+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,dcrx.roycalcpayee_bfbalance,"
					+ "dcrx.payee_code) RCP JOIN RMAESTRO_USER.SalesWorkArc SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ "
					+ "JOIN RMAESTRO_USER.Calclog CL ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' "
					+ "AND CL.CALCLOG_RUNTYPE='F' AND CL.CALCLOG_PERIODTYPE<>'A' and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) || SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)>='201401'  "
					+ "and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) || SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)<='201703' "
					+ "INNER JOIN RMAESTRO_USER.productS PD ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "
					+ "INNER JOIN RMAESTRO_USER.CONFIG CF ON CF.CONFIG_CODE=SWA.CONFIG_CODE "
					+ "INNER JOIN   RMAESTRO_USER.CONFIG PC ON PC.CONFIG_CODE=PD.CONFIG_CODE where SWA.DISTCHAN_CODE IN ('PPI') "
					+ "and rcp.royaltor_code in(" + clientIds + ") and rcp.royaltor_site='" + site
					+ "' and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) ='" + year + "' "
					+ "GROUP BY SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)";

			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection("jdbc:oracle:thin:@10.6.238.172:1480:DBOPRIM1", "RMAESTRO_USER",
					"counterp");
			stmtRematDb = con.createStatement();
			ResultSet royaltyTrendPublicPerformance = stmtRematDb
					.executeQuery(queryForRoyaltyTrendPublicPerformanceRema);
			String totalRoyalty = null;
			while (royaltyTrendPublicPerformance.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendPublicPerformance.getString("AMOUNT");
				}
			}
			royaltyTrendPublicPerformanceSourceDb = totalRoyalty;
			System.out.println("Royalty trend PublicPerformance of REMA db for  year:" + year + " is : "
					+ royaltyTrendPublicPerformanceSourceDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtRematDb.close();
			con.close();
		}
	}

	// *******************************************************************************************
	// * ******************** ROYALTY TREND ------ Merchandise
	// ************************* *
	// *******************************************************************************************

	public void DB_Query_RoyaltyTrendMerchandise(String client, ArrayList<String> scrossClientList,
			String crossClientId, String site, String year) throws SQLException {
		try {
			List<String> clientIdList = new ArrayList<String>();
			clientIdList.add("'" + client + "'");
			if (crossClientId != null) {
				clientIdList.add(crossClientId);
			} else if (!scrossClientList.isEmpty()) {
				String cId = "";
				cId = scrossClientList.toString().replace("[", "").replace("]", "");
				clientIdList.add(cId);
			}
			String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
			dbQueryForDestRoyaTrendMerchandise(year, site, clientIds, clientIdList);
			int yr = Integer.parseInt(year);
			if (yr <= 2016) {
				dbQueryForRemaRoyaTrendMerchandise(year, site, clientIds, clientIdList);
			} else {
				dbQueryForPrimeRoyaTrendMerchandise(year, site, clientIds, clientIdList);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void dbQueryForDestRoyaTrendMerchandise(String year, String site, String clientIds,
			List<String> clientIdList) throws SQLException {
		Connection conDestinationDb = null;
		Statement stmtDestDb = null;
		try {
			String queryforRoyaltyTrendMerchandiseDestDb = "SELECT SUM(RE.Earnings_Amount) AS totalRoyalty, substring(cast (PR.Period as varchar) ,1,4) as year "
					+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
					+ "INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
					+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
					+ "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
					+ "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
					+ "INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + "CL.Client_Code IN ("
					+ clientIds + ") AND CL.Client_Site='" + site + "' ";

			String prd = null;
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int yr = cal.get(Calendar.YEAR);
			if (year.equalsIgnoreCase(String.valueOf(yr))) {
				int month = cal.get(Calendar.MONTH);
				if (month > 06) {
					prd = "'" + yr + "06'";
					queryforRoyaltyTrendMerchandiseDestDb = queryforRoyaltyTrendMerchandiseDestDb + "AND  PR.Period ="
							+ prd;
				}
			} else {
				queryforRoyaltyTrendMerchandiseDestDb = queryforRoyaltyTrendMerchandiseDestDb
						+ "AND  substring(cast (PR.Period as varchar) ,1,4)='" + year + "' ";
			}
			queryforRoyaltyTrendMerchandiseDestDb = queryforRoyaltyTrendMerchandiseDestDb
					+ "AND DC.DistChannel_Category = 'Merchandise' "
					+ "group by  substring(cast (PR.Period as varchar) ,1,4)";
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=Mybmg4_stage_rec";
			conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007", "Si2RIbVdVkDyCIVVBmbA");
			stmtDestDb = conDestinationDb.createStatement();
			ResultSet royaltyTrendMerchandise = stmtDestDb.executeQuery(queryforRoyaltyTrendMerchandiseDestDb);
			String totalRoyalty = null;
			while (royaltyTrendMerchandise.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendMerchandise.getString("totalRoyalty");
					// year = royaltyTrendPhy.getString("year");
				}
			}
			royaltyTrendMerchandiseDestDb = totalRoyalty;
			System.out.println(
					"Royalty trend Merchandise of dest db for  :" + year + " is : " + royaltyTrendMerchandiseDestDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtDestDb.close();
			conDestinationDb.close();
		}
	}

	private void dbQueryForPrimeRoyaTrendMerchandise(String year, String site, String clientIds,
			List<String> clientIdList) throws SQLException {
		Connection connBmgPrime = null;
		Statement stmtRematDb = null;
		try {

			String queryforRoyaltyTrendMerchandisePrimeDb = "select substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  year,"
					+ "SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT from  "
					+ "(select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,"
					+ "dcrx.CALCLOG_SEQ,dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID "
					+ "from tbl_STG_RoycalcPayee dcrx inner join(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,"
					+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount from (select distinct ROYALTOR_CODE,"
					+ "ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code from tbl_STG_RoycalcPayee "
					+ "JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.CONTRACT')) CO "
					+ "ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE "
					+ "AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND "
					+ "isnull(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC') AND CO.CONTRACT_NAME IS NOT NULL) rp"
					+ " group by RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,"
					+ "RP.roycalcpayee_bfbalance) DUP on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and "
					+ "DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP " + "JOIN tbl_STG_SalesWorkArc "
					+ "SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ "
					+ "JOIN tbl_STG_Calclog CL "
					+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F' "
					+ "AND CL.CALCLOG_PERIODTYPE<>'A'  "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD "
					+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "
					+ "INNER JOIN   (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC ON PC.CONFIG_CODE=PD.CONFIG_CODE "
					+ "where SWA.DISTCHAN_CODE IN ('MERC')" + "and rcp.royaltor_code in(" + clientIds
					+ ") and rcp.royaltor_site='" + site + "' "
					+ "and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) ='" + year + "' "
					+ "GROUP BY substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)";

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dbUrlBmgPrimeLandingLayer = "jdbc:sqlserver://10.6.238.202:1433;databaseName=BMGPrime_LandingLayer";
			connBmgPrime = DriverManager.getConnection(dbUrlBmgPrimeLandingLayer, "batr007", "Si2RIbVdVkDyCIVVBmbA");
			stmtRematDb = connBmgPrime.createStatement();
			ResultSet royaltyTrendMerchandise = stmtRematDb.executeQuery(queryforRoyaltyTrendMerchandisePrimeDb);
			String totalRoyalty = null;
			while (royaltyTrendMerchandise.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendMerchandise.getString("AMOUNT");
				}
			}
			royaltyTrendMerchandiseSourceDb = totalRoyalty;
			System.out.println(
					"Royalty trend Merchandise for Prime db :" + year + " is : " + royaltyTrendMerchandiseSourceDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtRematDb.close();
			connBmgPrime.close();
		}

	}

	private void dbQueryForRemaRoyaTrendMerchandise(String year, String site, String clientIds,
			List<String> clientIdList) throws SQLException {
		Connection con = null;
		Statement stmtRematDb = null;
		try {

			String queryForRoyaltyTrendMerchandiseRema = "select SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  as year,SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT "
					+ "from (select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID from RMAESTRO_USER.RoycalcPayee dcrx "
					+ "inner join(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,RP.roycalcpayee_bfbalance,"
					+ "COUNT(*) as reccount from (select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,"
					+ "RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code from RMAESTRO_USER.RoycalcPayee JOIN RMAESTRO_USER.CONTRACT CO "
					+ " ON CO.CONTRACT_CODE=RMAESTRO_USER.RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=RMAESTRO_USER."
					+ "RoycalcPayee.RCONTRACT_SITE AND NVL(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')) rp group by "
					+ "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,RP.roycalcpayee_bfbalance) DUP "
					+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,dcrx.roycalcpayee_bfbalance,"
					+ "dcrx.payee_code) RCP JOIN RMAESTRO_USER.SalesWorkArc SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ "
					+ "JOIN RMAESTRO_USER.Calclog CL ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' "
					+ "AND CL.CALCLOG_RUNTYPE='F' AND CL.CALCLOG_PERIODTYPE<>'A' and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) || SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)>='201401'  "
					+ "and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) || SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)<='201703' "
					+ "INNER JOIN RMAESTRO_USER.productS PD ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "
					+ "INNER JOIN RMAESTRO_USER.CONFIG CF ON CF.CONFIG_CODE=SWA.CONFIG_CODE "
					+ "INNER JOIN   RMAESTRO_USER.CONFIG PC ON PC.CONFIG_CODE=PD.CONFIG_CODE where SWA.DISTCHAN_CODE IN ('MERC') "
					+ "and rcp.royaltor_code in(" + clientIds + ") and rcp.royaltor_site='" + site
					+ "' and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) ='" + year + "' "
					+ "GROUP BY SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)";

			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection("jdbc:oracle:thin:@10.6.238.172:1480:DBOPRIM1", "RMAESTRO_USER",
					"counterp");
			stmtRematDb = con.createStatement();
			ResultSet royaltyTrendMerchandise = stmtRematDb.executeQuery(queryForRoyaltyTrendMerchandiseRema);
			String totalRoyalty = null;
			while (royaltyTrendMerchandise.next()) {
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					totalRoyalty = royaltyTrendMerchandise.getString("AMOUNT");
				}
			}
			royaltyTrendMerchandiseSourceDb = totalRoyalty;
			System.out.println("Royalty trend Merchandise of REMA db for  year:" + year + " is : "
					+ royaltyTrendMerchandiseSourceDb);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			stmtRematDb.close();
			con.close();
		}
	}

	// *******************************************************************************************
	// * ******************** ROYALTY TREND ------ DoNut Graph
	// ************************* *
	// *******************************************************************************************

	public void DB_Query_RoyaltyTrendDonutGraph(String client, ArrayList<String> scrossClientList, String crossClientId,
			String site, String year, ArrayList<String> listTitle) {
		totalValueDestDb = 0;
		phyValueDestDb = 0;
		try {
			List<String> clientIdList = new ArrayList<String>();
			clientIdList.add("'" + client + "'");
			if (crossClientId != null) {
				clientIdList.add(crossClientId);
			} else if (!scrossClientList.isEmpty()) {
				String cId = "";
				cId = scrossClientList.toString().replace("[", "").replace("]", "");
				clientIdList.add(cId);
			}
			String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
			// For Destination db
			dbQueryForRoyaTrendTotalRoyDestDb(year, site, clientIds, clientIdList, null);
			totalValueDestDb = Double.parseDouble(royaltyTrendTotalRoyaltyDestDb);

			listDest = new ArrayList<>();
			listDest.add(totalValueDestDb);

			for (String s : listTitle) {
				if (s.contains("Physical")) {
					phyValueDestDb = 0.0;
					percentagePhyDestDb = 0.0;
					dbQueryDestDbForRoyTrendPhy(year, site, clientIds, clientIdList);
					if (royaltyTrendPhyDestDb != null) {
						phyValueDestDb = Double.parseDouble(royaltyTrendPhyDestDb);
						percentagePhyDestDb = phyValueDestDb * 100 / totalValueDestDb;
					}

					System.out.println(percentagePhyDestDb + ":percentagePhyDestDb");
					listDest.add(phyValueDestDb);
					listDest.add(percentagePhyDestDb);
				} else if (s.contains("Digital")) {
					digitalValueDestDb = 0.0;
					percentageDigitalDestDb = 0.0;
					dbQueryForDestRoyaTrendDigital(year, site, clientIds, clientIdList);
					if (royaltyTrendDigitalDestDb != null) {
						digitalValueDestDb = Double.parseDouble(royaltyTrendDigitalDestDb);
						percentageDigitalDestDb = digitalValueDestDb * 100 / totalValueDestDb;
					}
					System.out.println(percentageDigitalDestDb + ":percentageDigitalDestDb");
					listDest.add(digitalValueDestDb);
					listDest.add(percentageDigitalDestDb);
				} else if (s.contains("Licensing")) {
					licensingValueDestDb = 0.0;
					percentageLicensingDestDb = 0.0;
					dbQueryForDestRoyaTrendLicensing(year, site, clientIds, clientIdList);
					if (royaltyTrendLicensingDestDb != null) {
						licensingValueDestDb = Double.parseDouble(royaltyTrendLicensingDestDb);
						percentageLicensingDestDb = licensingValueDestDb * 100 / totalValueDestDb;
					}
					System.out.println(percentageLicensingDestDb + ":percentageLicensingDestDb");
					listDest.add(licensingValueDestDb);
					listDest.add(percentageLicensingDestDb);
				} else if (s.contains("Sync")) {
					syncValueDestDb = 0.0;
					percentageSyncDestDb = 0.0;
					dbQueryForDestRoyaTrendSync(year, site, clientIds, clientIdList);
					if (royaltyTrendSyncDestDb != null) {
						syncValueDestDb = Double.parseDouble(royaltyTrendSyncDestDb);
						percentageSyncDestDb = syncValueDestDb * 100 / totalValueDestDb;
					}
					System.out.println(percentageSyncDestDb + ":percentageSyncDestDb");
					listDest.add(syncValueDestDb);
					listDest.add(percentageSyncDestDb);
				} else if (s.contains("Public Performance")) {
					ppValueDestDb = 0.0;
					percentagePPDestDb = 0.0;
					dbQueryForDestRoyaTrendPublicPerformance(year, site, clientIds, clientIdList);
					if (royaltyTrendPublicPerformanceDestDb != null) {
						ppValueDestDb = Double.parseDouble(royaltyTrendPublicPerformanceDestDb);
						percentagePPDestDb = ppValueDestDb * 100 / totalValueDestDb;
					}
					System.out.println(percentagePPDestDb + ":percentagePPDestDb");
					listDest.add(ppValueDestDb);
					listDest.add(percentagePPDestDb);

				} else if (s.contains("Merchandise")) {
					merchandiseValueDestDb = 0.0;
					percentageMerchandiseDestDb = 0.0;
					dbQueryForDestRoyaTrendMerchandise(year, site, clientIds, clientIdList);
					if (royaltyTrendMerchandiseDestDb != null) {
						merchandiseValueDestDb = Double.parseDouble(royaltyTrendMerchandiseDestDb);
						percentageMerchandiseDestDb = merchandiseValueDestDb * 100 / totalValueDestDb;
					}
					System.out.println(percentageMerchandiseDestDb + ":percentageMerchandiseDestDb");
					listDest.add(merchandiseValueDestDb);
					listDest.add(percentageMerchandiseDestDb);
				}
			}

			// For source db
			dbQueryForRoyaTrendTotalRoyRecroy(year, site, clientIds, clientIdList, null);
			totalValueSrcDb = Double.parseDouble(royaltyTrendTotalRoyaltySourceDb);
			int yr = Integer.parseInt(year);
			if (yr <= 2016) {
				dbQuerysForRema(year, site, clientIds, clientIdList, totalValueSrcDb, listTitle);
			} else {
				dbQuerysForPrime(year, site, clientIds, clientIdList, totalValueSrcDb, listTitle);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void dbQuerysForPrime(String year, String site, String clientIds, List<String> clientIdList,
			double totalValueSrcDb, ArrayList<String> listTitle) {
		try {
			listSrc = new ArrayList<>();
			listSrc.add(totalValueSrcDb);

			for (String title : listTitle) {
				if (title.contains("Physical")) {
					phyValueSourceDb = 0.0;
					percentagePhySourceDb = 0.0;
					dbQueryForPrimeRoyaltyTrendPhy(year, site, clientIds, clientIdList);
					if (royaltyTrendPhySourceDb != null) {
						phyValueSourceDb = Double.parseDouble(royaltyTrendPhySourceDb);
						percentagePhySourceDb = phyValueSourceDb * 100 / totalValueSrcDb;
					}
					listSrc.add(phyValueSourceDb);
					listSrc.add(percentagePhySourceDb);
				} else if (title.contains("Digital")) {
					digitalValueSourceDb = 0.0;
					percentageDigitalSourceDb = 0.0;
					dbQueryForPrimeRoyaTrendDigital(year, site, clientIds, clientIdList);
					if (royaltyTrendDigitalSourceDb != null) {
						digitalValueSourceDb = Double.parseDouble(royaltyTrendDigitalSourceDb);
						percentageDigitalSourceDb = digitalValueSourceDb * 100 / totalValueSrcDb;
					}

					listSrc.add(digitalValueSourceDb);
					listSrc.add(percentageDigitalSourceDb);
				} else if (title.contains("Licensing")) {
					licensingValueSourceDb = 0.0;
					percentageLicensingSourceDb = 0.0;
					dbQueryForPrimeRoyaTrendLicensing(year, site, clientIds, clientIdList);
					if (royaltyTrendLicensingSourceDb != null) {
						licensingValueSourceDb = Double.parseDouble(royaltyTrendLicensingSourceDb);
						percentageLicensingSourceDb = licensingValueSourceDb * 100 / totalValueSrcDb;
					}
					listSrc.add(licensingValueSourceDb);
					listSrc.add(percentageLicensingSourceDb);
				} else if (title.contains("Sync")) {
					syncValueSourceDb = 0.0;
					percentageSyncSourceDb = 0.0;
					dbQueryForPrimeRoyaTrendSync(year, site, clientIds, clientIdList);
					if (royaltyTrendSyncSourceDb != null) {
						syncValueSourceDb = Double.parseDouble(royaltyTrendSyncSourceDb);
						percentageSyncSourceDb = syncValueSourceDb * 100 / totalValueSrcDb;
					}
					listSrc.add(syncValueSourceDb);
					listSrc.add(percentageSyncSourceDb);
				} else if (title.contains("Public Performance")) {
					ppValueSourceDb = 0.0;
					percentagePPSourceDb = 0.0;
					dbQueryForPrimeRoyaTrendPublicPerformance(year, site, clientIds, clientIdList);
					if (royaltyTrendPublicPerformanceSourceDb != null) {
						ppValueSourceDb = Double.parseDouble(royaltyTrendPublicPerformanceSourceDb);
						percentagePPSourceDb = ppValueSourceDb * 100 / totalValueSrcDb;
					}

					listSrc.add(ppValueSourceDb);
					listSrc.add(percentagePPSourceDb);

				} else if (title.contains("Merchandise")) {
					dbQueryForPrimeRoyaTrendMerchandise(year, site, clientIds, clientIdList);
					merchandiseValueSourceDb = 0.0;
					percentageMerchandiseSourceDb = 0.0;
					if (royaltyTrendMerchandiseSourceDb != null) {
						merchandiseValueSourceDb = Double.parseDouble(royaltyTrendMerchandiseSourceDb);
						percentageMerchandiseSourceDb = merchandiseValueSourceDb * 100 / totalValueSrcDb;
					}

					listSrc.add(merchandiseValueSourceDb);
					listSrc.add(percentageMerchandiseSourceDb);
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	private void dbQuerysForRema(String year, String site, String clientIds, List<String> clientIdList,
			double totalValueSrcDb, ArrayList<String> listTitle) {
		try {
			listSrc = new ArrayList<>();
			listSrc.add(totalValueSrcDb);
			for (String title : listTitle) {
				if (title.contains("Physical")) {
					phyValueSourceDb = 0.0;
					percentagePhySourceDb = 0.0;
					dbQueryForRemaRoyaltyTrendPhy(year, site, clientIds, clientIdList);
					if (royaltyTrendPhySourceDb != null) {
						phyValueSourceDb = Double.parseDouble(royaltyTrendPhySourceDb);
						percentagePhySourceDb = phyValueSourceDb * 100 / totalValueSrcDb;
					}

					listSrc.add(phyValueSourceDb);
					listSrc.add(percentagePhySourceDb);

				} else if (title.contains("Digital")) {
					digitalValueSourceDb = 0.0;
					percentageDigitalSourceDb = 0.0;
					dbQueryForRemaRoyaTrendDigital(year, site, clientIds, clientIdList);
					if (royaltyTrendDigitalSourceDb != null) {
						digitalValueSourceDb = Double.parseDouble(royaltyTrendDigitalSourceDb);
						percentageDigitalSourceDb = digitalValueSourceDb * 100 / totalValueSrcDb;
					}

					listSrc.add(digitalValueSourceDb);
					listSrc.add(percentageDigitalSourceDb);
				} else if (title.contains("Licensing")) {
					licensingValueSourceDb = 0.0;
					percentageLicensingSourceDb = 0.0;
					dbQueryForRemaRoyaTrendLicensing(year, site, clientIds, clientIdList);
					if (royaltyTrendLicensingSourceDb != null) {
						licensingValueSourceDb = Double.parseDouble(royaltyTrendLicensingSourceDb);
						percentageLicensingSourceDb = licensingValueSourceDb * 100 / totalValueSrcDb;
					}

					listSrc.add(licensingValueSourceDb);
					listSrc.add(percentageLicensingSourceDb);
				} else if (title.contains("Sync")) {
					syncValueSourceDb = 0.0;
					percentageSyncSourceDb = 0.0;
					dbQueryForRemaRoyaTrendSync(year, site, clientIds, clientIdList);
					if (royaltyTrendSyncSourceDb != null) {
						syncValueSourceDb = Double.parseDouble(royaltyTrendSyncSourceDb);
						percentageSyncSourceDb = syncValueSourceDb * 100 / totalValueSrcDb;
					}

					listSrc.add(syncValueSourceDb);
					listSrc.add(percentageSyncSourceDb);

				} else if (title.contains("Public Performance")) {
					ppValueSourceDb = 0.0;
					percentagePPSourceDb = 0.0;
					dbQueryForRemaRoyaTrendPublicPerformance(year, site, clientIds, clientIdList);
					if (royaltyTrendPublicPerformanceSourceDb != null) {
						ppValueSourceDb = Double.parseDouble(royaltyTrendPublicPerformanceSourceDb);
						percentagePPSourceDb = ppValueSourceDb * 100 / totalValueSrcDb;
					}

					listSrc.add(ppValueSourceDb);
					listSrc.add(percentagePPSourceDb);

				} else if (title.contains("Merchandise")) {
					merchandiseValueSourceDb = 0.0;
					percentageMerchandiseSourceDb = 0.0;
					dbQueryForRemaRoyaTrendMerchandise(year, site, clientIds, clientIdList);
					if (royaltyTrendMerchandiseSourceDb != null) {
						merchandiseValueSourceDb = Double.parseDouble(royaltyTrendMerchandiseSourceDb);
						percentageMerchandiseSourceDb = merchandiseValueSourceDb * 100 / totalValueSrcDb;
					}
					listSrc.add(merchandiseValueSourceDb);
					listSrc.add(percentageMerchandiseSourceDb);
				}

			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
}
