package com.infosys.bmg.MyBMGDataValidations.db.configurations;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBConnection {

	public  String periodDestinationDb = null;
	public  double closingBlanaceDestinationDb = 0;
	public  String currencyDestinationDb = null;
	public  String periodSourceDB = null;
	public  double closingBalanaceSoruceDb = 0;
	public  String currencySourceDb = null;
	public  String periodSourceDb = null;

	public  Map<String, String> mapTopTracksRoyaltyByDownload = new HashMap<>();
	public  Map<String, String> mapTopTracksRoyaltyByStream = new HashMap<>();
	public  Map<String, String> mapTopTracksUnitsByDownload = new HashMap<>();
	public  Map<String, String> mapTopTracksUnitsByStream = new HashMap<>();
	public  Map<String, String> mapTopTracksProductTitle= new HashMap<>();
	public  Map<String, String> mapTopTracksUnitDownloadSource = new HashMap<>();
	public  Map<String, String> mapTopTracksRoyaltyDownloadSource = new HashMap<>();
	public  Map<String, String> mapTopTracksRoyaltyStreamSource = new HashMap<>();
	public  Map<String, String> mapTopTracksUnitsStreamSource = new HashMap<>();
	public  List<String> TopTracksProductIsrcCode = new ArrayList<String>();
	public  List<String> TopTracksProductIsrcCodeSource = new ArrayList<String>();
	public  Map<String, String>  mapTopTracksProductTitleSource = new HashMap<>();
	public  String newStatementDueDest=null;
	public  String newStatementDueSource=null;
	public  String pipelineRoyltyRecordDest=null;
	public  String pipelineRoyltyPeriodRecordDest=null;
	public  String pipelineRoyltyPeriodRecordSource=null;
	public  String pipelineRoyltyRecordSource=null;

	public  void DB_Query(String site, String client, ArrayList<String> scrossClientList, String crossClientId,
			String datapoint, String period)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		mapTopTracksRoyaltyByDownload = new HashMap<>();
		mapTopTracksRoyaltyByStream = new HashMap<>();
		mapTopTracksUnitsByDownload = new HashMap<>();
		mapTopTracksUnitsByStream = new HashMap<>();
		 TopTracksProductIsrcCode= new ArrayList<String>();
		mapTopTracksUnitDownloadSource = new HashMap<>();
		mapTopTracksRoyaltyDownloadSource = new HashMap<>();
		mapTopTracksRoyaltyStreamSource = new HashMap<>();
		mapTopTracksUnitsStreamSource = new HashMap<>();
		 TopTracksProductIsrcCodeSource= new ArrayList<String>();
		 mapTopTracksProductTitle=new HashMap<>();
		 mapTopTracksProductTitleSource=new HashMap<>();
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=Mybmg4_stage_rec";
		List<String> clientIdList = new ArrayList<String>();
		clientIdList.add("'" + client + "'");
		if (crossClientId != null) {
			clientIdList.add(crossClientId);
		} else if (!scrossClientList.isEmpty()) {
			String cId = "";
			cId = scrossClientList.toString().replace("[", "").replace("]", "");
			clientIdList.add(cId);

		}
		String clientIds = clientIdList.toString().replace("[", "").replace("]", "");

		Connection conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");

		Statement stmtDestDb = conDestinationDb.createStatement();

		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dbUrlRecroProd = "jdbc:sqlserver://10.6.238.158:5555;databaseName=recroy_prod";
		Connection connRecroProd = DriverManager.getConnection(dbUrlRecroProd, "batr007", "Si2RIbVdVkDyCIVVBmbA");

		Statement stmtRecroProd = connRecroProd.createStatement();

		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dbUrlBmgPrimeLandingLaye = "jdbc:sqlserver://10.6.238.202:1433;databaseName=BMGPrime_LandingLayer";
		Connection connBmgPrime = DriverManager.getConnection(dbUrlBmgPrimeLandingLaye, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");
		Statement stmtBmgPrime = connBmgPrime.createStatement();

		if (datapoint.equalsIgnoreCase("Statement Balance")) {
			String queryToGetStatementBalance = "select client_code,period ,closing_balance"
					+ " from [mybmg4_stage_rec].[dbo].[Period]  p join [mybmg4_stage_rec].[dbo].Rec_Balances rb"
					+ " on p.period_key=rb.period_key join [mybmg4_stage_rec].[dbo].client c on rb.client_key=c.client_key"
					+ " where p.period_key =(select max(period_key) from [mybmg4_stage_rec].[dbo].Rec_Balances rb1 where rb1.client_key=rb.client_key)"
					+ "and c.client_code in (" + clientIds + ") and client_site='" + site + "'";

			ResultSet stmtBalList = stmtDestDb.executeQuery(queryToGetStatementBalance);

			double sbalDestination = 0;

			while (stmtBalList.next()) {
				String clientCode = stmtBalList.getString("client_code");

				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");

					if (clientCode.trim().equalsIgnoreCase(clientId)) {

						sbalDestination = sbalDestination
								+ Double.parseDouble(stmtBalList.getString("closing_balance"));
					}
				}
				periodDestinationDb = stmtBalList.getString("period");

			}
			closingBlanaceDestinationDb = sbalDestination;

			String queryToGetClosingBalPrime = "SELECT C.RCONTRACT_CODE,C.RCONTRACT_SITE, C.ROYALTOR_CODE,C.ROYALTOR_SITE,C.CALCLOG_PERIOD AS PERIOD,"
					+ "SUM(roycalcpayee_bfbalance) AS OpeningBalance FROM"
					+ "(select distinct dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,"
					+ "dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ, dcrx.roycalcpayee_bfbalance,dcrx.payee_code"
					+ ",CALCLOG_PERIOD from [BMGPrime_LandingLayer].[dbo].[tbl_STG_RoycalcPayee] " + "dcrx inner JOIN "
					+ "(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE, "
					+ "MAX(RP.CALCLOG_SEQ) AS CALCLOGSEQ, RP.roycalcpayee_bfbalance, "
					+ "COUNT(*) as reccount, MAX(substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) + "
					+ "substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)) AS CALCLOG_PERIOD from "
					+ "(select distinct  ROYALTOR_CODE,ROYALTOR_SITE, "
					+ "RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code "
					+ "from tbl_STG_RoycalcPayee where RCONTRACT_CODE IN (Select distinct [RCONTRACT_CODE] FROM [BMGPrime_LandingLayer].[dbo].[tbl_STG_RoycalcPayee] where royaltor_code in ("
					+ clientIds + ") and royaltor_Site='" + site + "') " + "AND RCONTRACT_SITE IN('" + site
					+ "'))rp inner "
					+ "join tbl_STG_Calclog CL on RP.CALCLOG_SEQ= CL.CALCLOG_SEQ AND CL.CALCLOG_RUNTYPE='F' "
					+ "AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_PERIODTYPE<>'A' group by  RP.ROYALTOR_CODE, "
					+ "RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ, RP.roycalcpayee_bfbalance)DUP "
					+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOGSEQ  "
					+ "= dcrx.CALCLOG_SEQ where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1))C "
					+ "WHERE C.ROYALTOR_CODE in (" + clientIds + ") AND C.ROYALTOR_SITE='" + site
					+ "' GROUP BY C.ROYALTOR_CODE,C.ROYALTOR_SITE, "
					+ "C.CALCLOG_PERIOD,PAYEE_CODE,RCONTRACT_SITE,RCONTRACT_CODE ORDER BY C.CALCLOG_PERIOD DESC ";

			ResultSet stmtBalListPrime = stmtBmgPrime.executeQuery(queryToGetClosingBalPrime);
			double primeStmtBal = 0;
			while (stmtBalListPrime.next()) {
				String clientCode = "'" + stmtBalListPrime.getString("ROYALTOR_CODE").trim() + "'";
				for (String id1 : clientIdList) {

					if (clientCode.equalsIgnoreCase(id1)
							&& stmtBalListPrime.getString("PERIOD").equalsIgnoreCase(periodDestinationDb)) {
						primeStmtBal = primeStmtBal + Double.parseDouble(stmtBalListPrime.getString("OpeningBalance"));
						periodSourceDb = stmtBalListPrime.getString("PERIOD");
					}
				}

			}

			String queryToGetTransaction = "select royaltor_code,royaltor_site,PERIOD, "
					+ "SUM(CONTOPTACT_RECOUPAMT) AS TRANSACTIONS_TOTAL from "
					+ "(select royaltor_code,royaltor_site,calclog_finalized,CONTOPTACT_RECOUPAMT,transtype_code,contract_code,contract_site "
					+ "from recroy_prod..CONTOPTACT) rt "
					+ "inner join [dbo].[tblRecordmaestroETL_CONTRACT_Table1] C on  "
					+ "rt.CONTRACT_CODE=C.CONTRACT_CODE AND rt.CONTRACT_SITE=C.CONTRACT_SITE AND  isnull(C.CONTTYPE_CODE,'') not in ('JVAT','PAGC') "
					+ "inner join ( "
					+ "select substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),4,4) + substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),1,2) as period , calclog_seq  from [dbo].[CALCLOG]) CL "
					+ "on CL.calclog_seq=rt.calclog_finalized " + "WHERE TRANSTYPE_CODE<>'ROYS' AND royaltor_code IN ("
					+ clientIds + ") AND ROYALTOR_SITE='" + site + "' " + "GROUP BY royaltor_code,royaltor_site,PERIOD "
					+ "order by PERIOD desc ";

			ResultSet trasactionList = stmtRecroProd.executeQuery(queryToGetTransaction);
			double trasactAmt = 0;
			while (trasactionList.next()) {
				String rCode = "'" + trasactionList.getString("royaltor_code").trim() + "'";
				for (String id : clientIdList) {

					if (rCode.equalsIgnoreCase(id)
							&& trasactionList.getString("PERIOD").equalsIgnoreCase(periodDestinationDb)) {
						trasactAmt = trasactAmt + Double.parseDouble(trasactionList.getString("TRANSACTIONS_TOTAL"));
					}
				}
			}

			String queryToGetRoyality = "select royaltor_code,royaltor_site,PERIOD, "
					+ "SUM(CONTOPTACT_RECOUPAMT) AS ROYALTY_TOTAL from "
					+ "(select royaltor_code,royaltor_site,calclog_finalized,CONTOPTACT_RECOUPAMT,transtype_code,contract_code,contract_site "
					+ "from recroy_prod..CONTOPTACT) rt "
					+ "inner join [dbo].[tblRecordmaestroETL_CONTRACT_Table1] C on "
					+ "rt.CONTRACT_CODE=C.CONTRACT_CODE AND rt.CONTRACT_SITE=C.CONTRACT_SITE AND  isnull(C.CONTTYPE_CODE,'') not in ('JVAT','PAGC') "
					+ "inner join ("
					+ "select substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),4,4) + substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),1,2) as period , calclog_seq  from [dbo].[CALCLOG]) CL "
					+ "on CL.calclog_seq=rt.calclog_finalized " + "WHERE TRANSTYPE_CODE='ROYS' AND royaltor_code IN ("
					+ clientIds + ") AND ROYALTOR_SITE='" + site + "' " + "GROUP BY royaltor_code,royaltor_site,PERIOD "
					+ "order by PERIOD desc ";

			// Statement stmtRecroProd = conn3.createStatement();
			ResultSet royalityList = stmtRecroProd.executeQuery(queryToGetRoyality);
			double royalityAmt = 0;
			while (royalityList.next()) {
				String rCode = "'" + royalityList.getString("royaltor_code").trim() + "'";
				for (String id : clientIdList) {

					if (rCode.equalsIgnoreCase(id)
							&& royalityList.getString("PERIOD").equalsIgnoreCase(periodDestinationDb)) {
						royalityAmt = royalityAmt + Double.parseDouble(royalityList.getString("ROYALTY_TOTAL"));
					}
				}
			}
			closingBalanaceSoruceDb = royalityAmt + trasactAmt + primeStmtBal;

			String queryToGetCurrencyCodeDestDb = "Select currency_code from  [mybmg4_stage_rec].[dbo].[Currency] where currency_key =("
					+ "Select siteCurrency_Key from mybmg4_stage_rec.[dbo].[Site] WHERE site_code='" + site + "')";
			ResultSet currencyList = stmtDestDb.executeQuery(queryToGetCurrencyCodeDestDb);
			while (currencyList.next()) {
				currencyDestinationDb = currencyList.getString("currency_code");

			}
		}
		if (datapoint.equalsIgnoreCase("Top Tracks")) {
			String month = null;
			String year = null;

			StringBuilder sb = new StringBuilder();
			period = period.toString().replace("(", "").replace(")", "");
			String s[] = period.split("/");
			month = s[0];
			year = s[1];
			sb.append(year + "" + month);
			period = sb.toString();
			System.out.println(period + ":period");

			String queryToGetProductCodes = "SELECT TOP 5 " + "P.Product_Key, " + "P.Product_Title, "
					+ "P.Product_UserCode ," + " SUM(RE.Earnings_Amount) AS totalRoyalty "
					+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
					+ "INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
					+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
					+ " INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
					+ " INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
					+ "INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + " CL.Client_Code IN ("
					+ clientIds + ") AND CL.Client_Site='" + site + "'" + " AND PR.Period IN ('201806') "
					+ "AND DC.DistChannel_Category = 'Sales' " + " AND EC.Config_ProductType IN ('Track') "
					+ "GROUP BY P.Product_Key, P.Product_Title ,P.Product_UserCode " + "ORDER BY totalRoyalty DESC;";

			ResultSet productCodeList = stmtDestDb.executeQuery(queryToGetProductCodes);

			List<String> pcodeList = new ArrayList<String>();

			while (productCodeList.next()) {
				String pcode = productCodeList.getString("Product_Key");
				String productTitle = productCodeList.getString("Product_Title");
				String productIsrcCode = productCodeList.getString("Product_UserCode");
				pcodeList.add(pcode);
				TopTracksProductIsrcCode.add(productIsrcCode);
				mapTopTracksProductTitle.put(productIsrcCode, productTitle);
			}
			if (!pcodeList.isEmpty()) {
				String pcodel = pcodeList.toString().replace("[", "").replace("]", "");
				String queryToGetTopTracksByIncomeSubtypeDownload = "SELECT TOP 5 " + "P.Product_Key, "
						+ "P.Product_Title, " + "PC.Config_Desc, " + "P.Product_UserCode, "
						+ "SUM(RE.Earnings_Amount) AS totalRoyalty "
						+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
						+ " INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
						+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
						+ "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
						+ "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
						+ "	INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + "  CL.Client_Code IN ("
						+ clientIds + ") AND CL.Client_Site='" + site + "'" + "AND PR.Period IN ('201806') "
						+ "AND DC.DistChannel_Category = 'Sales' " + "AND EC.Config_ProductType IN ('Track') "
						+ "	and EC.Income_SubType='Download' " + "	and P.Product_Key in  (" + pcodel + ") "
						+ "GROUP BY P.Product_Key, P.Product_Title, PC.Config_Desc, P.Product_UserCode, P.Product_Barcode "
						+ "ORDER BY totalRoyalty DESC;";

				ResultSet topTracksDownload = stmtDestDb.executeQuery(queryToGetTopTracksByIncomeSubtypeDownload);

				while (topTracksDownload.next()) {
					// ArrayList<String> topTracksList=new ArrayList<String>();
					String productTitle = topTracksDownload.getString("Product_Title");
					String totalRoyalty = topTracksDownload.getString("totalRoyalty");
					String productUserCode= topTracksDownload.getString("Product_UserCode");
					mapTopTracksRoyaltyByDownload.put(productUserCode, totalRoyalty);
				}

				String queryToGetTopTracksByIncomeSubtypeStream = "SELECT TOP 5 " + "P.Product_Key, "
						+ "P.Product_Title, " + "PC.Config_Desc, " + "P.Product_UserCode, "
						+ "SUM(RE.Earnings_Amount) AS totalRoyalty "
						+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
						+ " INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
						+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
						+ "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
						+ "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
						+ "	INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + "  CL.Client_Code IN ("
						+ clientIds + ") AND CL.Client_Site='" + site + "'" + "AND PR.Period IN ('201806') "
						+ "AND DC.DistChannel_Category = 'Sales' " + "AND EC.Config_ProductType IN ('Track') "
						+ "	and EC.Income_SubType='Stream' " + "	and P.Product_Key in  (" + pcodel + ") "
						+ "GROUP BY P.Product_Key, P.Product_Title, PC.Config_Desc, P.Product_UserCode, P.Product_Barcode "
						+ "ORDER BY totalRoyalty DESC;";

				ResultSet topTrackStream = stmtDestDb.executeQuery(queryToGetTopTracksByIncomeSubtypeStream);
				while (topTrackStream.next()) {
					String productTitle = topTrackStream.getString("Product_Title");
					String totalRoyalty = topTrackStream.getString("totalRoyalty");
					String productUserCode= topTrackStream.getString("Product_UserCode");
					mapTopTracksRoyaltyByStream.put(productUserCode, totalRoyalty);
				}
				String queryToGetTopTracksUnitsByDownload = "SELECT TOP 5 " + "P.Product_Key, " + "P.Product_Title, "
						+ "PC.Config_Desc, " + "P.Product_UserCode, " + "SUM(COALESCE(ERE.Units, 0))     AS Units  "
						+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
						+ " INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
						+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
						+ "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
						+ "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
						+ "INNER JOIN ExtnRecEarnings ERE ON RE.RecEarnings_Key = ERE.RecEarnings_Key "
						+ "	INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + "  CL.Client_Code IN ("
						+ clientIds + ") AND CL.Client_Site='" + site + "'" + "AND PR.Period IN ('201806') "
						+ "AND DC.DistChannel_Category = 'Sales' " + "AND EC.Config_ProductType IN ('Track') "
						+ "	and EC.Income_SubType='Download' " + "	and P.Product_Key in  (" + pcodel + ") "
						+ "GROUP BY P.Product_Key, P.Product_Title, PC.Config_Desc, P.Product_UserCode, P.Product_Barcode "
						+ "ORDER BY Units  DESC;";

				ResultSet topTrackUnitDownload = stmtDestDb.executeQuery(queryToGetTopTracksUnitsByDownload);
				while (topTrackUnitDownload.next()) {
					String productTitle = topTrackUnitDownload.getString("Product_Title");
					String totalUnits = topTrackUnitDownload.getString("Units");
					String productUserCode= topTrackUnitDownload.getString("Product_UserCode");
					mapTopTracksUnitsByDownload.put(productUserCode, totalUnits);
				}

				String queryToGetTopTracksUnitsByStream = "SELECT TOP 5 " + "P.Product_Key, " + "P.Product_Title, P.Product_UserCode ," +

						"SUM(COALESCE(ERE.Units, 0))     AS Units  "
						+ "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
						+ " INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
						+ "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "
						+ "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "
						+ "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "
						+ "INNER JOIN ExtnRecEarnings ERE ON RE.RecEarnings_Key = ERE.RecEarnings_Key "
						+ "	INNER JOIN Client CL ON CL.Client_Key=RE.Client_Key " + "WHERE " + "  CL.Client_Code IN ("
						+ clientIds + ") AND CL.Client_Site='" + site + "'" + "AND PR.Period IN ('201806') "
						+ "AND DC.DistChannel_Category = 'Sales' " + "AND EC.Config_ProductType IN ('Track') "
						+ "	and EC.Income_SubType='Stream' " + "	and P.Product_Key in  (" + pcodel + ") "
						+ "GROUP BY P.Product_Key, P.Product_Title, PC.Config_Desc, P.Product_UserCode, P.Product_Barcode "
						+ "ORDER BY Units  DESC;";

				ResultSet topTrackUnitStream = stmtDestDb.executeQuery(queryToGetTopTracksUnitsByStream);
				while (topTrackUnitStream.next()) {
					String productTitle = topTrackUnitStream.getString("Product_Title");
					String totalUnits = topTrackUnitStream.getString("Units");
					String productUserCode= topTrackUnitStream.getString("Product_UserCode");
					mapTopTracksUnitsByStream.put(productUserCode, totalUnits);
				}
			}

			String queryToGetCurrencyCodeDestDbtop = "Select currency_code from  [mybmg4_stage_rec].[dbo].[Currency] where currency_key =("
					+ "Select siteCurrency_Key from mybmg4_stage_rec.[dbo].[Site] WHERE site_code='" + site + "')";
			ResultSet currencyListtop = stmtDestDb.executeQuery(queryToGetCurrencyCodeDestDbtop);
			while (currencyListtop.next()) {
				currencyDestinationDb = currencyListtop.getString("currency_code");
			}

			String queryToFetchProductCodeTopTracks = "select top 5 "
					+ "PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC,PD.Product_UserCode,  "
					+ "SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT " + "from  "
					+ " (select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ, "
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID "
					+ "from tbl_STG_RoycalcPayee dcrx " + "inner join "
					+ "(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ , "
					+ " RP.roycalcpayee_bfbalance,COUNT(*) as reccount " + "from ( "
					+ "select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code "
					+ "from tbl_STG_RoycalcPayee  "
					+ "	JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Contract')) CO  "
					+ "     ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND isnull(CO.CONTTYPE_CODE,'')  "
					+ "   not in('JVAT','PAGC') " + "					AND CO.CONTRACT_NAME IS NOT NULL "
					+ "					 ) rp " + " group by "
					+ "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ, "
					+ "RP.roycalcpayee_bfbalance) DUP "
					+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ, "
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP  " + "  JOIN tbl_STG_SalesWorkArc "
					+ "  SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ  "
					+ " JOIN tbl_STG_Calclog CL "
					+ " ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F' "
					+ "AND CL.CALCLOG_PERIODTYPE<>'A'      "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD  "
					+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF "
					+ "ON CF.CONFIG_CODE=SWA.CONFIG_CODE "
					+ "			INNER JOIN   (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC  "
					+ "			ON PC.CONFIG_CODE=PD.CONFIG_CODE   "
					+ " where CF.CONFIG_CODE in('OTH','DD','DAS','DVS','DVT','MT','RB','VOIC','VT','SUB','RT','STR','DST','REAL','TNDU','PR','ST','DCLO','TD')  "
					+ "			AND SWA.DISTCHAN_CODE IN ('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO')  "
					+ " and rcp.royaltor_code in(" + clientIds + ") and rcp.royaltor_site='" + site + "'"
					+ "and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) +  substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)='"+period+"'  "
					+

					" GROUP BY  " + "PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC,  "
					+ "PD.Product_UserCode,PD.PRODUCT_BARCODE  " + "ORDER BY SUM(SWA.SALESWORKARC_EARNINGS)  DESC  ";
			ResultSet topTrackProductCodeSource = stmtBmgPrime.executeQuery(queryToFetchProductCodeTopTracks);
			List<String> prodCodeList = new ArrayList<String>();
			while (topTrackProductCodeSource.next()) {
				String productCode = "'" + topTrackProductCodeSource.getString("PRODUCT_CODE") + "'";
				prodCodeList.add(productCode);
				String productTitle=topTrackProductCodeSource.getString("PRODUCT_TITLE");
				String productIsrcCode=topTrackProductCodeSource.getString("Product_UserCode");
				TopTracksProductIsrcCodeSource.add(productIsrcCode);
				mapTopTracksProductTitleSource.put(productIsrcCode, productTitle);
					}
				
			
			if (!prodCodeList.isEmpty()) {
				
				
				String pcodeSource = prodCodeList.toString().replace("[", "").replace("]", "");
				String querytopTracksUnitsDownloadSource = "select "
						+ "PD.PRODUCT_TITLE,PC.CONFIG_DESC,PD.Product_UserCode, SUM(SWA.SALESWORKARC_UNITS) UNITS  "
						+ "from  "
						+ "(select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,  "
						+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID  "
						+ "  from tbl_STG_RoycalcPayee dcrx   " + " inner join   "
						+ "(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,   "
						+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount  " + "from (   "
						+ "select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code   "
						+ "from tbl_STG_RoycalcPayee    "
						+ "JOIN(select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Contract')) CO    "
						+ " ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND "
						+ "isnull(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')   "
						+ "					AND CO.CONTRACT_NAME IS NOT NULL   " + "					 ) rp   "
						+ " group by   "
						+ "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,   "
						+ "RP.roycalcpayee_bfbalance) DUP   "
						+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ   "
						+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1)   "
						+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,   "
						+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP    " + " JOIN tbl_STG_SalesWorkArc   "
						+ " SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ    "
						+ "JOIN tbl_STG_Calclog CL   "
						+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F'   "
						+ "AND CL.CALCLOG_PERIODTYPE<>'A'        "
						+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD   "
						+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE   "
						+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF   "
						+ "ON CF.CONFIG_CODE=SWA.CONFIG_CODE   "
						+ " 			INNER JOIN  (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC   "
						+ "			ON PC.CONFIG_CODE=PD.CONFIG_CODE    "
						+ "where CF.CONFIG_CODE in('OTH','DD','DAS','DVS','DVT','MT','RB','VOIC','VT','RT','DST','REAL','TNDU','TD')   "
						+ "			AND SWA.DISTCHAN_CODE IN ('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO')   "
						+ "and rcp.royaltor_code in(" + clientIds + ")and rcp.royaltor_site='" + site + "'"
						+ "and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  + substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)='"+period+"'  "
						+ "and PD.product_code in ("+pcodeSource+") "
						+ "GROUP BY    " + "PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC,   "
						+ "PD.Product_UserCode,PD.PRODUCT_BARCODE   "
						+ "ORDER BY SUM(SWA.SALESWORKARC_UNITS)  DESC ";

				ResultSet topTrackUnitDownloadSource = stmtBmgPrime.executeQuery(querytopTracksUnitsDownloadSource);
				while (topTrackUnitDownloadSource.next()) {
					String productTitle = topTrackUnitDownloadSource.getString("PRODUCT_TITLE");
					String totalUnits = topTrackUnitDownloadSource.getString("UNITS");
					String productIsrcCode = topTrackUnitDownloadSource.getString("Product_UserCode");
					mapTopTracksUnitDownloadSource.put(productIsrcCode, totalUnits);
				

				}

				String querytopTrackRoyaltyDownloadSource = "select "
						+ "PD.PRODUCT_TITLE,PC.CONFIG_DESC,PD.Product_UserCode, SUM(SWA.SALESWORKARC_Earnings) Amount  "
						+ "from  "
						+ "(select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,  "
						+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID  "
						+ "  from tbl_STG_RoycalcPayee dcrx   " + " inner join   "
						+ "(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,   "
						+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount  " + "from (   "
						+ "select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code   "
						+ "from tbl_STG_RoycalcPayee    "
						+ "JOIN(select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Contract')) CO    "
						+ " ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND "
						+ "isnull(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')   "
						+ "					AND CO.CONTRACT_NAME IS NOT NULL   " + "					 ) rp   "
						+ " group by   "
						+ "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,   "
						+ "RP.roycalcpayee_bfbalance) DUP   "
						+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ   "
						+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1)   "
						+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,   "
						+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP    " + " JOIN tbl_STG_SalesWorkArc   "
						+ " SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ    "
						+ "JOIN tbl_STG_Calclog CL   "
						+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F'   "
						+ "AND CL.CALCLOG_PERIODTYPE<>'A'        "
						+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD   "
						+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE   "
						+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF   "
						+ "ON CF.CONFIG_CODE=SWA.CONFIG_CODE   "
						+ " 			INNER JOIN  (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC   "
						+ "			ON PC.CONFIG_CODE=PD.CONFIG_CODE    "
						+ "where CF.CONFIG_CODE in('OTH','DD','DAS','DVS','DVT','MT','RB','VOIC','VT','RT','DST','REAL','TNDU','TD')   "
						+ "			AND SWA.DISTCHAN_CODE IN ('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO')   "
						+ "and rcp.royaltor_code in(" + clientIds + ")and rcp.royaltor_site='" + site + "'"
						+ "and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  + substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)='"+period+"'  "
						+ "and PD.product_code in ("+pcodeSource+") "
						+ "GROUP BY    " + "PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC,   "
						+ "PD.Product_UserCode,PD.PRODUCT_BARCODE   "
						+ "ORDER BY SUM(SWA.SALESWORKARC_Earnings)  DESC ";

				ResultSet topTrackRoyaltyDownloadSource = stmtBmgPrime.executeQuery(querytopTrackRoyaltyDownloadSource);
				List<String> topTracksProductTitleDownload = new ArrayList<String>();
				while (topTrackRoyaltyDownloadSource.next()) {
					String productTitle = topTrackRoyaltyDownloadSource.getString("PRODUCT_TITLE");
					String totalRoyalty = topTrackRoyaltyDownloadSource.getString("Amount");
					String productIsrcCode = topTrackUnitDownloadSource.getString("Product_UserCode");
					mapTopTracksRoyaltyDownloadSource.put(productIsrcCode, totalRoyalty);
					topTracksProductTitleDownload.add(productTitle);
				}

				String querytopTracksUnitsStreamSource = "select "
						+ "PD.PRODUCT_TITLE,PC.CONFIG_DESC,PD.Product_UserCode, SUM(SWA.SALESWORKARC_UNITS) UNITS  "
						+ "from  "
						+ "(select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,  "
						+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID  "
						+ "  from tbl_STG_RoycalcPayee dcrx   " + " inner join   "
						+ "(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,   "
						+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount  " + "from (   "
						+ "select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code   "
						+ "from tbl_STG_RoycalcPayee    "
						+ "JOIN(select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Contract')) CO    "
						+ " ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND "
						+ "isnull(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')   "
						+ "					AND CO.CONTRACT_NAME IS NOT NULL   " + "					 ) rp   "
						+ " group by   "
						+ "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,   "
						+ "RP.roycalcpayee_bfbalance) DUP   "
						+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ   "
						+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1)   "
						+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,   "
						+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP    " + " JOIN tbl_STG_SalesWorkArc   "
						+ " SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ    "
						+ "JOIN tbl_STG_Calclog CL   "
						+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F'   "
						+ "AND CL.CALCLOG_PERIODTYPE<>'A'        "
						+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD   "
						+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE   "
						+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF   "
						+ "ON CF.CONFIG_CODE=SWA.CONFIG_CODE   "
						+ " 			INNER JOIN  (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC   "
						+ "			ON PC.CONFIG_CODE=PD.CONFIG_CODE    "
						+ "where CF.CONFIG_CODE in('SUB','STR','PR','ST','DCLO')   "
						+ "			AND SWA.DISTCHAN_CODE IN ('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO')   "
						+ "and rcp.royaltor_code in(" + clientIds + ")and rcp.royaltor_site='" + site + "'"
						+ "and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  + substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)='"+period+"'  "
						+ "and PD.product_code in ("+pcodeSource+") "
						+ "GROUP BY    " + "PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC,   "
						+ "PD.Product_UserCode,PD.PRODUCT_BARCODE   "
						+ "ORDER BY SUM(SWA.SALESWORKARC_UNITS)  DESC ";
				ResultSet topTrackUnitStreamSource = stmtBmgPrime.executeQuery(querytopTracksUnitsStreamSource);
				while (topTrackUnitStreamSource.next()) {
					String productTitle = topTrackUnitStreamSource.getString("PRODUCT_TITLE");
					String totalUnits = topTrackUnitStreamSource.getString("UNITS");
					String productIsrcCode = topTrackUnitDownloadSource.getString("Product_UserCode");
					mapTopTracksUnitsStreamSource.put(productIsrcCode, totalUnits);

				}

				String querytopTracksRoyaltyStreamSource =  "select "
						+ "PD.PRODUCT_TITLE,PC.CONFIG_DESC,PD.Product_UserCode, SUM(SWA.SALESWORKARC_Earnings) Amount  "
						+ "from  "
						+ "(select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,  "
						+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID  "
						+ "  from tbl_STG_RoycalcPayee dcrx   " + " inner join   "
						+ "(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,   "
						+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount  " + "from (   "
						+ "select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code   "
						+ "from tbl_STG_RoycalcPayee    "
						+ "JOIN(select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Contract')) CO    "
						+ " ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE AND "
						+ "isnull(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')   "
						+ "					AND CO.CONTRACT_NAME IS NOT NULL   " + "					 ) rp   "
						+ " group by   "
						+ "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,   "
						+ "RP.roycalcpayee_bfbalance) DUP   "
						+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ   "
						+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1)   "
						+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,   "
						+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP    " + " JOIN tbl_STG_SalesWorkArc   "
						+ " SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ    "
						+ "JOIN tbl_STG_Calclog CL   "
						+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F'   "
						+ "AND CL.CALCLOG_PERIODTYPE<>'A'        "
						+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD   "
						+ "ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE   "
						+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF   "
						+ "ON CF.CONFIG_CODE=SWA.CONFIG_CODE   "
						+ " 			INNER JOIN  (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC   "
						+ "			ON PC.CONFIG_CODE=PD.CONFIG_CODE    "
						+ "where CF.CONFIG_CODE in('SUB','STR','PR','ST','DCLO')   "
						+ "			AND SWA.DISTCHAN_CODE IN ('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO')   "
						+ "and rcp.royaltor_code in(" + clientIds + ")and rcp.royaltor_site='" + site + "'"
						+ "and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4)  + substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)='"+period+"'  "
						+ "and PD.product_code in ("+pcodeSource+") "
						+ "GROUP BY    " + "PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC,   "
						+ "PD.Product_UserCode,PD.PRODUCT_BARCODE   "
						+ "ORDER BY SUM(SWA.SALESWORKARC_Earnings)  DESC ";

			ResultSet topTrackRoyaltyStreamSource = stmtBmgPrime.executeQuery(querytopTracksRoyaltyStreamSource);
			while (topTrackRoyaltyStreamSource.next()) {
				String productTitle = topTrackRoyaltyStreamSource.getString("PRODUCT_TITLE");
				String totalRoyalty = topTrackRoyaltyStreamSource.getString("Amount");
					String productIsrcCode = topTrackUnitDownloadSource.getString("Product_UserCode");
					mapTopTracksRoyaltyStreamSource.put(productIsrcCode, totalRoyalty);
			

			}

		}
		}
	}
	
	public  void DB_Query_NewStatementDue(String site, String client, ArrayList<String> scrossClientList, String crossClientId)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		 
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=Mybmg4_stage_rec";
		newStatementDueSource=null;
		newStatementDueDest=null;
		List<String> clientIdList = new ArrayList<String>();
		clientIdList.add("'" + client + "'");
		if (crossClientId != null) {
			clientIdList.add(crossClientId);
		} else if (!scrossClientList.isEmpty()) {
			String cId = "";
			cId = scrossClientList.toString().replace("[", "").replace("]", "");
			clientIdList.add(cId);

		}
		String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
		
		Connection conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");

		Statement stmtDestDb = conDestinationDb.createStatement();
		
		Class.forName("oracle.jdbc.driver.OracleDriver");  
		  
		//step2 create  the connection object  
		Connection conRemaDb=DriverManager.getConnection(  
		"jdbc:oracle:thin:@10.6.238.172:1480:DBOPRIM1","MUDH001","rt54rl27");  
		  
		Statement stmtRemaDb=conRemaDb.createStatement(); 
		
		String queryToFetchNewStatementDueDest="select  Client_code,Client_site,client_name, payment_days,Payment_Frequency, "+
				"case when Payment_Frequency=3 and Payment_Days=30 then datediff(DAY,getdate(),'Jan 31 2019') "+
				"when Payment_Frequency=3 and Payment_Days=45 then datediff(DAY,getdate(),'Feb 15 2019')  "+
				 "when Payment_Frequency=3 and Payment_Days=60 then datediff(DAY,getdate(),'Feb 28 2019') "+
				"when Payment_Frequency=3 and Payment_Days=90 then datediff(DAY,getdate(),'Dec 31 2019') "+
				"when Payment_Frequency=6 and Payment_Days=30 then datediff(DAY,getdate(),'Jan 31 2019') "+
				"when Payment_Frequency=6 and Payment_Days=45 then datediff(DAY,getdate(),'Feb 15 2019')  "+
				 "when Payment_Frequency=6 and Payment_Days=60 then datediff(DAY,getdate(),'Feb 28 2019') "+
				"when Payment_Frequency=6 and Payment_Days=90 then datediff(DAY,getdate(),'Mar 31 2019') end as 'Next Statement Due' "+
				"from client where client_code in (" + clientIds + ") and client_site='" + site + "'";
		ResultSet newStatementDueDestDB = stmtDestDb.executeQuery(queryToFetchNewStatementDueDest);
		
		while(newStatementDueDestDB.next()){
			newStatementDueDest=newStatementDueDestDB.getString("Next Statement Due");
		}
		
		String queryToFetchNewStatementDueDestSource= "select "+
				 "ROYALTOR.ROYALTOR_SITE as ROYALTOR_SITE, "+
				 " ROYALTOR.ROYALTOR_CODE as ROYALTOR_CODE, "+
				 " case "+
				"when ROYALTOR.ROYALTOR_PAYFREQ = 'S' then '6' "+
				"when ROYALTOR.ROYALTOR_PAYFREQ IS NULL or ROYALTOR.ROYALTOR_PAYFREQ ='Q' then '3' "+
				"when ROYALTOR.ROYALTOR_PAYFREQ = 'M' then '1' "+
				"else '0' "+
				"end Payement_Frequency, "+
				 " CASE WHEN ROYALTOR.ROYALTOR_REPPERIOD=1 THEN 30 "+
				 " WHEN ROYALTOR.ROYALTOR_REPPERIOD=2 THEN 45 "+
				 " WHEN ROYALTOR.ROYALTOR_REPPERIOD=3 THEN 60 "+
				 " WHEN ROYALTOR.ROYALTOR_REPPERIOD=4 THEN 75 "+
				 " WHEN ROYALTOR.ROYALTOR_REPPERIOD=5 THEN 90 "+
				 " WHEN ROYALTOR.ROYALTOR_REPPERIOD=6 THEN 120 "+
				 " WHEN ROYALTOR.ROYALTOR_REPPERIOD=7 THEN 180 "+
				 " ELSE 0 END AS PAYMENT_DAYS,  "+
				"case when ROYALTOR_PAYFREQ IS NULL or ROYALTOR.ROYALTOR_PAYFREQ ='Q' and ROYALTOR_REPPERIOD=1 then to_date('20190131','YYYYMMDD') "+ "-to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD')  "+
				"when ROYALTOR_PAYFREQ IS NULL or ROYALTOR.ROYALTOR_PAYFREQ ='Q' and ROYALTOR_REPPERIOD=2 then to_date('20190215','YYYYMMDD') "+ "-to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD') "+
				"when ROYALTOR_PAYFREQ IS NULL or ROYALTOR.ROYALTOR_PAYFREQ ='Q' and ROYALTOR_REPPERIOD=3 then to_date('20190228','YYYYMMDD') "+ "-to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD') "+
				"when ROYALTOR_PAYFREQ IS NULL or ROYALTOR.ROYALTOR_PAYFREQ ='Q' and ROYALTOR_REPPERIOD=5 then to_date('20191231','YYYYMMDD') "+ "-to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD') "+
				"when ROYALTOR_PAYFREQ ='S' and ROYALTOR_REPPERIOD=1 then to_date('20190131','YYYYMMDD') -to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD') "+
				"when ROYALTOR_PAYFREQ ='S' and ROYALTOR_REPPERIOD=2 then to_date('20190215','YYYYMMDD') -to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD') "+
				"when ROYALTOR_PAYFREQ ='S' and ROYALTOR_REPPERIOD=3 then to_date('20190228','YYYYMMDD') -to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD') "+
				" when ROYALTOR_PAYFREQ ='S' and ROYALTOR_REPPERIOD=5 then to_date('20190331','YYYYMMDD') -to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD') "+
				"end as NextStatementDue "+
				"from "+
				 " rmaestro_user.ROYALTOR "+
				  "where royaltor_code in (" + clientIds + ") and royaltor_site='" + site + "'";
				
				
       ResultSet newStatementDueSourceDB = stmtRemaDb.executeQuery(queryToFetchNewStatementDueDestSource);
		
		while(newStatementDueSourceDB.next()){
			newStatementDueSource=newStatementDueSourceDB.getString("NextStatementDue");
		}
		
}
	
	public  void DB_Query_PipelineRoyalty_Record(String site, String client, ArrayList<String> scrossClientList, String crossClientId)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=Mybmg4_stage_rec";
		
		Connection conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");

		Statement stmtDestDb = conDestinationDb.createStatement();
		

		
		
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dbUrlRecroProd = "jdbc:sqlserver://10.6.238.158:5555;databaseName=recroy_prod";
		Connection conRecroProdDb = DriverManager.getConnection(dbUrlRecroProd, "batr007", "Si2RIbVdVkDyCIVVBmbA");

		Statement stmtRecroProdDb = conRecroProdDb.createStatement();

		try{
			pipelineRoyltyRecordSource=null;
			pipelineRoyltyRecordDest=null;
		List<String> clientIdList = new ArrayList<String>();
		clientIdList.add("'" + client + "'");
		if (crossClientId != null) {
			clientIdList.add(crossClientId);
		} else if (!scrossClientList.isEmpty()) {
			String cId = "";
			cId = scrossClientList.toString().replace("[", "").replace("]", "");
			clientIdList.add(cId);

		}
		String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
		
		
		String queryToFetchPipleLineRoyltyDest="select   SUM(rb.Earnings_Amount) AS totalRoyalty ,P.Period  "+
				"from period  p join Rec_Earnings rb "+
				"on p.period_key=rb.period_key join client c on rb.client_key=c.client_key "+
			    "where  p.period_key =(select max(period_key) from Rec_Earnings rb1 where rb1.client_key=rb.client_key ) "+
				"  and c.client_code in  (" + clientIds + ")  and client_site='" + site + "'" +
			    "group by p.period "+
              "	order by p.period desc ";
				
		ResultSet pipleLineRoyltyDestDB = stmtDestDb.executeQuery(queryToFetchPipleLineRoyltyDest);
		
		while(pipleLineRoyltyDestDB.next()){
			pipelineRoyltyRecordDest=pipleLineRoyltyDestDB.getString("totalRoyalty");
			pipelineRoyltyPeriodRecordDest=pipleLineRoyltyDestDB.getString("Period");
		}
		
			String queryToFetchPipleLineRoyltySource = "select TOP 1  PERIOD,  "
					+ "SUM(CONTOPTACT_RECOUPAMT) AS ROYALTY_TOTAL  " + "from  "
					+ "(select royaltor_code,royaltor_site,calclog_finalized,CONTOPTACT_RECOUPAMT,transtype_code,contract_code,contract_site   "
					+ "				 from recroy_prod..CONTOPTACT  " + ") rt  "
					+ "inner join [dbo].[tblRecordmaestroETL_CONTRACT_Table1]  C on   "
					+ "rt.CONTRACT_CODE=C.CONTRACT_CODE AND rt.CONTRACT_SITE=C.CONTRACT_SITE AND  isnull(C.CONTTYPE_CODE,'') not in ('JVAT','PAGC')  "
					+ "inner join (  "
					+ "select substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),4,4) + substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),1,2) as period , calclog_seq  from [dbo].[CALCLOG]) CL "
					+ "on CL.calclog_seq=rt.calclog_finalized   " + "WHERE TRANSTYPE_CODE='ROYS' AND royaltor_code IN ("
					+ clientIds + ") AND ROYALTOR_SITE='" + site + "' GROUP BY PERIOD  "
					+ "order by PERIOD desc  " ;

				
				
				
       ResultSet pipleLineRoyltySourceDb = stmtRecroProdDb.executeQuery(queryToFetchPipleLineRoyltySource);
		
		while(pipleLineRoyltySourceDb.next()){
			pipelineRoyltyRecordSource=pipleLineRoyltySourceDb.getString("ROYALTY_TOTAL");
			pipelineRoyltyPeriodRecordSource=pipleLineRoyltySourceDb.getString("PERIOD");
		}
		 }catch(Exception e){
			 e.printStackTrace();
		 }finally{
			 stmtDestDb.close();
			 conDestinationDb.close();
			 stmtRecroProdDb.close();
			 conRecroProdDb.close();
		 }
		
}
	
}
