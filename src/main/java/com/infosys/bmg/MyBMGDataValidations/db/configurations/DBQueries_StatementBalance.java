package com.infosys.bmg.MyBMGDataValidations.db.configurations;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.infosys.bmg.MyBMGDataValidations.utility.Log;

public class DBQueries_StatementBalance {

	public  String periodDestinationDb = null;
	public  double closingBlanaceDestinationDb = 0;
	public  String currencyDestinationDb = null;
	public  String periodSourceDB = null;
	public  double closingBalanaceSoruceDb = 0;
	public  String currencySourceDb = null;
	public  String periodSourceDb = null;

	
	


	public  void DB_Query(String site, String client, ArrayList<String> scrossClientList, String crossClientId,
			String datapoint, String period)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=mybmg4_stage_rec";
		

		Connection conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");

		Statement stmtDestDb = conDestinationDb.createStatement();

		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dbUrlRecroProd = "jdbc:sqlserver://10.6.238.158:5555;databaseName=recroy_prod";
		Connection connRecroProd = DriverManager.getConnection(dbUrlRecroProd, "batr007", "Si2RIbVdVkDyCIVVBmbA");

		Statement stmtRecroProd = connRecroProd.createStatement();

		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dbUrlBmgPrimeLandingLaye = "jdbc:sqlserver://10.6.238.202:1433;databaseName=BMGPrime_LandingLayer";
		Connection connBmgPrime = DriverManager.getConnection(dbUrlBmgPrimeLandingLaye, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");
		Statement stmtBmgPrime = connBmgPrime.createStatement();

		try {
		List<String> clientIdList = new ArrayList<String>();
		
		clientIdList.add("'" + client + "'");
		if (crossClientId != null) {
			clientIdList.add(crossClientId);
		} else if (!scrossClientList.isEmpty()) {
			String cId = "";
			cId = scrossClientList.toString().replace("[", "").replace("]", "");
			clientIdList.add(cId);

		}
		String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
		
			String queryToGetStatementBalance = "select client_code,period ,closing_balance"
					+ " from [mybmg4_stage_rec].[dbo].[Period]  p join [mybmg4_stage_rec].[dbo].Rec_Balances rb"
					+ " on p.period_key=rb.period_key join [mybmg4_stage_rec].[dbo].client c on rb.client_key=c.client_key"
					+ " where p.period_key =(select max(period_key) from [mybmg4_stage_rec].[dbo].Rec_Balances rb1 where rb1.client_key=rb.client_key)"
					+ "and c.client_code in (" + clientIds + ") and client_site='" + site + "'";

			ResultSet stmtBalList = stmtDestDb.executeQuery(queryToGetStatementBalance);

			double sbalDestination = 0;

			while (stmtBalList.next()) {
				String clientCode = stmtBalList.getString("client_code");

				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");

					if (clientCode.trim().equalsIgnoreCase(clientId)) {

						sbalDestination = sbalDestination
								+ Double.parseDouble(stmtBalList.getString("closing_balance"));
					}
				}
				periodDestinationDb = stmtBalList.getString("period");

			}
			closingBlanaceDestinationDb = sbalDestination;

			String queryToGetClosingBalPrime = "SELECT C.RCONTRACT_CODE,C.RCONTRACT_SITE, C.ROYALTOR_CODE,C.ROYALTOR_SITE,C.CALCLOG_PERIOD AS PERIOD,"
					+ "SUM(roycalcpayee_bfbalance) AS OpeningBalance FROM"
					+ "(select distinct dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,"
					+ "dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ, dcrx.roycalcpayee_bfbalance,dcrx.payee_code"
					+ ",CALCLOG_PERIOD from [BMGPrime_LandingLayer].[dbo].[tbl_STG_RoycalcPayee] " + "dcrx inner JOIN "
					+ "(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE, "
					+ "MAX(RP.CALCLOG_SEQ) AS CALCLOGSEQ, RP.roycalcpayee_bfbalance, "
					+ "COUNT(*) as reccount, MAX(substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) + "
					+ "substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)) AS CALCLOG_PERIOD from "
					+ "(select distinct  ROYALTOR_CODE,ROYALTOR_SITE, "
					+ "RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code "
					+ "from tbl_STG_RoycalcPayee where RCONTRACT_CODE IN (Select distinct [RCONTRACT_CODE] FROM [BMGPrime_LandingLayer].[dbo].[tbl_STG_RoycalcPayee] where royaltor_code in ("
					+ clientIds + ") and royaltor_Site='" + site + "') " + "AND RCONTRACT_SITE IN('" + site
					+ "'))rp inner "
					+ "join tbl_STG_Calclog CL on RP.CALCLOG_SEQ= CL.CALCLOG_SEQ AND CL.CALCLOG_RUNTYPE='F' "
					+ "AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_PERIODTYPE<>'A' group by  RP.ROYALTOR_CODE, "
					+ "RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ, RP.roycalcpayee_bfbalance)DUP "
					+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOGSEQ  "
					+ "= dcrx.CALCLOG_SEQ where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1))C "
					+ "WHERE C.ROYALTOR_CODE in (" + clientIds + ") AND C.ROYALTOR_SITE='" + site
					+ "' GROUP BY C.ROYALTOR_CODE,C.ROYALTOR_SITE, "
					+ "C.CALCLOG_PERIOD,PAYEE_CODE,RCONTRACT_SITE,RCONTRACT_CODE ORDER BY C.CALCLOG_PERIOD DESC ";

			ResultSet stmtBalListPrime = stmtBmgPrime.executeQuery(queryToGetClosingBalPrime);
			double primeStmtBal = 0;
			while (stmtBalListPrime.next()) {
				String clientCode = "'" + stmtBalListPrime.getString("ROYALTOR_CODE").trim() + "'";
				for (String id1 : clientIdList) {

					if (clientCode.equalsIgnoreCase(id1)
							&& stmtBalListPrime.getString("PERIOD").equalsIgnoreCase(periodDestinationDb)) {
						primeStmtBal = primeStmtBal + Double.parseDouble(stmtBalListPrime.getString("OpeningBalance"));
						periodSourceDb = stmtBalListPrime.getString("PERIOD");
					}
				}

			}

			String queryToGetTransaction = "select royaltor_code,royaltor_site,PERIOD, "
					+ "SUM(CONTOPTACT_RECOUPAMT) AS TRANSACTIONS_TOTAL from "
					+ "(select royaltor_code,royaltor_site,calclog_finalized,CONTOPTACT_RECOUPAMT,transtype_code,contract_code,contract_site "
					+ "from recroy_prod..CONTOPTACT) rt "
					+ "inner join [dbo].[tblRecordmaestroETL_CONTRACT_Table1] C on  "
					+ "rt.CONTRACT_CODE=C.CONTRACT_CODE AND rt.CONTRACT_SITE=C.CONTRACT_SITE AND  isnull(C.CONTTYPE_CODE,'') not in ('JVAT','PAGC') "
					+ "inner join ( "
					+ "select substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),4,4) + substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),1,2) as period , calclog_seq  from [dbo].[CALCLOG]) CL "
					+ "on CL.calclog_seq=rt.calclog_finalized " + "WHERE TRANSTYPE_CODE<>'ROYS' AND royaltor_code IN ("
					+ clientIds + ") AND ROYALTOR_SITE='" + site + "' " + "GROUP BY royaltor_code,royaltor_site,PERIOD "
					+ "order by PERIOD desc ";

			ResultSet trasactionList = stmtRecroProd.executeQuery(queryToGetTransaction);
			double trasactAmt = 0;
			while (trasactionList.next()) {
				String rCode = "'" + trasactionList.getString("royaltor_code").trim() + "'";
				for (String id : clientIdList) {

					if (rCode.equalsIgnoreCase(id)
							&& trasactionList.getString("PERIOD").equalsIgnoreCase(periodDestinationDb)) {
						trasactAmt = trasactAmt + Double.parseDouble(trasactionList.getString("TRANSACTIONS_TOTAL"));
					}
				}
			}

			String queryToGetRoyality = "select royaltor_code,royaltor_site,PERIOD, "
					+ "SUM(CONTOPTACT_RECOUPAMT) AS ROYALTY_TOTAL from "
					+ "(select royaltor_code,royaltor_site,calclog_finalized,CONTOPTACT_RECOUPAMT,transtype_code,contract_code,contract_site "
					+ "from recroy_prod..CONTOPTACT) rt "
					+ "inner join [dbo].[tblRecordmaestroETL_CONTRACT_Table1] C on "
					+ "rt.CONTRACT_CODE=C.CONTRACT_CODE AND rt.CONTRACT_SITE=C.CONTRACT_SITE AND  isnull(C.CONTTYPE_CODE,'') not in ('JVAT','PAGC') "
					+ "inner join ("
					+ "select substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),4,4) + substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),1,2) as period , calclog_seq  from [dbo].[CALCLOG]) CL "
					+ "on CL.calclog_seq=rt.calclog_finalized " + "WHERE TRANSTYPE_CODE='ROYS' AND royaltor_code IN ("
					+ clientIds + ") AND ROYALTOR_SITE='" + site + "' " + "GROUP BY royaltor_code,royaltor_site,PERIOD "
					+ "order by PERIOD desc ";

			// Statement stmtRecroProd = conn3.createStatement();
			ResultSet royalityList = stmtRecroProd.executeQuery(queryToGetRoyality);
			double royalityAmt = 0;
			while (royalityList.next()) {
				String rCode = "'" + royalityList.getString("royaltor_code").trim() + "'";
				for (String id : clientIdList) {

					if (rCode.equalsIgnoreCase(id)
							&& royalityList.getString("PERIOD").equalsIgnoreCase(periodDestinationDb)) {
						royalityAmt = royalityAmt + Double.parseDouble(royalityList.getString("ROYALTY_TOTAL"));
					}
				}
			}
			closingBalanaceSoruceDb = royalityAmt + trasactAmt + primeStmtBal;

			String queryToGetCurrencyCodeDestDb = "Select currency_code from  [mybmg4_stage_rec].[dbo].[Currency] where currency_key =("
					+ "Select siteCurrency_Key from mybmg4_stage_rec.[dbo].[Site] WHERE site_code='" + site + "')";
			ResultSet currencyList = stmtDestDb.executeQuery(queryToGetCurrencyCodeDestDb);
			while (currencyList.next()) {
				currencyDestinationDb = currencyList.getString("currency_code");

			}
		}catch(Exception e) {
			e.printStackTrace();
			Log.error("Error in DBQueries_StatementBalance: "+e);
		}finally {
			stmtDestDb.close();
			conDestinationDb.close();
			stmtBmgPrime.close();
			connBmgPrime.close();
			stmtRecroProd.close();
			connRecroProd.close();
		}
		}
		
	
	
	
	
	
	
}
