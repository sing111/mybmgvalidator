package com.infosys.bmg.MyBMGDataValidations.db.configurations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBQueriesForTotalAlbumSingle {
	
	public  Integer totalNumOfAlbumSingleDestDb;
	public  Integer totalNumOfAlbumSingleSourceDb;


	public  void DB_Query_TopAlbumSingle(String client, ArrayList<String> scrossClientList, String crossClientId,
			String site, String period,List<String> allPeriods) {
		try{
		totalNumOfAlbumSingleDestDb=0;
		String month = null,year=null;
		String periodforRema=null;
		String periodforPrime=null;
		StringBuilder sb=new StringBuilder();
		StringBuilder sb1=new StringBuilder();
		String prefix="",prefix1="";
		if(period!=null){
			String s[]=period.split("/");
				month=s[0];
				year=s[1];
				sb.append(year+""+month);
				period=sb.toString();
				System.out.println(period+":period");
		}else{
			//default all periods
			boolean flag=false;
			for(String periods:allPeriods){
				String pe[]=periods.split("/");
				month=pe[0];
				year=pe[1];
				sb.append(prefix);
				prefix = ",";
				sb.append("'"+year+""+month+"'");
				period=sb.toString();
				System.out.println(period);
				int yr=Integer.parseInt(year);
				if(yr<=2016){
					sb1.append(prefix1);
					prefix1 = ",";
					sb1.append("'"+year+""+month+"'");
					periodforRema=sb.toString();
				}else if(yr>2016){
					sb1.append(prefix1);
					prefix1 = ",";
					sb1.append("'"+year+""+month+"'");
					periodforPrime=sb1.toString();
				}
				
			}
			
		}
		
		List<String> clientIdList = new ArrayList<String>();
		clientIdList.add("'" + client + "'");
		if (crossClientId != null) {
			clientIdList.add(crossClientId);
		} else if (!scrossClientList.isEmpty()) {
			String cId = "";
			cId = scrossClientList.toString().replace("[", "").replace("]", "");
			clientIdList.add(cId);

		}
		String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
		String queryforTotalNumOfAlbumSingle ="select count(*) as totalAlbumSingleCount from ("
				+ "SELECT  P.Product_Key, P.Product_Title,PC.Config_Desc,P.Product_UserCode,P.Product_Barcode,P.Product_Artist, SUM(RE.Earnings_Amount) AS totalRoyalty "
				+ "FROM (SELECT DISTINCT CLIENT_KEY,PRODUCT_KEY FROM ClientContProd ) CCP "
				+ "INNER JOIN Client CL ON CL.Client_Key=CCP.Client_Key "
				+ "INNER JOIN Products P ON CCP.Product_Key = P.Product_Key "
				+ "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key   "
				+ "LEFT OUTER JOIN Rec_Earnings RE ON RE.Client_Key=CL.Client_Key AND P.Product_Key=RE.Product_Key "
				+ "LEFT OUTER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "
				+ "LEFT OUTER JOIN Config EC ON RE.Config_Key = EC.Config_Key "
				+ "LEFT OUTER JOIN Period PR ON RE.Period_Key = PR.Period_Key  "
				+ "WHERE "
				+ "CL.Client_Code IN ("+clientIds+") AND CL.Client_Site='"+site+"' "
				+ "AND ((DC.DistChannel_Category = 'Sales'  AND EC.Config_ProductType IN ('Album', 'Single') "
				+ "AND PR.Period IN ('"+period+"')) OR (PC.Config_ProductType IN ('Album', 'Single') AND DC.DistChannel_Category is null "
				+ " AND PR.Period is null and  RE.Earnings_Amount is not null)) "
				+ "GROUP BY P.Product_Key, P.Product_Title, PC.Config_Desc, P.Product_UserCode, P.Product_Barcode,P.Product_Artist)a";
			
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=Mybmg4_stage_rec";
			Connection conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007","Si2RIbVdVkDyCIVVBmbA");

			Statement stmtDestDb = conDestinationDb.createStatement();
			ResultSet totalAlbumSingleCount = stmtDestDb.executeQuery(queryforTotalNumOfAlbumSingle);
			String totalAlbumCount=null;
			while(totalAlbumSingleCount.next()){
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					 totalAlbumCount = totalAlbumSingleCount.getString("totalAlbumSingleCount");
			}
			}
			totalNumOfAlbumSingleDestDb=Integer.parseInt(totalAlbumCount);
			System.out.println("TotalNo of Album single from DestDb for period:"+period+" is : "+totalNumOfAlbumSingleDestDb );

			int yr=Integer.parseInt(year);
		
			/*if(allPeriods!=null){
				for(String prd:periodforRema.split(",")){
					dbQueryForRema(prd,periodforRema,site,clientIds,clientIdList);
				}
				for(String prd:periodforPrime.split(",")){
					dbQueryForRema(prd,periodforRema,site,clientIds,clientIdList);
				}
				
				
			}else{*/
				if(yr<=2016){
					dbQueryForRema(period,periodforRema,site,clientIds,clientIdList);
				}else{
					dbQueryForPrime(period,periodforPrime,site,clientIds,clientIdList);	
				}
			//}
		
			
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	private  void dbQueryForPrime(String period, String periodforPrime, String site, String clientIds,
			List<String> clientIdList) {
	try{
			totalNumOfAlbumSingleSourceDb=0;
			String queryForTotalNumOfAlbumSinglePrime="select count(*) as totalAlbumSingleCount from (select PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC,"
					+ "PD.Product_UserCode,PD.PRODUCT_BARCODE,PD.Product_Artist, SUM(AB.SALESWORKARC_EARNINGS) AS totalRoyalty from"
					+ " (select * from OpenQuery(REMA1,'SELECT DISTINCT ROYALTOR_CODE,ROYALTOR_SITE,PRODUCT_CODE,PRODUCT_SITE FROM RMAESTRO_USER.contoptroy'))  ccp "
					+ "inner join (select * from OpenQuery(REMA1,'select ROYALTOR_CODE,ROYALTOR_SITE from RMAESTRO_USER.royaltor')) roy on roy.ROYALTOR_CODE=ccp.ROYALTOR_CODE and roy.ROYALTOR_SITE=ccp.ROYALTOR_SITE "
					+ "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD ON PD.PRODUCT_CODE=ccp.PRODUCT_CODE AND "
					+ "PD.PRODUCT_SITE=ccp.PRODUCT_SITE "
					+ "INNER JOIN   (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC ON PC.CONFIG_CODE=PD.CONFIG_CODE "
					+ "left outer join (SELECT SWA.DISTCHAN_CODE,CF.CONFIG_CODE,rcp.royaltor_code,rcp.royaltor_SITE,SWA.PRODUCT_CODE,SWA.PRODUCT_SITE,"
					+ "SWA.SALESWORKARC_EARNINGS,substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) +  substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2) AS PERIOD "
					+ "FROM((select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,dcrx.roycalcpayee_bfbalance,"
					+ "dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID from [BMGPrime_LandingLayer].[dbo].[tbl_STG_RoycalcPayee]  dcrx "
					+ "inner join (select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ ,"
					+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount from (select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,"
					+ "CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code from [BMGPrime_LandingLayer].[dbo].[tbl_STG_RoycalcPayee]  "
					+ "JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Contract where  CONTRACT_NAME IS NOT NULL AND NVL(CONTTYPE_CODE,'''') not in (''JVAT'',''PAGC'')')) CO "
					+ " ON CO.CONTRACT_CODE=[BMGPrime_LandingLayer].[dbo].[tbl_STG_RoycalcPayee].RCONTRACT_CODE "
					+ "AND CO.CONTRACT_SITE=[BMGPrime_LandingLayer].[dbo].[tbl_STG_RoycalcPayee].RCONTRACT_SITE) rp"
					+ " group by RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ,RP.roycalcpayee_bfbalance) DUP "
					+ "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
					+ "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,"
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP "
					+ "JOIN [BMGPrime_LandingLayer].[dbo].[tbl_STG_SalesWorkArc] SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ  "
					+ "JOIN [BMGPrime_LandingLayer].[dbo].[tbl_STG_Calclog] CL "
					+ "ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F' "
					+ " AND CL.CALCLOG_PERIODTYPE<>'A' "
					+ "INNER JOIN  (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF ON CF.CONFIG_CODE=SWA.CONFIG_CODE)  "
					+ "WHERE substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) +  substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)='"+period+"') AB "
					+ "ON AB.ROYALTOR_CODE=roy.ROYALTOR_CODE AND AB.ROYALTOR_SITE=ROY.ROYALTOR_SITE AND AB.PRODUCT_CODE=PD.PRODUCT_CODE AND AB.PRODUCT_SITE=PD.PRODUCT_SITE"
					+ " where ROY.royaltor_code in("+clientIds+") and ROY.royaltor_site='"+site+"'AND (("
					+ "AB.CONFIG_CODE in('ECDE','MCLP','LP33','CD','MC','LP','10IL','10IS','12I','3ICD','7I','7IEP','8TR','BOD','CDEP','CDRM','CDS','CROM','DAB','DAEP',"
					+ "'DAL','DCD','DCMB','DCMC','DDL','DMC','DMIX','DROM','DVAS','DVD','DVDP','DVDS','DVG','DVL','DVVS','ECD','ECDS','EMAX','EMD','LD','MAXI','MCEP',"
					+ "'MCS','MD','MEM','SACD','USB','VCD','VHS','VHSS','DABV','DS','DA','VS','CDE','ANDU','APU','12HB','12PD','12SB','12ST','7HB','7PD','7SB','7ST',"
					+ "'CDS5','CDSP','MCSP','SCSP','SGLM','VIGF','VISP','CDVD','DLP','10EP','12EP','12CD','BOX','12SP','CDDV','BRAY','DGAB','VISS','VOD') "
					+ "AND AB.DISTCHAN_CODE IN ('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO')     and AB.PERIOD='"+period+"') "
					+ "OR (PC.CONFIG_CODE in('ECDE','MCLP','LP33','CD','MC','LP','10IL','10IS','12I','3ICD','7I','7IEP','8TR','BOD','CDEP','CDRM','CDS','CROM','DAB','DAEP','DAL','DCD','DCMB','DCMC','DDL','DMC','DMIX','DROM','DVAS','DVD','DVDP','DVDS','DVG','DVL','DVVS','ECD','ECDS','EMAX','EMD','LD','MAXI','MCEP','MCS','MD','MEM','SACD','USB','VCD','VHS','VHSS','DABV','DS','DA','VS','CDE','ANDU','APU','12HB','12PD','12SB','12ST','7HB','7PD','7SB','7ST','CDS5','CDSP','MCSP','SCSP','SGLM','VIGF','VISP','CDVD','DLP','10EP','12EP','12CD','BOX','12SP','CDDV','BRAY','DGAB','VISS','VOD') "
					+ "AND AB.DISTCHAN_CODE IS NULL AND AB.PERIOD IS NULL and AB.SALESWORKARC_EARNINGS is not null)) "
					+ "GROUP BY  PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC, "
					+ "PD.Product_UserCode,PD.PRODUCT_BARCODE,PD.Product_Artist) gg";
			
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dbUrlBmgPrimeLandingLayer = "jdbc:sqlserver://10.6.238.202:1433;databaseName=BMGPrime_LandingLayer";
			Connection connBmgPrime = DriverManager.getConnection(dbUrlBmgPrimeLandingLayer, "batr007","Si2RIbVdVkDyCIVVBmbA");
			Statement stmtRematDb=connBmgPrime.createStatement();  
			ResultSet totalAlbumSingleCount = stmtRematDb.executeQuery(queryForTotalNumOfAlbumSinglePrime);	
			String totalAlbumCount = null;
			while(totalAlbumSingleCount.next()){
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					 totalAlbumCount = totalAlbumSingleCount.getString("totalAlbumSingleCount");
			}
			}
			totalNumOfAlbumSingleSourceDb=Integer.parseInt(totalAlbumCount);
			System.out.println("TotalNo of Album single from SourceDb for period:"+period+" is : "+totalNumOfAlbumSingleSourceDb );
			}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		
	}


	private  void dbQueryForRema(String period, String periodforRema, String site, String clientIds,
			List<String> clientIdList) {
		try{
			
			totalNumOfAlbumSingleSourceDb=0;
			String queryForTotalNumOfAlbumSingleRema="Select count(*) AS totalAlbumSingleCount from (select PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC,PD.Product_UserCode,"
					+ "PD.PRODUCT_BARCODE,PD.Product_Artist,SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT from  "
					+ "(select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ,dcrx.roycalcpayee_bfbalance,"
					+ "dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID from RMAESTRO_USER.RoycalcPayee dcrx "
					+ "inner join (select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ , "
					+ "RP.roycalcpayee_bfbalance,COUNT(*) as reccount  "
					+ "from ( select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code "
					+ "from RMAESTRO_USER.RoycalcPayee "
					+ "JOIN RMAESTRO_USER.CONTRACT CO  ON CO.CONTRACT_CODE=RMAESTRO_USER.RoycalcPayee.RCONTRACT_CODE AND "
					+ "CO.CONTRACT_SITE=RMAESTRO_USER.RoycalcPayee.RCONTRACT_SITE AND NVL(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')) rp"
					+ " group by RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ, "
					+ "RP.roycalcpayee_bfbalance) DUP on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "
					+ "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "
					+ " group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ, "
					+ "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP "
					+ "JOIN RMAESTRO_USER.SalesWorkArc"
					+ " SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ "
					+ " JOIN RMAESTRO_USER.Calclog CL ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and "
					+ "SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F' AND CL.CALCLOG_PERIODTYPE<>'A' "
					+ " and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) || SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)>='201401' "
					+ " and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) || SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)<='201703' "
					+ " INNER JOIN RMAESTRO_USER.productS PD ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "
					+ "INNER JOIN RMAESTRO_USER.CONFIG CF ON CF.CONFIG_CODE=SWA.CONFIG_CODE "
					+ "INNER JOIN   RMAESTRO_USER.CONFIG PC ON PC.CONFIG_CODE=PD.CONFIG_CODE "
					+ "where SWA.CONFIG_CODE in('ECDE','MCLP','LP33','CD','MC','LP','10IL','10IS','12I','3ICD','7I','7IEP','8TR','BOD','CDEP','CDRM','CDS',"
					+ "'CROM','DAB','DAEP','DAL','DCD','DCMB','DCMC','DDL','DMC','DMIX','DROM','DVAS','DVD','DVDP','DVDS','DVG','DVL','DVVS','ECD','ECDS','EMAX',"
					+ "'EMD','LD','MAXI','MCEP','MCS','MD','MEM','SACD','USB','VCD','VHS','VHSS','DABV','DS','DA','VS','CDE','ANDU','APU','12HB','12PD','12SB','12ST',"
					+ "'7HB','7PD','7SB','7ST','CDS5','CDSP','MCSP','SCSP','SGLM','VIGF','VISP','CDVD','DLP','10EP','12EP','12CD','BOX','12SP','CDDV','BRAY','DGAB','VISS',"
					+ "'VOD') AND SWA.DISTCHAN_CODE IN ('TOUR','NORM','CLUB','WEB','RING','TVAD','MAIL','ITUN','MISC','DIG','PREM','INCO') "
					+ "and rcp.royaltor_code in("+clientIds+") and rcp.royaltor_site='"+site+"'"
					+ " and SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) || SUBSTR(NVL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)='"+period+"' "
					+ "GROUP BY PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE,PC.CONFIG_DESC,PD.Product_UserCode,PD.PRODUCT_BARCODE,PD.Product_Artist "
					+ "ORDER BY SUM(SWA.SALESWORKARC_EARNINGS)  DESC)a";
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con=DriverManager.getConnection("jdbc:oracle:thin:@10.6.238.172:1480:DBOPRIM1","RMAESTRO_USER","counterp");  
			Statement stmtRematDb=con.createStatement();  
			ResultSet totalAlbumSingleCount = stmtRematDb.executeQuery(queryForTotalNumOfAlbumSingleRema);	
			String totalAlbumCount = null;
			while(totalAlbumSingleCount.next()){
				for (String clientId : clientIdList) {
					clientId = clientId.toString().replace("'", "").replace("'", "");
					 totalAlbumCount = totalAlbumSingleCount.getString("totalAlbumSingleCount");
			}
			}
			totalNumOfAlbumSingleSourceDb=Integer.parseInt(totalAlbumCount);
			System.out.println("TotalNo of Album single from SourceDb for period:"+period+" is : "+totalNumOfAlbumSingleSourceDb);
			}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
	}

}
