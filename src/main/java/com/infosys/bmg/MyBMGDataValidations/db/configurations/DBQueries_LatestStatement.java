package com.infosys.bmg.MyBMGDataValidations.db.configurations;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.infosys.bmg.MyBMGDataValidations.utility.Log;

public class DBQueries_LatestStatement {
	

	public  Map<String,String>  mapLatestStatementRoyaltyDest= new HashMap<String,String>();
	public  Map<String,String>  mapLatestStatementRoyaltorNameDest= new HashMap<String,String>();
	public  Map<String,String>  mapLatestStatementPeriodDest= new HashMap<String,String>();
	public  List<String> latestStatmntRoyaltorCodeDest = new ArrayList<String>();
	public  List<String> latestStatmntRoyaltorCodeSource = new ArrayList<String>();
	public  Map<String,String>  mapLatestStatementPeriodSource= new HashMap<String,String>();
	public  Map<String,String>  mapLatestStatementRoyaltySource= new HashMap<String,String>();
	public  Map<String,String>  mapLatestStatementRoyaltorNameSource= new HashMap<String,String>();


	
	


	public  void DB_Query(String site, String client, ArrayList<String> scrossClientList, String crossClientId,
			 String period)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=mybmg4_stage_rec";
		

		Connection conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");

		Statement stmtDestDb = conDestinationDb.createStatement();

		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dbUrlRecroProd = "jdbc:sqlserver://10.6.238.158:5555;databaseName=recroy_prod";
		Connection connRecroProd = DriverManager.getConnection(dbUrlRecroProd, "batr007", "Si2RIbVdVkDyCIVVBmbA");

		Statement stmtRecroProd = connRecroProd.createStatement();

		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dbUrlBmgPrimeLandingLaye = "jdbc:sqlserver://10.6.238.202:1433;databaseName=BMGPrime_LandingLayer";
		
		try {
			
			   mapLatestStatementRoyaltyDest= new HashMap<String,String>();
			   mapLatestStatementRoyaltorNameDest= new HashMap<String,String>();
			 mapLatestStatementPeriodDest= new HashMap<String,String>();
			 latestStatmntRoyaltorCodeDest = new ArrayList<String>();
				latestStatmntRoyaltorCodeSource = new ArrayList<String>();
			mapLatestStatementPeriodSource= new HashMap<String,String>();
			mapLatestStatementRoyaltySource= new HashMap<String,String>();
			 mapLatestStatementRoyaltorNameSource= new HashMap<String,String>();

		List<String> clientIdList = new ArrayList<String>();
		
		clientIdList.add("'" + client + "'");
		if (crossClientId != null) {
			clientIdList.add(crossClientId);
		} else if (!scrossClientList.isEmpty()) {
			String cId = "";
			cId = scrossClientList.toString().replace("[", "").replace("]", "");
			clientIdList.add(cId);

		}
		String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
		
		
		String month = null;
		String year = null;
        if(period!=null) {
		StringBuilder sb = new StringBuilder();
		System.out.println(period);
		period = period.toString().replace("(", "").replace(")", "");
		String s[] = period.split("/");
		month = s[0];
		year = s[1];
		sb.append(year + "" + month);
		period = sb.toString();
		System.out.println(period + ":period");
			String queryToGetLatestStatementDest = "select c.client_code,c.Client_Name,p.period, SUM(rb.Royalty_total) as royalty "+
					"	from period  p join Rec_Balances rb "+
					"	on p.period_key=rb.period_key join client c on rb.client_key=c.client_key "+
					"where p.period_key =(select max(period_key) from Rec_Balances rb1 where rb1.client_key=rb.client_key) "+
					"	and c.client_code in (" + clientIds + ") and client_site='" + site + "' and  p.period='"+period+"'" +

					"	group by c.client_code,c.Client_Name,p.period ";

			ResultSet latstStateDestList = stmtDestDb.executeQuery(queryToGetLatestStatementDest);

			double totalRoyaltyDestination = 0;
         
			while (latstStateDestList.next()) {
				String clientCode = latstStateDestList.getString("client_code");
				String clientName=latstStateDestList.getString("Client_Name");
                String royalty =latstStateDestList.getString("royalty");
                String Period=latstStateDestList.getString("period");
                totalRoyaltyDestination=totalRoyaltyDestination+Double.parseDouble(latstStateDestList.getString("royalty"));
                   
					latestStatmntRoyaltorCodeDest.add(clientCode);
					mapLatestStatementRoyaltyDest.put(clientCode,royalty);
					mapLatestStatementRoyaltorNameDest.put(clientCode,clientName);
					mapLatestStatementPeriodDest.put(clientCode, Period);
				}
			for (Map.Entry<String, String> map : mapLatestStatementRoyaltyDest.entrySet()) {
				if (map.getKey().trim().equalsIgnoreCase("'" + client + "'")) {
					mapLatestStatementRoyaltyDest.replace(map.getKey(), Double.toString(totalRoyaltyDestination));
					break;
				}else {
					mapLatestStatementRoyaltyDest.put("'" + client + "'", Double.toString(totalRoyaltyDestination));
				}
			}
				
		
		
		

			String queryToGetLatestStatementSource =    "select rt.royaltor_code,rt.royaltor_site,PERIOD,roy.rcontract_name, "+
					"				 SUM(CONTOPTACT_RECOUPAMT) AS ROYALTY_TOTAL from "+
					"				 (select royaltor_code,royaltor_site,calclog_finalized,CONTOPTACT_RECOUPAMT,transtype_code,contract_code,contract_site "+
					"				 from recroy_prod..CONTOPTACT) rt  join    (select * from OpenQuery([GTLNMISQL0034''\''DBMPBMG13],'SELECT * FROM BMGPrime_LandingLayer.[dbo].[tbl_STG_RoycalcPayee]')) roy on rt.royaltor_code=roy.royaltor_code "+
					"				 inner join [dbo].[tblRecordmaestroETL_CONTRACT_Table1] C on "+
					"				 rt.CONTRACT_CODE=C.CONTRACT_CODE AND rt.CONTRACT_SITE=C.CONTRACT_SITE AND  isnull(C.CONTTYPE_CODE,'') not in ('JVAT','PAGC') "+
					"				 inner join ( "+
					"				 select  substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),4,4) + substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),1,2) as period , calclog_seq  from [dbo].[CALCLOG]) CL  "+

					"				 on CL.calclog_seq=rt.calclog_finalized WHERE TRANSTYPE_CODE='ROYS' AND rt.royaltor_code IN (" + clientIds + ") AND rt.ROYALTOR_SITE='"+site+"' and Period='"+period+"' GROUP BY rt.royaltor_code,rt.royaltor_site,PERIOD ,roy.rcontract_name "+
					"				 order by PERIOD desc " ;

					
					
					

					
					
					
			

			ResultSet latstStateSourceList = stmtRecroProd.executeQuery(queryToGetLatestStatementSource);
			double totalRoyaltySource = 0;
			while (latstStateSourceList.next()) {
				String clientCode = latstStateSourceList.getString("royaltor_code");
				String clientName=latstStateSourceList.getString("rcontract_name");
                String royalty =latstStateSourceList.getString("ROYALTY_TOTAL");
                String Period=latstStateSourceList.getString("PERIOD");
                totalRoyaltySource=totalRoyaltySource+Double.parseDouble(royalty);
                   
					latestStatmntRoyaltorCodeSource.add(clientCode);
					mapLatestStatementRoyaltySource.put(clientCode,royalty);
					mapLatestStatementRoyaltorNameSource.put(clientCode,clientName);
					mapLatestStatementPeriodSource.put(clientCode, Period);
				}
			for (Map.Entry<String, String> map : mapLatestStatementRoyaltySource.entrySet()) {
				if (map.getKey().trim().equalsIgnoreCase("'" + client + "'")) {
					mapLatestStatementRoyaltySource.replace(map.getKey(), Double.toString(totalRoyaltyDestination));
					break;
				}else {
					mapLatestStatementRoyaltyDest.put("'" + client + "'", Double.toString(totalRoyaltyDestination));
				}
			}
				
        }
			
		}catch(Exception e) {
			e.printStackTrace();
			Log.error("Error in DBQueries_StatementBalance: "+e);
		}finally {
			stmtDestDb.close();
			conDestinationDb.close();
		
			stmtRecroProd.close();
			connRecroProd.close();
		}
		}
		
	
	
	
	
	
	
}