package com.infosys.bmg.MyBMGDataValidations.db.configurations;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.infosys.bmg.MyBMGDataValidations.utility.Log;



public class DBQueries_Top_Mrchndise_Products {
	public  Map<String, String> mapTopMrchndiseRoyaltyDest = new HashMap<>();
	public  Map<String, String> mapTopMrchndiseConfigDest = new HashMap<>();
	public  Map<String, String> mapTopMrchndiseProductTitleDest = new HashMap<>();
	public  Map<String, String> mapTopMrchndisePeriodDest = new HashMap<>();
	public  List<String> topMrchndiseProductCodeDest = new ArrayList<String>();
	public  Map<String, String> mapTopMrchndiseRoyaltySource = new HashMap<>();
	public  Map<String, String> mapTopMrchndiseProductTitleSource = new HashMap<>();
	public  Map<String, String> mapTopMrchndiseConfigSource = new HashMap<>();
	public  Map<String, String> mapTopMrchndisePeriodSource = new HashMap<>();
	public  List<String> topMrchndiseProductCodeSource = new ArrayList<String>();

	public  List<List<String>> topMrchndiseProductFinalList = new ArrayList<List<String>>();


	public  void DB_Query(String site, String client, ArrayList<String> scrossClientList, String crossClientId,
			 String period)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		mapTopMrchndiseConfigDest= new HashMap<>();
		mapTopMrchndiseConfigSource = new HashMap<>();
		mapTopMrchndisePeriodDest = new HashMap<>();
		mapTopMrchndisePeriodSource= new HashMap<>();
		mapTopMrchndiseProductTitleDest = new HashMap<>();
		mapTopMrchndiseProductTitleSource = new HashMap<>();
		mapTopMrchndiseRoyaltyDest = new HashMap<>();
		mapTopMrchndiseRoyaltySource = new HashMap<>();
		topMrchndiseProductCodeSource = new ArrayList<String>();
		topMrchndiseProductCodeDest = new ArrayList<String>();
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=mybmg4_stage_rec";
		
		Connection conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");

		Statement stmtDestDb = conDestinationDb.createStatement();

		
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dbUrlBmgPrimeLandingLaye = "jdbc:sqlserver://10.6.238.202:1433;databaseName=BMGPrime_LandingLayer";
		Connection connBmgPrime = DriverManager.getConnection(dbUrlBmgPrimeLandingLaye, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");
		Statement stmtBmgPrime = connBmgPrime.createStatement();
		try {
		List<String> clientIdList = new ArrayList<String>();
		clientIdList.add("'" + client + "'");
		if (crossClientId != null) {
			clientIdList.add(crossClientId);
		} else if (!scrossClientList.isEmpty()) {
			String cId = "";
			cId = scrossClientList.toString().replace("[", "").replace("]", "");
			clientIdList.add(cId);

		}
		String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
		String month = null;
		String year = null;
        if(period!=null) {
		StringBuilder sb = new StringBuilder();
		System.out.println(period);
		period = period.toString().replace("(", "").replace(")", "");
		String s[] = period.split("/");
		month = s[0];
		year = s[1];
		sb.append(year + "" + month);
		period = sb.toString();
		System.out.println(period + ":period");

		String queryToGetTopMrchndiseProductsDest = "SELECT TOP 5 "+
				  "P.Product_Title, "+
				  " P.Product_UserCode, "+
				  " PC.Config_Desc, "+
				  " SUM(RE.Earnings_Amount) AS totalRoyalty "+
				 "FROM Rec_Earnings RE INNER JOIN DistChannel DC ON RE.DistChannel_Key = DC.DistChannel_Key "+
				   "INNER JOIN Config EC ON RE.Config_Key = EC.Config_Key "+
				   "INNER JOIN Products P ON RE.Product_Key = P.Product_Key "+
				  "INNER JOIN Config PC ON P.Config_Key = PC.Config_Key "+
				   "INNER JOIN Period PR ON RE.Period_Key = PR.Period_Key "+
				   "INNER JOIN Client CL ON RE.Client_Key = CL.client_key "+
				 "WHERE "+
				   "CL.Client_Code IN (" + clientIds + ")  AND CL.Client_Site='" + site + "'" +
				   "AND PR.Period IN ('"+period+"') "+
				   "AND DC.DistChannel_Category = 'License' "+
				 "GROUP BY P.Product_Key, P.Product_Title,P.Product_UserCode, "+
				  " PC.Config_Desc "+
				 "ORDER BY totalRoyalty DESC";

		ResultSet topMrchndiseProductListDest = stmtDestDb.executeQuery(queryToGetTopMrchndiseProductsDest);

			while (topMrchndiseProductListDest.next()) {
			String productTitle = topMrchndiseProductListDest.getString("Product_Title");
			String productIsrcCode = topMrchndiseProductListDest.getString("Product_UserCode");
			String configDesc=topMrchndiseProductListDest.getString("Config_Desc");
			String totalRoyalty=topMrchndiseProductListDest.getString("totalRoyalty");
			
			topMrchndiseProductCodeDest.add(productIsrcCode);
			mapTopMrchndiseProductTitleDest.put(productIsrcCode, productTitle);
			mapTopMrchndiseConfigDest.put(productIsrcCode, configDesc);
			mapTopMrchndiseRoyaltyDest.put(productIsrcCode, totalRoyalty);
			mapTopMrchndisePeriodDest.put(productIsrcCode, period);
		}
		
        

	

		String queryToGetTopMrchndiseProductsSource = "select top 5 "+
				"PD.PRODUCT_TITLE, "+
				"PD.Product_UserCode, "+
				"PC.Config_Desc, "+
				" SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT "+
				 "from  "+
				"  (select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ, "+
				 "dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID "+
				 " from tbl_STG_RoycalcPayee dcrx "+
				" inner join "+
				"(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ , "+
				" RP.roycalcpayee_bfbalance,COUNT(*) as reccount "+
				" from ( "+
				" select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code "+
				 "from tbl_STG_RoycalcPayee  "+
				"	JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Contract where  CONTRACT_NAME IS NOT NULL AND NVL(CONTTYPE_CODE,'''') not in (''JVAT'',''PAGC'')')) CO  "+
				 "    ON CO.CONTRACT_CODE=tbl_STG_RoycalcPayee.RCONTRACT_CODE AND CO.CONTRACT_SITE=tbl_STG_RoycalcPayee.RCONTRACT_SITE "+
				"					 ) rp "+
								
				 " group by  "+
				 "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ, "+
				" RP.roycalcpayee_bfbalance) DUP "+
				" on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "+
				 "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "+
				" group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ, "+
				 "dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP  "+
				 "  JOIN tbl_STG_SalesWorkArc "+
				 "   SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ  "+
				  "  JOIN tbl_STG_Calclog CL "+
				 "    ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F' "+
				 "    AND CL.CALCLOG_PERIODTYPE<>'A'   "+   
				 "INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Products')) PD "+
				 "    ON PD.PRODUCT_CODE=SWA.PRODUCT_CODE AND PD.PRODUCT_SITE=SWA.PRODUCT_SITE "+
				 "    INNER JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) CF "+
				  "   ON CF.CONFIG_CODE=SWA.CONFIG_CODE "+
				"INNER JOIN   (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Config')) PC "+
					"				ON PC.CONFIG_CODE=PD.CONFIG_CODE   "+
				   " where SWA.DISTCHAN_CODE IN ('LI','MSTL') "+
				  "   and rcp.royaltor_code in (" + clientIds + ") and rcp.royaltor_site='" + site + "'" +
				  "   and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) +  substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)=('"+period+"') "+
									
				  "   GROUP BY  "+
				  "   PD.PRODUCT_CODE,PD.PRODUCT_SITE,PD.PRODUCT_TITLE, PD.Product_UserCode, "+
				"PC.Config_Desc "+
				" ORDER BY SUM(SWA.SALESWORKARC_EARNINGS)  DESC ";


		ResultSet topMrchndiseProductsSource = stmtBmgPrime.executeQuery(queryToGetTopMrchndiseProductsSource);
	
		while (topMrchndiseProductsSource.next()) {
			String productTitle = topMrchndiseProductsSource.getString("PRODUCT_TITLE");
			String productIsrcCode = topMrchndiseProductsSource.getString("Product_UserCode");
			String configDesc=topMrchndiseProductsSource.getString("Config_Desc");
			String totalRoyalty=topMrchndiseProductsSource.getString("AMOUNT");
			
			topMrchndiseProductCodeSource.add(productIsrcCode);
			mapTopMrchndiseProductTitleSource.put(productIsrcCode, productTitle);
			mapTopMrchndiseConfigSource.put(productIsrcCode, configDesc);
			mapTopMrchndiseRoyaltySource.put(productIsrcCode, totalRoyalty);
			mapTopMrchndisePeriodSource.put(productIsrcCode, period);
		}

        }
	}catch(Exception e) {
		e.printStackTrace();
		Log.error("Error in DBQueries_Top_MrchndiseProducts: "+e);
		
	}finally {
		stmtDestDb.close();
		conDestinationDb.close();
		stmtBmgPrime.close();
		conDestinationDb.close();
		
	}
	
}	}


