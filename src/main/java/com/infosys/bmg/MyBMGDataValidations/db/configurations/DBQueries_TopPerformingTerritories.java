package com.infosys.bmg.MyBMGDataValidations.db.configurations;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.infosys.bmg.MyBMGDataValidations.utility.Log;



public class DBQueries_TopPerformingTerritories {
	public  Map<String, String> mapTopPerformTerrRoyaltyDest = new HashMap<>();
	public  List<String> mapTopPerformTerrNameDest =new ArrayList<String>();
	
	public  Map<String, String> mapTopPerformTerrPeriodDest = new HashMap<>();
	public  Map<String, String> mapTopPerformTerrRoyaltySource = new HashMap<>();
	public   List<String> mapTopPerformTerrNameSource = new ArrayList<String>();
	public  Map<String, String> mapTopPerformTerrPeriodSource = new HashMap<>();
	public  List<List<String>> topPerfromTerrFinalList = new ArrayList<List<String>>();



	public  void DB_Query(String site, String client, ArrayList<String> scrossClientList, String crossClientId,
			 String period)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		mapTopPerformTerrRoyaltyDest= new HashMap<>();
		mapTopPerformTerrNameDest =new ArrayList<String>();
		mapTopPerformTerrRoyaltySource = new HashMap<>();
		mapTopPerformTerrNameSource=new ArrayList<String>();
	
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=mybmg4_stage_rec";
		
		Connection conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");

		Statement stmtDestDb = conDestinationDb.createStatement();

		
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dbUrlBmgPrimeLandingLaye = "jdbc:sqlserver://10.6.238.202:1433;databaseName=BMGPrime_LandingLayer";
		Connection connBmgPrime = DriverManager.getConnection(dbUrlBmgPrimeLandingLaye, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");
		Statement stmtBmgPrime = connBmgPrime.createStatement();
		try {
		List<String> clientIdList = new ArrayList<String>();
		clientIdList.add("'" + client + "'");
		if (crossClientId != null) {
			clientIdList.add(crossClientId);
		} else if (!scrossClientList.isEmpty()) {
			String cId = "";
			cId = scrossClientList.toString().replace("[", "").replace("]", "");
			clientIdList.add(cId);

		}
		String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
		String month = null;
		String year = null;
        if(period!=null) {
		StringBuilder sb = new StringBuilder();
		System.out.println(period);
		period = period.toString().replace("(", "").replace(")", "");
		String s[] = period.split("/");
		month = s[0];
		year = s[1];
		sb.append(year + "" + month);
		period = sb.toString();
		System.out.println(period + ":period");

		String queryToGetTopPerformTerrDest ="SELECT TOP 4  Territory.Territory_Name,Territory.territory_Code,Period.Period, "+
				"sum(Rec_Earnings.Earnings_Amount) as totalRoyalty "+
				"FROM         Products INNER JOIN "+
				"Rec_Earnings ON Products.Product_Key = Rec_Earnings.Product_Key INNER JOIN "+
				"Client ON Rec_Earnings.Client_Key = Client.Client_Key INNER JOIN "+
				"Period ON Rec_Earnings.Period_Key = Period.Period_Key INNER JOIN "+
				"Territory ON Rec_Earnings.Territory_Key = Territory.Territory_Key "+
				"WHERE     (Period.Period = '"+period+") AND (Client.Client_Site = '"+site+"') AND (Client.Client_Code IN (" + clientIds + ") ) "+
				"group by Territory.Territory_Name ,Territory.territory_Code ,Period.Period order by sum(Rec_Earnings.Earnings_Amount) desc ";


		ResultSet topPerformTerrListDest = stmtDestDb.executeQuery(queryToGetTopPerformTerrDest);

			while (topPerformTerrListDest.next()) {
			String territoryName = topPerformTerrListDest.getString("Territory_Name");
			String Period= topPerformTerrListDest.getString("Period");
			String totalRoyalty=topPerformTerrListDest.getString("totalRoyalty");
			
			mapTopPerformTerrNameDest.add(territoryName);
			mapTopPerformTerrPeriodDest.put(territoryName, Period);
			mapTopPerformTerrRoyaltyDest.put(territoryName, totalRoyalty);
		
		}
		
        

	

		String queryToGetTopPerformTerrSource = "select TOP 4 "+
				"TRR.TERRITORY_NAME, TRR.TERRITORY_CODE, substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) +  substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2) as Period , "+
				" SUM(SWA.SALESWORKARC_EARNINGS) AMOUNT "+
				" from  "+
				 " (select  dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ, "+
				" dcrx.roycalcpayee_bfbalance,dcrx.payee_code,max(dcrx.ROYCALCPAYEE_ID)ROYCALCPAYEE_ID "+
				"  from tbl_STG_RoycalcPayee dcrx "+
				"inner join "+
				"(select RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ , "+
				" RP.roycalcpayee_bfbalance,COUNT(*) as reccount "+
				 "from ( "+
				 "select distinct ROYALTOR_CODE,ROYALTOR_SITE,RCONTRACT_CODE,RCONTRACT_SITE,CALCLOG_SEQ,roycalcpayee_bfbalance,Payee_Code "+
				 "from tbl_STG_RoycalcPayee ) rp "+
				 " group by  "+
				 "RP.ROYALTOR_CODE,RP.ROYALTOR_SITE,RP.RCONTRACT_CODE,RP.RCONTRACT_SITE,RP.CALCLOG_SEQ, "+
				" RP.roycalcpayee_bfbalance) DUP "+
				 "on DUP.ROYALTOR_CODE=dcrx.royaltor_code and dup.royaltor_site =dcrx.ROYALTOR_SITE and DUP.CALCLOG_SEQ = dcrx.CALCLOG_SEQ "+
				 "where not(dcrx.PAYEE_CODE=dcrx.ROYALTOR_CODE and reccount>1) "+
				 "group by dcrx.ROYALTOR_CODE,dcrx.ROYALTOR_SITE,dcrx.RCONTRACT_CODE,dcrx.RCONTRACT_SITE,dcrx.CALCLOG_SEQ, "+
				" dcrx.roycalcpayee_bfbalance,dcrx.payee_code) RCP  "+
				 "  JOIN tbl_STG_SalesWorkArc "+
				 "   SWA ON RCP.ROYCALCPAYEE_ID = SWA.ROYCALCPAYEE_ID AND RCP.CALCLOG_SEQ=SWA.CALCLOG_SEQ  "+
				 "   JOIN tbl_STG_Calclog CL "+
				  "   ON RCP.CALCLOG_SEQ= CL.CALCLOG_SEQ and SWA.CALCLOG_SEQ = CL.CALCLOG_SEQ AND CL.CALCLOG_TYPE='A' AND CL.CALCLOG_RUNTYPE='F' "+
				 "    AND CL.CALCLOG_PERIODTYPE<>'A' "+
				  "   JOIN (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.Contract')) CO  "+
				  "   ON CO.CONTRACT_CODE=RCP.RCONTRACT_CODE AND CO.CONTRACT_SITE=RCP.RCONTRACT_SITE AND isnull(CO.CONTTYPE_CODE,'') not in ('JVAT','PAGC')  "+
				      
				"INNER JOIN	 "+
				    " (select * from OpenQuery(REMA1,'SELECT * FROM RMAESTRO_USER.TERRITORY')) TRR "+
				    " ON "+
				    " TRR.TERRITORY_CODE=CASE WHEN LTRIM(RTRIM(SWA.TERRITORY_CODE))='EX' THEN '**' ELSE LTRIM(RTRIM(SWA.TERRITORY_CODE)) END   "+
				   " where rcp.royaltor_code in (" + clientIds + ") and rcp.royaltor_site='" + site + "'" +
				   " and substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) +  substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2)='"+period+"' "+
				   "  GROUP BY  "+
				   "  TRR.TERRITORY_NAME, TRR.TERRITORY_CODE ,substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),4,4) +  substring(ISNULL(CL.CALCLOG_STATTO, CL.CALCLOG_TO),1,2) "+
				    " ORDER BY  SUM(SWA.SALESWORKARC_EARNINGS) DESC ";

		ResultSet topPerformTerrSource = stmtBmgPrime.executeQuery(queryToGetTopPerformTerrSource);
	
		while (topPerformTerrSource.next()) {
			String territoryName = topPerformTerrListDest.getString("TERRITORY_NAME");
			String Period= topPerformTerrListDest.getString("Period");
			String totalRoyalty=topPerformTerrListDest.getString("AMOUNT");
			
			mapTopPerformTerrNameSource.add(territoryName);
			mapTopPerformTerrPeriodSource.put(territoryName, Period);
			mapTopPerformTerrRoyaltySource.put(territoryName, totalRoyalty);
		}

        }
	}catch(Exception e) {
		e.printStackTrace();
		Log.error("Error in DBQueries_Top_PerformingTerritories: "+e);
		
	}finally {
		stmtDestDb.close();
		conDestinationDb.close();
		stmtBmgPrime.close();
		conDestinationDb.close();
		
	}
	
}	}


