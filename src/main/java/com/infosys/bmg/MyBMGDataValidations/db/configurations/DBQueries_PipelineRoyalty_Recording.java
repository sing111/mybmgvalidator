package com.infosys.bmg.MyBMGDataValidations.db.configurations;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBQueries_PipelineRoyalty_Recording {
	public  String pipelineRoyltyRecordDest = null;
	public  String pipelineRoyltyPeriodRecordDest = null;
	public  String pipelineRoyltyPeriodRecordSource = null;
	public  String pipelineRoyltyRecordSource = null;

	public  void DB_Query_PipelineRoyalty_Record(String site, String client, ArrayList<String> scrossClientList,
			String crossClientId) throws ClassNotFoundException, SQLException, InterruptedException, IOException {

		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=mybmg4_stage_rec";

		Connection conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");

		Statement stmtDestDb = conDestinationDb.createStatement();

		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dbUrlRecroProd = "jdbc:sqlserver://10.6.238.158:5555;databaseName=recroy_prod";
		Connection conRecroProdDb = DriverManager.getConnection(dbUrlRecroProd, "batr007", "Si2RIbVdVkDyCIVVBmbA");

		Statement stmtRecroProdDb = conRecroProdDb.createStatement();

		try {
			pipelineRoyltyRecordSource = null;
			pipelineRoyltyRecordDest = null;
			List<String> clientIdList = new ArrayList<String>();
			clientIdList.add("'" + client + "'");
			if (crossClientId != null) {
				clientIdList.add(crossClientId);
			} else if (!scrossClientList.isEmpty()) {
				String cId = "";
				cId = scrossClientList.toString().replace("[", "").replace("]", "");
				clientIdList.add(cId);

			}
			String clientIds = clientIdList.toString().replace("[", "").replace("]", "");

			String queryToFetchPipleLineRoyltyDest = "select   SUM(rb.Earnings_Amount) AS totalRoyalty ,P.Period  "
					+ "from period  p join Rec_Earnings rb "
					+ "on p.period_key=rb.period_key join client c on rb.client_key=c.client_key "
					+ "where  p.period_key =(select max(period_key) from Rec_Earnings rb1 where rb1.client_key=rb.client_key ) "
					+ "  and c.client_code in  (" + clientIds + ")  and client_site='" + site + "'"
					+ "group by p.period " + "	order by p.period desc ";

			ResultSet pipleLineRoyltyDestDB = stmtDestDb.executeQuery(queryToFetchPipleLineRoyltyDest);

			while (pipleLineRoyltyDestDB.next()) {
				pipelineRoyltyRecordDest = pipleLineRoyltyDestDB.getString("totalRoyalty");
				pipelineRoyltyPeriodRecordDest = pipleLineRoyltyDestDB.getString("Period");
			}

			String queryToFetchPipleLineRoyltySource = "select TOP 1  PERIOD,  "
					+ "SUM(CONTOPTACT_RECOUPAMT) AS ROYALTY_TOTAL  " + "from  "
					+ "(select royaltor_code,royaltor_site,calclog_finalized,CONTOPTACT_RECOUPAMT,transtype_code,contract_code,contract_site   "
					+ "				 from recroy_prod..CONTOPTACT  " + ") rt  "
					+ "inner join [dbo].[tblRecordmaestroETL_CONTRACT_Table1]  C on   "
					+ "rt.CONTRACT_CODE=C.CONTRACT_CODE AND rt.CONTRACT_SITE=C.CONTRACT_SITE AND  isnull(C.CONTTYPE_CODE,'') not in ('JVAT','PAGC')  "
					+ "inner join (  "
					+ "select substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),4,4) + substring(ISNULL(CALCLOG_STATTO, CALCLOG_TO),1,2) as period , calclog_seq  from [dbo].[CALCLOG]) CL "
					+ "on CL.calclog_seq=rt.calclog_finalized   " + "WHERE TRANSTYPE_CODE='ROYS' AND royaltor_code IN ("
					+ clientIds + ") AND ROYALTOR_SITE='" + site + "' GROUP BY PERIOD  " + "order by PERIOD desc  ";

			ResultSet pipleLineRoyltySourceDb = stmtRecroProdDb.executeQuery(queryToFetchPipleLineRoyltySource);

			while (pipleLineRoyltySourceDb.next()) {
				pipelineRoyltyRecordSource = pipleLineRoyltySourceDb.getString("ROYALTY_TOTAL");
				pipelineRoyltyPeriodRecordSource = pipleLineRoyltySourceDb.getString("PERIOD");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			stmtDestDb.close();
			conDestinationDb.close();
			stmtRecroProdDb.close();
			conRecroProdDb.close();
		}

	}
}
