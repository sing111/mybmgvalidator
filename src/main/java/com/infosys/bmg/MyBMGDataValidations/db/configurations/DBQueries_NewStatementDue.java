package com.infosys.bmg.MyBMGDataValidations.db.configurations;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.infosys.bmg.MyBMGDataValidations.utility.Log;



public class DBQueries_NewStatementDue {
	public  String newStatementDueDest=null;
	public  String newStatementDueSource=null;
	
	public  void DB_Query_NewStatementDue(String site, String client, ArrayList<String> scrossClientList, String crossClientId)
			throws ClassNotFoundException, SQLException, InterruptedException, IOException {
		 
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String dburlMyBmgDestination = "jdbc:sqlserver://10.6.238.152:5555;databaseName=Mybmg4_stage_rec";
		Connection conDestinationDb = DriverManager.getConnection(dburlMyBmgDestination, "batr007",
				"Si2RIbVdVkDyCIVVBmbA");

		Statement stmtDestDb = conDestinationDb.createStatement();
		
		Class.forName("oracle.jdbc.driver.OracleDriver");  
		  
		//step2 create  the connection object  
		Connection conRemaDb=DriverManager.getConnection(  
		"jdbc:oracle:thin:@10.6.238.172:1480:DBOPRIM1","MUDH001","rt54rl27");  
		  
		Statement stmtRemaDb=conRemaDb.createStatement(); 
		newStatementDueSource=null;
		newStatementDueDest=null;
		try {
		List<String> clientIdList = new ArrayList<String>();
		clientIdList.add("'" + client + "'");
		if (crossClientId != null) {
			clientIdList.add(crossClientId);
		} else if (!scrossClientList.isEmpty()) {
			String cId = "";
			cId = scrossClientList.toString().replace("[", "").replace("]", "");
			clientIdList.add(cId);

		}
		String clientIds = clientIdList.toString().replace("[", "").replace("]", "");
		
		
		
		String queryToFetchNewStatementDueDest="select  Client_code,Client_site,client_name, payment_days,Payment_Frequency, "+
				"case when Payment_Frequency=3 and Payment_Days=30 then datediff(DAY,getdate(),'Jan 31 2019') "+
				"when Payment_Frequency=3 and Payment_Days=45 then datediff(DAY,getdate(),'Feb 15 2019')  "+
				 "when Payment_Frequency=3 and Payment_Days=60 then datediff(DAY,getdate(),'Feb 28 2019') "+
				"when Payment_Frequency=3 and Payment_Days=90 then datediff(DAY,getdate(),'Dec 31 2019') "+
				"when Payment_Frequency=6 and Payment_Days=30 then datediff(DAY,getdate(),'Jan 31 2019') "+
				"when Payment_Frequency=6 and Payment_Days=45 then datediff(DAY,getdate(),'Feb 15 2019')  "+
				 "when Payment_Frequency=6 and Payment_Days=60 then datediff(DAY,getdate(),'Feb 28 2019') "+
				"when Payment_Frequency=6 and Payment_Days=90 then datediff(DAY,getdate(),'Mar 31 2019') end as 'Next Statement Due' "+
				"from client where client_code in (" + clientIds + ") and client_site='" + site + "'";
		ResultSet newStatementDueDestDB = stmtDestDb.executeQuery(queryToFetchNewStatementDueDest);
		
		while(newStatementDueDestDB.next()){
			newStatementDueDest=newStatementDueDestDB.getString("Next Statement Due");
		}
		
		String queryToFetchNewStatementDueDestSource= "select "+
				 "ROYALTOR.ROYALTOR_SITE as ROYALTOR_SITE, "+
				 " ROYALTOR.ROYALTOR_CODE as ROYALTOR_CODE, "+
				 " case "+
				"when ROYALTOR.ROYALTOR_PAYFREQ = 'S' then '6' "+
				"when ROYALTOR.ROYALTOR_PAYFREQ IS NULL or ROYALTOR.ROYALTOR_PAYFREQ ='Q' then '3' "+
				"when ROYALTOR.ROYALTOR_PAYFREQ = 'M' then '1' "+
				"else '0' "+
				"end Payement_Frequency, "+
				 " CASE WHEN ROYALTOR.ROYALTOR_REPPERIOD=1 THEN 30 "+
				 " WHEN ROYALTOR.ROYALTOR_REPPERIOD=2 THEN 45 "+
				 " WHEN ROYALTOR.ROYALTOR_REPPERIOD=3 THEN 60 "+
				 " WHEN ROYALTOR.ROYALTOR_REPPERIOD=4 THEN 75 "+
				 " WHEN ROYALTOR.ROYALTOR_REPPERIOD=5 THEN 90 "+
				 " WHEN ROYALTOR.ROYALTOR_REPPERIOD=6 THEN 120 "+
				 " WHEN ROYALTOR.ROYALTOR_REPPERIOD=7 THEN 180 "+
				 " ELSE 0 END AS PAYMENT_DAYS,  "+
				"case when ROYALTOR_PAYFREQ IS NULL or ROYALTOR.ROYALTOR_PAYFREQ ='Q' and ROYALTOR_REPPERIOD=1 then to_date('20190131','YYYYMMDD') "+ "-to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD')  "+
				"when ROYALTOR_PAYFREQ IS NULL or ROYALTOR.ROYALTOR_PAYFREQ ='Q' and ROYALTOR_REPPERIOD=2 then to_date('20190215','YYYYMMDD') "+ "-to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD') "+
				"when ROYALTOR_PAYFREQ IS NULL or ROYALTOR.ROYALTOR_PAYFREQ ='Q' and ROYALTOR_REPPERIOD=3 then to_date('20190228','YYYYMMDD') "+ "-to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD') "+
				"when ROYALTOR_PAYFREQ IS NULL or ROYALTOR.ROYALTOR_PAYFREQ ='Q' and ROYALTOR_REPPERIOD=5 then to_date('20191231','YYYYMMDD') "+ "-to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD') "+
				"when ROYALTOR_PAYFREQ ='S' and ROYALTOR_REPPERIOD=1 then to_date('20190131','YYYYMMDD') -to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD') "+
				"when ROYALTOR_PAYFREQ ='S' and ROYALTOR_REPPERIOD=2 then to_date('20190215','YYYYMMDD') -to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD') "+
				"when ROYALTOR_PAYFREQ ='S' and ROYALTOR_REPPERIOD=3 then to_date('20190228','YYYYMMDD') -to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD') "+
				" when ROYALTOR_PAYFREQ ='S' and ROYALTOR_REPPERIOD=5 then to_date('20190331','YYYYMMDD') -to_date(to_char(current_date,'YYYYMMDD'),'YYYYMMDD') "+
				"end as NextStatementDue "+
				"from "+
				 " rmaestro_user.ROYALTOR "+
				  "where royaltor_code in (" + clientIds + ") and royaltor_site='" + site + "'";
				
				
       ResultSet newStatementDueSourceDB = stmtRemaDb.executeQuery(queryToFetchNewStatementDueDestSource);
		
		while(newStatementDueSourceDB.next()){
			newStatementDueSource=newStatementDueSourceDB.getString("NextStatementDue");
		}
		}catch(Exception e) {
			e.printStackTrace();
			Log.error("Error in DBQueries_NewStatementDue :" +e);
		}finally {
			stmtDestDb.close();
			conDestinationDb.close();
			stmtRemaDb.close();
			conRemaDb.close();
			
		}
}
}
