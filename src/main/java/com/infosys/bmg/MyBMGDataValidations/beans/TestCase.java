package com.infosys.bmg.MyBMGDataValidations.beans;

public class TestCase {
	String testCaseNumber;
	String testCase, module;
	Boolean runStatus = false;

	public String getTestCaseNumber() {
		return testCaseNumber;
	}

	public void setTestCaseNumber(String testCaseNumber) {
		this.testCaseNumber = testCaseNumber;
	}

	public String getTestCase() {
		return testCase;
	}

	public void setTestCase(String testCase) {
		this.testCase = testCase;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public Boolean getRunStatus() {
		return runStatus;
	}

	public void setRunStatus(Boolean runStatus) {
		this.runStatus = runStatus;
	}

	@Override
	public String toString() {
		return "TestCase [testCaseNumber=" + testCaseNumber + ", testCase=" + testCase + ", module=" + module
				+ ", runStatus=" + runStatus + "]";
	}

}
