package com.infosys.bmg.MyBMGDataValidations.beans;

public class SingleClient {

	String royaltorSite, royaltorCode,royaltorName,payeeOrCrossClient,Currency_Desc;

	public String getRoyaltorSite() {
		return royaltorSite;
	}

	public void setRoyaltorSite(String royaltorSite) {
		this.royaltorSite = royaltorSite;
	}

	public String getRoyaltorCode() {
		return royaltorCode;
	}

	public void setRoyaltorCode(String royaltorCode) {
		this.royaltorCode = royaltorCode;
	}

	public String getRoyaltorName() {
		return royaltorName;
	}

	public void setRoyaltorName(String royaltorName) {
		this.royaltorName = royaltorName;
	}

	public String getPayeeOrCrossClient() {
		return payeeOrCrossClient;
	}

	public void setPayeeOrCrossClient(String payeeOrCrossClient) {
		this.payeeOrCrossClient = payeeOrCrossClient;
	}

	public String getCurrency_Desc() {
		return Currency_Desc;
	}

	public void setCurrency_Desc(String currency_Desc) {
		Currency_Desc = currency_Desc;
	}

	@Override
	public String toString() {
		return "SingleClient [royaltorSite=" + royaltorSite + ", royaltorCode=" + royaltorCode + ", royaltorName="
				+ royaltorName + ", payeeOrCrossClient=" + payeeOrCrossClient + ", Currency_Desc=" + Currency_Desc
				+ "]";
	}

}
