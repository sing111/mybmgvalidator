package com.infosys.bmg.MyBMGDataValidations.beans;

import java.util.List;

public class ExcelData {
	String userName, password, siteURL;
	List<SingleClient> singlsClient;
	List<CrossClient> crossClient;
	List<TestCase> testCases;
	
	public List<TestCase> getTestCases() {
		return testCases;
	}
	public void setTestCases(List<TestCase> testCases) {
		this.testCases = testCases;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSiteURL() {
		return siteURL;
	}
	public void setSiteURL(String siteURL) {
		this.siteURL = siteURL;
	}
	public List<SingleClient> getSinglsClient() {
		return singlsClient;
	}
	public void setSinglsClient(List<SingleClient> singlsClient) {
		this.singlsClient = singlsClient;
	}
	public List<CrossClient> getCrossClient() {
		return crossClient;
	}
	public void setCrossClient(List<CrossClient> crossClient) {
		this.crossClient = crossClient;
	}
	@Override
	public String toString() {
		return "ExcelData [userName=" + userName + ", password=" + password + ", siteURL=" + siteURL + ", singlsClient="
				+ singlsClient + ", crossClient=" + crossClient + ", testCases=" + testCases + "]";
	}
	  
}
