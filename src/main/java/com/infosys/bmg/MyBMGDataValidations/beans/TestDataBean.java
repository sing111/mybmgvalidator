package com.infosys.bmg.MyBMGDataValidations.beans;

import java.util.Date;

public class TestDataBean {
	private Integer processId;
	private String baseFileName, finalFileName, resultFileName, resultFileLocation, filePath,resultFileNameWithPath;
	private Date createdDate, completedDate;

	private String statusMessage;
	private Integer statusCode;
	private Integer totalValidations=0, valProcessed=0;

	public Integer getTotalValidations() {
		return totalValidations;
	}

	public String getResultFileNameWithPath() {
		return resultFileNameWithPath;
	}

	public void setResultFileNameWithPath(String resultFileNameWithPath) {
		this.resultFileNameWithPath = resultFileNameWithPath;
	}

	public void setTotalValidations(Integer totalValidations) {
		this.totalValidations = totalValidations;
	}

	public int getValProcessed() {
		return valProcessed;
	}

	public void setValProcessed(Integer valProcessed) {
		this.valProcessed = valProcessed;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(Date completedDate) {
		this.completedDate = completedDate;
	}

	public Integer getProcessId() {
		return processId;
	}

	public void setProcessId(Integer processId) {
		this.processId = processId;
	}

	public String getBaseFileName() {
		return baseFileName;
	}

	public void setBaseFileName(String baseFileName) {
		this.baseFileName = baseFileName;
	}

	public String getFinalFileName() {
		return finalFileName;
	}

	public void setFinalFileName(String finalFileName) {
		this.finalFileName = finalFileName;
	}

	public String getResultFileName() {
		return resultFileName;
	}

	public void setResultFileName(String resultFileName) {
		this.resultFileName = resultFileName;
	}

	public String getResultFileLocation() {
		return resultFileLocation;
	}

	public void setResultFileLocation(String resultFileLocation) {
		this.resultFileLocation = resultFileLocation;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public String toString() {
		return "TestDataBean [processId=" + processId + ", baseFileName=" + baseFileName + ", finalFileName="
				+ finalFileName + ", resultFileName=" + resultFileName + ", resultFileLocation=" + resultFileLocation
				+ ", filePath=" + filePath + ", createdDate=" + createdDate + ", completedDate=" + completedDate
				+ ", statusMessage=" + statusMessage + ", statusCode=" + statusCode  + "]";
	}

}
